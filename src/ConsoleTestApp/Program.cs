﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using CoxAuto.ObjectStores;
using Homenet.RecallSplat.Core.Model;
using Homenet.RecallSplat.Core.Repositories;
using Homenet.RecallSplat.Core.Interfaces;
using Homenet.RecallSplat.Core.Services;
using Homenet.RecallSplat.DependencyInjection;
using Homenet.RecallSplat.Framework.Bootstrapper;

namespace ConsoleTestApp
{
	internal class Program
	{
// ReSharper disable once UnusedParameter.Local
		private static void Main(string[] args)
		{
			RecallSplatKernelConfigurator.ConfigureDefaults();
			//TestVINManufacturerConfigMappingRepository();


			//TestExternalRecallServiceConfigRepository();

			//TestGraphzilliness();

            TestMazda();

			//GetInvalidVinServiceResponses();
		}

	    private static void TestMazda()
	    {
	        string html;
	        HttpStatusCode statusCode;
            HttpFetcher fetcher = new HttpFetcher();
            fetcher.Post("http://www.mazdausa.com/MusaWeb/displayRecallOwners.action", new NameValueCollection {{"vin", "1YVHP80C165M67229"}}, out html, out statusCode);
            
            File.WriteAllText(@"c:\mazda.html", html);
	    }

		private static void TestExternalRecallServiceConfigRepository()
		{
			const string key = "AudiOEM";
			Console.WriteLine("Bench testing ExternalRecallServiceConfigRepository...");
			ExternalRecallServiceConfigRepository configRepo =
				RecallSplatKernel.Get<ExternalRecallServiceConfigRepository>();
			ExternalRecallServiceConfig externalRecallServiceConfig1 = new ExternalRecallServiceConfig
			{
				MappingDataKey = key,
				OpenRecallVINURL = "http://web.audiusa.com/audirecall/vin/{0}",
				MappingMethod = "Blah blha"
			};

			Console.WriteLine("Storing value for key \"{0}\"", key);
			configRepo.Store(key, externalRecallServiceConfig1);
			configRepo.Store(key + "1", externalRecallServiceConfig1);
			IEnumerable<ExternalRecallServiceConfig> x = configRepo.GetValues();
			configRepo.Store(key + "2", externalRecallServiceConfig1);

			Console.WriteLine(x.Count());
			Console.WriteLine("Getting value for key \"{0}\"", key);
			ExternalRecallServiceConfig externalRecallServiceConfig2 = configRepo.Get(key);

			if (externalRecallServiceConfig1.MappingDataKey == externalRecallServiceConfig2.MappingDataKey
			    && externalRecallServiceConfig1.OpenRecallVINURL == externalRecallServiceConfig2.OpenRecallVINURL)
			{
				// I know this is a weak test, good enough for this bench test
				Console.WriteLine("Verified stored and retrieved values are equal");
				Console.WriteLine("Removing stored value for  key \"{0}\"", key);
				//configRepo.Remove(key);

				//try
				//{
				//	// this should fail
				//	configRepo.Get(key);
				//}
				//catch (NotFoundInObjectStoreException)
				//{
				//	Console.WriteLine("Verified removal of stroed value for  key \"{0}\"", key);
				//}
			}
		}

// ReSharper disable once InconsistentNaming
		private static void TestVINManufacturerConfigMappingRepository()
		{
			const string key = "WAU";
			Console.WriteLine("Bench testing VINManufacturerConfigMappingRepository...");
			VINManufacturerConfigMappingRepository mappingRepo = RecallSplatKernel.Get<VINManufacturerConfigMappingRepository>();
			VINManufacturerConfigMapping vinManufacturerConfigMapping1 = new VINManufacturerConfigMapping
			{
				ManufacturerName = "Audi",
				Make = "Audi",
				ConfigKey = "AudiOEM"
			};

			Console.WriteLine("Storing value for  key \"{0}\"", key);
			mappingRepo.Store(key, vinManufacturerConfigMapping1);
			mappingRepo.Store(key + "3", vinManufacturerConfigMapping1);

			Console.WriteLine("Getting value for  key \"{0}\"", key);
			VINManufacturerConfigMapping vinManufacturerConfigMapping2 = mappingRepo.Get(key);

			if (vinManufacturerConfigMapping1.ManufacturerName == vinManufacturerConfigMapping2.ManufacturerName
			    && vinManufacturerConfigMapping1.Make == vinManufacturerConfigMapping2.Make
			    && vinManufacturerConfigMapping1.ConfigKey == vinManufacturerConfigMapping2.ConfigKey)
			{
				Console.WriteLine("Verified stored and retrieved values are equal");
				Console.WriteLine("Removing stored value for  key \"{0}\"", key);
				//mappingRepo.Remove(key);

				//try
				//{
				//	// this should fail
				//	mappingRepo.Get(key);
				//}
				//catch (NotFoundInObjectStoreException)
				//{
				//	Console.WriteLine("Verified removal of stroed value for  key \"{0}\"", key);
				//}
			}
		}


		private static void TestGraphzilliness()
		{
			//RecallSplatKernelConfigurator.ConfigureDefaults();

			IRecallSplatService recallSplatService = RecallSplatKernel.Get<IRecallSplatService>();


			#region vins

			string[] vins =
			{
				"2HNYD18625H514645",
				"2HN11111111111111",
				"11111111111111111",
				"2HNYD18625H514645",
				"WAU2GAFC8CN075058",
				"WAU2GAFC8CN075XYZ",
				"WAU2GAFC8CN075058",
				"WAU2GAFC8CN075XYZ",
				"WAU2GAFC8CN075058",
				"WAU11111111111111",
			};
	

			#endregion

			foreach (string vin in vins)
			{
				try
				{
					Console.WriteLine("Looking up VIN {0}", vin);
					VINRecallResponse response = recallSplatService.Lookup(vin);

				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.Message);
					//   throw;
				}
			}
		}


		private static void GetInvalidVinServiceResponses()
		{
			ExternalRecallServiceConfigRepository externalRecallServiceConfigRepository = RecallSplatKernel.Get<ExternalRecallServiceConfigRepository>();

			IEnumerable<ExternalRecallServiceConfig> configs = externalRecallServiceConfigRepository.GetValues();
			IHttpFetcher httpFetcher = RecallSplatKernel.Get<HttpFetcher>();

			foreach (ExternalRecallServiceConfig c in configs)
			{
				try
				{
					HttpStatusCode statusCode;
					string serviceResponse;
					httpFetcher.Get(c.OpenRecallVINURL.Replace("{vin}", "11111111111111111"), out serviceResponse, out statusCode);
					Console.WriteLine("url:{0}, response:{1}", c.OpenRecallVINURL, serviceResponse);					
				}
				catch (Exception ex)
				{
					Console.WriteLine("url:{0}, response:{1}", c.OpenRecallVINURL, ex.Message);
				}
			}
			
		}
	}
}
