﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Homenet.RecallSplat.Core.Interfaces;
using Homenet.RecallSplat.Core.Model;
using Homenet.RecallSplat.DependencyInjection;
using Homenet.RecallSplat.Framework.Bootstrapper;

namespace BulkRun
{
    class Program
    {
        static void Main(string[] args)
        {
            RecallSplatKernelConfigurator.ConfigureDefaults();
            var recallSplatService = RecallSplatKernel.Get<IRecallSplatService>();

            string filename = Path.Combine(AssemblyDirectory, "input.txt");
            Console.WriteLine(filename);
            string outputFilename = Path.Combine(AssemblyDirectory, string.Format("output{0}.csv", DateTime.Now.ToFileTime()));

            HashSet <string> vins=new HashSet<string>();
            using (StreamReader reader = new StreamReader(filename))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    if (!string.IsNullOrWhiteSpace(line))
                    {
                        vins.Add(line);
                    }
                }
            }

            int index = 0;
            List<ResultRow> results = new List<ResultRow>();
            foreach (var vin in vins)
            {
                Console.WriteLine("{0} / {1}", index, vins.Count);


                Console.WriteLine(vin);

                    var response = recallSplatService.Lookup(vin, true);



                if (response.VINs.Length == 1)
                {
                    var vinResponse = response.VINs[0];
                    ResultRow row = new ResultRow();
                    results.Add(row);
                    row.VIN = vin;
                    try
                    {


                        row.ServiceStatus = response.Status.ToString();
                        Console.WriteLine("\t" + row.ServiceStatus);
                        row.ServiceErrorMessage = response.ErrorMessage;

                        row.VINResponseErrorMessage = vinResponse.ErrorMessage;
                        row.Make = vinResponse.Make;
                        row.Model = vinResponse.Model;
                        row.OEMFeedLastUpdated = vinResponse.OEMFeedLastUpdated.ToString();
                        row.FeedLastUpdated = vinResponse.FeedLastUpdated.ToString();
                        row.Year = vinResponse.Year.ToString();
                        row.OpenRecallCount = vinResponse.OpenRecallCount.ToString();
                        foreach (var recall in vinResponse.OpenRecalls)
                        {

                            try
                            {
                                ResultRow recallRow = new ResultRow();
                                results.Add(recallRow);
                                recallRow.VIN = vin;


                                recallRow.Status = recall.Status.ToString();
                                recallRow.NhtsaId = recall.CampaignID;
                                recallRow.RecallDescription = recall.Description;
                                recallRow.CampaignDate = recall.CampaignDate.ToString();
                                recallRow.CampaignID = recall.CampaignID;
                                recallRow.NHTSACampaignURL = recall.NHTSACampaignURL;
                                recallRow.OEMCampaignID = recall.OEMCampaignID;
                                recallRow.RemedyDescription = recall.RemedyDescription;
                                recallRow.SafetyRisk = recall.SafetyRisk;
                                recallRow.Title = recall.Title;

                                recallRow.VINResponseStatus = vinResponse.Status.ToString();
                            }
                            catch (Exception ex)
                            {
                                row.CallerErrorMessage = ex.Message;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        row.CallerErrorMessage = ex.Message;
                    }
                }
                else
                {
                    throw new Exception(string.Format("Invalid number of response objects: {0}",
                        response.VINs.Length));
                }

                index++;

                    //if (index > 1000) break;
                //}
                //catch()
            }

            CsvExport<ResultRow> csv = new CsvExport<ResultRow>(results);
            using (StreamWriter writer = new StreamWriter(outputFilename, false))
            {
                writer.Write(csv.Export());

                writer.Flush();
            }
        }

        private static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }
    }

}
