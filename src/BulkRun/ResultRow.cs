﻿namespace BulkRun
{
    public class ResultRow
    {
        public string VIN { get; set; } 
        public string ServiceStatus { get; set; }
        public string ServiceErrorMessage { get; set; }
        public string VINResponseErrorMessage { get; set; }
        public string VINResponseStatus { get; set; }

        public string CallerErrorMessage { get; set; }

        public string OEMFeedLastUpdated { get; set; }
        public string FeedLastUpdated { get; set; }
        public string Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }

        public string OpenRecallCount { get; set; }

        public string Status { get; set; }
        public string NhtsaId { get; set; }

        public string RecallDescription { get; set; }
        public string CampaignDate { get; set; }
        public string CampaignID { get; set; }
        public string NHTSACampaignURL { get; set; }
        public string OEMCampaignID { get; set; }
        public string RemedyDescription { get; set; }
        public string SafetyRisk { get; set; }
        public string Title { get; set; }

    }
}