﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using Homenet.RecallSplat.AdminWebsite.Helpers;
using Homenet.RecallSplat.Core.Model;

namespace Homenet.RecallSplat.AdminWebsite.Controllers
{
	public class RecallsController : Controller
	{
		public ActionResult Single(string vin, string view, bool bypassCache = false)
		{
			VINRecallResponse rr = null;

            vin = CleanVin(vin);

			if (!String.IsNullOrWhiteSpace(vin))
			{
				rr = RecallSplatServiceHelper.MakeRequest(vin, bypassCache);
			}

			if (string.Equals(view, "raw", StringComparison.InvariantCultureIgnoreCase))
			{
				if (rr == null)
				{
					return HttpNotFound();
				}
				return Json(rr, JsonRequestBehavior.AllowGet);
			}
			return View(rr);
		}

	    public ActionResult Multi(string vins, string view, bool bypassCache = false)
	    {
            VINRecallResponse rr = null;
	        ViewBag.initialTextAreaRows = 1;


            HashSet<string> vinList = new HashSet<string>();
            if (!String.IsNullOrWhiteSpace(vins))
            {
                string[] parts = vins.Split(new char[] {',', ';', '\n', '\r'}, StringSplitOptions.RemoveEmptyEntries);
                foreach (var part in parts)
                {
                    if (String.IsNullOrWhiteSpace(part)) continue;

                    string cleanVin = CleanVin(part);

                    if (String.IsNullOrWhiteSpace(cleanVin)) continue;

                    vinList.Add(cleanVin);
                }
            }

	        StringBuilder sb = new StringBuilder();
	        foreach (var aVin in vinList)
	        {
	            sb.AppendLine(aVin);
	        }
	        ViewBag.vins = sb.ToString();


            if (vinList.Count > 0)
	        {
                rr = RecallSplatServiceHelper.MakeMultipleRequest(vinList, bypassCache);

	            ViewBag.initialTextAreaRows = vinList.Count;
	        }


            if (string.Equals(view, "raw", StringComparison.InvariantCultureIgnoreCase))
            {
                if (rr == null)
                {
                    return HttpNotFound();
                }
                return Json(rr, JsonRequestBehavior.AllowGet);
            }

            return View(rr);
        }


        //Trim VIN; Put in uppercase
        private static string CleanVin(string vin)
	    {
            if (!String.IsNullOrWhiteSpace(vin))
            {
                vin = vin.Trim().ToUpper();
	            string str = vin;
	            StringBuilder sb = new StringBuilder();
	            foreach (char c in str)
	            {
	                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z'))
	                {
	                    sb.Append(c);
	                }
	            }
	            vin = sb.ToString();
	        }
	        return vin;
	    }
	}

}