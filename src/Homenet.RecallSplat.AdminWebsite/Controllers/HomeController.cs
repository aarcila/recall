﻿using System.Web.Mvc;

namespace Homenet.RecallSplat.AdminWebsite.Controllers
{
	public class HomeController : Controller
	{
		// GET: Home
		public ActionResult Index()
		{
			return RedirectToAction("index", "recalls");
		}
	}
}