﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using Homenet.RecallSplat.AdminWebsite.Models;
using Homenet.RecallSplat.Core.Model;
using Homenet.RecallSplat.Core.Repositories;
using Newtonsoft.Json;

namespace Homenet.RecallSplat.AdminWebsite.Controllers
{
	public class ConfigurationsController : Controller
	{
		private readonly VINManufacturerConfigMappingRepository _vinManufacturerConfigMappingRepository;
		private readonly ExternalRecallServiceConfigRepository _externalRecallServiceConfigRepository;
		private readonly MappingDataRepository _mappingDataRepository;

		public ConfigurationsController(
			VINManufacturerConfigMappingRepository vinManufacturerConfigMappingRepository,
			ExternalRecallServiceConfigRepository externalRecallServiceConfigRepository,
			MappingDataRepository mappingDataRepository)

		{
			_vinManufacturerConfigMappingRepository = vinManufacturerConfigMappingRepository;
			_mappingDataRepository = mappingDataRepository;
			_externalRecallServiceConfigRepository = externalRecallServiceConfigRepository;
		}

		// GET: Configuration
		public ActionResult Index(string configType, string key)
		{
			ConfigurationType configurationType;
			ConfigurationsFilterViewModel filter = new ConfigurationsFilterViewModel {ConfigurationKey = key};
			if (Enum.TryParse(configType, true, out configurationType))
			{
				filter.ConfigurationType = configurationType;
			}
			return
				View(new ConfigurationsIndexViewModel
				{
					ConfigurationType = filter.ConfigurationType,
					ConfigurationKey = filter.ConfigurationKey,
					GridData = GetGridData(filter)
				});
		}

		[HttpPost]
		public ActionResult Index(ConfigurationsFilterViewModel filter)
		{
			return RedirectToAction("Index", "Configurations",
				new RouteValueDictionary {{"configType", filter.ConfigurationType}, {"key", filter.ConfigurationKey}});
		}


		// GET: /Configurations/Edit?configType=1&key=WBS
		public ActionResult Edit(string configType, string key)
		{
			ConfigurationType configurationType;

			if (string.IsNullOrWhiteSpace(key) || !Enum.TryParse(configType, true, out configurationType))
			{
				// invalid params. redirect to configurations grid
				return RedirectToAction("index", "configurations");
			}

			ConfigurationViewModel cvm = GetConfigurationViewModelData(configurationType, key);
			if (cvm == null)
			{
				// configuration not found. redirect to configurations grid
				return RedirectToAction("index", "configurations");
			}
			
			return View("EditConfigurationView", cvm);
		}

		// GET: /Configurations/Create
		public ActionResult Create()
		{
			return View("CreateConfigurationView", new ConfigurationViewModel { ConfigurationType = ConfigurationType.VinManufacturerConfigMapping });
		}

		[HttpPost]
		public ActionResult Create(ConfigurationViewModel cvm)
		{
			if (!ModelState.IsValid)
			{
				return View("CreateConfigurationView", cvm);
			}

			// Persist the data
			StoreConfiguration(cvm);

			return RedirectToAction("Index");

			//return RedirectToAction("Index", "Configurations",
			//	new RouteValueDictionary { { "configType", cvm.ConfigurationType }, { "key", cvm.ConfigurationKey } });
		}

		[HttpPost]
		public ActionResult Edit(ConfigurationViewModel cvm)
		{
			if (!ModelState.IsValid)
			{
				return View("EditConfigurationView", cvm);
			}

			// Persist the data
			StoreConfiguration(cvm);

			return RedirectToAction("Index");

			//return RedirectToAction("Edit", "Configurations",
			//	new RouteValueDictionary {{"key", cvm.ConfigurationKey}, {"configType", cvm.ConfigurationType}});
		}

		public ActionResult Delete(ConfigurationType configType, string key)
		{
			switch (configType)
			{
				case ConfigurationType.VinManufacturerConfigMapping:
					_vinManufacturerConfigMappingRepository.Remove(key);
					break;
				case ConfigurationType.ExternalRecallServiceConfig:
					_externalRecallServiceConfigRepository.Remove(key);
					break;
				case ConfigurationType.ExternalRecallServiceMappingData:
					_mappingDataRepository.Remove(key);
					break;
			}

			return RedirectToAction("Index");
		}

		public IEnumerable<ConfigurationsGridViewModel> GetGridData(ConfigurationsFilterViewModel filter)
		{
			IEnumerable<string> keys;
			switch (filter.ConfigurationType)
			{
				case ConfigurationType.VinManufacturerConfigMapping:
					keys = _vinManufacturerConfigMappingRepository.GetKeys();
					return GetGridDataHelper(filter.ConfigurationType.Value, RecallSplatSettings.VINManufacturerConfigMappingKeyPrefix,
						keys.Where(
							k =>
								string.IsNullOrWhiteSpace(filter.ConfigurationKey) ||
								string.Equals(k, RecallSplatSettings.VINManufacturerConfigMappingKeyPrefix + filter.ConfigurationKey,
									StringComparison.OrdinalIgnoreCase)));

				case ConfigurationType.ExternalRecallServiceConfig:
					keys =_externalRecallServiceConfigRepository.GetKeys();
					return GetGridDataHelper(filter.ConfigurationType.Value, RecallSplatSettings.ExternalRecallServiceConfigKeyPrefix,
						keys.Where(
							k =>
								string.IsNullOrWhiteSpace(filter.ConfigurationKey) ||
								string.Equals(k, RecallSplatSettings.ExternalRecallServiceConfigKeyPrefix + filter.ConfigurationKey,
									StringComparison.OrdinalIgnoreCase)));

				case ConfigurationType.ExternalRecallServiceMappingData:
					keys = _mappingDataRepository.GetKeys();
					return GetGridDataHelper(filter.ConfigurationType.Value, RecallSplatSettings.MappingDataKeyPrefix,
						keys.Where(
							k =>
								string.IsNullOrWhiteSpace(filter.ConfigurationKey) ||
								string.Equals(k, RecallSplatSettings.MappingDataKeyPrefix + filter.ConfigurationKey,
									StringComparison.OrdinalIgnoreCase)));

				default:
					List<ConfigurationsGridViewModel> gridData = new List<ConfigurationsGridViewModel>();

					keys = _vinManufacturerConfigMappingRepository.GetKeys();
					gridData.AddRange(GetGridDataHelper(ConfigurationType.VinManufacturerConfigMapping,
						RecallSplatSettings.VINManufacturerConfigMappingKeyPrefix,
						keys.Where(
							k =>
								string.IsNullOrWhiteSpace(filter.ConfigurationKey) ||
								string.Equals(k, RecallSplatSettings.VINManufacturerConfigMappingKeyPrefix + filter.ConfigurationKey,
									StringComparison.OrdinalIgnoreCase))));

					keys = _externalRecallServiceConfigRepository.GetKeys();
					gridData.AddRange(GetGridDataHelper(ConfigurationType.ExternalRecallServiceConfig,
						RecallSplatSettings.ExternalRecallServiceConfigKeyPrefix,
						keys.Where(
							k =>
								string.IsNullOrWhiteSpace(filter.ConfigurationKey) ||
								string.Equals(k, RecallSplatSettings.ExternalRecallServiceConfigKeyPrefix + filter.ConfigurationKey,
									StringComparison.OrdinalIgnoreCase))));


					keys = _mappingDataRepository.GetKeys();
					gridData.AddRange(GetGridDataHelper(ConfigurationType.ExternalRecallServiceMappingData,
						RecallSplatSettings.MappingDataKeyPrefix,
						keys.Where(
							k =>
								string.IsNullOrWhiteSpace(filter.ConfigurationKey) ||
								string.Equals(k, RecallSplatSettings.MappingDataKeyPrefix + filter.ConfigurationKey,
									StringComparison.OrdinalIgnoreCase))));
					return gridData;
			}
		}

		private IEnumerable<ConfigurationsGridViewModel> GetGridDataHelper(ConfigurationType configType, string keyPrefix, IEnumerable<string> keys)
		{
			List<ConfigurationsGridViewModel> gridData = new List<ConfigurationsGridViewModel>();
// ReSharper disable once LoopCanBeConvertedToQuery
			foreach (string key in keys)
			{
				ConfigurationsGridViewModel configViewModel = new ConfigurationsGridViewModel
				{
					ConfigurationType = configType.ToString(),
					ConfigurationKey = key,
					ConfigurationShortKey = key.Substring(keyPrefix.Length),
				};

				gridData.Add(configViewModel);
			}
			return gridData;
		}


		private ConfigurationViewModel GetConfigurationViewModelData(ConfigurationType configType, string key)
		{
			object obj = null;
			switch (configType)
			{
				case ConfigurationType.VinManufacturerConfigMapping:
					obj = _vinManufacturerConfigMappingRepository.Get(key);
					break;

				case ConfigurationType.ExternalRecallServiceConfig:
					obj = _externalRecallServiceConfigRepository.Get(key);
					break;

				case ConfigurationType.ExternalRecallServiceMappingData:
					obj = _mappingDataRepository.Get(key);
					break;
			}

			return (obj == null)
				? null
				: new ConfigurationViewModel
				{
					ConfigurationType = configType,
					ConfigurationKey = key,
					ConfigurationValue = JsonConvert.SerializeObject(obj, Formatting.Indented)
				};
		}

		private void StoreConfiguration(ConfigurationViewModel cvm)
		{
			switch (cvm.ConfigurationType)
			{
				case ConfigurationType.VinManufacturerConfigMapping:
					_vinManufacturerConfigMappingRepository.Store(cvm.ConfigurationKey,
						JsonConvert.DeserializeObject<VINManufacturerConfigMapping>(cvm.ConfigurationValue));
					break;
				case ConfigurationType.ExternalRecallServiceConfig:
					_externalRecallServiceConfigRepository.Store(cvm.ConfigurationKey,
						JsonConvert.DeserializeObject<ExternalRecallServiceConfig>(cvm.ConfigurationValue));
					break;
				case ConfigurationType.ExternalRecallServiceMappingData:
					_mappingDataRepository.Store(cvm.ConfigurationKey,
						JsonConvert.DeserializeObject<ExternalRecallServiceMappingData>(cvm.ConfigurationValue));
					break;
			}
		}
	}
}