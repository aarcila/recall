﻿using System.Collections.Generic;
using Homenet.MoreCommon;
using Homenet.RecallSplat.Core.Model;
using Newtonsoft.Json;
using RestSharp;

namespace Homenet.RecallSplat.AdminWebsite.Helpers
{
	public class RecallSplatServiceHelper
	{
		public static VINRecallResponse MakeRequest(string vin, bool bypassCache)
		{
			RestClient client = new RestClient(Settings.DefaultEnvironment().RecallSplat.WebServiceBaseURL);

			RestRequest request = new RestRequest("vin/{id}", Method.GET);
			request.AddUrlSegment("id", vin);

            if (bypassCache) request.AddQueryParameter("bypassCache", "true");

            // execute the request
            IRestResponse response = client.Execute(request);

			if (response.ErrorException != null)
			{
				throw response.ErrorException;
			}

			string content = response.Content; // raw content as string

			VINRecallResponse d = JsonConvert.DeserializeObject<VINRecallResponse>(content);
			return d;
		}

        public static VINRecallResponse MakeMultipleRequest(HashSet<string> vins, bool bypassCache)
        {
            RestClient client = new RestClient(Settings.DefaultEnvironment().RecallSplat.WebServiceBaseURL);

            RestRequest request = new RestRequest("vins", Method.GET);

            foreach (var vin in vins)
            {
                request.AddQueryParameter("vin", vin);
            }

            if (bypassCache) request.AddQueryParameter("bypassCache", "true");

            // execute the request
            IRestResponse response = client.Execute(request);

            if (response.ErrorException != null)
            {
                throw response.ErrorException;
            }

            string content = response.Content; // raw content as string

            VINRecallResponse d = JsonConvert.DeserializeObject<VINRecallResponse>(content);
            return d;
        }
    }
}