﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Homenet.RecallSplat.AdminWebsite.Adapters;
using Homenet.RecallSplat.DependencyInjection;

namespace Homenet.RecallSplat.AdminWebsite
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

			RecallSplatKernelConfigurator.ConfigureDefaults();
			DependencyResolver.SetResolver(new RecallSplatDependencyResolver());
		}
    }
}
