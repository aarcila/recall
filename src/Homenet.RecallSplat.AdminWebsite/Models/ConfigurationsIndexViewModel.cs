﻿using System.Collections.Generic;
using Homenet.RecallSplat.Core.Model;

namespace Homenet.RecallSplat.AdminWebsite.Models
{
	public class ConfigurationsIndexViewModel
	{
		public ConfigurationType? ConfigurationType { get; set; }
		public string ConfigurationKey { get; set; }
		public IEnumerable<ConfigurationsGridViewModel> GridData { get; set; }
	}
}