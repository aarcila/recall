﻿using System.ComponentModel;
using Homenet.RecallSplat.Core.Model;
using System.ComponentModel.DataAnnotations;

namespace Homenet.RecallSplat.AdminWebsite.Models
{
	public class ConfigurationViewModel
	{
		[DisplayName("Configuration Type")]
		[Required]
		public ConfigurationType ConfigurationType { get; set; }

		[DisplayName("Configuration Key")]
		[Required]
		public  string ConfigurationKey { get; set; }

		[DisplayName("Configuration Value")]
		[CustomJsonValidator]
		public string ConfigurationValue { get; set; }
	}
}