﻿namespace Homenet.RecallSplat.AdminWebsite.Models
{
	public class ConfigurationsGridViewModel
	{
		public string ConfigurationType { get; set; }
		public string ConfigurationKey { get; set; }
		public string ConfigurationShortKey { get; set; }
	}
}