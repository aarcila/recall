﻿using System.ComponentModel;
using Homenet.RecallSplat.Core.Model;

namespace Homenet.RecallSplat.AdminWebsite.Models
{
	public class ConfigurationsFilterViewModel
	{
		[DisplayName("Configuration Type")]
		public ConfigurationType? ConfigurationType { get; set; }
		[DisplayName("Configuration Key")]
		public string ConfigurationKey { get; set; }
	}
}