﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json.Linq;

namespace Homenet.RecallSplat.AdminWebsite.Models
{
	public class CustomJsonValidator:ValidationAttribute
	{
		protected override ValidationResult IsValid(object value, ValidationContext validationContext)
		{
			if (string.IsNullOrWhiteSpace(value as string))
			{
				return new ValidationResult("The " + validationContext.DisplayName + " field is required");
			}

			try
			{
				JObject.Parse(value.ToString());
				return ValidationResult.Success;
			}
			catch (Exception)
			{
				return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
			}
		}
	}
}