﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using Homenet.RecallSplat.Framework.Bootstrapper;

namespace Homenet.RecallSplat.AdminWebsite.Adapters
{
	public class RecallSplatDependencyResolver : IDependencyResolver
	{
		public object GetService(Type serviceType)
		{
			try
			{
				MethodInfo method = typeof(RecallSplatKernel).GetMethod("Get");
				MethodInfo generic = method.MakeGenericMethod(serviceType);

				return generic.Invoke(this, null);
			}
			catch (Exception)
			{
				return null;
			}
		}

		public IEnumerable<object> GetServices(Type serviceType)
		{
			try
			{
				object service = GetService(serviceType);
				if (service == null) return Enumerable.Empty<object>();
				return new[] { service };
			}
			catch (Exception)
			{
				return Enumerable.Empty<object>();
			}
		}
	}
}