﻿using System;
using System.IO;
using System.Text;

namespace ConvertRptToCsv
{
	public class CsvWriter : CsvStreamBase, IDisposable
	{
		private readonly TextWriter _writer;
		private bool _headersWritten;
		private bool _firstField = true;

		#region Constructors

		public CsvWriter(TextWriter writer)
		{
			_writer = writer;
		}

		public CsvWriter(Stream stream)
		{
			_writer = new StreamWriter(stream);
		}

		public CsvWriter(Stream stream, Encoding encoding)
		{
			_writer = new StreamWriter(stream, encoding);
		}

		#endregion

		#region WriteField

		public void WriteField(bool value)
		{
			WriteFieldInternal(value.ToString());
		}
		public void WriteField(int value)
		{
			WriteFieldInternal(value.ToString(_writer.FormatProvider));
		}
		public void WriteField(long value)
		{
			WriteFieldInternal(value.ToString(_writer.FormatProvider));
		}
		public void WriteField(decimal value)
		{
			WriteFieldInternal(value.ToString(_writer.FormatProvider));
		}
		public void WriteField(float value)
		{
			WriteFieldInternal(value.ToString(_writer.FormatProvider));
		}
		public void WriteField(double value)
		{
			WriteFieldInternal(value.ToString(_writer.FormatProvider));
		}
		public void WriteField(uint value)
		{
			WriteFieldInternal(value.ToString(_writer.FormatProvider));
		}
		public void WriteField(ulong value)
		{
			WriteFieldInternal(value.ToString(_writer.FormatProvider));
		}

		public void WriteField(object value)
		{
			if (value != null)
			{
				var formattable = value as IFormattable;
				WriteFieldInternal(
						formattable != null 
								? formattable.ToString(null, _writer.FormatProvider) 
								: value.ToString()
						);
			}
			else
			{
				WriteFieldInternal(null);
			}
		}

		public void WriteField(string format, object arg0)
		{
			WriteFieldInternal(string.Format(format, arg0));
		}

		public void WriteField(string format, object arg0, object arg1)
		{
			WriteFieldInternal(string.Format(format, arg0, arg1));
		}

		public void WriteField(string format, params string[] args)
		{
			WriteFieldInternal(string.Format(format, args));
		}

		public void WriteField(string value)
		{
			WriteFieldInternal(value);
		}

		#endregion

		public void CommitRecord()
		{
			_headersWritten = true;
			_writer.Write(RecordTerminator);
			_writer.Flush();
			_firstField = true;
		}

		public void WriteHeader(string caption)
		{
			WriteHeaderInternal(caption);
		}

		protected virtual void WriteFieldInternal(string value)
		{
			_headersWritten = true;
			WriteValueInternal(value);
			_firstField = false;
		}

		protected virtual void WriteHeaderInternal(string caption)
		{
			if (_headersWritten)
				throw new InvalidOperationException("CSV Headers have already been written");

			WriteValueInternal(caption);
			_firstField = false;
		}

		protected virtual void WriteValueInternal(string value)
		{
			string writeValue = value ?? string.Empty;
			if (!string.IsNullOrEmpty(writeValue))
				if (EncloseInQuotes || writeValue.IndexOfAny(RecordTerminator.ToCharArray()) > -1 || writeValue.IndexOf(DataQualifier) > -1 || writeValue.IndexOf(FieldSeparator) > -1)
					writeValue = DataQualifier + writeValue.Replace(DataQualifier.ToString(), EscapedDataQualifier) + DataQualifier;

			if (!_firstField)
				_writer.Write(FieldSeparator);
			_writer.Write(writeValue);
		}

		#region IDisposable

		public void Dispose()
		{
			Dispose(true);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
				_writer.Dispose();
			GC.SuppressFinalize(this);
		}

		#endregion
	}
}