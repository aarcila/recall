﻿using System;
using System.Diagnostics;
using System.IO;

namespace ConvertRptToCsv
{
	class Program
	{
		static void Main(string[] args)
		{
			CommandLine commandLine = new CommandLine();
			if (commandLine.DebugOnStart && !Debugger.IsAttached) Debugger.Break();


			string inputFile = commandLine.InputFile;
			string outputFile = commandLine.OutputFile;
			if (string.IsNullOrEmpty(outputFile)) outputFile = Path.ChangeExtension(inputFile, ".csv");

			Console.WriteLine("Converting {0} to {1} ...", inputFile, outputFile);
			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start();

			using (FileStream inputStream = new FileStream(inputFile, FileMode.Open, FileAccess.Read))
			{
				using (FileStream outputStream = new FileStream(outputFile, FileMode.Create, FileAccess.Write))
				{
					using (RptToCsvConverter converter = new RptToCsvConverter(inputStream, outputStream))
					{
						converter.Convert();
					}
				}
			}
			stopwatch.Stop();
			Console.WriteLine("Took {0} seconds.", stopwatch.Elapsed.TotalSeconds);
			Console.Read();
		}
	}
}
