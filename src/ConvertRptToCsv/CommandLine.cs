﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoxAuto.CommandLine;
using CoxAuto.Extensions;

namespace ConvertRptToCsv
{
	internal class CommandLine : CommandLineOptions<CommandLine>
	{
		private static readonly Option[] _options =
		{
			new Option
			{
				Short = "-d",
				Long = "--debugOnStart",
				ArgsAreOptional = true,
				Description = "Attach Debugger on start.",
				Handler =
					(args, result) =>
						result.DebugOnStart = !(args.FirstOrDefault() ?? string.Empty).Equals("false", StringComparison.OrdinalIgnoreCase),
			},
			new Option
			{
				Short = "-i",
				Long = "--input",
				ArgsAreOptional = false,
				ArgCount = 1,
				Description = "Input file.",
				Handler = (args, result) => result.InputFile = args.FirstOrDefault() ?? string.Empty,
			},
			new Option
			{
				Short = "-o",
				Long = "--output",
				ArgsAreOptional = false,
				Description = "Output file.",
				Handler = (args, result) => result.OutputFile = args.FirstOrDefault() ?? string.Empty,
			},
		};

		public bool DebugOnStart { get; set; }
		public string InputFile { get; set; }
		public string OutputFile { get; set; }


		public CommandLine() : base(_options) { }

	}
}
