﻿using System;
using System.Linq;

namespace ConvertRptToCsv
{
	public abstract class CsvStreamBase
	{
		public static readonly char[] QuotableChars = new[] { '"', '\r', '\n' };
		public const int BufferSize = 4096;
		public const char DefaultFieldSeparator = ',';
		public const char DefaultDataQualifier = '"';
		public const string DefaultRecordTerminator = "\r\n";

		private char _fieldSeparator = DefaultFieldSeparator;
		public char FieldSeparator
		{
			get { return _fieldSeparator; }
			set
			{
				if (value == _dataQualifier || RecordTerminator.Contains(value) || QuotableChars.Contains(value))
					throw new ArgumentException("Invalid Character", "value");

				_fieldSeparator = value;
			}
		}

		public bool EncloseInQuotes { get; set; }

		public char DataQualifier
		{
			get { return _dataQualifier; }
			set
			{
				if (value == _fieldSeparator || RecordTerminator.Contains(value))
					throw new ArgumentException("Invalid Character", "value");

				_escapedDataQualifier = value + value.ToString();
				_dataQualifier = value;
			}
		}
		private char _dataQualifier = DefaultDataQualifier;

		public string EscapedDataQualifier
		{
			get { return _escapedDataQualifier; }
		}
		private string _escapedDataQualifier;

		public string RecordTerminator
		{
			get { return _recordTerminator; }
			set
			{
				if (value.Contains(DataQualifier) || value.Contains(FieldSeparator))
					throw new ArgumentException("Invalid Character", "value");

				_recordTerminator = value;
			}
		}
		private string _recordTerminator = DefaultRecordTerminator;
	}
}