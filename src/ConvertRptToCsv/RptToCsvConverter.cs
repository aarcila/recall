﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ConvertRptToCsv
{
	public class RptToCsvConverter : IDisposable
	{
		private readonly Stream _rptStream;
		private readonly Stream _csvStream;
		private readonly StreamReader _rptReader;
		private readonly CsvWriter _csvWriter;

		public RptToCsvConverter(
			Stream rptStream,
			Stream csvStream
			)
		{
			_rptStream = rptStream;
			_csvStream = csvStream;
			_rptReader = new StreamReader(_rptStream);
			_csvWriter = new CsvWriter(_csvStream);
		}

		public void Convert()
		{

			string headerRow;
			string separatorLine;

			List<string> preDataLines = new List<string>(5);
			string line = _rptReader.ReadLine();
			while (true)
			{
				if (line == null) throw new Exception("Could not find Separating Line.");
				if (IsSeparatingLine(line))
				{
					separatorLine = line;
					headerRow = preDataLines.Last();
					break;
				}
				preDataLines.Add(line);
				line = _rptReader.ReadLine();
			}



			List<int> columnIndices = new List<int>();
			if (string.IsNullOrEmpty(separatorLine)) throw new Exception("Could not find Separating Line.");

			bool isTrackingDashes = true;
			for (int i = 0; i < separatorLine.Length; i++)
			{
				char c = separatorLine[i];
				if (c == '-')
				{
					if (isTrackingDashes)
					{
						columnIndices.Add(i);
						isTrackingDashes = false;
					}
					continue;
				}

				if (!isTrackingDashes) isTrackingDashes = true;
			}





			IEnumerable<string> headerRowColumns = ParseRow(headerRow, columnIndices);
			foreach (string headerRowColumn in headerRowColumns)
			{
				_csvWriter.WriteHeader(headerRowColumn);
			}
			_csvWriter.CommitRecord();

			string dataRow = _rptReader.ReadLine();
			while (dataRow != null)
			{
				IEnumerable<string> dataRowColumns = ParseRow(dataRow, columnIndices);
				if (!dataRowColumns.Any()) break;
				foreach (string dataRowColumn in dataRowColumns)
				{
					_csvWriter.WriteField(dataRowColumn);
				}
				_csvWriter.CommitRecord();
				dataRow = _rptReader.ReadLine();
			}
		}

		

		private bool IsSeparatingLine(string line)
		{
			return line.All(c => new[] { '-', ' ', /*'\t'*/ }.Contains(c));
		}

		private IEnumerable<string> ParseRow(string row, IEnumerable<int> columnIndices)
		{
			List<string> columns = new List<string>();
			if (string.IsNullOrEmpty(row)) return columns;

			int columnCount = columnIndices.Count();
			int rowLength = row.Length;

			int index = 0;
			string substring;
			string trimmed;
			for (int i = 1; i < columnCount; i++)
			{
				int prevIndex = columnIndices.ElementAt(i - 1);
				index = columnIndices.ElementAt(i);
				if (index >= rowLength) return columns;

				substring = row.Substring(prevIndex, index - prevIndex);
				trimmed = substring.TrimEnd();
				columns.Add(trimmed == "NULL" ? null : trimmed);
			}

			substring = row.Substring(index);
			trimmed = substring.TrimEnd();
			columns.Add(trimmed == "NULL" ? null : trimmed);

			return columns;
		}


		public void Dispose()
		{
			if (_csvWriter != null) _csvWriter.Dispose();
			if (_csvStream != null) _csvStream.Dispose();
			if (_rptReader != null) _rptReader.Dispose();
			if (_rptStream != null) _rptStream.Dispose();
		}
	}
}