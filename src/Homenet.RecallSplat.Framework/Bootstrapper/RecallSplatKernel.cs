﻿using System;
using System.Collections.Generic;
using Ninject;
using Ninject.Modules;
//using Homenet.RecallSplat.DependencyInjection;

namespace Homenet.RecallSplat.Framework.Bootstrapper
{
	public class RecallSplatKernel
	{
		public static IKernel Instance
		{
			get
			{
				if (_kernel == null)
					throw new Exception("RecallSplatKernel has not been configured");
				return _kernel;
			}
		}
		private static IKernel _kernel;

		public static bool CanResolve<T>()
		{
			return Instance.CanResolve<T>();
		}

		public static IEnumerable<T> GetAll<T>()
		{
			return Instance.GetAll<T>();
		}

		public static T Get<T>()
		{
			return Instance.Get<T>();
		}

		public static T GetByName<T>(string name)
		{
			return Instance.Get<T>(name);
		}

		public static void Bind<T>(Type type)
		{
			Instance.Bind<T>().To(type);
		}

		public static void Configure(IRecallSplatBindingModule bindingModule)
		{
			Configure(new List<IRecallSplatBindingModule>() {bindingModule});
		}

		public static void Configure(IEnumerable<IRecallSplatBindingModule> bindingModules)
		{
			List<INinjectModule> ninjectModules = new List<INinjectModule>();
			foreach (IRecallSplatBindingModule bindingModule in bindingModules)
			{
				INinjectModule nbm = bindingModule as INinjectModule;
				if (nbm != null)
				{
					ninjectModules.Add(nbm);
				}
			}

			_kernel = new StandardKernel(ninjectModules.ToArray());
		}
	}
}