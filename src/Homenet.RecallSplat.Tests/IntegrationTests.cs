﻿using Homenet.RecallSplat.Core.Interfaces;
using Homenet.RecallSplat.Core.Model;
using Homenet.RecallSplat.DependencyInjection;
using Homenet.RecallSplat.Framework.Bootstrapper;
using NUnit.Framework;

namespace Homenet.RecallSplat.Tests
{
	public class IntegrationTests
	{
		private IRecallSplatService _recallSplatService;

		[TestFixtureSetUp]
		public void TestFixtureSetup()
		{
			RecallSplatKernelConfigurator.ConfigureDefaults();

			_recallSplatService = RecallSplatKernel.Get<IRecallSplatService>();
		}

		[Test, Explicit]
		public void HondaIntegrationTest()
		{
			VINRecallResponse response = _recallSplatService.Lookup("1HGCP368X8A082916", true);
		}
	}
}