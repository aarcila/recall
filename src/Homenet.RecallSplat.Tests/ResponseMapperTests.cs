﻿using System;
using System.Linq;
using Homenet.RecallSplat.Core.Interfaces;
using Homenet.RecallSplat.Core.Model;
using Homenet.RecallSplat.Core.Model.Exceptions;
using Homenet.RecallSplat.Core.Services;
using Homenet.RecallSplat.DependencyInjection;
using Homenet.RecallSplat.Framework.Bootstrapper;
using Homenet.RecallSplat.Setup;
using NUnit.Framework;

namespace Homenet.RecallSplat.Tests
{
	[TestFixture]
	public class ResponseMapperTests
	{
		private IResponseMappingService _responseMappingService;

		[TestFixtureSetUp]
		public void TestFixtureSetup()
		{
			RecallSplatKernelConfigurator.ConfigureDefaults();

			_responseMappingService = RecallSplatKernel.GetByName<IResponseMappingService>("JsonPath");
		}

		[Test]
		public void AcuraTest()
		{
			// http://owners.acura.com/Recalls/GetRecallsByVin/1HGCP368X8A082916/true
			string serviceResponse =
				@"{""LastUpdated"":""Nov 05, 2015"",""Warning"":false,""WarningCode"":null,""WarningText"":null,""DivisionMismatch"":false,""VIN"":""2HNYD18625H514645"",""Make"":""Acura"",""ModelYear"":""2005"",""DivisionCd"":""B"",""ModelGroupId"":null,""ModelGroupName"":""MDX"",""RecallsUrl"":""//owners.acura.com/recalls/2HNYD18625H514645"",""SaveVinUrl"":""/account/add-vehicle/2HNYD18625H514645"",""IsLoggedIn"":false,""CampaignTypes"":[{""SortOrder"":1,""Campaigns"":[{""AgencyTypeCd"":""N"",""AgencyType"":""National Highway Transportation Safety Administration (NHTSA)"",""AgencyName"":""NHTSA"",""RecallNumber"":""15V-320"",""Description"":""MDX Driver's Airbag"",""Id"":""JR7"",""RecallDate"":""05/27/2015"",""RecallStatus"":""Recall INCOMPLETE"",""RecallDescription"":""THIS RECALL CONSOLIDATES ALL OF ACURA'S PREVIOUS RECALLS CONCERNING THE DRIVER'S FRONT AIRBAG INFLATOR. EVEN IF YOUR VEHICLE WAS PREVIOUSLY REPAIRED, YOUR VEHICLE IS STILL COVERED BY THIS RECALL AND WILL NEED TO BE REPAIRED AGAIN."",""SafetyRiskDescription"":""SPECIFICALLY, IN SOME VEHICLES, THE DRIVER'S FRONT AIRBAG INFLATOR COULD PRODUCE EXCESSIVE INTERNAL PRESSURE UPON DEPLOYMENT. IF AN AFFECTED AIRBAG DEPLOYS, THE INCREASED INTERNAL PRESSURE MAY CAUSE THE INFLATOR TO RUPTURE (BREAK APART) AND DEPLOY ABNORMALLY. IN THE EVENT OF AN INFLATOR RUPTURE, METAL FRAGMENTS COULD PASS THROUGH THE AIRBAG CUSHION MATERIAL POSSIBLY CAUSING SERIOUS INJURY OR FATALITY TO YOU OR OTHERS IN THE VEHICLE. PAST RUPTURES LIKE THIS HAVE KILLED AND INJURED VEHICLE DRIVERS."",""RemedyDescription"":""ACURA WILL NOTIFY OWNERS AND DEALERS WILL REPLACE THE DRIVER FRONTAL AIRBAG INFLATOR WITH AN INFLATOR OF DIFFERENT DESIGN, FREE OF CHARGE. WHILE PARTS ARE AVAILABLE TO CONDUCT AIRBAG INFLATOR REPLACEMENTS AT THE TIME OF THIS NOTICE, THE SCOPE OF THE CURRENT AIRBAG INFLATOR RECALLS CREATES THE POSSIBILITY THAT THE PARTS NECESSARY TO COMPLETE THE RECALL REPAIR MAY NOT BE AVAILABLE AT THE TIME YOU CALL TO SCHEDULE YOUR REPAIR. IF THIS OCCURS, PLEASE DISCUSS YOUR SPECIFIC NEEDS AND CONCERNS WITH YOUR DEALER, INCLUDING THE PROVISION OF, OR REIMBURSEMENT FOR, TEMPORARY ALTERNATIVE TRANSPORTATION AS NECESSARY. OWNERS MAY CONTACT ACURA'S CLIENT RELATIONS AT 1-800-382-2238, OPTION 4. ACURA'S CAMPAIGN NUMBER FOR THIS RECALL IS JR7.""}]}]}";


            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetHondaAcuraMappingData(),
                ResponseText = serviceResponse
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("2HNYD18625H514645", mappedResponse.VIN);
			Assert.AreEqual(2005, mappedResponse.Year);
			Assert.AreEqual("Acura", mappedResponse.Make);
			Assert.AreEqual("MDX", mappedResponse.Model);
			Assert.AreEqual(new DateTime(2015, 11, 5), mappedResponse.OEMFeedLastUpdated);

			Assert.AreEqual(1, mappedResponse.OpenRecalls.Count());

			Assert.AreEqual("15V-320", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
			Assert.AreEqual("SPECIFICALLY, IN SOME VEHICLES, THE DRIVER'S FRONT AIRBAG INFLATOR COULD PRODUCE EXCESSIVE INTERNAL PRESSURE UPON DEPLOYMENT. IF AN AFFECTED AIRBAG DEPLOYS, THE INCREASED INTERNAL PRESSURE MAY CAUSE THE INFLATOR TO RUPTURE (BREAK APART) AND DEPLOY ABNORMALLY. IN THE EVENT OF AN INFLATOR RUPTURE, METAL FRAGMENTS COULD PASS THROUGH THE AIRBAG CUSHION MATERIAL POSSIBLY CAUSING SERIOUS INJURY OR FATALITY TO YOU OR OTHERS IN THE VEHICLE. PAST RUPTURES LIKE THIS HAVE KILLED AND INJURED VEHICLE DRIVERS.", mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
			Assert.AreEqual("THIS RECALL CONSOLIDATES ALL OF ACURA'S PREVIOUS RECALLS CONCERNING THE DRIVER'S FRONT AIRBAG INFLATOR. EVEN IF YOUR VEHICLE WAS PREVIOUSLY REPAIRED, YOUR VEHICLE IS STILL COVERED BY THIS RECALL AND WILL NEED TO BE REPAIRED AGAIN.", mappedResponse.OpenRecalls.ElementAt(0).Description);
			Assert.AreEqual("ACURA WILL NOTIFY OWNERS AND DEALERS WILL REPLACE THE DRIVER FRONTAL AIRBAG INFLATOR WITH AN INFLATOR OF DIFFERENT DESIGN, FREE OF CHARGE. WHILE PARTS ARE AVAILABLE TO CONDUCT AIRBAG INFLATOR REPLACEMENTS AT THE TIME OF THIS NOTICE, THE SCOPE OF THE CURRENT AIRBAG INFLATOR RECALLS CREATES THE POSSIBILITY THAT THE PARTS NECESSARY TO COMPLETE THE RECALL REPAIR MAY NOT BE AVAILABLE AT THE TIME YOU CALL TO SCHEDULE YOUR REPAIR. IF THIS OCCURS, PLEASE DISCUSS YOUR SPECIFIC NEEDS AND CONCERNS WITH YOUR DEALER, INCLUDING THE PROVISION OF, OR REIMBURSEMENT FOR, TEMPORARY ALTERNATIVE TRANSPORTATION AS NECESSARY. OWNERS MAY CONTACT ACURA'S CLIENT RELATIONS AT 1-800-382-2238, OPTION 4. ACURA'S CAMPAIGN NUMBER FOR THIS RECALL IS JR7.", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
			Assert.AreEqual(new DateTime(2015, 5, 27), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.RecallIncomplete, mappedResponse.OpenRecalls.ElementAt(0).Status);
			Assert.AreEqual("JR7", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);
			

			//weird char difference at index 3
			//Assert.AreEqual("MDX Driver's Airbag", mappedResponse.OpenRecalls.ElementAt(0).Title);
		}

		[Test]
		public void HondaTest()
		{
			// http://owners.honda.com/Recalls/GetRecallsByVin/1HGCP368X8A082916/true
			string serviceResponse =
				@"{""LastUpdated"":""Nov 05, 2015"",""Warning"":false,""WarningCode"":null,""WarningText"":null,""DivisionMismatch"":false,""VIN"":""1HGCP368X8A082916"",""Make"":""Honda"",""ModelYear"":""2008"",""DivisionCd"":""A"",""ModelGroupId"":null,""ModelGroupName"":""Accord Sedan"",""RecallsUrl"":""//owners.honda.com/recalls/1HGCP368X8A082916"",""SaveVinUrl"":""/account/add-vehicle/1HGCP368X8A082916"",""IsLoggedIn"":false,""CampaignTypes"":[{""SortOrder"":1,""Campaigns"":[{""AgencyTypeCd"":""N"",""AgencyType"":""National Highway Transportation Safety Administration (NHTSA)"",""AgencyName"":""NHTSA"",""RecallNumber"":""09E-063"",""Description"":""08-2010 ACCORD NOSE MASK"",""Id"":""R21"",""RecallDate"":""11/09/2009"",""RecallStatus"":""Recall INCOMPLETE"",""RecallDescription"":""HONDA IS RECALLING AFTERMARKET FULL NOSE MASKS, PART NUMBER 08P35-TA0-100 AND 08P35-TA6-100 MANUFACTURED BETWEEN AUGUST 9, 2007 THROUGH DECEMBER 11, 2007. THE ACCESSORY WAS DESIGNED SPECIFICALLY FOR MODEL YEAR 2008-2010 HONDA ACCORD 4-DOOR VEHICLES. IT IS POSSIBLE FOR THE NOSE MASK MATERIAL TO INTERFERE WITH PART OF THE HOOD LATCH. IF THE HOOD HAS NOT BEEN COMPLETELY CLOSED AND THE NOSE MASK INTERFERES WITH THE SECONDARY HOOD LATCH MECHANISM THE HOOD MAY POP OPEN WHILE THE VEHICLE BEING DRIVEN."",""SafetyRiskDescription"":""THIS MAY RESULT IN REDUCED DRIVER VISIBILITY THAT COULD INCREASE THE RISK OF A CRASH."",""RemedyDescription"":""HONDA IS ASKING OWNERS TO PLEASE REMOVE THE NOSE MASK IMMEDIATELY AND CALL ANY AUTHORIZED HONDA DEALER TO MAKE AN APPOINTMENT TO RETURN THE NOSE MASK AND HAVE IT REPLACED FREE OF CHARGE.  THE SAFETY RECALL BEGAN ON MARCH 2009.   OWNERS MAY CONTACT HONDA CUSTOMER SERVICE AT 1-800-999-1009.""}]}]}";

            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetHondaAcuraMappingData(),
                ResponseText = serviceResponse
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("1HGCP368X8A082916", mappedResponse.VIN);
			Assert.AreEqual(2008, mappedResponse.Year);
			Assert.AreEqual("Honda", mappedResponse.Make);
			Assert.AreEqual("Accord Sedan", mappedResponse.Model);
			Assert.AreEqual(new DateTime(2015, 11, 5), mappedResponse.OEMFeedLastUpdated);

			Assert.AreEqual(1, mappedResponse.OpenRecalls.Count());

			Assert.AreEqual("09E-063", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
			Assert.AreEqual("THIS MAY RESULT IN REDUCED DRIVER VISIBILITY THAT COULD INCREASE THE RISK OF A CRASH.", mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
			Assert.AreEqual("HONDA IS RECALLING AFTERMARKET FULL NOSE MASKS, PART NUMBER 08P35-TA0-100 AND 08P35-TA6-100 MANUFACTURED BETWEEN AUGUST 9, 2007 THROUGH DECEMBER 11, 2007. THE ACCESSORY WAS DESIGNED SPECIFICALLY FOR MODEL YEAR 2008-2010 HONDA ACCORD 4-DOOR VEHICLES. IT IS POSSIBLE FOR THE NOSE MASK MATERIAL TO INTERFERE WITH PART OF THE HOOD LATCH. IF THE HOOD HAS NOT BEEN COMPLETELY CLOSED AND THE NOSE MASK INTERFERES WITH THE SECONDARY HOOD LATCH MECHANISM THE HOOD MAY POP OPEN WHILE THE VEHICLE BEING DRIVEN.", mappedResponse.OpenRecalls.ElementAt(0).Description);
			Assert.AreEqual("HONDA IS ASKING OWNERS TO PLEASE REMOVE THE NOSE MASK IMMEDIATELY AND CALL ANY AUTHORIZED HONDA DEALER TO MAKE AN APPOINTMENT TO RETURN THE NOSE MASK AND HAVE IT REPLACED FREE OF CHARGE.  THE SAFETY RECALL BEGAN ON MARCH 2009.   OWNERS MAY CONTACT HONDA CUSTOMER SERVICE AT 1-800-999-1009.", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
			Assert.AreEqual("08-2010 ACCORD NOSE MASK", mappedResponse.OpenRecalls.ElementAt(0).Title);
			Assert.AreEqual(new DateTime(2009, 11, 9), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.RecallIncomplete, mappedResponse.OpenRecalls.ElementAt(0).Status);
			Assert.AreEqual("R21", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);
		}

		[Test]
		public void AudiTest()
		{
			// http://web.audiusa.com/audirecall/vin/WAU2GAFC8CN075058
			string serviceResponse = @"{""vin"":""WAU2GAFC8CN075058"",""status"":true,""year"":2012,""make"":""AUDI"",""model"":""A7 3.0 4-DR AUTO QUATTRO"",""manufacturerId"":2,""isRecallsAvailable"":true,""numberOfRecalls"":1,""refreshDate"":""Nov 05, 2015"",""recalls"":[{""vwgoaActionTitle"":""Fuel Injecton System Leak"",""mfrRecallNumber"":""24AP"",""nhtsaRecallNumber"":""15V-019"",""recallDate"":""Jan 26, 2015"",""recallDescription"":""Due to a combination of production tolerance issues and vehicle vibration during dynamic driving, it is possible that, in rare cases, a fuel leak may occur in the vehicle's fuel injection system. Leaking fuel, in the presence of an ignition source, may result in a fire.  Additionally, some vehicles may benefit from having the thermostat and/or the crankcase pressure valve updated. Repairs to these other components are not related to the safety recall, but can be easily performed at the same time as recall repair.\n"",""safetyRiskDescription"":"" Leaking fuel, in the presence of an ignition source, may result in a fire."",""remedyDescription"":""To help correct the safety defect, your authorized Audi dealer will replace both fuel rails and the fuel injector seals. The thermostat and/or the crankcase pressure valve will also be updated on certain vehicles. These repairs are not related to the safety recall, but can easily be performed at the same time as the recall repair. These repairs will be performed free of charge. \n"",""mfrRecallStatus"":11,""mfrNotes"":"" "",""refreshDate"":""Nov 05, 2015"",""vwgoaActionType"":""SAFETY""}]}";


            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetAudiMappingData(),
                ResponseText = serviceResponse
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("WAU2GAFC8CN075058", mappedResponse.VIN);
			Assert.AreEqual(2012, mappedResponse.Year);
			Assert.AreEqual("AUDI", mappedResponse.Make);
			Assert.AreEqual("A7 3.0 4-DR AUTO QUATTRO", mappedResponse.Model);
			Assert.AreEqual(new DateTime(2015, 11, 5), mappedResponse.OEMFeedLastUpdated);

			Assert.AreEqual(1, mappedResponse.OpenRecalls.Count());

			Assert.AreEqual("15V-019", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
			Assert.AreEqual("Leaking fuel, in the presence of an ignition source, may result in a fire.", mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
			Assert.AreEqual("Due to a combination of production tolerance issues and vehicle vibration during dynamic driving, it is possible that, in rare cases, a fuel leak may occur in the vehicle's fuel injection system. Leaking fuel, in the presence of an ignition source, may result in a fire.  Additionally, some vehicles may benefit from having the thermostat and/or the crankcase pressure valve updated. Repairs to these other components are not related to the safety recall, but can be easily performed at the same time as recall repair.", mappedResponse.OpenRecalls.ElementAt(0).Description);
			Assert.AreEqual("To help correct the safety defect, your authorized Audi dealer will replace both fuel rails and the fuel injector seals. The thermostat and/or the crankcase pressure valve will also be updated on certain vehicles. These repairs are not related to the safety recall, but can easily be performed at the same time as the recall repair. These repairs will be performed free of charge.", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
			Assert.AreEqual("Fuel Injecton System Leak", mappedResponse.OpenRecalls.ElementAt(0).Title);
			Assert.AreEqual(new DateTime(2015, 1, 26), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.RecallIncomplete, mappedResponse.OpenRecalls.ElementAt(0).Status);
			Assert.AreEqual("24AP", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);
		}

		[Test]
		public void BuickTest()
		{
			// https://recalls.gm.com/recall/services/1G4GC5GD1BF219931/recalls
			string serviceResponse =
				@"{""messages"":[],""serverErrorMsgs"":[],""data"":{""vin"":""1G4GC5GD1BF219931"",""year"":2011,""make"":""Buick"",""model"":""LaCrosse"",""manufacturer_id"":1,""recalls_available"":true,""number_of_recalls"":2,""recalls"":[{""recall_type"":""ZPSR"",""nhtsa_recall_number"":""14V447"",""mfr_recall_number"":""N140271"",""recall_date"":""Aug 12, 2014"",""recall_title"":""POWER HEIGHT ADJUSTABLE DRIVER AND PASSENGER FRONT SEATS"",""recall_description"":""General Motors has decided that a defect which relates to motor vehicle safety exists in certain 2011-2012 MY Buick Regal and LaCrosse, 2010-2012 MY Cadillac SRX, 2011-2012 MY Chevrolet Camaro, and 2010-2012 MY Chevrolet Equinox and GMC Terrain vehicles equipped with power height adjustable driver and passenger front seats.  Some of these vehicles may have a condition where the bolt that secures the power front seat height adjuster can become loose or fall out."",""safety_risk_description"":""If the bolt falls out, the seat will drop suddenly to the lowest vertical position.  The sudden seat movement may affect a driver\u001As ability to safely operate a vehicle or increase the risk of injury to the seat occupant in a vehicle crash. UNTIL YOU HAVE HAD THE RECALL REPAIR, DO NOT ADJUST THE SEAT HEIGHT."",""remedy_description"":""Dealers are to replace the height adjuster shoulder bolt free of charge."",""mfr_recall_status"":""11""},{""recall_type"":""ZCSP"",""nhtsa_recall_number"":null,""mfr_recall_number"":""N130322"",""recall_date"":""Dec 19, 2014"",""recall_title"":""NAVIGATION RADIO LOSS OF CUSTOMER SETTINGS"",""recall_description"":null,""safety_risk_description"":null,""remedy_description"":null,""mfr_recall_status"":""11""}],""status"":true,""error_code"":0,""refresh_date"":""Nov 05, 2015""}}";

            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetGMMappingData(),
                ResponseText = serviceResponse
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("1G4GC5GD1BF219931", mappedResponse.VIN);
			Assert.AreEqual(2011, mappedResponse.Year);
			Assert.AreEqual("Buick", mappedResponse.Make);
			Assert.AreEqual("LaCrosse", mappedResponse.Model);
			Assert.AreEqual(new DateTime(2015, 11, 5), mappedResponse.OEMFeedLastUpdated);

			Assert.AreEqual(2, mappedResponse.OpenRecalls.Count());

			Assert.AreEqual("14V447", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
			Assert.AreEqual("If the bolt falls out, the seat will drop suddenly to the lowest vertical position.  The sudden seat movement may affect a driver\u001as ability to safely operate a vehicle or increase the risk of injury to the seat occupant in a vehicle crash. UNTIL YOU HAVE HAD THE RECALL REPAIR, DO NOT ADJUST THE SEAT HEIGHT.", mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
			Assert.AreEqual("General Motors has decided that a defect which relates to motor vehicle safety exists in certain 2011-2012 MY Buick Regal and LaCrosse, 2010-2012 MY Cadillac SRX, 2011-2012 MY Chevrolet Camaro, and 2010-2012 MY Chevrolet Equinox and GMC Terrain vehicles equipped with power height adjustable driver and passenger front seats.  Some of these vehicles may have a condition where the bolt that secures the power front seat height adjuster can become loose or fall out.", mappedResponse.OpenRecalls.ElementAt(0).Description);
			Assert.AreEqual("Dealers are to replace the height adjuster shoulder bolt free of charge.", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
			Assert.AreEqual("POWER HEIGHT ADJUSTABLE DRIVER AND PASSENGER FRONT SEATS", mappedResponse.OpenRecalls.ElementAt(0).Title);
			Assert.AreEqual(new DateTime(2014, 8, 12), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.RecallIncomplete, mappedResponse.OpenRecalls.ElementAt(0).Status);
			Assert.AreEqual("14V447", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
			Assert.AreEqual("N140271", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);


			Assert.AreEqual(null, mappedResponse.OpenRecalls.ElementAt(1).CampaignID);
			Assert.AreEqual(null, mappedResponse.OpenRecalls.ElementAt(1).SafetyRisk);
			Assert.AreEqual(null, mappedResponse.OpenRecalls.ElementAt(1).Description);
			Assert.AreEqual(null, mappedResponse.OpenRecalls.ElementAt(1).RemedyDescription);
			Assert.AreEqual("NAVIGATION RADIO LOSS OF CUSTOMER SETTINGS", mappedResponse.OpenRecalls.ElementAt(1).Title);
			Assert.AreEqual(new DateTime(2014, 12, 19), mappedResponse.OpenRecalls.ElementAt(1).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.RecallIncomplete, mappedResponse.OpenRecalls.ElementAt(1).Status);
			Assert.AreEqual(null, mappedResponse.OpenRecalls.ElementAt(1).CampaignID);
			Assert.AreEqual("N130322", mappedResponse.OpenRecalls.ElementAt(1).OEMCampaignID);
		}

		[Test]
		public void CadillacTest()
		{
			// https://recalls.gm.com/recall/services/1G6AA5RA2D0135385/recalls
			string serviceResponse = @"{""messages"":[],""serverErrorMsgs"":[],""data"":{""vin"":""1G6AA5RA2D0135385"",""year"":2013,""make"":""Cadillac"",""model"":""ATS"",""manufacturer_id"":1,""recalls_available"":true,""number_of_recalls"":1,""recalls"":[{""recall_type"":""ZPSR"",""nhtsa_recall_number"":""15V558"",""mfr_recall_number"":""N150299"",""recall_date"":""Sep 03, 2015"",""recall_title"":""DRIVER SIDE REAR PILLAR INTERIOR FIRES"",""recall_description"":""General Motors has decided that a defect which relates to motor vehicle safety exists in certain 2013-2016 model year ATS sedan vehicles. These vehicles have a coil antenna module that powers the rear defogger system located in the driver side rear pillar.  In certain of these vehicles, the coil antenna module may generate significant temperatures if the module is subjected to excessive cycling or continuous operation and the module was manufactured with critically weak terminal connectivity."",""safety_risk_description"":""Where this condition exists, a fire may develop inside the rear pillar on the driver’s side of the vehicle."",""remedy_description"":""Dealers will reflash the Electronic Climate Control (ECC) module to remove the automatic rear defogger “on” function that occurs as a result of manual or remote start of the vehicle.  2013 models also have a “continuous on” feature (enabled at highway speeds in low temperatures) that will also be disabled.  The base functionality of the rear defogger will not be lost. The customer will retain the ability to turn on the function manually from inside the vehicle. This service will be free of charge."",""mfr_recall_status"":""11""}],""status"":true,""error_code"":0,""refresh_date"":""Nov 05, 2015""}}";

            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetGMMappingData(),
                ResponseText = serviceResponse
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("1G6AA5RA2D0135385", mappedResponse.VIN);
			Assert.AreEqual(2013, mappedResponse.Year);
			Assert.AreEqual("Cadillac", mappedResponse.Make);
			Assert.AreEqual("ATS", mappedResponse.Model);
			Assert.AreEqual(new DateTime(2015, 11, 5), mappedResponse.OEMFeedLastUpdated);

			Assert.AreEqual(1, mappedResponse.OpenRecalls.Count());

			Assert.AreEqual("15V558", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
			Assert.AreEqual("Where this condition exists, a fire may develop inside the rear pillar on the driver’s side of the vehicle.", mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
			Assert.AreEqual("General Motors has decided that a defect which relates to motor vehicle safety exists in certain 2013-2016 model year ATS sedan vehicles. These vehicles have a coil antenna module that powers the rear defogger system located in the driver side rear pillar.  In certain of these vehicles, the coil antenna module may generate significant temperatures if the module is subjected to excessive cycling or continuous operation and the module was manufactured with critically weak terminal connectivity.", mappedResponse.OpenRecalls.ElementAt(0).Description);
			Assert.AreEqual("Dealers will reflash the Electronic Climate Control (ECC) module to remove the automatic rear defogger “on” function that occurs as a result of manual or remote start of the vehicle.  2013 models also have a “continuous on” feature (enabled at highway speeds in low temperatures) that will also be disabled.  The base functionality of the rear defogger will not be lost. The customer will retain the ability to turn on the function manually from inside the vehicle. This service will be free of charge.", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
			Assert.AreEqual("DRIVER SIDE REAR PILLAR INTERIOR FIRES", mappedResponse.OpenRecalls.ElementAt(0).Title);
			Assert.AreEqual(new DateTime(2015, 9, 3), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.RecallIncomplete, mappedResponse.OpenRecalls.ElementAt(0).Status);
			Assert.AreEqual("15V558", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
			Assert.AreEqual("N150299", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);
		}

		[Test]
		public void CadillacNoneTest()
		{
			string serviceResponse = @"{""messages"":[],""serverErrorMsgs"":[],""data"":{""vin"":""1G6AA5RX8D0143205"",""year"":2013,""make"":""Cadillac"",""model"":""ATS"",""manufacturer_id"":1,""recalls_available"":false,""number_of_recalls"":0,""recalls"":[],""status"":true,""error_code"":0,""refresh_date"":""Nov 06, 2015""}}";

            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetGMMappingData(),
                ResponseText = serviceResponse
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("1G6AA5RX8D0143205", mappedResponse.VIN);
			Assert.AreEqual(2013, mappedResponse.Year);
			Assert.AreEqual("Cadillac", mappedResponse.Make);
			Assert.AreEqual("ATS", mappedResponse.Model);
			Assert.AreEqual(new DateTime(2015, 11, 6), mappedResponse.OEMFeedLastUpdated);

			Assert.AreEqual(0, mappedResponse.OpenRecalls.Count());
		}

		[Test]
		public void ChevroletTest()
		{
			// https://recalls.gm.com/recall/services/1G1AF5F57A7169601/recalls
			string serviceResponse =
				@"{""messages"":[],""serverErrorMsgs"":[],""data"":{""vin"":""1G1AF5F57A7169601"",""year"":2010,""make"":""Chevrolet"",""model"":""Cobalt"",""manufacturer_id"":1,""recalls_available"":true,""number_of_recalls"":3,""recalls"":[{""recall_type"":""ZPSR"",""nhtsa_recall_number"":""14V047"",""mfr_recall_number"":""N140092"",""recall_date"":""Apr 02, 2014"",""recall_title"":""IGNITION SWITCH REPLACEMENT"",""recall_description"":""General Motors notified the agency that the defective ignition switches may have been used as service replacement parts on other vehicles, and as a result General Motors is recalling certain model year 2008-2010 Chevrolet Cobalt, Saturn Sky, and Pontiac G5 and Solstice, and 2008-2011 Chevrolet HHR vehicles.  The part numbers for the service parts are 10392423 (a/k/a ACDelco D1461F), 10392737, 15857948, 15854953, 15896640, and 25846762."",""safety_risk_description"":""If the key is not in the run position, the air bags may not deploy if the vehicle is involved in a crash, increasing the risk of injury."",""remedy_description"":""Dealers will replace the ignition switch, free of charge.  Please contact your local GM Certified Service Dealer to order parts or visit www.gmignitionupdate.com for additional information."",""mfr_recall_status"":""11""},{""recall_type"":""ZPSR"",""nhtsa_recall_number"":""14V171"",""mfr_recall_number"":""N140113"",""recall_date"":""Apr 03, 2014"",""recall_title"":""REPLACE IGNITION LOCK CYLINDER AND IGNITION KEY"",""recall_description"":""General Motors is recalling certain model year 2005-2010 Chevrolet Cobalt, 2006-2011 Chevrolet HHR, 2007-2010 Pontiac G5, 2006-2010 Pontiac Solstice, 2003-2007 Saturn Ion, and 2007-2010 Saturn Sky vehicles.  In the affected vehicles, the key can be removed from the ignition when the ignition is not in the \""Off\"" position."",""safety_risk_description"":""If the key can be removed from the ignition when the ignition is not in the \""off\"" position, the vehicle could roll away: (a) for an automatic transmission, if the transmission is not in the \""Park\"" position; or (b) for a manual transmission, if the parking brake is not engaged and the transmission is not in the \""Reverse\"" position. This potential for rollaway increases the risk for a crash and occupant or pedestrian injuries."",""remedy_description"":""Dealers will replace the ignition cylinder and cut two ignition/door keys and relearn keys, if necessary for each vehicle free of charge.  Please contact your local GM Certified Service Dealer to order parts or visit www.gmignitionupdate.com for additional information."",""mfr_recall_status"":""11""},{""recall_type"":""ZPSR"",""nhtsa_recall_number"":""15V500"",""mfr_recall_number"":""N150075"",""recall_date"":""Aug 05, 2015"",""recall_title"":""SIDE IMPACT SENSOR (SIS) WIRE CHAFE AT WINDOW REGULATOR"",""recall_description"":""General Motors has decided that a defect which relates to motor vehicle safety exists in certain 2010 Chevrolet Cobalt sedan vehicles.  In some of these vehicles, the side-impact sensor wire harness in the driver-side front door may have been improperly routed.  If the wire harness was improperly routed, the window regulator could contact the harness when the window is fully lowered and, over time, chafe the harness insulation.  If the regulator penetrates the insulation, a short could occur in the side-impact sensor circuit.  When a short occurs, the vehicle’s sensing and diagnostic module (SDM) may no longer receive sensing data from that sensor, preventing the driver-side roof-rail airbag from deploying during a crash."",""safety_risk_description"":""Disabling the driver-side roof-rail airbag increases the risk of occupant injury in certain kinds of crashes."",""remedy_description"":""Dealers will inspect the SIS wiring in the front left doors.  If the condition is present, they will repair damaged wires, correct the clip orientation, and correct the wire routing Free of charge."",""mfr_recall_status"":""11""}],""status"":true,""error_code"":0,""refresh_date"":""Nov 05, 2015""}}";

            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetGMMappingData(),
                ResponseText = serviceResponse
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("1G1AF5F57A7169601", mappedResponse.VIN);
			Assert.AreEqual(2010, mappedResponse.Year);
			Assert.AreEqual("Chevrolet", mappedResponse.Make);
			Assert.AreEqual("Cobalt", mappedResponse.Model);
			Assert.AreEqual(new DateTime(2015, 11, 5), mappedResponse.OEMFeedLastUpdated);

			Assert.AreEqual(3, mappedResponse.OpenRecalls.Count());

			Assert.AreEqual("14V047", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
			Assert.AreEqual("If the key is not in the run position, the air bags may not deploy if the vehicle is involved in a crash, increasing the risk of injury.", mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
			Assert.AreEqual("General Motors notified the agency that the defective ignition switches may have been used as service replacement parts on other vehicles, and as a result General Motors is recalling certain model year 2008-2010 Chevrolet Cobalt, Saturn Sky, and Pontiac G5 and Solstice, and 2008-2011 Chevrolet HHR vehicles.  The part numbers for the service parts are 10392423 (a/k/a ACDelco D1461F), 10392737, 15857948, 15854953, 15896640, and 25846762.", mappedResponse.OpenRecalls.ElementAt(0).Description);
			Assert.AreEqual("Dealers will replace the ignition switch, free of charge.  Please contact your local GM Certified Service Dealer to order parts or visit www.gmignitionupdate.com for additional information.", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
			Assert.AreEqual("IGNITION SWITCH REPLACEMENT", mappedResponse.OpenRecalls.ElementAt(0).Title);
			Assert.AreEqual(new DateTime(2014, 4, 2), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.RecallIncomplete, mappedResponse.OpenRecalls.ElementAt(0).Status);
			Assert.AreEqual("N140092", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);

			Assert.AreEqual("14V171", mappedResponse.OpenRecalls.ElementAt(1).CampaignID);
			Assert.AreEqual("If the key can be removed from the ignition when the ignition is not in the \"off\" position, the vehicle could roll away: (a) for an automatic transmission, if the transmission is not in the \"Park\" position; or (b) for a manual transmission, if the parking brake is not engaged and the transmission is not in the \"Reverse\" position. This potential for rollaway increases the risk for a crash and occupant or pedestrian injuries.", mappedResponse.OpenRecalls.ElementAt(1).SafetyRisk);
			Assert.AreEqual("General Motors is recalling certain model year 2005-2010 Chevrolet Cobalt, 2006-2011 Chevrolet HHR, 2007-2010 Pontiac G5, 2006-2010 Pontiac Solstice, 2003-2007 Saturn Ion, and 2007-2010 Saturn Sky vehicles.  In the affected vehicles, the key can be removed from the ignition when the ignition is not in the \"Off\" position.", mappedResponse.OpenRecalls.ElementAt(1).Description);
			Assert.AreEqual("Dealers will replace the ignition cylinder and cut two ignition/door keys and relearn keys, if necessary for each vehicle free of charge.  Please contact your local GM Certified Service Dealer to order parts or visit www.gmignitionupdate.com for additional information.", mappedResponse.OpenRecalls.ElementAt(1).RemedyDescription);
			Assert.AreEqual("REPLACE IGNITION LOCK CYLINDER AND IGNITION KEY", mappedResponse.OpenRecalls.ElementAt(1).Title);
			Assert.AreEqual(new DateTime(2014, 4, 3), mappedResponse.OpenRecalls.ElementAt(1).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.RecallIncomplete, mappedResponse.OpenRecalls.ElementAt(1).Status);
			Assert.AreEqual("N140113", mappedResponse.OpenRecalls.ElementAt(1).OEMCampaignID);

			Assert.AreEqual("15V500", mappedResponse.OpenRecalls.ElementAt(2).CampaignID);
			Assert.AreEqual("Disabling the driver-side roof-rail airbag increases the risk of occupant injury in certain kinds of crashes.", mappedResponse.OpenRecalls.ElementAt(2).SafetyRisk);
			Assert.AreEqual("General Motors has decided that a defect which relates to motor vehicle safety exists in certain 2010 Chevrolet Cobalt sedan vehicles.  In some of these vehicles, the side-impact sensor wire harness in the driver-side front door may have been improperly routed.  If the wire harness was improperly routed, the window regulator could contact the harness when the window is fully lowered and, over time, chafe the harness insulation.  If the regulator penetrates the insulation, a short could occur in the side-impact sensor circuit.  When a short occurs, the vehicle’s sensing and diagnostic module (SDM) may no longer receive sensing data from that sensor, preventing the driver-side roof-rail airbag from deploying during a crash.", mappedResponse.OpenRecalls.ElementAt(2).Description);
			Assert.AreEqual("Dealers will inspect the SIS wiring in the front left doors.  If the condition is present, they will repair damaged wires, correct the clip orientation, and correct the wire routing Free of charge.", mappedResponse.OpenRecalls.ElementAt(2).RemedyDescription);
			Assert.AreEqual("SIDE IMPACT SENSOR (SIS) WIRE CHAFE AT WINDOW REGULATOR", mappedResponse.OpenRecalls.ElementAt(2).Title);
			Assert.AreEqual(new DateTime(2015, 8, 5), mappedResponse.OpenRecalls.ElementAt(2).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.RecallIncomplete, mappedResponse.OpenRecalls.ElementAt(2).Status);
			Assert.AreEqual("N150075", mappedResponse.OpenRecalls.ElementAt(2).OEMCampaignID);
		}

		[Test]
		public void ChevorletNoneTest()
		{
			string serviceResponse = @"{""messages"":[],""serverErrorMsgs"":[],""data"":{""vin"":""1G1AA1F59A7119161"",""year"":2010,""make"":""Chevrolet"",""model"":""Cobalt"",""manufacturer_id"":1,""recalls_available"":false,""number_of_recalls"":0,""recalls"":[],""status"":true,""error_code"":0,""refresh_date"":""Nov 06, 2015""}}";

            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetGMMappingData(),
                ResponseText = serviceResponse
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("1G1AA1F59A7119161", mappedResponse.VIN);
			Assert.AreEqual(2010, mappedResponse.Year);
			Assert.AreEqual("Chevrolet", mappedResponse.Make);
			Assert.AreEqual("Cobalt", mappedResponse.Model);
			Assert.AreEqual(new DateTime(2015, 11, 6), mappedResponse.OEMFeedLastUpdated);

			Assert.AreEqual(0, mappedResponse.OpenRecalls.Count());
		}

		[Test]
		public void HummerTest()
		{
			// https://recalls.gm.com/recall/services/5GTDN13E778158243/recalls
			string serviceResponse =
				@"{""messages"":[],""serverErrorMsgs"":[],""data"":{""vin"":""5GTDN13E778158243"",""year"":2007,""make"":""HUMMER"",""model"":""HUMMER H3"",""manufacturer_id"":1,""recalls_available"":true,""number_of_recalls"":2,""recalls"":[{""recall_type"":""ZPSR"",""nhtsa_recall_number"":""10V179"",""mfr_recall_number"":""N100088"",""recall_date"":""Apr 23, 2010"",""recall_title"":""HOOD LOUVER SEPARATION"",""recall_description"":""GM is recalling certain model year 2006 through 2010 Hummer H3 vehicles.  Some of these vehicles have a condition in which the integral clip-tabs on the louver on the hood may fracture, causing the hood louver rattle against the hood and become loose. If this indicator is unnoticed, additional clips could fracture and the hood louver could detach from the vehicle."",""safety_risk_description"":""If the vehicle is being driving when this occurs, it could strike a following vehicle and cause injury and/or property damage."",""remedy_description"":""Dealers will apply an adhesive to the hood louver to secure it to the hood free of charge."",""mfr_recall_status"":""11""},{""recall_type"":""ZPSR"",""nhtsa_recall_number"":""15V421"",""mfr_recall_number"":""N150042"",""recall_date"":""Jun 25, 2015"",""recall_title"":""Blower Motor Resistor"",""recall_description"":""General Motors has decided that a defect which relates to motor vehicle safety exists in 2006-2010 model year Hummer H3 and 2009-2010 Hummer H3T vehicles.  The connector module that controls the blower motor speed in the heat/vent/air conditioning (HVAC) system may overheat under extended operational periods at high and medium-high blower speeds."",""safety_risk_description"":""If this condition occurs, there is the risk that the heat generated could melt the plastic surrounding the connector module, which increases the risk of a fire."",""remedy_description"":""Dealers will replace the female connector and harness, free of charge."",""mfr_recall_status"":""12""}],""status"":true,""error_code"":0,""refresh_date"":""Nov 05, 2015""}}";

            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetGMMappingData(),
                ResponseText = serviceResponse
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("5GTDN13E778158243", mappedResponse.VIN);
			Assert.AreEqual(2007, mappedResponse.Year);
			Assert.AreEqual("HUMMER", mappedResponse.Make);
			Assert.AreEqual("HUMMER H3", mappedResponse.Model);
			Assert.AreEqual(new DateTime(2015, 11, 5), mappedResponse.OEMFeedLastUpdated);

			Assert.AreEqual(2, mappedResponse.OpenRecalls.Count());

			Assert.AreEqual("10V179", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
			Assert.AreEqual("If the vehicle is being driving when this occurs, it could strike a following vehicle and cause injury and/or property damage.", mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
			Assert.AreEqual("GM is recalling certain model year 2006 through 2010 Hummer H3 vehicles.  Some of these vehicles have a condition in which the integral clip-tabs on the louver on the hood may fracture, causing the hood louver rattle against the hood and become loose. If this indicator is unnoticed, additional clips could fracture and the hood louver could detach from the vehicle.", mappedResponse.OpenRecalls.ElementAt(0).Description);
			Assert.AreEqual("Dealers will apply an adhesive to the hood louver to secure it to the hood free of charge.", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
			Assert.AreEqual("HOOD LOUVER SEPARATION", mappedResponse.OpenRecalls.ElementAt(0).Title);
			Assert.AreEqual(new DateTime(2010, 4, 23), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.RecallIncomplete, mappedResponse.OpenRecalls.ElementAt(0).Status);
			Assert.AreEqual("N100088", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);

			Assert.AreEqual("15V421", mappedResponse.OpenRecalls.ElementAt(1).CampaignID);
			Assert.AreEqual("If this condition occurs, there is the risk that the heat generated could melt the plastic surrounding the connector module, which increases the risk of a fire.", mappedResponse.OpenRecalls.ElementAt(1).SafetyRisk);
			Assert.AreEqual("General Motors has decided that a defect which relates to motor vehicle safety exists in 2006-2010 model year Hummer H3 and 2009-2010 Hummer H3T vehicles.  The connector module that controls the blower motor speed in the heat/vent/air conditioning (HVAC) system may overheat under extended operational periods at high and medium-high blower speeds.", mappedResponse.OpenRecalls.ElementAt(1).Description);
			Assert.AreEqual("Dealers will replace the female connector and harness, free of charge.", mappedResponse.OpenRecalls.ElementAt(1).RemedyDescription);
			Assert.AreEqual("Blower Motor Resistor", mappedResponse.OpenRecalls.ElementAt(1).Title);
			Assert.AreEqual(new DateTime(2015, 6, 25), mappedResponse.OpenRecalls.ElementAt(1).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.RecallIncomplete_RemedyNotYetAvailable, mappedResponse.OpenRecalls.ElementAt(1).Status);
			Assert.AreEqual("N150042", mappedResponse.OpenRecalls.ElementAt(1).OEMCampaignID);
		}

		[Test]
		public void GMCTest()
		{
			// https://recalls.gm.com/recall/services/1GKER13738J140127/recalls
			string serviceResponse =
				@"{""messages"":[],""serverErrorMsgs"":[],""data"":{""vin"":""1GKER13738J140127"",""year"":2008,""make"":""GMC"",""model"":""Acadia"",""manufacturer_id"":1,""recalls_available"":true,""number_of_recalls"":1,""recalls"":[{""recall_type"":""ZPSR"",""nhtsa_recall_number"":""14V118"",""mfr_recall_number"":""N140030"",""recall_date"":""Mar 11, 2014"",""recall_title"":""SIDE IMPACT AIRBAG CONNECTOR"",""recall_description"":""General Motors is recalling certain model year 2008-2013 Buick Enclave and GMC Acadia and 2009-2013 Chevrolet Traverse and 2008-2010 Saturn Outlook vehicles.  In the affected vehicles, increased resistance in the driver and passenger seat mounted side impact air bag (SIAB) wiring harnesses may result in the SIAB and seat belt pretensioners not deploying in the event of a crash."",""safety_risk_description"":""Failure of the side impact air bags and seat belt pretensioners to deploy in a crash increase the risk of injury to the driver and front seat occupant."",""remedy_description"":""Dealers will replace the affected harness connections with soldered connections, free of charge."",""mfr_recall_status"":""11""}],""status"":true,""error_code"":0,""refresh_date"":""Nov 05, 2015""}}";

            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetGMMappingData(),
                ResponseText = serviceResponse
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("1GKER13738J140127", mappedResponse.VIN);
			Assert.AreEqual(2008, mappedResponse.Year);
			Assert.AreEqual("GMC", mappedResponse.Make);
			Assert.AreEqual("Acadia", mappedResponse.Model);
			Assert.AreEqual(new DateTime(2015, 11, 5), mappedResponse.OEMFeedLastUpdated);

			Assert.AreEqual(1, mappedResponse.OpenRecalls.Count());

			Assert.AreEqual("14V118", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
			Assert.AreEqual("Failure of the side impact air bags and seat belt pretensioners to deploy in a crash increase the risk of injury to the driver and front seat occupant.", mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
			Assert.AreEqual("General Motors is recalling certain model year 2008-2013 Buick Enclave and GMC Acadia and 2009-2013 Chevrolet Traverse and 2008-2010 Saturn Outlook vehicles.  In the affected vehicles, increased resistance in the driver and passenger seat mounted side impact air bag (SIAB) wiring harnesses may result in the SIAB and seat belt pretensioners not deploying in the event of a crash.", mappedResponse.OpenRecalls.ElementAt(0).Description);
			Assert.AreEqual("Dealers will replace the affected harness connections with soldered connections, free of charge.", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
			Assert.AreEqual("SIDE IMPACT AIRBAG CONNECTOR", mappedResponse.OpenRecalls.ElementAt(0).Title);
			Assert.AreEqual(new DateTime(2014, 3, 11), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.RecallIncomplete, mappedResponse.OpenRecalls.ElementAt(0).Status);
			Assert.AreEqual("N140030", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);
		}

		[Test]
		public void InfinitiTest()
		{
			// http://www.infinitiusa.com/dealercenter/api/recalls?vin=JN1BV7AP5EM686084
			string serviceResponse =
				@"{""make"":""INFINITI"",""modelYear"":2014,""recalls"":[{""effectiveDate"":""2014-04-10T05:00:00Z"",""make"":""INFINITI"",""modelCode"":""Q50 PREMIUM"",""modelYear"":2014,""nhtsaId"":""14V-138"",""nnaId"":""R1404"",""primaryDescription"":""In the affected vehicles, the occupant classification system (OCS) software may incorrectly classify the passenger seat as empty, when it is occupied by an adult."",""remedyDescription"":""Dealers will update the OCS software."",""riskIfNotRepaired"":""If the OCS does not detect an adult occupant in the passenger seat, the passenger airbag would be deactivated.  Failure of the passenger airbag to deploy during a crash (where deployment is warranted) could increase the risk of injury to the passenger."",""secondaryDescription"":""OCCUPANT CLASS SYSTEM OCS"",""typeCode"":""Recall Campaign"",""vin"":""JN1BV7AP5EM686084""}],""vehicleName"":""Q50 PREMIUM"",""vin"":""JN1BV7AP5EM686084""}";

            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetInfinitiMappingData(),
                ResponseText = serviceResponse
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("JN1BV7AP5EM686084", mappedResponse.VIN);
			Assert.AreEqual(2014, mappedResponse.Year);
			Assert.AreEqual("INFINITI", mappedResponse.Make);
			Assert.AreEqual("Q50 PREMIUM", mappedResponse.Model);
			Assert.AreEqual(null, mappedResponse.OEMFeedLastUpdated);

			Assert.AreEqual(1, mappedResponse.OpenRecalls.Count());

			Assert.AreEqual("14V-138", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
			Assert.AreEqual("If the OCS does not detect an adult occupant in the passenger seat, the passenger airbag would be deactivated.  Failure of the passenger airbag to deploy during a crash (where deployment is warranted) could increase the risk of injury to the passenger.", mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
			Assert.AreEqual("In the affected vehicles, the occupant classification system (OCS) software may incorrectly classify the passenger seat as empty, when it is occupied by an adult.", mappedResponse.OpenRecalls.ElementAt(0).Description);
			Assert.AreEqual("Dealers will update the OCS software.", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
			Assert.AreEqual("OCCUPANT CLASS SYSTEM OCS", mappedResponse.OpenRecalls.ElementAt(0).Title);
			Assert.AreEqual(new DateTime(2014, 4, 10), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.Unknown, mappedResponse.OpenRecalls.ElementAt(0).Status);
			Assert.AreEqual("R1404", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);
		}

		[Test]
		public void JaguarTest()
		{
			// http://www.jaguarusa.com/owners/vin-recall.html?view=vinRecallQuery&vin=SAJWA4EC4FMB55013
			string serviceResponse =
				@"{""vehicle"":""XK 5.0 V8 SC Petrol Convert"",""model"":""2015"",""totalRecalls"":""1 recall found for"",""vin"":""SAJWA4EC4FMB55013"",""results"":[{""number"":""15V-038"",""date"":""01/26/2015"",""recallDesc"":""Jaguar Land Rover is conducting a non-compliance recall campaign involving certain 2012 Model Year Jaguar XK vehicles built at the Castle Bromwich (UK) Assembly Plant from July 2, 2011 to September 25, 2012."",""manufacturerRecallNumber"":""J049"",""safetyDescription"":""A non-compliance concern has been identified on 2012MY Jaguar XK vehicles where the front side lights, when used as parking lamps, will extinguish in error after approximately 5 minutes.  Where the side lights are switched on with the ignition off the front lights should remain illuminated (until the battery is exhausted). On Jaguar XK 2012MY vehicles the front side lights when operated in this manner will only remain on until the Body Control Module enters sleep mode. They will then turn off. Vehicles in this condition will not comply with the requirements of Federal Motor Vehicle Safety Standard (FMVSS) 108 ��� Lamps, Reflective Devices, and Associated Equipment.  Non-functioning parking lamps may increase the risk of the vehicle being involved in a crash."",""repairDesc"":""Owners will be notified and instructed to take their vehicle to a Jaguar dealer who will update the vehicles with the latest software.\n\nThere will be no charge to owners for this action"",""status"":""Incomplete But Repair Available""}]}";

            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetJaguarMappingData(),
                ResponseText = serviceResponse
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("SAJWA4EC4FMB55013", mappedResponse.VIN);
			Assert.AreEqual(2015, mappedResponse.Year);
			Assert.AreEqual("Jaguar", mappedResponse.Make);
			Assert.AreEqual("XK 5.0 V8 SC Petrol Convert", mappedResponse.Model);
			Assert.AreEqual(null, mappedResponse.OEMFeedLastUpdated);

			Assert.AreEqual(1, mappedResponse.OpenRecalls.Count());

			Assert.AreEqual("15V-038", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
			Assert.AreEqual("A non-compliance concern has been identified on 2012MY Jaguar XK vehicles where the front side lights, when used as parking lamps, will extinguish in error after approximately 5 minutes.  Where the side lights are switched on with the ignition off the front lights should remain illuminated (until the battery is exhausted). On Jaguar XK 2012MY vehicles the front side lights when operated in this manner will only remain on until the Body Control Module enters sleep mode. They will then turn off. Vehicles in this condition will not comply with the requirements of Federal Motor Vehicle Safety Standard (FMVSS) 108 ��� Lamps, Reflective Devices, and Associated Equipment.  Non-functioning parking lamps may increase the risk of the vehicle being involved in a crash.", mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
			Assert.AreEqual("Jaguar Land Rover is conducting a non-compliance recall campaign involving certain 2012 Model Year Jaguar XK vehicles built at the Castle Bromwich (UK) Assembly Plant from July 2, 2011 to September 25, 2012.", mappedResponse.OpenRecalls.ElementAt(0).Description);
			Assert.AreEqual("Owners will be notified and instructed to take their vehicle to a Jaguar dealer who will update the vehicles with the latest software.\n\nThere will be no charge to owners for this action", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
			Assert.AreEqual(null, mappedResponse.OpenRecalls.ElementAt(0).Title);
			Assert.AreEqual(new DateTime(2015, 1, 26), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.RecallIncomplete, mappedResponse.OpenRecalls.ElementAt(0).Status);
			Assert.AreEqual("J049", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);
		}

		[Test]
		public void LandRoverTest()
		{
			// http://www.landroverusa.com/ownership/product-recall-search.html?view=vinRecallQuery&vin=SALME1D42CA376647
			string serviceResponse =
				@"{""vehicle"":""L322 R/ROVER V8 5L PET"",""model"":""2012"",""totalRecalls"":""1 recall found for"",""vin"":""SALME1D42CA376647"",""results"":[{""number"":""15V-039"",""date"":""01/26/2015"",""recallDesc"":""Jaguar Land Rover are conducting a safety recall campaign involving certain 2006 to 2012 Model Year Range Rover vehicles built at the Solihull (UK) Assembly Plant between March 14, 2005 to August 14, 2006 to replace the front brake hoses."",""manufacturerRecallNumber"":""P054"",""safetyDescription"":""A concern has been identified with brake hoses installed on 2006 Model Year (MY) Range Rover vehicles. Customers may experience a loss of brake fluid as a result of rupturing of one or both of the front brake jump (flexi) hoses. This may result in the loss of the front brake circuit. The vehicle braking system is a dual circuit system, one system operates the front braking circuit and the other system operates the rear braking circuit."",""repairDesc"":""Owners will be notified and instructed to take their vehicle to a Land Rover dealer who will replace both Left Hand and Right Hand front brake hoses.\n\nThere will be no charge to owners for this repair."",""status"":""Recall outstanding""}]}";

            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetLandRoverMappingData(),
                ResponseText = serviceResponse
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("SALME1D42CA376647", mappedResponse.VIN);
			Assert.AreEqual(2012, mappedResponse.Year);
			Assert.AreEqual("Land Rover", mappedResponse.Make);
			Assert.AreEqual("L322 R/ROVER V8 5L PET", mappedResponse.Model);
			Assert.AreEqual(null, mappedResponse.OEMFeedLastUpdated);

			Assert.AreEqual(1, mappedResponse.OpenRecalls.Count());

			Assert.AreEqual("15V-039", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
			Assert.AreEqual("A concern has been identified with brake hoses installed on 2006 Model Year (MY) Range Rover vehicles. Customers may experience a loss of brake fluid as a result of rupturing of one or both of the front brake jump (flexi) hoses. This may result in the loss of the front brake circuit. The vehicle braking system is a dual circuit system, one system operates the front braking circuit and the other system operates the rear braking circuit.", mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
			Assert.AreEqual("Jaguar Land Rover are conducting a safety recall campaign involving certain 2006 to 2012 Model Year Range Rover vehicles built at the Solihull (UK) Assembly Plant between March 14, 2005 to August 14, 2006 to replace the front brake hoses.", mappedResponse.OpenRecalls.ElementAt(0).Description);
			Assert.AreEqual("Owners will be notified and instructed to take their vehicle to a Land Rover dealer who will replace both Left Hand and Right Hand front brake hoses.\n\nThere will be no charge to owners for this repair.", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
			Assert.AreEqual(null, mappedResponse.OpenRecalls.ElementAt(0).Title);
			Assert.AreEqual(new DateTime(2015, 1, 26), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.RecallIncomplete, mappedResponse.OpenRecalls.ElementAt(0).Status);
			Assert.AreEqual("P054", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);
		}

		[Test]
		public void KiaTest()
		{
			// http://www.kia.com/us/en/data/owners/recalls/search/KNDKGCA33A7715510
			string serviceResponse = @"{""statusCode"":200,""statusDesc"":""Successfully completed request"",""result"":{""recallsResult"":{""make"":""Kia"",""manufacturer_id"":4.0,""model"":""SPORTAGE LX V6 4X4 AUTO"",""number_of_recalls"":1.0,""recalls_available"":true,""recalls"":[{""mfr_notes"":"""",""mfr_recall_number"":""SC098KM"",""mfr_recall_status"":11.0,""nhtsa_recall_number"":""13V114000"",""recall_date"":""1364799600000"",""recall_description"":""KIA IS RECALLING CERTAIN MODEL YEAR 2007-2010 RONDO, MODEL YEAR 2007-2011 SEDONA, SORENTO AND SPORTAGE, MODEL YEAR 2010-2011 SOUL, MODEL YEAR 2008-2011 OPTIMA, MODEL YEAR 2010 FORTE, MODEL YEAR 2007-2009 AMANTI, AND MODEL YEAR 2009 BORREGO VEHICLES. ORIGINALLY, IN APRIL 2013, KIA RECALLED 623,658 MODEL YEAR 2007-2010 RONDO AND SPORTAGE, MODEL YEAR 2007-2011 SORENTO, MODEL YEAR 2007 SEDONA, MODEL YEAR 2010-2011 SOUL, AND MODEL YEAR 2011 OPTIMA VEHICLES. IN SEPTEMBER 2013, KIA INFORMED THE AGENCY THAT IT WAS ADDING AN ADDITIONAL 356,719 VEHICLES TO THIS CAMPAIGN. THE TOTAL NUMBER OF VEHICLES BEING RECALLED IS NOW 980,377. IN THE AFFECTED VEHICLES, THE STOP LAMP SWITCH MAY MALFUNCTION. A MALFUNCTIONING STOP LAMP SWITCH MAY CAUSE THE BRAKE LIGHTS TO NOT ILLUMINATE WHEN THE BRAKE PEDAL IS DEPRESSED OR MAY CAUSE AN INABILITY TO DEACTIVATE THE CRUISE CONTROL BY DEPRESSING THE BRAKE PEDAL. ADDITIONALLY, A MALFUNCTIONING STOP LAMP SWITCH MAY ALSO RESULT IN INTERMITTENT OPERATION OF THE PUSH-BUTTON START FEATURE, AFFECT THE OPERATION OF THE BRAKE-TRANSMISSION SHIFT INTERLOCK FEATURE PREVENTING THE SHIFTER FROM BEING MOVED OUT OF THE PARK POSITION AND CAUSE THE ELECTRONIC STABILITY CONTROL (ESC) MALFUNCTION LIGHT TO ILLUMINATE."",""refresh_date"":""1446710400000"",""remedy_description"":""KIA WILL SEND AN INTERIM NOTIFICATION TO OWNERS IN MAY 2013. WHEN PARTS ARE AVAILABLE, OWNERS WILL RECEIVE A SECOND NOTIFICATION AND DEALERS WILL REPLACE THE STOP LAMP SWITCH FREE OF CHARGE. IF YOU HAVE ANY QUESTIONS REGARDING THIS RECALL, PLEASE CONTACT YOUR KIA DEALER, LOG ON TO THE OWNER\u0027S SECTION OF WWW.KIA.COM, OR CONTACT THE KIA CONSUMER ASSISTANCE CENTER AT 1-800-333-4542(4KIA), MONDAY THROUGH FRIDAY, 5 AM TO 6PM PST."",""safety_risk_description"":""FAILURE TO ILLUMINATE THE STOP LAMPS DURING BRAKING OR INABILITY TO DISENGAGE THE CRUISE CONTROL COULD INCREASE THE RISK OF A CRASH. ADDITIONALLY, WHEN THE IGNITION IS IN THE \""ON\"" POSITION, THE TRANSMISSION SHIFTER MAY BE ABLE TO BE MOVED OUT OF PARK WITHOUT FIRST APPLYING THE BRAKE. THIS MAY LEAD TO UNINTENTIONAL MOVEMENT OF THE CAR WHICH MAY INCREASE THE RISK OF A CRASH."",""recall_status"":""RECALL INCOMPLETE""}],""refresh_date"":""1446710400000"",""status"":true,""vin"":""KNDKGCA33A7715510"",""year"":2010.0}}}";

            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetKiaMappingData(),
                ResponseText = serviceResponse
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("KNDKGCA33A7715510", mappedResponse.VIN);
			Assert.AreEqual(2010, mappedResponse.Year);
			Assert.AreEqual("Kia", mappedResponse.Make);
			Assert.AreEqual("SPORTAGE LX V6 4X4 AUTO", mappedResponse.Model);
			Assert.AreEqual(new DateTime(2015, 11, 5), mappedResponse.OEMFeedLastUpdated);

			Assert.AreEqual(1, mappedResponse.OpenRecalls.Count());

			Assert.AreEqual("13V114000", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
			Assert.AreEqual("FAILURE TO ILLUMINATE THE STOP LAMPS DURING BRAKING OR INABILITY TO DISENGAGE THE CRUISE CONTROL COULD INCREASE THE RISK OF A CRASH. ADDITIONALLY, WHEN THE IGNITION IS IN THE \"ON\" POSITION, THE TRANSMISSION SHIFTER MAY BE ABLE TO BE MOVED OUT OF PARK WITHOUT FIRST APPLYING THE BRAKE. THIS MAY LEAD TO UNINTENTIONAL MOVEMENT OF THE CAR WHICH MAY INCREASE THE RISK OF A CRASH.", mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
			Assert.AreEqual("KIA IS RECALLING CERTAIN MODEL YEAR 2007-2010 RONDO, MODEL YEAR 2007-2011 SEDONA, SORENTO AND SPORTAGE, MODEL YEAR 2010-2011 SOUL, MODEL YEAR 2008-2011 OPTIMA, MODEL YEAR 2010 FORTE, MODEL YEAR 2007-2009 AMANTI, AND MODEL YEAR 2009 BORREGO VEHICLES. ORIGINALLY, IN APRIL 2013, KIA RECALLED 623,658 MODEL YEAR 2007-2010 RONDO AND SPORTAGE, MODEL YEAR 2007-2011 SORENTO, MODEL YEAR 2007 SEDONA, MODEL YEAR 2010-2011 SOUL, AND MODEL YEAR 2011 OPTIMA VEHICLES. IN SEPTEMBER 2013, KIA INFORMED THE AGENCY THAT IT WAS ADDING AN ADDITIONAL 356,719 VEHICLES TO THIS CAMPAIGN. THE TOTAL NUMBER OF VEHICLES BEING RECALLED IS NOW 980,377. IN THE AFFECTED VEHICLES, THE STOP LAMP SWITCH MAY MALFUNCTION. A MALFUNCTIONING STOP LAMP SWITCH MAY CAUSE THE BRAKE LIGHTS TO NOT ILLUMINATE WHEN THE BRAKE PEDAL IS DEPRESSED OR MAY CAUSE AN INABILITY TO DEACTIVATE THE CRUISE CONTROL BY DEPRESSING THE BRAKE PEDAL. ADDITIONALLY, A MALFUNCTIONING STOP LAMP SWITCH MAY ALSO RESULT IN INTERMITTENT OPERATION OF THE PUSH-BUTTON START FEATURE, AFFECT THE OPERATION OF THE BRAKE-TRANSMISSION SHIFT INTERLOCK FEATURE PREVENTING THE SHIFTER FROM BEING MOVED OUT OF THE PARK POSITION AND CAUSE THE ELECTRONIC STABILITY CONTROL (ESC) MALFUNCTION LIGHT TO ILLUMINATE.", mappedResponse.OpenRecalls.ElementAt(0).Description);
			Assert.AreEqual("KIA WILL SEND AN INTERIM NOTIFICATION TO OWNERS IN MAY 2013. WHEN PARTS ARE AVAILABLE, OWNERS WILL RECEIVE A SECOND NOTIFICATION AND DEALERS WILL REPLACE THE STOP LAMP SWITCH FREE OF CHARGE. IF YOU HAVE ANY QUESTIONS REGARDING THIS RECALL, PLEASE CONTACT YOUR KIA DEALER, LOG ON TO THE OWNER'S SECTION OF WWW.KIA.COM, OR CONTACT THE KIA CONSUMER ASSISTANCE CENTER AT 1-800-333-4542(4KIA), MONDAY THROUGH FRIDAY, 5 AM TO 6PM PST.", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
			Assert.AreEqual(null, mappedResponse.OpenRecalls.ElementAt(0).Title);
			Assert.AreEqual(new DateTime(2013, 4, 1), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.RecallIncomplete, mappedResponse.OpenRecalls.ElementAt(0).Status);
			Assert.AreEqual("SC098KM", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);
		}

		[Test]
		public void FordTest()
		{
			// https://owner.ford.com/sharedServices/recalls/query.do?country=USA&language=EN&vin=3FADP4BJ9DM217808
			string serviceResponse = @"{""lang_script_c"":"""",""return_status"":{""description"":""Required data found"",""code"":""00""},""recalls"":{""nhtsa_recall_item"":{""nhtsa_recall_number"":""15V246"",""recall_description"":""A DOOR LATCH WITH A BROKEN PAWL SPRING TAB TYPICALLY RESULTS IN A &quot;DOOR WILL NOT CLOSE&quot; CONDITION.  SHOULD THE CUSTOMER BE ABLE TO LATCH THE DOOR, THERE IS A POTENTIAL THAT THE DOOR MAY UNLATCH WHILE DRIVING. FORD IS AWARE OF TWO ALLEGATIONS OF INJURY (SORENESS) RESULTING FROM AN UNLATCHED DOOR BOUNCING BACK WHEN THE CUSTOMER ATTEMPTED TO CLOSE IT, AND ONE ACCIDENT ALLEGATION WHEN AN UNLATCHED DOOR SWUNG OPEN AND STRUCK THE ADJACENT VEHICLE WHILE THE DRIVER WAS PULLING INTO A PARKING SPACE."",""remedy_description_lang"":""OWNERS WILL BE NOTIFIED BY MAIL AND INSTRUCTED TO TAKE THEIR VEHICLE TO A FORD OR LINCOLN DEALER TO HAVE ALL FOUR DOOR LATCHES REPLACED WITH A MORE ROBUST SERVICE PART. THERE WILL BE NO CHARGE FOR THIS SERVICE. FORD PROVIDED THE GENERAL REIMBURSEMENT PLAN FOR THE COST OF REMEDIES PAID FOR BY VEHICLE OWNERS PRIOR TO NOTIFICATION OF A SAFETY RECALL IN FEBRUARY 2015. THE ENDING DATE FOR REIMBURSEMENT ELIGIBILITY IS DECEMBER 1, 2015. FORD WILL FORWARD A COPY OF THE NOTIFICATION LETTERS TO DEALERS TO THE AGENCY WHEN AVAILABLE.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 "",""description_lang"":""DOOR LATCH REPLACEMENT                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       "",""mfr_recall_number"":""15S16"",""refresh_date"":""NOV 09, 2015"",""mfr_notes_lang"":""TO CHECK FOR NON-SAFETY-RELATED PROGRAMS APPLICABLE TO YOUR VEHICLE, SEE HTTP://WWW.FORD.COM/ OR CALL YOUR FORD DEALER.                                                                                                                                                                                                                                                                                                                                           "",""mfr_recall_status_lang"":""11 - RECALL INCOMPLETE"",""safety_risk_description_lang"":""A DOOR LATCH WITH A BROKEN PAWL SPRING TAB TYPICALLY RESULTS IN A &quot;DOOR WILL NOT CLOSE&quot; CONDITION.  SHOULD THE CUSTOMER BE ABLE TO LATCH THE DOOR, THERE IS A POTENTIAL THE DOOR MAY UNLATCH WHILE DRIVING, INCREASING THE RISK OF INJURY.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      "",""safety_risk_description"":""A DOOR LATCH WITH A BROKEN PAWL SPRING TAB TYPICALLY RESULTS IN A &quot;DOOR WILL NOT CLOSE&quot; CONDITION.  SHOULD THE CUSTOMER BE ABLE TO LATCH THE DOOR, THERE IS A POTENTIAL THE DOOR MAY UNLATCH WHILE DRIVING, INCREASING THE RISK OF INJURY."",""mfr_recall_status"":""11 - RECALL INCOMPLETE"",""description_eng"":""DOOR LATCH REPLACEMENT"",""mfr_notes"":""TO CHECK FOR NON-SAFETY-RELATED PROGRAMS APPLICABLE TO YOUR VEHICLE, SEE HTTP://WWW.FORD.COM/ OR CALL YOUR FORD DEALER."",""recall_date"":""APR 23, 2015"",""remedy_description"":""OWNERS WILL BE NOTIFIED BY MAIL AND INSTRUCTED TO TAKE THEIR VEHICLE TO A FORD OR LINCOLN DEALER TO HAVE ALL FOUR DOOR LATCHES REPLACED WITH A MORE ROBUST SERVICE PART. THERE WILL BE NO CHARGE FOR THIS SERVICE. FORD PROVIDED THE GENERAL REIMBURSEMENT PLAN FOR THE COST OF REMEDIES PAID FOR BY VEHICLE OWNERS PRIOR TO NOTIFICATION OF A SAFETY RECALL IN FEBRUARY 2015. THE ENDING DATE FOR REIMBURSEMENT ELIGIBILITY IS DECEMBER 1, 2015. FORD WILL FORWARD A COPY OF THE NOTIFICATION LETTERS TO DEALERS TO THE AGENCY WHEN AVAILABLE."",""recall_description_lang"":""A DOOR LATCH WITH A BROKEN PAWL SPRING TAB TYPICALLY RESULTS IN A &quot;DOOR WILL NOT CLOSE&quot; CONDITION.  SHOULD THE CUSTOMER BE ABLE TO LATCH THE DOOR, THERE IS A POTENTIAL THAT THE DOOR MAY UNLATCH WHILE DRIVING. FORD IS AWARE OF TWO ALLEGATIONS OF INJURY (SORENESS) RESULTING FROM AN UNLATCHED DOOR BOUNCING BACK WHEN THE CUSTOMER ATTEMPTED TO CLOSE IT, AND ONE ACCIDENT ALLEGATION WHEN AN UNLATCHED DOOR SWUNG OPEN AND STRUCK THE ADJACENT VEHICLE WHILE THE DRIVER WAS PULLING INTO A PARKING SPACE.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 ""},""fsa_item"":{""description_lang"":""TRANSMISSION CLUTCH SHUDDER / TRANSMISSION INPUT SHAFT SEAL WARRANTY EXTENSION                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               "",""local_fsa_r"":""14M01"",""description_eng"":""TRANSMISSION CLUTCH SHUDDER / TRANSMISSION INPUT SHAFT SEAL WARRANTY EXTENSION""}},""nhtsaReq"":""Y"",""nhtsa_header_details"":{""number_of_recalls"":""02"",""refresh_date"":""NOV 09, 2015"",""status"":true,""manufacturer_id"":""0002"",""model"":""Fiesta"",""recalls_available"":true,""year"":2013,""make"":""FORD""},""lang_iso_c"":""EN"",""lang_region_c"":"""",""stpr_state_c"":""  "",""country_iso3_c"":""USA"",""vin"":""3FADP4BJ9DM217808""}";

            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetFordLincolnMercuryData(),
                ResponseText = serviceResponse
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("3FADP4BJ9DM217808", mappedResponse.VIN);
			Assert.AreEqual(2013, mappedResponse.Year);
			Assert.AreEqual("FORD", mappedResponse.Make);
			Assert.AreEqual("Fiesta", mappedResponse.Model);
			Assert.AreEqual(new DateTime(2015, 11, 9), mappedResponse.OEMFeedLastUpdated);

			Assert.AreEqual(1, mappedResponse.OpenRecalls.Count());

			Assert.AreEqual("15V246", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
			Assert.AreEqual("A DOOR LATCH WITH A BROKEN PAWL SPRING TAB TYPICALLY RESULTS IN A &quot;DOOR WILL NOT CLOSE&quot; CONDITION.  SHOULD THE CUSTOMER BE ABLE TO LATCH THE DOOR, THERE IS A POTENTIAL THE DOOR MAY UNLATCH WHILE DRIVING, INCREASING THE RISK OF INJURY.", mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
			Assert.AreEqual("A DOOR LATCH WITH A BROKEN PAWL SPRING TAB TYPICALLY RESULTS IN A &quot;DOOR WILL NOT CLOSE&quot; CONDITION.  SHOULD THE CUSTOMER BE ABLE TO LATCH THE DOOR, THERE IS A POTENTIAL THAT THE DOOR MAY UNLATCH WHILE DRIVING. FORD IS AWARE OF TWO ALLEGATIONS OF INJURY (SORENESS) RESULTING FROM AN UNLATCHED DOOR BOUNCING BACK WHEN THE CUSTOMER ATTEMPTED TO CLOSE IT, AND ONE ACCIDENT ALLEGATION WHEN AN UNLATCHED DOOR SWUNG OPEN AND STRUCK THE ADJACENT VEHICLE WHILE THE DRIVER WAS PULLING INTO A PARKING SPACE.", mappedResponse.OpenRecalls.ElementAt(0).Description);
			Assert.AreEqual("OWNERS WILL BE NOTIFIED BY MAIL AND INSTRUCTED TO TAKE THEIR VEHICLE TO A FORD OR LINCOLN DEALER TO HAVE ALL FOUR DOOR LATCHES REPLACED WITH A MORE ROBUST SERVICE PART. THERE WILL BE NO CHARGE FOR THIS SERVICE. FORD PROVIDED THE GENERAL REIMBURSEMENT PLAN FOR THE COST OF REMEDIES PAID FOR BY VEHICLE OWNERS PRIOR TO NOTIFICATION OF A SAFETY RECALL IN FEBRUARY 2015. THE ENDING DATE FOR REIMBURSEMENT ELIGIBILITY IS DECEMBER 1, 2015. FORD WILL FORWARD A COPY OF THE NOTIFICATION LETTERS TO DEALERS TO THE AGENCY WHEN AVAILABLE.", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
			Assert.AreEqual("DOOR LATCH REPLACEMENT", mappedResponse.OpenRecalls.ElementAt(0).Title);
			Assert.AreEqual(new DateTime(2015, 4, 23), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.RecallIncomplete, mappedResponse.OpenRecalls.ElementAt(0).Status);
			Assert.AreEqual("15S16", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);
		}

		[Test]
		public void FordTestMultiple()
		{
			// https://owner.ford.com/sharedServices/recalls/query.do?country=USA&language=EN&vin=1FTYR14UXWPB39225
			string serviceResponse = @"{""lang_script_c"":"""",""return_status"":{""description"":""Required data found"",""code"":""00""},""recalls"":{""nhtsa_recall_item"":[{""nhtsa_recall_number"":""05V017000"",""recall_description"":""ON CERTAIN SPORT UTILITY VEHICLES AND PICKUP TRUCKS, THE SPEED CONTROL DEACTIVATION SWITCH MAY OVERHEAT, SMOKE, OR BURN."",""remedy_description_lang"":""AS AN INTERIM REPAIR, OWNERS WILL BE INSTRUCTED TO RETURN THEIR VEHICLES TO THEIR DEALERS TO HAVE THE SPEED CONTROL DEACTIVATION SWITCH DISCONNECTED.  AS SOON AS REPLACEMENT PARTS ARE AVAILABLE (EARLY APRIL 2005), OWNERS WILL BE INSTRUCTED TO RETURN TO THE DEALERS FOR INSTALLATION OF A NEW SWITCH FREE OF CHARGE.  OWNERS MAY CONTACT FORD AT 1-866-436-7332.  (NOTE:  ALSO SEE RECALLS 05V388 AND 06V286)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              "",""description_lang"":""SPEED CONTROL SYSTEM MODIFICATION                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            "",""mfr_recall_number"":""05S28"",""refresh_date"":""NOV 09, 2015"",""mfr_notes_lang"":""To check for non-safety-related programs applicable to your vehicle, see www.Ford.com &lt;http://www.ford.com/&gt;  or call your Ford dealer.                                                                                                                                                                                                                                                                                                                           "",""mfr_recall_status_lang"":""11 - RECALL INCOMPLETE"",""safety_risk_description_lang"":""THIS CONDITION COULD LEAD TO A FIRE.  FIRES HAVE OCCURRED WHILE THE VEHICLES WERE PARKED WITH THE IGNITION &apos;OFF.&apos;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               "",""safety_risk_description"":""THIS CONDITION COULD LEAD TO A FIRE.  FIRES HAVE OCCURRED WHILE THE VEHICLES WERE PARKED WITH THE IGNITION &apos;OFF.&apos;"",""mfr_recall_status"":""11 - RECALL INCOMPLETE"",""description_eng"":""SPEED CONTROL SYSTEM MODIFICATION"",""mfr_notes"":""To check for non-safety-related programs applicable to your vehicle, see www.Ford.com &lt;http://www.ford.com/&gt;  or call your Ford dealer."",""recall_date"":""JAN 27, 2005"",""remedy_description"":""AS AN INTERIM REPAIR, OWNERS WILL BE INSTRUCTED TO RETURN THEIR VEHICLES TO THEIR DEALERS TO HAVE THE SPEED CONTROL DEACTIVATION SWITCH DISCONNECTED.  AS SOON AS REPLACEMENT PARTS ARE AVAILABLE (EARLY APRIL 2005), OWNERS WILL BE INSTRUCTED TO RETURN TO THE DEALERS FOR INSTALLATION OF A NEW SWITCH FREE OF CHARGE.  OWNERS MAY CONTACT FORD AT 1-866-436-7332.  (NOTE:  ALSO SEE RECALLS 05V388 AND 06V286)"",""recall_description_lang"":""ON CERTAIN SPORT UTILITY VEHICLES AND PICKUP TRUCKS, THE SPEED CONTROL DEACTIVATION SWITCH MAY OVERHEAT, SMOKE, OR BURN.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ""},{""nhtsa_recall_number"":""99V062001"",""recall_description"":""VEHICLE DESCRIPTION:  CERTAIN 1997-1999 RHD EXPLORER EQUIPPED WITH 4.0L OHV/SOHC ENGINES AND CRUISE CONTROL BUILT FROM MAY 29, 1996 THROUGH MARCH 4, 1999; 1998-1999 EXPLORER/MOUNTAINEERS EQUIPPED WITH 4.0L OHV/SOHC OR 5.0L ENGINES OR 1998-1999 RANGERS EQUIPPED WITH 2.5L, 3.0L FFV/EFI OR 4.0L ENGINES AND CRUISE CONTROL BUILT FROM JANUARY 5, 1998 THROUGH MARCH 4, 1999; 1998-1999 MUSTANGS EQUIPPED WITH 3.8L, 4.6L 2-VALVE OR 4-VALVE ENGINES AND CRUISE CONTROL BUILT FROM MARCH 2, 1998 THROUGH MARCH 4, 1999; 1999 F250/F350/F450/F550 (OVER 8,500 LBS.) TRUCKS EQUIPPED WITH 5.4L OR 6.8L ENGINES AND CRUISE CONTROL BUILT FROM MARCH 2, 1998 THROUGH MARCH 4, 1999; AND 1999 F-53 STRIPPED CHASSIS EQUIPPED WITH 5.4L OR 6.8L ENGINES AND CRUISE CONTROL BUILT FROM MARCH 2, 1998 THROUGH MARCH 4, 1999.    A CRUISE CONTROL CABLE CAN INTERFERE WITH THE SPEED CONTROL SERVO PULLEY AND NOT ALLOW THE THROTTLE TO RETURN TO IDLE WHEN DISENGAGING THE CRUISE CONTROL."",""remedy_description_lang"":""DEALERS WILL REPLACE THE CRUISE CONTROL CABLES.  AT THE PRESENT TIME, THE REPLACEMENT CABLES ARE NOT AVAILABLE.  CUSTOMERS ARE BEING ADVISED NOT TO USE THE CRUISE CONTROL SYSTEM UNTIL A REPLACEMENT CAN BE MADE.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              "",""description_lang"":""SPEED CONTROL CABLE                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          "",""mfr_recall_number"":""99S09"",""refresh_date"":""NOV 09, 2015"",""mfr_notes_lang"":""To check for non-safety-related programs applicable to your vehicle, see www.Ford.com &lt;http://www.ford.com/&gt;  or call your Ford dealer.                                                                                                                                                                                                                                                                                                                           "",""mfr_recall_status_lang"":""11 - RECALL INCOMPLETE"",""safety_risk_description_lang"":""IF THE CRUISE CONTROL IS USED AND THIS CONDITION IS PRESENT, A STUCK THROTTLE COULD RESULT, WHICH COULD POTENTIALLY RESULT IN A CRASH.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          "",""safety_risk_description"":""IF THE CRUISE CONTROL IS USED AND THIS CONDITION IS PRESENT, A STUCK THROTTLE COULD RESULT, WHICH COULD POTENTIALLY RESULT IN A CRASH."",""mfr_recall_status"":""11 - RECALL INCOMPLETE"",""description_eng"":""SPEED CONTROL CABLE"",""mfr_notes"":""To check for non-safety-related programs applicable to your vehicle, see www.Ford.com &lt;http://www.ford.com/&gt;  or call your Ford dealer."",""recall_date"":""MAR 30, 1999"",""remedy_description"":""DEALERS WILL REPLACE THE CRUISE CONTROL CABLES.  AT THE PRESENT TIME, THE REPLACEMENT CABLES ARE NOT AVAILABLE.  CUSTOMERS ARE BEING ADVISED NOT TO USE THE CRUISE CONTROL SYSTEM UNTIL A REPLACEMENT CAN BE MADE."",""recall_description_lang"":""VEHICLE DESCRIPTION:  CERTAIN 1997-1999 RHD EXPLORER EQUIPPED WITH 4.0L OHV/SOHC ENGINES AND CRUISE CONTROL BUILT FROM MAY 29, 1996 THROUGH MARCH 4, 1999; 1998-1999 EXPLORER/MOUNTAINEERS EQUIPPED WITH 4.0L OHV/SOHC OR 5.0L ENGINES OR 1998-1999 RANGERS EQUIPPED WITH 2.5L, 3.0L FFV/EFI OR 4.0L ENGINES AND CRUISE CONTROL BUILT FROM JANUARY 5, 1998 THROUGH MARCH 4, 1999; 1998-1999 MUSTANGS EQUIPPED WITH 3.8L, 4.6L 2-VALVE OR 4-VALVE ENGINES AND CRUISE CONTROL BUILT FROM MARCH 2, 1998 THROUGH MARCH 4, 1999; 1999 F250/F350/F450/F550 (OVER 8,500 LBS.) TRUCKS EQUIPPED WITH 5.4L OR 6.8L ENGINES AND CRUISE CONTROL BUILT FROM MARCH 2, 1998 THROUGH MARCH 4, 1999; AND 1999 F-53 STRIPPED CHASSIS EQUIPPED WITH 5.4L OR 6.8L ENGINES AND CRUISE CONTROL BUILT FROM MARCH 2, 1998 THROUGH MARCH 4, 1999.    A CRUISE CONTROL CABLE CAN INTERFERE WITH THE SPEED CONTROL SERVO PULLEY AND NOT ALLOW THE THROTTLE TO RETURN TO IDLE WHEN DISENGAGING THE CRUISE CONTROL.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ""}]},""nhtsaReq"":""Y"",""nhtsa_header_details"":{""number_of_recalls"":""02"",""refresh_date"":""NOV 09, 2015"",""status"":true,""manufacturer_id"":""0002"",""model"":""Ranger"",""recalls_available"":true,""year"":1998,""make"":""FORD""},""lang_iso_c"":""EN"",""lang_region_c"":"""",""stpr_state_c"":""  "",""country_iso3_c"":""USA"",""vin"":""1FTYR14UXWPB39225""}";

            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetFordLincolnMercuryData(),
                ResponseText = serviceResponse
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("1FTYR14UXWPB39225", mappedResponse.VIN);
			Assert.AreEqual(1998, mappedResponse.Year);
			Assert.AreEqual("FORD", mappedResponse.Make);
			Assert.AreEqual("Ranger", mappedResponse.Model);
			Assert.AreEqual(new DateTime(2015, 11, 9), mappedResponse.OEMFeedLastUpdated);

			Assert.AreEqual(2, mappedResponse.OpenRecalls.Count());

			Assert.AreEqual("05V017000", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
			Assert.AreEqual(new DateTime(2005, 1, 27), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
			Assert.AreEqual("05S28", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);

			Assert.AreEqual("99V062001", mappedResponse.OpenRecalls.ElementAt(1).CampaignID);
			Assert.AreEqual(new DateTime(1999, 3, 30), mappedResponse.OpenRecalls.ElementAt(1).CampaignDate);
			Assert.AreEqual("99S09", mappedResponse.OpenRecalls.ElementAt(1).OEMCampaignID);
		}

		[Test]
		public void FordTestNone()
		{
			// https://owner.ford.com/sharedServices/recalls/query.do?country=USA&language=EN&vin=1FADP3E20FL229705
			string serviceResponse = @"{""lang_script_c"":"""",""return_status"":{""description"":""Data not found"",""code"":""02""},""fsa_items"":{""count"":0},""lang_iso_c"":""EN"",""lang_region_c"":"""",""stpr_state_c"":""  "",""country_iso3_c"":""USA"",""vin"":""1FADP3E20FL229705""}";

            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetFordLincolnMercuryData(),
                ResponseText = serviceResponse
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("1FADP3E20FL229705", mappedResponse.VIN);
			Assert.AreEqual(null, mappedResponse.Year);
			Assert.AreEqual(null, mappedResponse.Make);
			Assert.AreEqual(null, mappedResponse.Model);
			Assert.AreEqual(null, mappedResponse.OEMFeedLastUpdated);

			Assert.AreEqual(0, mappedResponse.OpenRecalls.Count());
		}


		[Test]
		public void LincolnTest()
		{
			// https://owner.ford.com/sharedServices/recalls/query.do?country=USA&language=EN&vin=1LNHL9FT1CG802385
			string serviceResponse = @"{""lang_script_c"":"""",""return_status"":{""description"":""Required data found"",""code"":""00""},""recalls"":{""nhtsa_recall_item"":[{""nhtsa_recall_number"":""15V340"",""recall_description"":""AN INTERMITTENT ELECTRICAL CONNECTION CAUSED BY QUALITY ISSUES PERTAINING TO CONFORMAL COAT CONTAMINATION AND RIBBON CABLE PIN MISALIGNMENT WITHIN THE STEERING GEAR MOTOR MAY LEAD TO A LOSS OF THE MOTOR POSITION SENSOR SIGNAL THAT MAY RESULT IN LOSS OF POWER STEERING ASSIST.  FORD IS AWARE OF 16 ACCIDENT ALLEGATIONS AND THREE INJURY ALLEGATIONS RELATED TO THIS CONDITION IN THE VEHICLES INCLUDED IN THIS FIELD ACTION.  MOST OF THE ACCIDENT ALLEGATIONS REPORT THAT MINOR VEHICLE DAMAGE WAS INCURRED, TYPICALLY EITHER WHEEL DAMAGE RESULTING FROM CONTACT WITH A CURB OR MINOR BODY DAMAGE.  THE INJURY ALLEGATIONS WERE ALL REPORTED AS MINOR WITH NO INDICATION THAT MEDICAL ATTENTION WAS SOUGHT FOR ANY OF THE INJURIES."",""remedy_description_lang"":""OWNERS WILL BE NOTIFIED BY MAIL AND INSTRUCTED TO TAKE THEIR VEHICLE TO A FORD OR LINCOLN DEALER TO HAVE THE POWER STEERING CONTROL MODULE (PSCM) CHECKED FOR DIAGNOSTIC TROUBLE CODES (DTCS).  IF NO LOSS OF STEERING ASSIST DTCS ARE PRESENT, DEALERS WILL UPDATE THE PSCM SOFTWARE. THE UPDATED PSCM SOFTWARE WILL PROVIDE STEERING ASSIST FOR THE REMAINDER OF THE DRIVE CYCLE IF A MOTOR POSITION SENSOR ENCODER FAULT OCCURS, AND A CLUSTER WARNING AND CHIME WILL BE PROVIDED. UPON SUBSEQUENT VEHICLE START-UPS, IF MULTIPLE FAULTS HAVE BEEN VERIFIED, STEERING ASSIST WILL BE REMOVED AND A CLUSTER WARNING AND CHIME WILL BE PROVIDED.  IF ANY LOSS OF STEERING ASSIST DTCS ARE PRESENT, DEALERS WILL REPLACE THE STEERING GEAR.  IF A VEHICLE HAS THE PSCM SOFTWARE UPDATED IN ACCORDANCE WITH THIS FIELD ACTION, AND LOSS OF POWER STEERING ASSIST OCCURS AT A LATER DATE, THE OWNER WILL BE ABLE TO RETURN THEIR VEHICLE TO A FORD OR LINCOLN DEALER TO HAVE THE STEERING GEAR REPLACED IF IT OCCURS WITHIN A PERIOD OF 10 YEARS FROM THE WARRANTY START DATE FOR THE VEHICLE OR 150K MILES, WHICHEVER OCCURS FIRST.  THERE WILL BE NO CHARGE FOR THIS SERVICE.  FORD PROVIDED THE GENERAL REIMBURSEMENT PLAN FOR THE COST OF REMEDIES PAID FOR BY VEHICLE OWNERS PRIOR TO NOTIFICATION OF A SAFETY RECALL IN FEBRUARY 2015.  THE ENDING DATE FOR REIMBURSEMENT ELIGIBILITY IS DECEMBER 31, 2015.  FORD WILL FORWARD A COPY OF THE NOTIFICATION LETTERS TO DEALERS TO THE AGENCY WHEN AVAILABLE.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  "",""description_lang"":""REPROGRAM POWER STEERING CONTROL MODULE                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      "",""mfr_recall_number"":""15S18"",""refresh_date"":""NOV 09, 2015"",""mfr_notes_lang"":""TO CHECK FOR NON-SAFETY-RELATED PROGRAMS APPLICABLE TO YOUR VEHICLE, SEE HTTP://WWW.FORD.COM/ OR CALL YOUR FORD DEALER.                                                                                                                                                                                                                                                                                                                                           "",""mfr_recall_status_lang"":""11 - RECALL INCOMPLETE"",""safety_risk_description_lang"":""IN THE EVENT OF A LOSS OF POWER STEERING ASSIST, THE MECHANICAL LINKAGE BETWEEN THE STEERING WHEEL AND THE ROAD IS MAINTAINED AT ALL TIMES. THE STEERING SYSTEM WILL DEFAULT TO MANUAL STEERING MODE, ALLOWING THE VEHICLE TO BE STEERED IN A SAFE AND CONTROLLED MANNER. LOSS OF POWER STEERING ASSIST IS UNLIKELY TO BE ASSOCIATED WITH ACCIDENTS AT HIGHER SPEEDS, AS THE AMOUNT OF ASSIST SUPPLIED IS INVERSELY PROPORTIONAL TO VEHICLE SPEED. LOSS OF POWER STEERING ASSIST WOULD REQUIRE HIGHER STEERING EFFORT, ESPECIALLY AT LOWER SPEEDS, WHICH MAY RESULT IN AN INCREASED RISK OF A CRASH.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            "",""safety_risk_description"":""IN THE EVENT OF A LOSS OF POWER STEERING ASSIST, THE MECHANICAL LINKAGE BETWEEN THE STEERING WHEEL AND THE ROAD IS MAINTAINED AT ALL TIMES. THE STEERING SYSTEM WILL DEFAULT TO MANUAL STEERING MODE, ALLOWING THE VEHICLE TO BE STEERED IN A SAFE AND CONTROLLED MANNER. LOSS OF POWER STEERING ASSIST IS UNLIKELY TO BE ASSOCIATED WITH ACCIDENTS AT HIGHER SPEEDS, AS THE AMOUNT OF ASSIST SUPPLIED IS INVERSELY PROPORTIONAL TO VEHICLE SPEED. LOSS OF POWER STEERING ASSIST WOULD REQUIRE HIGHER STEERING EFFORT, ESPECIALLY AT LOWER SPEEDS, WHICH MAY RESULT IN AN INCREASED RISK OF A CRASH."",""mfr_recall_status"":""11 - RECALL INCOMPLETE"",""description_eng"":""REPROGRAM POWER STEERING CONTROL MODULE"",""mfr_notes"":""TO CHECK FOR NON-SAFETY-RELATED PROGRAMS APPLICABLE TO YOUR VEHICLE, SEE HTTP://WWW.FORD.COM/ OR CALL YOUR FORD DEALER."",""recall_date"":""MAY 26, 2015"",""remedy_description"":""OWNERS WILL BE NOTIFIED BY MAIL AND INSTRUCTED TO TAKE THEIR VEHICLE TO A FORD OR LINCOLN DEALER TO HAVE THE POWER STEERING CONTROL MODULE (PSCM) CHECKED FOR DIAGNOSTIC TROUBLE CODES (DTCS).  IF NO LOSS OF STEERING ASSIST DTCS ARE PRESENT, DEALERS WILL UPDATE THE PSCM SOFTWARE. THE UPDATED PSCM SOFTWARE WILL PROVIDE STEERING ASSIST FOR THE REMAINDER OF THE DRIVE CYCLE IF A MOTOR POSITION SENSOR ENCODER FAULT OCCURS, AND A CLUSTER WARNING AND CHIME WILL BE PROVIDED. UPON SUBSEQUENT VEHICLE START-UPS, IF MULTIPLE FAULTS HAVE BEEN VERIFIED, STEERING ASSIST WILL BE REMOVED AND A CLUSTER WARNING AND CHIME WILL BE PROVIDED.  IF ANY LOSS OF STEERING ASSIST DTCS ARE PRESENT, DEALERS WILL REPLACE THE STEERING GEAR.  IF A VEHICLE HAS THE PSCM SOFTWARE UPDATED IN ACCORDANCE WITH THIS FIELD ACTION, AND LOSS OF POWER STEERING ASSIST OCCURS AT A LATER DATE, THE OWNER WILL BE ABLE TO RETURN THEIR VEHICLE TO A FORD OR LINCOLN DEALER TO HAVE THE STEERING GEAR REPLACED IF IT OCCURS WITHIN A PERIOD OF 10 YEARS FROM THE WARRANTY START DATE FOR THE VEHICLE OR 150K MILES, WHICHEVER OCCURS FIRST.  THERE WILL BE NO CHARGE FOR THIS SERVICE.  FORD PROVIDED THE GENERAL REIMBURSEMENT PLAN FOR THE COST OF REMEDIES PAID FOR BY VEHICLE OWNERS PRIOR TO NOTIFICATION OF A SAFETY RECALL IN FEBRUARY 2015.  THE ENDING DATE FOR REIMBURSEMENT ELIGIBILITY IS DECEMBER 31, 2015.  FORD WILL FORWARD A COPY OF THE NOTIFICATION LETTERS TO DEALERS TO THE AGENCY WHEN AVAILABLE."",""recall_description_lang"":""AN INTERMITTENT ELECTRICAL CONNECTION CAUSED BY QUALITY ISSUES PERTAINING TO CONFORMAL COAT CONTAMINATION AND RIBBON CABLE PIN MISALIGNMENT WITHIN THE STEERING GEAR MOTOR MAY LEAD TO A LOSS OF THE MOTOR POSITION SENSOR SIGNAL THAT MAY RESULT IN LOSS OF POWER STEERING ASSIST.  FORD IS AWARE OF 16 ACCIDENT ALLEGATIONS AND THREE INJURY ALLEGATIONS RELATED TO THIS CONDITION IN THE VEHICLES INCLUDED IN THIS FIELD ACTION.  MOST OF THE ACCIDENT ALLEGATIONS REPORT THAT MINOR VEHICLE DAMAGE WAS INCURRED, TYPICALLY EITHER WHEEL DAMAGE RESULTING FROM CONTACT WITH A CURB OR MINOR BODY DAMAGE.  THE INJURY ALLEGATIONS WERE ALL REPORTED AS MINOR WITH NO INDICATION THAT MEDICAL ATTENTION WAS SOUGHT FOR ANY OF THE INJURIES.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    ""},{""nhtsa_recall_number"":""15V040"",""recall_description"":""THE INTERIOR DOOR HANDLE RETURN SPRING MAY BECOME UNSEATED, RESULTING IN AN INTERIOR HANDLE THAT IS FLOPPY OR DOES NOT RETURN TO THE FULLY STOWED POSITION AFTER ACTUATION.  WHILE THE DOOR LATCH ACTUATION AND ENGAGEMENT DURING NORMAL VEHICLE USAGE AND OPERATION IS UNAFFECTED, THIS CONDITION MAY ALLOW A DOOR TO UNLATCH DURING A SIDE IMPACT CRASH."",""remedy_description_lang"":""NOTIFICATION TO DEALERS WILL OCCUR ON JANUARY 27, 2015.  MAILING OF OWNER NOTIFICATION LETTERS WILL BEGIN MARCH 16, 2015, AND IS EXPECTED TO BE COMPLETED BY MARCH 20, 2015.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    "",""description_lang"":""INTERIOR DOOR HANDLE INSPECTION AND REPAIR                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   "",""mfr_recall_number"":""15S02"",""refresh_date"":""NOV 09, 2015"",""mfr_notes_lang"":""TO CHECK FOR NON-SAFETY-RELATED PROGRAMS APPLICABLE TO YOUR VEHICLE, SEE HTTP://WWW.FORD.COM/ OR CALL YOUR FORD DEALER.                                                                                                                                                                                                                                                                                                                                           "",""mfr_recall_status_lang"":""11 - RECALL INCOMPLETE"",""safety_risk_description_lang"":""IF THE INTERIOR DOOR HANDLE RETURN SPRING IS UNSEATED, THE DOOR MAY UNLATCH DURING A SIDE IMPACT CRASH, INCREASING THE RISK OF INJURY.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          "",""safety_risk_description"":""IF THE INTERIOR DOOR HANDLE RETURN SPRING IS UNSEATED, THE DOOR MAY UNLATCH DURING A SIDE IMPACT CRASH, INCREASING THE RISK OF INJURY."",""mfr_recall_status"":""11 - RECALL INCOMPLETE"",""description_eng"":""INTERIOR DOOR HANDLE INSPECTION AND REPAIR"",""mfr_notes"":""TO CHECK FOR NON-SAFETY-RELATED PROGRAMS APPLICABLE TO YOUR VEHICLE, SEE HTTP://WWW.FORD.COM/ OR CALL YOUR FORD DEALER."",""recall_date"":""JAN 27, 2015"",""remedy_description"":""NOTIFICATION TO DEALERS WILL OCCUR ON JANUARY 27, 2015.  MAILING OF OWNER NOTIFICATION LETTERS WILL BEGIN MARCH 16, 2015, AND IS EXPECTED TO BE COMPLETED BY MARCH 20, 2015."",""recall_description_lang"":""THE INTERIOR DOOR HANDLE RETURN SPRING MAY BECOME UNSEATED, RESULTING IN AN INTERIOR HANDLE THAT IS FLOPPY OR DOES NOT RETURN TO THE FULLY STOWED POSITION AFTER ACTUATION.  WHILE THE DOOR LATCH ACTUATION AND ENGAGEMENT DURING NORMAL VEHICLE USAGE AND OPERATION IS UNAFFECTED, THIS CONDITION MAY ALLOW A DOOR TO UNLATCH DURING A SIDE IMPACT CRASH.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      ""}],""fsa_item"":{""description_lang"":""EPAS STEERING EFFORTS                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        "",""local_fsa_r"":""15N01"",""description_eng"":""EPAS STEERING EFFORTS""}},""nhtsaReq"":""Y"",""nhtsa_header_details"":{""number_of_recalls"":""03"",""refresh_date"":""NOV 09, 2015"",""status"":true,""manufacturer_id"":""0002"",""model"":""MKS"",""recalls_available"":true,""year"":2012,""make"":""LINCOLN""},""lang_iso_c"":""EN"",""lang_region_c"":"""",""stpr_state_c"":""  "",""country_iso3_c"":""USA"",""vin"":""1LNHL9FT1CG802385""}";

            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetFordLincolnMercuryData(),
                ResponseText = serviceResponse
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("1LNHL9FT1CG802385", mappedResponse.VIN);
			Assert.AreEqual(2012, mappedResponse.Year);
			Assert.AreEqual("LINCOLN", mappedResponse.Make);
			Assert.AreEqual("MKS", mappedResponse.Model);
			Assert.AreEqual(new DateTime(2015, 11, 9), mappedResponse.OEMFeedLastUpdated);

			Assert.AreEqual(2, mappedResponse.OpenRecalls.Count());

			Assert.AreEqual("15V340", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
			Assert.AreEqual("IN THE EVENT OF A LOSS OF POWER STEERING ASSIST, THE MECHANICAL LINKAGE BETWEEN THE STEERING WHEEL AND THE ROAD IS MAINTAINED AT ALL TIMES. THE STEERING SYSTEM WILL DEFAULT TO MANUAL STEERING MODE, ALLOWING THE VEHICLE TO BE STEERED IN A SAFE AND CONTROLLED MANNER. LOSS OF POWER STEERING ASSIST IS UNLIKELY TO BE ASSOCIATED WITH ACCIDENTS AT HIGHER SPEEDS, AS THE AMOUNT OF ASSIST SUPPLIED IS INVERSELY PROPORTIONAL TO VEHICLE SPEED. LOSS OF POWER STEERING ASSIST WOULD REQUIRE HIGHER STEERING EFFORT, ESPECIALLY AT LOWER SPEEDS, WHICH MAY RESULT IN AN INCREASED RISK OF A CRASH.", mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
			Assert.AreEqual("AN INTERMITTENT ELECTRICAL CONNECTION CAUSED BY QUALITY ISSUES PERTAINING TO CONFORMAL COAT CONTAMINATION AND RIBBON CABLE PIN MISALIGNMENT WITHIN THE STEERING GEAR MOTOR MAY LEAD TO A LOSS OF THE MOTOR POSITION SENSOR SIGNAL THAT MAY RESULT IN LOSS OF POWER STEERING ASSIST.  FORD IS AWARE OF 16 ACCIDENT ALLEGATIONS AND THREE INJURY ALLEGATIONS RELATED TO THIS CONDITION IN THE VEHICLES INCLUDED IN THIS FIELD ACTION.  MOST OF THE ACCIDENT ALLEGATIONS REPORT THAT MINOR VEHICLE DAMAGE WAS INCURRED, TYPICALLY EITHER WHEEL DAMAGE RESULTING FROM CONTACT WITH A CURB OR MINOR BODY DAMAGE.  THE INJURY ALLEGATIONS WERE ALL REPORTED AS MINOR WITH NO INDICATION THAT MEDICAL ATTENTION WAS SOUGHT FOR ANY OF THE INJURIES.", mappedResponse.OpenRecalls.ElementAt(0).Description);
			Assert.AreEqual("OWNERS WILL BE NOTIFIED BY MAIL AND INSTRUCTED TO TAKE THEIR VEHICLE TO A FORD OR LINCOLN DEALER TO HAVE THE POWER STEERING CONTROL MODULE (PSCM) CHECKED FOR DIAGNOSTIC TROUBLE CODES (DTCS).  IF NO LOSS OF STEERING ASSIST DTCS ARE PRESENT, DEALERS WILL UPDATE THE PSCM SOFTWARE. THE UPDATED PSCM SOFTWARE WILL PROVIDE STEERING ASSIST FOR THE REMAINDER OF THE DRIVE CYCLE IF A MOTOR POSITION SENSOR ENCODER FAULT OCCURS, AND A CLUSTER WARNING AND CHIME WILL BE PROVIDED. UPON SUBSEQUENT VEHICLE START-UPS, IF MULTIPLE FAULTS HAVE BEEN VERIFIED, STEERING ASSIST WILL BE REMOVED AND A CLUSTER WARNING AND CHIME WILL BE PROVIDED.  IF ANY LOSS OF STEERING ASSIST DTCS ARE PRESENT, DEALERS WILL REPLACE THE STEERING GEAR.  IF A VEHICLE HAS THE PSCM SOFTWARE UPDATED IN ACCORDANCE WITH THIS FIELD ACTION, AND LOSS OF POWER STEERING ASSIST OCCURS AT A LATER DATE, THE OWNER WILL BE ABLE TO RETURN THEIR VEHICLE TO A FORD OR LINCOLN DEALER TO HAVE THE STEERING GEAR REPLACED IF IT OCCURS WITHIN A PERIOD OF 10 YEARS FROM THE WARRANTY START DATE FOR THE VEHICLE OR 150K MILES, WHICHEVER OCCURS FIRST.  THERE WILL BE NO CHARGE FOR THIS SERVICE.  FORD PROVIDED THE GENERAL REIMBURSEMENT PLAN FOR THE COST OF REMEDIES PAID FOR BY VEHICLE OWNERS PRIOR TO NOTIFICATION OF A SAFETY RECALL IN FEBRUARY 2015.  THE ENDING DATE FOR REIMBURSEMENT ELIGIBILITY IS DECEMBER 31, 2015.  FORD WILL FORWARD A COPY OF THE NOTIFICATION LETTERS TO DEALERS TO THE AGENCY WHEN AVAILABLE.", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
			Assert.AreEqual("REPROGRAM POWER STEERING CONTROL MODULE", mappedResponse.OpenRecalls.ElementAt(0).Title);
			Assert.AreEqual(new DateTime(2015, 5, 26), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.RecallIncomplete, mappedResponse.OpenRecalls.ElementAt(0).Status);
			Assert.AreEqual("15S18", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);

			Assert.AreEqual("15V040", mappedResponse.OpenRecalls.ElementAt(1).CampaignID);
			Assert.AreEqual("IF THE INTERIOR DOOR HANDLE RETURN SPRING IS UNSEATED, THE DOOR MAY UNLATCH DURING A SIDE IMPACT CRASH, INCREASING THE RISK OF INJURY.", mappedResponse.OpenRecalls.ElementAt(1).SafetyRisk);
			Assert.AreEqual("THE INTERIOR DOOR HANDLE RETURN SPRING MAY BECOME UNSEATED, RESULTING IN AN INTERIOR HANDLE THAT IS FLOPPY OR DOES NOT RETURN TO THE FULLY STOWED POSITION AFTER ACTUATION.  WHILE THE DOOR LATCH ACTUATION AND ENGAGEMENT DURING NORMAL VEHICLE USAGE AND OPERATION IS UNAFFECTED, THIS CONDITION MAY ALLOW A DOOR TO UNLATCH DURING A SIDE IMPACT CRASH.", mappedResponse.OpenRecalls.ElementAt(1).Description);
			Assert.AreEqual("NOTIFICATION TO DEALERS WILL OCCUR ON JANUARY 27, 2015.  MAILING OF OWNER NOTIFICATION LETTERS WILL BEGIN MARCH 16, 2015, AND IS EXPECTED TO BE COMPLETED BY MARCH 20, 2015.", mappedResponse.OpenRecalls.ElementAt(1).RemedyDescription);
			Assert.AreEqual("INTERIOR DOOR HANDLE INSPECTION AND REPAIR", mappedResponse.OpenRecalls.ElementAt(1).Title);
			Assert.AreEqual(new DateTime(2015, 1, 27), mappedResponse.OpenRecalls.ElementAt(1).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.RecallIncomplete, mappedResponse.OpenRecalls.ElementAt(1).Status);
			Assert.AreEqual("15S02", mappedResponse.OpenRecalls.ElementAt(1).OEMCampaignID);
		}

		[Test]
		public void MercuryTest()
		{
			// https://owner.ford.com/sharedServices/recalls/query.do?country=USA&language=EN&vin=1MEFM50U42G643071
			string serviceResponse = @"{""lang_script_c"":"""",""return_status"":{""description"":""Required data found"",""code"":""00""},""recalls"":{""nhtsa_recall_item"":{""nhtsa_recall_number"":""04V106000"",""recall_description"":""CERTAIN PASSENGER VEHICLES ARE BEING RECALLED TO CORRECT A PROBLEM WITH A MALFUNCTIONING STOP LAMP SWITCH AND/OR ASSOCIATED WIRING.  THIS MALFUNCTIONING COULD RENDER THE STOP LAMPS INOPERABLE OR CAUSE THEM TO STAY ON ALL THE TIME.  THIS COULD CAUSE CONFUSION TO A FOLLOWING DRIVER, WHICH COULD LEAD TO A CRASH."",""remedy_description_lang"":""DEALERS WILL REMOVE THE STOP LAMP SWITCH AND ASSOCIATED WIRING ASSEMBLY AND INSTALL A NEWLY DESIGNED STOP LAMP SWITCH AND WIRE ASSEMBLY.  OWNER NOTIFICATION IS EXPECTED TO BEGIN ON APRIL 5, 2004.  OWNERS SHOULD CONTACT FORD AT 1-866-436-7332.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              "",""description_lang"":""STOP LAMP SWITCH AND WIRE HARNESS REPLACEMENT                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                "",""mfr_recall_number"":""04S12"",""refresh_date"":""NOV 09, 2015"",""mfr_notes_lang"":""To check for non-safety-related programs applicable to your vehicle, see www.Ford.com &lt;http://www.ford.com/&gt;  or call your Ford dealer.                                                                                                                                                                                                                                                                                                                           "",""mfr_recall_status_lang"":""11 - RECALL INCOMPLETE"",""safety_risk_description_lang"":""IN ADDITION, IF THE SWITCH AND/OR ASSOCIATED WIRING FAIL IN THE OPEN POSITION, THE BRAKE LIGHTS WILL NOT ACTUATE AND THE DRIVER WILL NOT BE ABLE TO SHIFT THE VEHICLE OUT OF &quot;PARK&quot;  IF THEY FAIL IN THE CLOSED POSITION, THE BRAKE LIGHTS WILL REMAIN ON, WHICH WILL NOT ALLOW THE SPEED CONTROL TO BE ACTIVATED.  THIS COULD ALSO CAUSE THE BATTERY TO DISCHARGE.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             "",""safety_risk_description"":""IN ADDITION, IF THE SWITCH AND/OR ASSOCIATED WIRING FAIL IN THE OPEN POSITION, THE BRAKE LIGHTS WILL NOT ACTUATE AND THE DRIVER WILL NOT BE ABLE TO SHIFT THE VEHICLE OUT OF &quot;PARK&quot;  IF THEY FAIL IN THE CLOSED POSITION, THE BRAKE LIGHTS WILL REMAIN ON, WHICH WILL NOT ALLOW THE SPEED CONTROL TO BE ACTIVATED.  THIS COULD ALSO CAUSE THE BATTERY TO DISCHARGE."",""mfr_recall_status"":""11 - RECALL INCOMPLETE"",""description_eng"":""STOP LAMP SWITCH AND WIRE HARNESS REPLACEMENT"",""mfr_notes"":""To check for non-safety-related programs applicable to your vehicle, see www.Ford.com &lt;http://www.ford.com/&gt;  or call your Ford dealer."",""recall_date"":""MAR 02, 2004"",""remedy_description"":""DEALERS WILL REMOVE THE STOP LAMP SWITCH AND ASSOCIATED WIRING ASSEMBLY AND INSTALL A NEWLY DESIGNED STOP LAMP SWITCH AND WIRE ASSEMBLY.  OWNER NOTIFICATION IS EXPECTED TO BEGIN ON APRIL 5, 2004.  OWNERS SHOULD CONTACT FORD AT 1-866-436-7332."",""recall_description_lang"":""CERTAIN PASSENGER VEHICLES ARE BEING RECALLED TO CORRECT A PROBLEM WITH A MALFUNCTIONING STOP LAMP SWITCH AND/OR ASSOCIATED WIRING.  THIS MALFUNCTIONING COULD RENDER THE STOP LAMPS INOPERABLE OR CAUSE THEM TO STAY ON ALL THE TIME.  THIS COULD CAUSE CONFUSION TO A FOLLOWING DRIVER, WHICH COULD LEAD TO A CRASH.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ""}},""nhtsaReq"":""Y"",""nhtsa_header_details"":{""number_of_recalls"":""01"",""refresh_date"":""NOV 09, 2015"",""status"":true,""manufacturer_id"":""0002"",""model"":""Sable"",""recalls_available"":true,""year"":2002,""make"":""MERCURY""},""lang_iso_c"":""EN"",""lang_region_c"":"""",""stpr_state_c"":""  "",""country_iso3_c"":""USA"",""vin"":""1MEFM50U42G643071""}";

            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetFordLincolnMercuryData(),
                ResponseText = serviceResponse
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("1MEFM50U42G643071", mappedResponse.VIN);
			Assert.AreEqual(2002, mappedResponse.Year);
			Assert.AreEqual("MERCURY", mappedResponse.Make);
			Assert.AreEqual("Sable", mappedResponse.Model);
			Assert.AreEqual(new DateTime(2015, 11, 9), mappedResponse.OEMFeedLastUpdated);

			Assert.AreEqual(1, mappedResponse.OpenRecalls.Count());

			Assert.AreEqual("04V106000", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
			Assert.AreEqual("IN ADDITION, IF THE SWITCH AND/OR ASSOCIATED WIRING FAIL IN THE OPEN POSITION, THE BRAKE LIGHTS WILL NOT ACTUATE AND THE DRIVER WILL NOT BE ABLE TO SHIFT THE VEHICLE OUT OF &quot;PARK&quot;  IF THEY FAIL IN THE CLOSED POSITION, THE BRAKE LIGHTS WILL REMAIN ON, WHICH WILL NOT ALLOW THE SPEED CONTROL TO BE ACTIVATED.  THIS COULD ALSO CAUSE THE BATTERY TO DISCHARGE.", mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
			Assert.AreEqual("CERTAIN PASSENGER VEHICLES ARE BEING RECALLED TO CORRECT A PROBLEM WITH A MALFUNCTIONING STOP LAMP SWITCH AND/OR ASSOCIATED WIRING.  THIS MALFUNCTIONING COULD RENDER THE STOP LAMPS INOPERABLE OR CAUSE THEM TO STAY ON ALL THE TIME.  THIS COULD CAUSE CONFUSION TO A FOLLOWING DRIVER, WHICH COULD LEAD TO A CRASH.", mappedResponse.OpenRecalls.ElementAt(0).Description);
			Assert.AreEqual("DEALERS WILL REMOVE THE STOP LAMP SWITCH AND ASSOCIATED WIRING ASSEMBLY AND INSTALL A NEWLY DESIGNED STOP LAMP SWITCH AND WIRE ASSEMBLY.  OWNER NOTIFICATION IS EXPECTED TO BEGIN ON APRIL 5, 2004.  OWNERS SHOULD CONTACT FORD AT 1-866-436-7332.", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
			Assert.AreEqual("STOP LAMP SWITCH AND WIRE HARNESS REPLACEMENT", mappedResponse.OpenRecalls.ElementAt(0).Title);
			Assert.AreEqual(new DateTime(2004, 3, 2), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.RecallIncomplete, mappedResponse.OpenRecalls.ElementAt(0).Status);
			Assert.AreEqual("04S12", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);
		}

		[Test]
		public void MitsubishiTest()
		{
			// https://www.mitsubishicars.com/rs/warranty?vin=4A3AC54H62E083686
			string serviceResponse = @"{""vehicleInfo"":{""vin"":""4A3AC54H62E083686"",""model"":""ECLIPSE"",""country"":""US"",""priceline"":""GT  PREMIUM"",""modelYear"":2002,""exteriorColor"":""DOVER WHITE PEARL"",""interiorColor"":""BEIGE LEATHER"",""transmissionCode"":""4ST"",""inServiceDate"":""04/08/2002"",""bodyModel"":""EC24""},""ownerWarrantyDetails"":[{""warrantyDescription"":""NEW VEHICLE LIMITED WARRANTY (BASIC) COVERAGE"",""mileageCovered"":36000,""monthsCovered"":36,""mileageRemaining"":0,""timeRemaining"":""Expired""},{""warrantyDescription"":""POWERTRAIN LIMITED WARRANTY COVERAGE"",""mileageCovered"":60000,""monthsCovered"":60,""mileageRemaining"":0,""timeRemaining"":""Expired""},{""warrantyDescription"":""RESTRAINT SYSTEM COVERAGE (AIR BAGS, SEAT BELTS)"",""mileageCovered"":60000,""monthsCovered"":60,""mileageRemaining"":0,""timeRemaining"":""Expired""},{""warrantyDescription"":""ANTI-CORROSION/PERFORATION LTD WARRANTY COVERAGE"",""mileageCovered"":100000,""monthsCovered"":84,""mileageRemaining"":0,""timeRemaining"":""Expired""},{""warrantyDescription"":""STARTER BATTERY (PRO-RATE 50% AFTER 24 MONTHS)"",""mileageCovered"":999999,""monthsCovered"":36,""mileageRemaining"":0,""timeRemaining"":""Expired""},{""warrantyDescription"":""AUDIO, NAVIGATION UNITS & ENTERTAINMENT SYSTEMS"",""mileageCovered"":36000,""monthsCovered"":36,""mileageRemaining"":0,""timeRemaining"":""Expired""},{""warrantyDescription"":""ADJUSTMENT WARRANTY"",""mileageCovered"":12000,""monthsCovered"":12,""mileageRemaining"":0,""timeRemaining"":""Expired""},{""warrantyDescription"":""A/C REFRIGERANT CHARGE"",""mileageCovered"":12000,""monthsCovered"":12,""mileageRemaining"":0,""timeRemaining"":""Expired""},{""warrantyDescription"":""FEDERAL EMISSION DEFECT WARRANTY"",""mileageCovered"":36000,""monthsCovered"":36,""mileageRemaining"":0,""timeRemaining"":""Expired""},{""warrantyDescription"":""FEDERAL EMISSION WARRANTY ECU,PCM,CAT CONVERTER"",""mileageCovered"":80000,""monthsCovered"":96,""mileageRemaining"":0,""timeRemaining"":""Expired""},{""warrantyDescription"":""ROADSIDE ASSISTANCE"",""mileageCovered"":36000,""monthsCovered"":36,""mileageRemaining"":0,""timeRemaining"":""Expired""},{""warrantyDescription"":""WARRANTABLE TOW ONLY"",""mileageCovered"":36000,""monthsCovered"":36,""mileageRemaining"":0,""timeRemaining"":""Expired""}],""subsequentOwnerWarrantyDetails"":[{""warrantyDescription"":""NEW VEHICLE LIMITED WARRANTY (BASIC) COVERAGE"",""mileageCovered"":36000,""monthsCovered"":36,""mileageRemaining"":0,""timeRemaining"":""04/08/2005""},{""warrantyDescription"":""POWERTRAIN LIMITED WARRANTY COVERAGE"",""mileageCovered"":60000,""monthsCovered"":60,""mileageRemaining"":0,""timeRemaining"":""04/08/2007""},{""warrantyDescription"":""RESTRAINT SYSTEM COVERAGE (AIR BAGS, SEAT BELTS)"",""mileageCovered"":60000,""monthsCovered"":60,""mileageRemaining"":0,""timeRemaining"":""04/08/2007""},{""warrantyDescription"":""ANTI-CORROSION/PERFORATION LTD WARRANTY COVERAGE"",""mileageCovered"":100000,""monthsCovered"":84,""mileageRemaining"":0,""timeRemaining"":""04/08/2009""},{""warrantyDescription"":""STARTER BATTERY (PRO-RATE 50% AFTER 24 MONTHS)"",""mileageCovered"":999999,""monthsCovered"":36,""mileageRemaining"":999999,""timeRemaining"":""04/08/2005""},{""warrantyDescription"":""AUDIO, NAVIGATION UNITS & ENTERTAINMENT SYSTEMS"",""mileageCovered"":36000,""monthsCovered"":36,""mileageRemaining"":0,""timeRemaining"":""04/08/2005""},{""warrantyDescription"":""ADJUSTMENT WARRANTY"",""mileageCovered"":12000,""monthsCovered"":12,""mileageRemaining"":0,""timeRemaining"":""04/08/2003""},{""warrantyDescription"":""A/C REFRIGERANT CHARGE"",""mileageCovered"":12000,""monthsCovered"":12,""mileageRemaining"":0,""timeRemaining"":""04/08/2003""},{""warrantyDescription"":""FEDERAL EMISSION DEFECT WARRANTY"",""mileageCovered"":36000,""monthsCovered"":36,""mileageRemaining"":0,""timeRemaining"":""04/08/2005""},{""warrantyDescription"":""FEDERAL EMISSION WARRANTY ECU,PCM,CAT CONVERTER"",""mileageCovered"":80000,""monthsCovered"":96,""mileageRemaining"":0,""timeRemaining"":""04/08/2010""},{""warrantyDescription"":""ROADSIDE ASSISTANCE"",""mileageCovered"":36000,""monthsCovered"":36,""mileageRemaining"":0,""timeRemaining"":""04/08/2005""},{""warrantyDescription"":""WARRANTABLE TOW ONLY"",""mileageCovered"":36000,""monthsCovered"":36,""mileageRemaining"":0,""timeRemaining"":""04/08/2005""}],""openedRecalls"":[{""recallNumber"":""C1505E"",""recallDescription"":""2000-2005MY ECLIPSE & ECLIPSE SPYDER PASSENGER SIDE SUNVISOR TETHER INSTALLATION                                                                                                                                                                          "",""recallStatus"":""Recall Open""}],""completedRecalls"":[],""promoMessageDetails"":[],""serviceMessageDetails"":[],""warExtMessageDetails"":[],""customer"":{""city"":""LAS VEGAS           "",""emailAddress"":""                                                  "",""emailBypassed"":false,""residencePhone"":""7024379004"",""stateProvince"":""NV"",""streetAddress"":""575 MARION DR                 "",""streetAddress2"":""                              "",""zipPostalCode"":""89110     "",""displayEmailAddress"":""                                                  "",""formatResidencePhone"":""(702)437-9004"",""custFirstName"":""ROSARIO        "",""custLastName"":""RODRIGUEZ                ""},""claimDetails"":[{""dealerCode"":""34033 "",""dealerName"":""COURTESY MITSUBISHI-LAS VEGAS - TERM"",""claimTypeDesc"":""Customer Pay Claim"",""mileage"":31170,""laborOpDesc"":""30,000 MILE SERVICE                     "",""repairDate"":""05/20/2004""}],""nhtsaVehicle"":{""recallsAvailable"":true,""numberOfRecalls"":1,""recallRefreshDate"":""Nov 09, 2015""},""nhtsaOpenRecalls"":[{""nhtsaRecallNumber"":""15V338    "",""mfrRecallNumber"":""SR-15-005      "",""recallDescription"":""DURING A FRONTAL ACCIDENT RESULTING IN FRONTAL AIR BAG DEPLOYMENT, THERE IS THE POTENTIAL FOR INTERACTION BETWEEN THE DEPLOYING PASSENGER AIRBAG AND THE PASSENGER SUN VISOR IF THE SUN VISOR IS IN THE DOWN POSITION.  DEPENDING ON THE POSITION AND ANGLE OF THE SUN VISOR IN THE DOWN POSITION, THE DEPLOYING PASSENGER AIRBAG MAY REMOVE THE SUN VISOR, PROPELLING IT REARWARD."",""mfrRecallStatus"":""INCOMPLETE BUT REPAIR AVAILABLE"",""recallDate"":""May 26, 2015"",""recallType"":""SAFETY RECALL"",""safetyRiskDescription"":""THE PASSENGER SUN VISOR, IF REMOVED AND PROPELLED REARWARD BY THE DEPLOYING PASSENGER AIRBAG, COULD INCREASE THE RISK OF INJURY TO A PASSENGER SEATED IN THE FRONT PASSENGER SEAT."",""remedyDescription"":""A TETHER STRAP WILL BE ADDED TO THE PASSENGER SUN VISOR ON EACH VEHICLE.  FOR THE ECLIPSE SPYDER VEHICLES, THE SUN VISOR HOLDER WILL BE ALSO REPLACED WITH A COUNTERMEASURE ONE.  THESE REPAIRS WILL BE PERFORMED FREE OF CHARGE.""}],""success"":true,""statusCode"":""200"",""statusMessage"":null}";

            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetMitsubishiData(),
                ResponseText = serviceResponse
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("4A3AC54H62E083686", mappedResponse.VIN);
			Assert.AreEqual(2002, mappedResponse.Year);
			Assert.AreEqual("Mitsubishi", mappedResponse.Make);
			Assert.AreEqual("ECLIPSE", mappedResponse.Model);
			Assert.AreEqual(new DateTime(2015, 11, 9), mappedResponse.OEMFeedLastUpdated);

			Assert.AreEqual(1, mappedResponse.OpenRecalls.Count());

			Assert.AreEqual("15V338", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
			Assert.AreEqual("THE PASSENGER SUN VISOR, IF REMOVED AND PROPELLED REARWARD BY THE DEPLOYING PASSENGER AIRBAG, COULD INCREASE THE RISK OF INJURY TO A PASSENGER SEATED IN THE FRONT PASSENGER SEAT.", mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
			Assert.AreEqual("DURING A FRONTAL ACCIDENT RESULTING IN FRONTAL AIR BAG DEPLOYMENT, THERE IS THE POTENTIAL FOR INTERACTION BETWEEN THE DEPLOYING PASSENGER AIRBAG AND THE PASSENGER SUN VISOR IF THE SUN VISOR IS IN THE DOWN POSITION.  DEPENDING ON THE POSITION AND ANGLE OF THE SUN VISOR IN THE DOWN POSITION, THE DEPLOYING PASSENGER AIRBAG MAY REMOVE THE SUN VISOR, PROPELLING IT REARWARD.", mappedResponse.OpenRecalls.ElementAt(0).Description);
			Assert.AreEqual("A TETHER STRAP WILL BE ADDED TO THE PASSENGER SUN VISOR ON EACH VEHICLE.  FOR THE ECLIPSE SPYDER VEHICLES, THE SUN VISOR HOLDER WILL BE ALSO REPLACED WITH A COUNTERMEASURE ONE.  THESE REPAIRS WILL BE PERFORMED FREE OF CHARGE.", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
			Assert.AreEqual(null, mappedResponse.OpenRecalls.ElementAt(0).Title);
			Assert.AreEqual(new DateTime(2015, 5, 26), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.RecallIncomplete, mappedResponse.OpenRecalls.ElementAt(0).Status);
			Assert.AreEqual("SR-15-005", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);
		}

		[Test]
		public void NissanTest()
		{
			// http://www.nissanusa.com/dealercenter/api/recalls?vin=5N1AR2MN8DC618355
			string serviceResponse = @"{""make"":""NISSAN"",""modelYear"":2013,""recalls"":[{""effectiveDate"":""2015-01-26T06:00:00Z"",""make"":""NISSAN"",""modelCode"":""PATHFINDER SV"",""modelYear"":2013,""nhtsaId"":""15V-033"",""nnaId"":""PC343"",""primaryDescription"":""On certain 2013-2014 Model Year Pathfinder and 2014 Model Year Pathfinder Hybrid vehicles, the secondary hood latch may not function properly.  If the primary hood latch is released, the secondary hood latch may not hold the hood closed as designed while the vehicle is in motion."",""remedyDescription"":""Nissan will be instructed to modify the angle of the hood release mechanism to provide additional length to the release cable.  This repair should take less than an hour to complete. This repair will be offered at no charge for parts and labor.  Your dealer may require your vehicle for a longer period of time based upon their work schedule."",""riskIfNotRepaired"":""In certain circumstances, this may cause the hood to open while driving and partially obscure the driver?s vision, increasing the risk of a crash.  \r\n\r\nIn the interim, we recommend that you check to ensure the hood is fully closed and latched.  Also, we ask that you be careful to pull the fuel door release lever when refueling, and not the hood release lever."",""secondaryDescription"":""HOOD LATCH PATHFINDER"",""typeCode"":""Recall Campaign"",""vin"":""5N1AR2MN8DC618355""},{""effectiveDate"":""2015-03-12T05:00:00Z"",""make"":""NISSAN"",""modelCode"":""PATHFINDER SV"",""modelYear"":2013,""nhtsaId"":"""",""nnaId"":""PC357"",""primaryDescription"":""Nissan is informing affected customers that their vehicles (non-Hybrid models) are eligible for Continually Variable Transmission (CVT) Transmission Control Module (?TCM?) software programming update to improve CVT service diagnostics (PC357)."",""remedyDescription"":"""",""riskIfNotRepaired"":""This diagnostic software update will enhance the dealer?s ability to better diagnose and service the CVT by including two new Diagnostic Trouble Codes (DTCs) and enhanced active test capabilities for CONSULT-III."",""secondaryDescription"":""CVT REPROGRAMM"",""typeCode"":""Service Campaign"",""vin"":""5N1AR2MN8DC618355""}],""vehicleName"":""PATHFINDER SV"",""vin"":""5N1AR2MN8DC618355""}";

            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetNissanData(),
                ResponseText = serviceResponse
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("5N1AR2MN8DC618355", mappedResponse.VIN);
			Assert.AreEqual(2013, mappedResponse.Year);
			Assert.AreEqual("NISSAN", mappedResponse.Make);
			Assert.AreEqual("PATHFINDER SV", mappedResponse.Model);
			Assert.AreEqual(null, mappedResponse.OEMFeedLastUpdated);

			Assert.AreEqual(2, mappedResponse.OpenRecalls.Count());

			Assert.AreEqual("15V-033", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
			Assert.AreEqual("In certain circumstances, this may cause the hood to open while driving and partially obscure the driver?s vision, increasing the risk of a crash.  \r\n\r\nIn the interim, we recommend that you check to ensure the hood is fully closed and latched.  Also, we ask that you be careful to pull the fuel door release lever when refueling, and not the hood release lever.", mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
			Assert.AreEqual("On certain 2013-2014 Model Year Pathfinder and 2014 Model Year Pathfinder Hybrid vehicles, the secondary hood latch may not function properly.  If the primary hood latch is released, the secondary hood latch may not hold the hood closed as designed while the vehicle is in motion.", mappedResponse.OpenRecalls.ElementAt(0).Description);
			Assert.AreEqual("Nissan will be instructed to modify the angle of the hood release mechanism to provide additional length to the release cable.  This repair should take less than an hour to complete. This repair will be offered at no charge for parts and labor.  Your dealer may require your vehicle for a longer period of time based upon their work schedule.", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
			Assert.AreEqual("HOOD LATCH PATHFINDER", mappedResponse.OpenRecalls.ElementAt(0).Title);
			Assert.AreEqual(new DateTime(2015, 1, 26), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.Unknown, mappedResponse.OpenRecalls.ElementAt(0).Status);
			Assert.AreEqual("PC343", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);

			Assert.AreEqual(null, mappedResponse.OpenRecalls.ElementAt(1).CampaignID);
			Assert.AreEqual("This diagnostic software update will enhance the dealer?s ability to better diagnose and service the CVT by including two new Diagnostic Trouble Codes (DTCs) and enhanced active test capabilities for CONSULT-III.", mappedResponse.OpenRecalls.ElementAt(1).SafetyRisk);
			Assert.AreEqual("Nissan is informing affected customers that their vehicles (non-Hybrid models) are eligible for Continually Variable Transmission (CVT) Transmission Control Module (?TCM?) software programming update to improve CVT service diagnostics (PC357).", mappedResponse.OpenRecalls.ElementAt(1).Description);
			Assert.AreEqual(null, mappedResponse.OpenRecalls.ElementAt(1).RemedyDescription);
			Assert.AreEqual("CVT REPROGRAMM", mappedResponse.OpenRecalls.ElementAt(1).Title);
			Assert.AreEqual(new DateTime(2015, 3, 12), mappedResponse.OpenRecalls.ElementAt(1).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.Unknown, mappedResponse.OpenRecalls.ElementAt(1).Status);
			Assert.AreEqual("PC357", mappedResponse.OpenRecalls.ElementAt(1).OEMCampaignID);
		}

		[Test]
		public void OldsMobileTest()
		{
			// https://recalls.gm.com/recall/services/1G3NF52E73C170207/recalls
			string serviceResponse =
				@"{""messages"":[],""serverErrorMsgs"":[],""data"":{""vin"":""1G3NF52E73C170207"",""year"":2003,""make"":""Oldsmobile"",""model"":""Alero"",""manufacturer_id"":1,""recalls_available"":true,""number_of_recalls"":1,""recalls"":[{""recall_type"":""ZPSR"",""nhtsa_recall_number"":""14V400"",""mfr_recall_number"":""N140350"",""recall_date"":""Aug 12, 2014"",""recall_title"":""UNINTENDED IGNITION KEY ROTATION"",""recall_description"":""General Motors has decided that a defect which relates to motor vehicle safety exists in 2000-2005 MY Chevrolet Impala and Monte Carlo, 1997-2003 MY Chevrolet Malibu, 2004-2005 MY Chevrolet Malibu Classic, 1999-2004 MY Oldsmobile Alero, 1998-2002 MY Oldsmobile Intrigue, 1999-2005 MY Pontiac Grand Am, and 2004-2008 MY Pontiac Grand Prix vehicles.  If the key ring is carrying added weight and the vehicle goes off road or experiences some other jarring event, it may unintentionally move the key away from the “run” position.  If this occurs, engine power, power steering and power braking may be affected, increasing the risk of a crash."",""safety_risk_description"":""The timing of the key movement out of the \u001Arun\u001A position, relative to the activation of the sensing algorithm of the crash event, may result in the airbags not deploying, increasing the potential for occupant injury in certain kinds of crashes.  Until the recall has been performed, it is very important that customers remove all items from their key ring, leaving only the vehicle key.  The key fob (if applicable), should also be removed from the key ring."",""remedy_description"":""Dealers are to install two key rings and an insert in the key slot or a cover over the key head on all ignition keys, free of charge."",""mfr_recall_status"":""11""}],""status"":true,""error_code"":0,""refresh_date"":""Nov 09, 2015""}}";

            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetGMMappingData(),
                ResponseText = serviceResponse
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("1G3NF52E73C170207", mappedResponse.VIN);
			Assert.AreEqual(2003, mappedResponse.Year);
			Assert.AreEqual("Oldsmobile", mappedResponse.Make);
			Assert.AreEqual("Alero", mappedResponse.Model);
			Assert.AreEqual(new DateTime(2015, 11, 9), mappedResponse.OEMFeedLastUpdated);

			Assert.AreEqual(1, mappedResponse.OpenRecalls.Count());

			Assert.AreEqual("14V400", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
			Assert.AreEqual("The timing of the key movement out of the \u001arun\u001a position, relative to the activation of the sensing algorithm of the crash event, may result in the airbags not deploying, increasing the potential for occupant injury in certain kinds of crashes.  Until the recall has been performed, it is very important that customers remove all items from their key ring, leaving only the vehicle key.  The key fob (if applicable), should also be removed from the key ring.", mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
			Assert.AreEqual("General Motors has decided that a defect which relates to motor vehicle safety exists in 2000-2005 MY Chevrolet Impala and Monte Carlo, 1997-2003 MY Chevrolet Malibu, 2004-2005 MY Chevrolet Malibu Classic, 1999-2004 MY Oldsmobile Alero, 1998-2002 MY Oldsmobile Intrigue, 1999-2005 MY Pontiac Grand Am, and 2004-2008 MY Pontiac Grand Prix vehicles.  If the key ring is carrying added weight and the vehicle goes off road or experiences some other jarring event, it may unintentionally move the key away from the “run” position.  If this occurs, engine power, power steering and power braking may be affected, increasing the risk of a crash.", mappedResponse.OpenRecalls.ElementAt(0).Description);
			Assert.AreEqual("Dealers are to install two key rings and an insert in the key slot or a cover over the key head on all ignition keys, free of charge.", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
			Assert.AreEqual("UNINTENDED IGNITION KEY ROTATION", mappedResponse.OpenRecalls.ElementAt(0).Title);
			Assert.AreEqual(new DateTime(2014, 8, 12), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.RecallIncomplete, mappedResponse.OpenRecalls.ElementAt(0).Status);
			Assert.AreEqual("N140350", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);
		}

		[Test]
		public void PontiacTest()
		{
			// https://recalls.gm.com/recall/services/1G2NF52E54C129032/recalls
			string serviceResponse =
				@"{""messages"":[],""serverErrorMsgs"":[],""data"":{""vin"":""1G2NF52E54C129032"",""year"":2004,""make"":""Pontiac"",""model"":""Grand Am"",""manufacturer_id"":1,""recalls_available"":true,""number_of_recalls"":1,""recalls"":[{""recall_type"":""ZPSR"",""nhtsa_recall_number"":""14V400"",""mfr_recall_number"":""N140350"",""recall_date"":""Aug 12, 2014"",""recall_title"":""UNINTENDED IGNITION KEY ROTATION"",""recall_description"":""General Motors has decided that a defect which relates to motor vehicle safety exists in 2000-2005 MY Chevrolet Impala and Monte Carlo, 1997-2003 MY Chevrolet Malibu, 2004-2005 MY Chevrolet Malibu Classic, 1999-2004 MY Oldsmobile Alero, 1998-2002 MY Oldsmobile Intrigue, 1999-2005 MY Pontiac Grand Am, and 2004-2008 MY Pontiac Grand Prix vehicles.  If the key ring is carrying added weight and the vehicle goes off road or experiences some other jarring event, it may unintentionally move the key away from the “run” position.  If this occurs, engine power, power steering and power braking may be affected, increasing the risk of a crash."",""safety_risk_description"":""The timing of the key movement out of the \u001Arun\u001A position, relative to the activation of the sensing algorithm of the crash event, may result in the airbags not deploying, increasing the potential for occupant injury in certain kinds of crashes.  Until the recall has been performed, it is very important that customers remove all items from their key ring, leaving only the vehicle key.  The key fob (if applicable), should also be removed from the key ring."",""remedy_description"":""Dealers are to install two key rings and an insert in the key slot or a cover over the key head on all ignition keys, free of charge."",""mfr_recall_status"":""11""}],""status"":true,""error_code"":0,""refresh_date"":""Nov 09, 2015""}}";

            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetGMMappingData(),
                ResponseText = serviceResponse
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("1G2NF52E54C129032", mappedResponse.VIN);
			Assert.AreEqual(2004, mappedResponse.Year);
			Assert.AreEqual("Pontiac", mappedResponse.Make);
			Assert.AreEqual("Grand Am", mappedResponse.Model);
			Assert.AreEqual(new DateTime(2015, 11, 9), mappedResponse.OEMFeedLastUpdated);

			Assert.AreEqual(1, mappedResponse.OpenRecalls.Count());

			Assert.AreEqual("14V400", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
			Assert.AreEqual("The timing of the key movement out of the \u001arun\u001a position, relative to the activation of the sensing algorithm of the crash event, may result in the airbags not deploying, increasing the potential for occupant injury in certain kinds of crashes.  Until the recall has been performed, it is very important that customers remove all items from their key ring, leaving only the vehicle key.  The key fob (if applicable), should also be removed from the key ring.", mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
			Assert.AreEqual("General Motors has decided that a defect which relates to motor vehicle safety exists in 2000-2005 MY Chevrolet Impala and Monte Carlo, 1997-2003 MY Chevrolet Malibu, 2004-2005 MY Chevrolet Malibu Classic, 1999-2004 MY Oldsmobile Alero, 1998-2002 MY Oldsmobile Intrigue, 1999-2005 MY Pontiac Grand Am, and 2004-2008 MY Pontiac Grand Prix vehicles.  If the key ring is carrying added weight and the vehicle goes off road or experiences some other jarring event, it may unintentionally move the key away from the “run” position.  If this occurs, engine power, power steering and power braking may be affected, increasing the risk of a crash.", mappedResponse.OpenRecalls.ElementAt(0).Description);
			Assert.AreEqual("Dealers are to install two key rings and an insert in the key slot or a cover over the key head on all ignition keys, free of charge.", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
			Assert.AreEqual("UNINTENDED IGNITION KEY ROTATION", mappedResponse.OpenRecalls.ElementAt(0).Title);
			Assert.AreEqual(new DateTime(2014, 8, 12), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.RecallIncomplete, mappedResponse.OpenRecalls.ElementAt(0).Status);
			Assert.AreEqual("N140350", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);
		}

		[Test]
		public void SaturnTest()
		{
			// https://recalls.gm.com/recall/services/1G8ZS57N88F113195/recalls
			string serviceResponse =
				@"{""messages"":[],""serverErrorMsgs"":[],""data"":{""vin"":""1G8ZS57N88F113195"",""year"":2008,""make"":""Saturn"",""model"":""Aura"",""manufacturer_id"":1,""recalls_available"":true,""number_of_recalls"":2,""recalls"":[{""recall_type"":""ZPSR"",""nhtsa_recall_number"":""14V252"",""mfr_recall_number"":""N130036"",""recall_date"":""May 16, 2014"",""recall_title"":""BRAKE LAMP MALFUNCTION"",""recall_description"":""General Motors has decided that a defect which relates to motor vehicle safety exists in 2004-2012 Malibu, 2004-2007 Malibu Maxx, 2005-2010 Pontiac G6, 2007-2010 Saturn Aura vehicles.  On these vehicles, over time an increased resistance can develop in the Body Control Module (BCM) connection system and result in voltage fluctuations or intermittency in the Brake Apply Sensor (BAS) circuit that can cause service brake lamp malfunction.  As a result, the service brake lamps may illuminate when the service brakes are not being applied, or may not illuminate when the service brakes are being applied.  Additionally, cruise control may not engage.  If cruise control is engaged, additional service brake pedal travel may be required to disengage it.  Service brake pedal application may not be required to move the shift lever out of PARK, or additional service brake pedal travel may be required to move the shift lever out of PARK.  Traction control, electronic stability control, and panic braking assist features, if equipped, may be disabled.  Service ESC and/or Traction Control tell-tales may illuminate with this condition."",""safety_risk_description"":""These conditions may increase the risk of a crash."",""remedy_description"":""Dealers are to attach the wiring harness to the BCM with a spacer, apply dielectric lubricant to both the BCM and harness connector and on the BAS and harness connector, and relearn the brake pedal home position free of charge."",""mfr_recall_status"":""11""},{""recall_type"":""ZPSR"",""nhtsa_recall_number"":""14V224"",""mfr_recall_number"":""N140152"",""recall_date"":""May 16, 2014"",""recall_title"":""TRANSMISSION SHIFT CABLE FRACTURE"",""recall_description"":""General Motors has decided that a defect which relates to motor vehicle safety exists in 2004-2008 MY Chevrolet Malibu (GMX380) and 2005-2008 MY Pontiac G6 with 4-speed transmissions (MN5); except for vehicles already repaired under safety recall 12106 12V-460 with labor code V2671 or V2672 and 2007 and some 2008 MY Saturn AURA vehicles equipped with a 4-speed automatic transmission (ME7/MN5).  These vehicles have a condition in which the transmission shift cable may fracture at any time."",""safety_risk_description"":""When the fracture occurs, the driver may not be able to select a different gear or place the transmission in park.  If the driver cannot place the vehicle in park, and exits the vehicle without applying the park brake, the vehicle could roll away and a crash could occur without prior warning."",""remedy_description"":""Dealers are to inspect the shift cable assembly, and install a shift cable clamshell, or replace the shift cable with a new cable kit and new bracket if necessary, free of charge."",""mfr_recall_status"":""11""}],""status"":true,""error_code"":0,""refresh_date"":""Nov 09, 2015""}}";

            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetGMMappingData(),
                ResponseText = serviceResponse
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("1G8ZS57N88F113195", mappedResponse.VIN);
			Assert.AreEqual(2008, mappedResponse.Year);
			Assert.AreEqual("Saturn", mappedResponse.Make);
			Assert.AreEqual("Aura", mappedResponse.Model);
			Assert.AreEqual(new DateTime(2015, 11, 9), mappedResponse.OEMFeedLastUpdated);

			Assert.AreEqual(2, mappedResponse.OpenRecalls.Count());

			Assert.AreEqual("14V252", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
			Assert.AreEqual("These conditions may increase the risk of a crash.", mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
			Assert.AreEqual("General Motors has decided that a defect which relates to motor vehicle safety exists in 2004-2012 Malibu, 2004-2007 Malibu Maxx, 2005-2010 Pontiac G6, 2007-2010 Saturn Aura vehicles.  On these vehicles, over time an increased resistance can develop in the Body Control Module (BCM) connection system and result in voltage fluctuations or intermittency in the Brake Apply Sensor (BAS) circuit that can cause service brake lamp malfunction.  As a result, the service brake lamps may illuminate when the service brakes are not being applied, or may not illuminate when the service brakes are being applied.  Additionally, cruise control may not engage.  If cruise control is engaged, additional service brake pedal travel may be required to disengage it.  Service brake pedal application may not be required to move the shift lever out of PARK, or additional service brake pedal travel may be required to move the shift lever out of PARK.  Traction control, electronic stability control, and panic braking assist features, if equipped, may be disabled.  Service ESC and/or Traction Control tell-tales may illuminate with this condition.", mappedResponse.OpenRecalls.ElementAt(0).Description);
			Assert.AreEqual("Dealers are to attach the wiring harness to the BCM with a spacer, apply dielectric lubricant to both the BCM and harness connector and on the BAS and harness connector, and relearn the brake pedal home position free of charge.", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
			Assert.AreEqual("BRAKE LAMP MALFUNCTION", mappedResponse.OpenRecalls.ElementAt(0).Title);
			Assert.AreEqual(new DateTime(2014, 5, 16), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.RecallIncomplete, mappedResponse.OpenRecalls.ElementAt(0).Status);
			Assert.AreEqual("N130036", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);

			Assert.AreEqual("14V224", mappedResponse.OpenRecalls.ElementAt(1).CampaignID);
			Assert.AreEqual("When the fracture occurs, the driver may not be able to select a different gear or place the transmission in park.  If the driver cannot place the vehicle in park, and exits the vehicle without applying the park brake, the vehicle could roll away and a crash could occur without prior warning.", mappedResponse.OpenRecalls.ElementAt(1).SafetyRisk);
			Assert.AreEqual("General Motors has decided that a defect which relates to motor vehicle safety exists in 2004-2008 MY Chevrolet Malibu (GMX380) and 2005-2008 MY Pontiac G6 with 4-speed transmissions (MN5); except for vehicles already repaired under safety recall 12106 12V-460 with labor code V2671 or V2672 and 2007 and some 2008 MY Saturn AURA vehicles equipped with a 4-speed automatic transmission (ME7/MN5).  These vehicles have a condition in which the transmission shift cable may fracture at any time.", mappedResponse.OpenRecalls.ElementAt(1).Description);
			Assert.AreEqual("Dealers are to inspect the shift cable assembly, and install a shift cable clamshell, or replace the shift cable with a new cable kit and new bracket if necessary, free of charge.", mappedResponse.OpenRecalls.ElementAt(1).RemedyDescription);
			Assert.AreEqual("TRANSMISSION SHIFT CABLE FRACTURE", mappedResponse.OpenRecalls.ElementAt(1).Title);
			Assert.AreEqual(new DateTime(2014, 5, 16), mappedResponse.OpenRecalls.ElementAt(1).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.RecallIncomplete, mappedResponse.OpenRecalls.ElementAt(1).Status);
			Assert.AreEqual("N140152", mappedResponse.OpenRecalls.ElementAt(1).OEMCampaignID);
		}

		[Test]
		public void SAABTest()
		{
			// https://recalls.gm.com/recall/services/5S3ET13M562801290/recalls
			string serviceResponse =
				@"{""messages"":[],""serverErrorMsgs"":[],""data"":{""vin"":""5S3ET13M562801290"",""year"":2006,""make"":""Saab"",""model"":""9-7X"",""manufacturer_id"":1,""recalls_available"":true,""number_of_recalls"":1,""recalls"":[{""recall_type"":""ZPSR"",""nhtsa_recall_number"":""12V406"",""mfr_recall_number"":""12V406"",""recall_date"":""Aug 02, 2013"",""recall_title"":""DRIVER DOOR SWITCH SHORT CIRCUIT"",""recall_description"":""General Motors (GM) is recalling certain model year 2006 Chevrolet Trailblazer EXT and GMC Envoy XL and 2006-2007 Chevrolet Trailblazer, GMC Envoy, Buick Rainier, Saab 9-7x, and Isuzu Ascender vehicles, originally sold or currently registered in Connecticut, Delaware, Illinois, Indiana, Iowa, Maine, Maryland, Massachusetts, Michigan, Minnesota, Missouri, New Hampshire, New Jersey, New York, Ohio, Pennsylvania, Rhode Island, Vermont, West Virginia, Wisconsin, and the District of Columbia. Fluid may enter the driver's door module, causing corrosion that could result in a short in the circuit board.  A short may cause the power door lock and power window switches to function intermittently or become inoperative.  The short may also cause overheating, which could melt components of the door module, producing odor, smoke, or a fire."",""safety_risk_description"":""A short in the circuit board could lead to a fire, increasing the risk of personal injury.  A fire could occur even while the vehicle is not in use.  As a precaution, owners are advised to park outside until the remedy has been made."",""remedy_description"":""Dealers will replace the door module free of charge."",""mfr_recall_status"":""11""}],""status"":true,""error_code"":0,""refresh_date"":""Nov 09, 2015""}}";

            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetGMMappingData(),
                ResponseText = serviceResponse
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("5S3ET13M562801290", mappedResponse.VIN);
			Assert.AreEqual(2006, mappedResponse.Year);
			Assert.AreEqual("Saab", mappedResponse.Make);
			Assert.AreEqual("9-7X", mappedResponse.Model);
			Assert.AreEqual(new DateTime(2015, 11, 9), mappedResponse.OEMFeedLastUpdated);

			Assert.AreEqual(1, mappedResponse.OpenRecalls.Count());

			Assert.AreEqual("12V406", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
			Assert.AreEqual("A short in the circuit board could lead to a fire, increasing the risk of personal injury.  A fire could occur even while the vehicle is not in use.  As a precaution, owners are advised to park outside until the remedy has been made.", mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
			Assert.AreEqual("General Motors (GM) is recalling certain model year 2006 Chevrolet Trailblazer EXT and GMC Envoy XL and 2006-2007 Chevrolet Trailblazer, GMC Envoy, Buick Rainier, Saab 9-7x, and Isuzu Ascender vehicles, originally sold or currently registered in Connecticut, Delaware, Illinois, Indiana, Iowa, Maine, Maryland, Massachusetts, Michigan, Minnesota, Missouri, New Hampshire, New Jersey, New York, Ohio, Pennsylvania, Rhode Island, Vermont, West Virginia, Wisconsin, and the District of Columbia. Fluid may enter the driver's door module, causing corrosion that could result in a short in the circuit board.  A short may cause the power door lock and power window switches to function intermittently or become inoperative.  The short may also cause overheating, which could melt components of the door module, producing odor, smoke, or a fire.", mappedResponse.OpenRecalls.ElementAt(0).Description);
			Assert.AreEqual("Dealers will replace the door module free of charge.", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
			Assert.AreEqual("DRIVER DOOR SWITCH SHORT CIRCUIT", mappedResponse.OpenRecalls.ElementAt(0).Title);
			Assert.AreEqual(new DateTime(2013, 8, 2), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.RecallIncomplete, mappedResponse.OpenRecalls.ElementAt(0).Status);
			Assert.AreEqual("12V406", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);
		}

		[Test]
		public void SubaruTest()
		{
			// http://www.subaru.com/services/vehicles/recalls/?vin=4S3BNEN60F3033379
			string serviceResponse =
				@"{""lastModified"":{""rawDate"":1446858092000,""fullDate"":""2015-11-06T20:01:32-05:00"",""year"":2015,""month"":""November"",""day"":6,""dayOfWeek"":""Friday""},""modelYear"":2015,""modelName"":""Legacy"",""trimName"":""3.6R Limited"",""vin"":""4S3BNEN60F3033379"",""recalls"":[{""lastModified"":{""rawDate"":1435636800000,""fullDate"":""2015-06-30T00:00:00-04:00"",""year"":2015,""month"":""June"",""day"":30,""dayOfWeek"":""Tuesday""},""date"":{""rawDate"":1433390400000,""fullDate"":""2015-06-04T00:00:00-04:00"",""year"":2015,""month"":""June"",""day"":4,""dayOfWeek"":""Thursday""},""code"":""WQS54"",""nhtsaRecallNumber"":""15V366000"",""procedureCode"":1,""nhtsaVehicleId"":""4s3bnen60f3033379wqs541"",""type"":""Safety"",""status"":""Open"",""description"":""Subaru of America, Inc. (Subaru) is recalling certain model year 2015 Legacy vehicles manufactured March 10, 2014, to April 16, 2015, Outback vehicles manufactured February 24, 2014, to April 16, 2015, Impreza vehicles manufactured September 9, 2014, to April 14, 2015, XV Crosstrek vehicles manufactured October 16, 2014, to April 15, 2015, and 2016 WRX vehicles manufactured March 23, 2015, and equipped with the Eyesight Driver Assist System. If the switch that activates the brake lights fails, the automatic pre-collision braking component of the driver assist system will not function."",""shortDescription"":""Driver Assist System Delay in Warning Indicator"",""safetyRiskDescription"":""If the automatic pre-collision braking system does not function as intended, the vehicle will not react to an obstacle in its path, increasing the risk of a crash."",""remedyDescription"":""Subaru will notify owners, and dealers will reprogram the driver assist system, free of charge. The manufacturer has not yet provided a notification schedule. Owners may contact Subaru customer service at 1-800-782-2783. Subaru's number for this recall is WQS-54.  Owners may also contact the National Highway Traffic Safety Administration Vehicle Safety Hotline at 1-888-327-4236 (TTY 1-800-424-9153), or go to www.safercar.gov."",""recallLetter"":""<html><body><P style=\""MARGIN: 0in 0in 0pt\"" class=MsoBodyText><B style=\""mso-bidi-font-weight: normal\""><SPAN style=\""FONT-FAMILY: 'Calibri','sans-serif'; FONT-SIZE: 10pt; mso-ascii-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-theme-font: minor-latin\"">Dear Subaru Owner:<?xml:namespace prefix = o ns = \""urn:schemas-microsoft-com:office:office\"" /><o:p></o:p></SPAN></B></P>\r\n<P style=\""MARGIN: 0in 0in 0pt\"" class=MsoBodyText><SPAN style=\""FONT-FAMILY: 'Calibri','sans-serif'; FONT-SIZE: 10pt; mso-ascii-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-theme-font: minor-latin\""><o:p>&nbsp;</o:p></SPAN></P>\r\n<P class=MsoBodyTextCxSpMiddle><SPAN style=\""FONT-FAMILY: 'Calibri','sans-serif'; FONT-SIZE: 10pt; mso-ascii-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-theme-font: minor-latin\"">This notice is sent to you in accordance with the National Traffic and Motor Vehicle Safety Act.<o:p></o:p></SPAN></P>\r\n<P class=MsoBodyTextCxSpLast><SPAN style=\""FONT-FAMILY: 'Calibri','sans-serif'; FONT-SIZE: 10pt; mso-ascii-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-theme-font: minor-latin\""><SPAN style=\""mso-tab-count: 1\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </SPAN><o:p></o:p></SPAN></P>\r\n<P style=\""MARGIN: 0in 0in 0pt\"" class=MsoNormal><FONT size=2><SPAN style=\""FONT-FAMILY: 'Calibri','sans-serif'; mso-ascii-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-theme-font: minor-latin\"">SUBARU OF AMERICA, INC. has decided that a defect, which relates to motor vehicle safety, exists in certain </SPAN><SPAN style=\""FONT-FAMILY: 'Calibri','sans-serif'; COLOR: black; mso-bidi-font-weight: bold; mso-bidi-font-family: 'Times New Roman'\"">2015 model year Legacy vehicles, 2015 model year Outback vehicles, 2015 model year Impreza vehicles, 2015 model year XV Crosstrek vehicles and 2016 model year WRX vehicles equipped with the EyeSight Driver Assist System.<o:p></o:p></SPAN></FONT></P>\r\n<P style=\""MARGIN: 0in 0in 0pt\"" class=MsoPlainText><SPAN style=\""FONT-FAMILY: 'Calibri','sans-serif'; mso-ascii-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-theme-font: minor-latin\""><o:p><FONT size=2>&nbsp;</FONT></o:p></SPAN></P>\r\n<P style=\""MARGIN: 0in 0in 0pt\"" class=MsoPlainText><SPAN style=\""FONT-FAMILY: 'Calibri','sans-serif'; mso-ascii-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-theme-font: minor-latin\""><FONT size=2>You received this notice because our records indicate that you currently own one of these vehicles.<o:p></o:p></FONT></SPAN></P>\r\n<P class=MsoBodyTextCxSpFirst><SPAN style=\""FONT-FAMILY: 'Calibri','sans-serif'; FONT-SIZE: 10pt; mso-ascii-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-theme-font: minor-latin\""><SPAN style=\""mso-spacerun: yes\"">&nbsp;</SPAN><SPAN style=\""mso-tab-count: 5\"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </SPAN><o:p></o:p></SPAN></P>\r\n<DIV style=\""BORDER-BOTTOM: windowtext 1pt solid; BORDER-LEFT: windowtext 1pt solid; PADDING-BOTTOM: 1pt; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; BORDER-TOP: windowtext 1pt solid; BORDER-RIGHT: windowtext 1pt solid; PADDING-TOP: 1pt; mso-element: para-border-div; mso-border-alt: solid windowtext .5pt\"">\r\n<P style=\""BORDER-BOTTOM: medium none; BORDER-LEFT: medium none; PADDING-BOTTOM: 0in; MARGIN: 0in 0in 0pt; PADDING-LEFT: 0in; PADDING-RIGHT: 0in; BORDER-TOP: medium none; BORDER-RIGHT: medium none; PADDING-TOP: 0in; mso-border-alt: solid windowtext .5pt; mso-padding-alt: 1.0pt 4.0pt 1.0pt 4.0pt\"" class=MsoBodyText><B style=\""mso-bidi-font-weight: normal\""><SPAN style=\""FONT-FAMILY: 'Calibri','sans-serif'; FONT-SIZE: 10pt; mso-ascii-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-theme-font: minor-latin\"">DESCRIPTION OF THE SAFETY DEFECT AND SAFETY HAZARD<o:p></o:p></SPAN></B></P>\r\n<P style=\""BORDER-BOTTOM: medium none; BORDER-LEFT: medium none; PADDING-BOTTOM: 0in; MARGIN: 0in 0in 0pt; PADDING-LEFT: 0in; PADDING-RIGHT: 0in; BORDER-TOP: medium none; BORDER-RIGHT: medium none; PADDING-TOP: 0in; mso-border-alt: solid windowtext .5pt; mso-padding-alt: 1.0pt 4.0pt 1.0pt 4.0pt\"" class=MsoNormal><SPAN style=\""FONT-FAMILY: 'Calibri','sans-serif'\""><FONT size=2>In the event of a brake lamp switch failure, the automatic pre-collision braking feature of your vehicle’s EyeSight Driver Assist System will not operate even though the \""Obstacle Detected\"" warning will be displayed to alert the driver that a frontal collision may be imminent.<SPAN style=\""mso-spacerun: yes\"">&nbsp; </SPAN>If the driver does not manually apply the brake pedal, when appropriate, in response to the warning, there may be an increased risk of a crash, possibly resulting in personal injury or death.<SPAN style=\""mso-spacerun: yes\"">&nbsp; </SPAN><o:p></o:p></FONT></SPAN></P></DIV>\r\n<P style=\""MARGIN: 0in 0in 0pt\"" class=MsoNormal><SPAN style=\""FONT-FAMILY: 'Calibri','sans-serif'\""><o:p><FONT size=2>&nbsp;</FONT></o:p></SPAN></P>\r\n<P style=\""MARGIN: 0in 0in 0pt\"" class=MsoNormal><SPAN style=\""FONT-FAMILY: 'Calibri','sans-serif'\""><FONT size=2>Upon noticing the “Obstacle Detected” warning, the driver should manually apply the brake pedal, when appropriate, to safely stop the vehicle. <SPAN style=\""mso-spacerun: yes\"">&nbsp;</SPAN><B style=\""mso-bidi-font-weight: normal\"">THE BRAKES IN YOUR VEHICLE WILL CONTINUE TO OPERATE NORMALLY DESPITE THE UNAVAILABILITY OF AUTOMATIC PRE-COLLISION BRAKING.</B><o:p></o:p></FONT></SPAN></P>\r\n<P style=\""MARGIN: 0in 0in 0pt\"" class=MsoNormal><FONT size=2><B style=\""mso-bidi-font-weight: normal\""><I style=\""mso-bidi-font-style: normal\""><SPAN style=\""FONT-FAMILY: 'Calibri','sans-serif'\""><SPAN style=\""mso-spacerun: yes\"">&nbsp;</SPAN></SPAN></I></B><SPAN style=\""FONT-FAMILY: 'Calibri','sans-serif'\""><o:p></o:p></SPAN></FONT></P>\r\n<P style=\""MARGIN: 0in 0in 0pt\"" class=MsoBodyText><B style=\""mso-bidi-font-weight: normal\""><SPAN style=\""FONT-FAMILY: 'Calibri','sans-serif'; FONT-SIZE: 10pt; mso-ascii-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-theme-font: minor-latin\"">REPAIR<o:p></o:p></SPAN></B></P>\r\n<P style=\""MARGIN: 0in 0in 0pt\"" class=MsoPlainText><SPAN style=\""FONT-FAMILY: 'Calibri','sans-serif'; mso-ascii-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-theme-font: minor-latin\""><FONT size=2>Subaru will reprogram the EyeSight Driver Assist System in your vehicle <SPAN style=\""mso-bidi-font-weight: bold\"">at no cost to you. <o:p></o:p></SPAN></FONT></SPAN></P>\r\n<P style=\""MARGIN: 0in 0in 0pt\"" class=MsoBodyText><B style=\""mso-bidi-font-weight: normal\""><SPAN style=\""FONT-FAMILY: 'Calibri','sans-serif'; FONT-SIZE: 10pt; mso-ascii-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-theme-font: minor-latin\""><o:p>&nbsp;</o:p></SPAN></B></P>\r\n<DIV style=\""BORDER-BOTTOM: windowtext 1pt solid; BORDER-LEFT: windowtext 1pt solid; PADDING-BOTTOM: 1pt; PADDING-LEFT: 4pt; PADDING-RIGHT: 4pt; BORDER-TOP: windowtext 1pt solid; BORDER-RIGHT: windowtext 1pt solid; PADDING-TOP: 1pt; mso-element: para-border-div; mso-border-alt: solid windowtext .5pt\"">\r\n<P style=\""BORDER-BOTTOM: medium none; BORDER-LEFT: medium none; PADDING-BOTTOM: 0in; MARGIN: 0in 0in 0pt; PADDING-LEFT: 0in; PADDING-RIGHT: 0in; BORDER-TOP: medium none; BORDER-RIGHT: medium none; PADDING-TOP: 0in; mso-border-alt: solid windowtext .5pt; mso-padding-alt: 1.0pt 4.0pt 1.0pt 4.0pt\"" class=MsoBodyText><B style=\""mso-bidi-font-weight: normal\""><SPAN style=\""FONT-FAMILY: 'Calibri','sans-serif'; FONT-SIZE: 10pt\"">WHAT YOU SHOULD DO<o:p></o:p></SPAN></B></P>\r\n<P style=\""BORDER-BOTTOM: medium none; BORDER-LEFT: medium none; PADDING-BOTTOM: 0in; MARGIN: 0in 0in 0pt; PADDING-LEFT: 0in; PADDING-RIGHT: 0in; BORDER-TOP: medium none; BORDER-RIGHT: medium none; PADDING-TOP: 0in; mso-border-alt: solid windowtext .5pt; mso-padding-alt: 1.0pt 4.0pt 1.0pt 4.0pt\"" class=MsoBodyText><SPAN style=\""FONT-FAMILY: 'Calibri','sans-serif'; FONT-SIZE: 10pt\"">You should immediately contact your Subaru retailer (dealer) for an appointment to have the EyeSight Driver Assist System in your vehicle reprogrammed.<o:p></o:p></SPAN></P>\r\n<P style=\""BORDER-BOTTOM: medium none; BORDER-LEFT: medium none; PADDING-BOTTOM: 0in; MARGIN: 0in 0in 0pt; PADDING-LEFT: 0in; PADDING-RIGHT: 0in; BORDER-TOP: medium none; BORDER-RIGHT: medium none; PADDING-TOP: 0in; mso-border-alt: solid windowtext .5pt; mso-padding-alt: 1.0pt 4.0pt 1.0pt 4.0pt\"" class=MsoBodyText><SPAN style=\""FONT-FAMILY: 'Calibri','sans-serif'; FONT-SIZE: 10pt\""><o:p>&nbsp;</o:p></SPAN></P>\r\n<P style=\""BORDER-BOTTOM: medium none; BORDER-LEFT: medium none; PADDING-BOTTOM: 0in; MARGIN: 0in 0in 0pt; PADDING-LEFT: 0in; PADDING-RIGHT: 0in; BORDER-TOP: medium none; BORDER-RIGHT: medium none; PADDING-TOP: 0in; mso-border-alt: solid windowtext .5pt; mso-padding-alt: 1.0pt 4.0pt 1.0pt 4.0pt\"" class=MsoBodyText><B style=\""mso-bidi-font-weight: normal\""><I style=\""mso-bidi-font-style: normal\""><SPAN style=\""FONT-FAMILY: 'Calibri','sans-serif'; mso-bidi-font-size: 11.0pt\"">As a precaution, until this repair is performed, upon noticing the “Obstacle Detected” warning, you should manually apply the brake pedal to safely stop the vehicle.<SPAN style=\""mso-spacerun: yes\"">&nbsp; </SPAN>If you do not, the EyeSight Driver Assist System will not automatically apply the brakes for you. <o:p></o:p></SPAN></I></B></P>\r\n<P style=\""BORDER-BOTTOM: medium none; BORDER-LEFT: medium none; PADDING-BOTTOM: 0in; MARGIN: 0in 0in 0pt; PADDING-LEFT: 0in; PADDING-RIGHT: 0in; BORDER-TOP: medium none; BORDER-RIGHT: medium none; PADDING-TOP: 0in; mso-border-alt: solid windowtext .5pt; mso-padding-alt: 1.0pt 4.0pt 1.0pt 4.0pt\"" class=MsoBodyText><B style=\""mso-bidi-font-weight: normal\""><I style=\""mso-bidi-font-style: normal\""><SPAN style=\""FONT-FAMILY: 'Calibri','sans-serif'; mso-bidi-font-size: 11.0pt\""><o:p>&nbsp;</o:p></SPAN></I></B></P>\r\n<P style=\""BORDER-BOTTOM: medium none; BORDER-LEFT: medium none; PADDING-BOTTOM: 0in; MARGIN: 0in 0in 0pt; PADDING-LEFT: 0in; PADDING-RIGHT: 0in; BORDER-TOP: medium none; BORDER-RIGHT: medium none; PADDING-TOP: 0in; mso-border-alt: solid windowtext .5pt; mso-padding-alt: 1.0pt 4.0pt 1.0pt 4.0pt\"" class=MsoBodyText><B style=\""mso-bidi-font-weight: normal\""><I style=\""mso-bidi-font-style: normal\""><SPAN style=\""FONT-FAMILY: 'Calibri','sans-serif'; mso-bidi-font-size: 11.0pt\"">You should also avoid using the EyeSight Driver Assist System’s adaptive cruise control feature until this repair is performed.<SPAN style=\""mso-spacerun: yes\"">&nbsp; </SPAN>Adaptive cruise control will not automatically apply braking when the vehicle you are following reduces its speed. <SPAN style=\""mso-spacerun: yes\"">&nbsp;</SPAN>Instead it will operate in a manner similar to ordinary cruise control, requiring you to manually apply the brake pedal to ensure that there is sufficient following distance.<o:p></o:p></SPAN></I></B></P></DIV>\r\n<P style=\""MARGIN: 0in 0in 0pt 0.5in\"" class=MsoBodyText><B style=\""mso-bidi-font-weight: normal\""><SPAN style=\""FONT-FAMILY: 'Calibri','sans-serif'; mso-ascii-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-theme-font: minor-latin\""><o:p>&nbsp;</o:p></SPAN></B></P>\r\n<P style=\""MARGIN: 0in 0in 0pt\"" class=MsoBodyText><B style=\""mso-bidi-font-weight: normal\""><SPAN style=\""FONT-""}]}";

            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetSubaruMappingData(),
                ResponseText = serviceResponse
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("4S3BNEN60F3033379", mappedResponse.VIN);
			Assert.AreEqual(2015, mappedResponse.Year);
			Assert.AreEqual("Subaru", mappedResponse.Make);
			Assert.AreEqual("Legacy", mappedResponse.Model);
			Assert.AreEqual(new DateTime(2015, 11, 6), mappedResponse.OEMFeedLastUpdated);

			Assert.AreEqual(1, mappedResponse.OpenRecalls.Count());

			Assert.AreEqual("15V366000", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
			Assert.AreEqual("If the automatic pre-collision braking system does not function as intended, the vehicle will not react to an obstacle in its path, increasing the risk of a crash.", mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
			Assert.AreEqual("Subaru of America, Inc. (Subaru) is recalling certain model year 2015 Legacy vehicles manufactured March 10, 2014, to April 16, 2015, Outback vehicles manufactured February 24, 2014, to April 16, 2015, Impreza vehicles manufactured September 9, 2014, to April 14, 2015, XV Crosstrek vehicles manufactured October 16, 2014, to April 15, 2015, and 2016 WRX vehicles manufactured March 23, 2015, and equipped with the Eyesight Driver Assist System. If the switch that activates the brake lights fails, the automatic pre-collision braking component of the driver assist system will not function.", mappedResponse.OpenRecalls.ElementAt(0).Description);
			Assert.AreEqual("Subaru will notify owners, and dealers will reprogram the driver assist system, free of charge. The manufacturer has not yet provided a notification schedule. Owners may contact Subaru customer service at 1-800-782-2783. Subaru's number for this recall is WQS-54.  Owners may also contact the National Highway Traffic Safety Administration Vehicle Safety Hotline at 1-888-327-4236 (TTY 1-800-424-9153), or go to www.safercar.gov.", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
			Assert.AreEqual("Driver Assist System Delay in Warning Indicator", mappedResponse.OpenRecalls.ElementAt(0).Title);
			Assert.AreEqual(new DateTime(2015, 6, 4), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.RecallIncomplete, mappedResponse.OpenRecalls.ElementAt(0).Status);
			Assert.AreEqual("WQS54", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);
		}

		[Test]
		public void VolkswagenTest()
		{
			// https://www.vw.com/s2f/vwrecall-nhtsa2/vin/1VWAH7A36DC036520
			string serviceResponse =
				@"{""vin"":""1VWAH7A36DC036520"",""status"":true,""year"":2013,""make"":""VW"",""model"":""PASSAT S 2.5L Manual Federal"",""manufacturerId"":1,""isRecallsAvailable"":true,""numberOfRecalls"":1,""refreshDate"":""Nov 09, 2015"",""recalls"":[{""vwgoaActionTitle"":""Steering Wheel Clock Spring"",""mfrRecallNumber"":""69L2"",""nhtsaRecallNumber"":""15V-483"",""recallDate"":""Jul 30, 2015"",""recallDescription"":""Debris may contaminate the airbag clock spring (a spiral wound, flat cable that keeps the airbag powered while the steering wheel is being turned). This contamination may tear the cable and result in a loss of electrical connection to the driver's frontal airbag. A loss of electrical connection to the driver's frontal airbag will prevent the airbag from deploying in the event of a vehicle crash, increasing the risk of injury."",""safetyRiskDescription"":""A loss of electrical connection to the driver's frontal airbag will prevent the airbag from deploying in the event of a vehicle crash, increasing the risk of injury.\n\nIf the airbag monitoring indicator light on a vehicle affected by this recall comes on, the customer should immediately contact the nearest authorized dealer or qualified workshop in order to have the vehicle inspected/repaired. Customers are advised to see the vehicle OwnerÃ‚Â’s Manual for more information about the airbag system. "",""remedyDescription"":""Repair not yet available."",""mfrRecallStatus"":12,""mfrNotes"":"" "",""refreshDate"":""Nov 09, 2015"",""vwgoaActionType"":""SAFETY""}]}";

            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetVolkswagenMappingData(),
                ResponseText = serviceResponse
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("1VWAH7A36DC036520", mappedResponse.VIN);
			Assert.AreEqual(2013, mappedResponse.Year);
			Assert.AreEqual("VW", mappedResponse.Make);
			Assert.AreEqual("PASSAT S 2.5L Manual Federal", mappedResponse.Model);
			Assert.AreEqual(new DateTime(2015, 11, 9), mappedResponse.OEMFeedLastUpdated);

			Assert.AreEqual(1, mappedResponse.OpenRecalls.Count());

			Assert.AreEqual("15V-483", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
			Assert.AreEqual("A loss of electrical connection to the driver's frontal airbag will prevent the airbag from deploying in the event of a vehicle crash, increasing the risk of injury.\n\nIf the airbag monitoring indicator light on a vehicle affected by this recall comes on, the customer should immediately contact the nearest authorized dealer or qualified workshop in order to have the vehicle inspected/repaired. Customers are advised to see the vehicle OwnerÃ‚Â’s Manual for more information about the airbag system.", mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
			Assert.AreEqual("Debris may contaminate the airbag clock spring (a spiral wound, flat cable that keeps the airbag powered while the steering wheel is being turned). This contamination may tear the cable and result in a loss of electrical connection to the driver's frontal airbag. A loss of electrical connection to the driver's frontal airbag will prevent the airbag from deploying in the event of a vehicle crash, increasing the risk of injury.", mappedResponse.OpenRecalls.ElementAt(0).Description);
			Assert.AreEqual("Repair not yet available.", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
			Assert.AreEqual("Steering Wheel Clock Spring", mappedResponse.OpenRecalls.ElementAt(0).Title);
			Assert.AreEqual(new DateTime(2015, 7, 30), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.RecallIncomplete_RemedyNotYetAvailable, mappedResponse.OpenRecalls.ElementAt(0).Status);
			Assert.AreEqual("69L2", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);
		}

		[Test]
		public void BMWMTest()
		{
			// http://www.bmwusa.com/Services/VinRecallService.svc/GetRecallCampaignsForVin/WBANF73597CU22309
			string serviceResponse =
				@"{""Status"":0,""StatusData"":{""ContractValidationResults"":[],""ExceptionDetail"":null},""ViewModel"":{""Error"":0,""Make"":""BMW"",""Model"":""530xi Sedan"",""RecallCampaigns"":[{""ValidationRules"":""{\""rules\"":{},\""messages\"":{}}"",""ViewModelValidation"":{""IsValid"":false,""PropertyValidations"":[]},""Description"":""On certain model year 2006 325i, 325xi, 330i, 330xi sedans and 325xi Sports Wagons, model year 2007 328i, 328xi, 335i, 335xi sedans and 328i and 328xi Sports Wagons, model year 2006 and 2007 525i, 525xi, 530i, 530xi, 550i sedans and 530xi Sports Wagons, model year 2006 760i sedans, model year 2006 and 2007 750i, 750Li, and 760Li sedans, and model year 2006 X5 Sports Activity Vehicles (SAV), equipped with certain seat types, the front passenger seat occupant detection mat that determines if and how the front passenger air bag should deploy in a crash may fatigue and develop cracks which could lead to a system failure."",""HasRecallLetter"":true,""ManufacturerNotes"":""For additional information or assistance, please contact Customer Relations and Services at 1-800-525-7417 or email at CustomerRelations@bmwusa.com."",""ManufacturerRecallNumber"":""NA"",""ManufacturerRecallStatus"":11,""RecallDate"":""November 8, 2013"",""RecallNumber"":""13V-564"",""RefreshDate"":""November 10, 2015"",""RemedyDescription"":""Your BMW center will repair the occupant detection mat to eliminate the possibility that it may crack.  Additionally, owners of certain model year 2006-2007 3 Series with standard seats, 5 Series with comfort seats, and Z4 models, will receive an extended warranty on their front passenger seat occupant detection mat to address this condition."",""SafetyRiskDescription"":""In the event of a crash, the front passenger air bag would be deactivated, increasing the risk of injury."",""SortOrder"":0,""Title"":""FRONT PASSENGER AIR BAG SEAT OCCUPANCY SENSOR MAT""}],""RefreshDate"":""November 10, 2015"",""Vin"":""WBANF73597CU22309"",""Year"":2007}}";

            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetBMWMappingData(),
                ResponseText = serviceResponse
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("WBANF73597CU22309", mappedResponse.VIN);
			Assert.AreEqual(2007, mappedResponse.Year);
			Assert.AreEqual("BMW", mappedResponse.Make);
			Assert.AreEqual("530xi Sedan", mappedResponse.Model);
			Assert.AreEqual(new DateTime(2015, 11, 10), mappedResponse.OEMFeedLastUpdated);

			Assert.AreEqual(1, mappedResponse.OpenRecalls.Count());

			Assert.AreEqual("13V-564", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
			Assert.AreEqual("In the event of a crash, the front passenger air bag would be deactivated, increasing the risk of injury.", mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
			Assert.AreEqual("On certain model year 2006 325i, 325xi, 330i, 330xi sedans and 325xi Sports Wagons, model year 2007 328i, 328xi, 335i, 335xi sedans and 328i and 328xi Sports Wagons, model year 2006 and 2007 525i, 525xi, 530i, 530xi, 550i sedans and 530xi Sports Wagons, model year 2006 760i sedans, model year 2006 and 2007 750i, 750Li, and 760Li sedans, and model year 2006 X5 Sports Activity Vehicles (SAV), equipped with certain seat types, the front passenger seat occupant detection mat that determines if and how the front passenger air bag should deploy in a crash may fatigue and develop cracks which could lead to a system failure.", mappedResponse.OpenRecalls.ElementAt(0).Description);
			Assert.AreEqual("Your BMW center will repair the occupant detection mat to eliminate the possibility that it may crack.  Additionally, owners of certain model year 2006-2007 3 Series with standard seats, 5 Series with comfort seats, and Z4 models, will receive an extended warranty on their front passenger seat occupant detection mat to address this condition.", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
			Assert.AreEqual("FRONT PASSENGER AIR BAG SEAT OCCUPANCY SENSOR MAT", mappedResponse.OpenRecalls.ElementAt(0).Title);
			Assert.AreEqual(new DateTime(2013, 11, 8), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.RecallIncomplete, mappedResponse.OpenRecalls.ElementAt(0).Status);
			Assert.AreEqual("NA", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);
		}


		[Test]
		public void MiniTest()
		{
			// http://www.miniusa.com/api/safetyRecall.action?vin=WMWRH33526TK57688
			string serviceResponse =
				@"{""vin"":""WMWRH33526TK57688"",""status"":true,""year"":2006,""make"":""MINI"",""model"":""Cooper S Convertible"",""manufacturer_id"":15,""recalls_available"":true,""number_of_recalls"":1,""refresh_date"":""Nov 10, 2015"",""recalls"":[{""nhtsa_recall_number"":""15V-205"",""mfr_recall_number"":""NA"",""recall_date"":""APR 03, 2015"",""recall_title"":""FRONT PASSENGER SEAT OCCUPANCY SENSOR MAT"",""recall_description"":""On certain model year 2006-2006 MINI Cooper and Cooper S and certain model year 2005-2008 MINI Cooper and Cooper S Convertibles, the front passenger seat occupant detection mat sensor may not function correctly, due to several manufacturing, installation and field exposure issues."",""safety_risk_description"":""In the event of a crash, the front passenger air bag may not activate when the seat is occupied, increasing the risk of injury."",""remedy_description"":""Your MINI dealer will replace the front passenger seat occupant detection mat. "",""mfr_recall_status"":11,""mfr_notes"":""For additional information or assistance, please contact Customer Relations and Services at 1-866-825-1525 or email at MINI.Assistance@askminiusa.com."",""refresh_date"":""NOV 10, 2015"",""owner_notification_letter_available"":false}]}";

            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetMiniData(),
                ResponseText = serviceResponse
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("WMWRH33526TK57688", mappedResponse.VIN);
			Assert.AreEqual(2006, mappedResponse.Year);
			Assert.AreEqual("MINI", mappedResponse.Make);
			Assert.AreEqual("Cooper S Convertible", mappedResponse.Model);
			Assert.AreEqual(new DateTime(2015, 11, 10), mappedResponse.OEMFeedLastUpdated);

			Assert.AreEqual(1, mappedResponse.OpenRecalls.Count());

			Assert.AreEqual("15V-205", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
			Assert.AreEqual("In the event of a crash, the front passenger air bag may not activate when the seat is occupied, increasing the risk of injury.", mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
			Assert.AreEqual("On certain model year 2006-2006 MINI Cooper and Cooper S and certain model year 2005-2008 MINI Cooper and Cooper S Convertibles, the front passenger seat occupant detection mat sensor may not function correctly, due to several manufacturing, installation and field exposure issues.", mappedResponse.OpenRecalls.ElementAt(0).Description);
			Assert.AreEqual("Your MINI dealer will replace the front passenger seat occupant detection mat.", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
			Assert.AreEqual("FRONT PASSENGER SEAT OCCUPANCY SENSOR MAT", mappedResponse.OpenRecalls.ElementAt(0).Title);
			Assert.AreEqual(new DateTime(2015, 4, 3), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.RecallIncomplete, mappedResponse.OpenRecalls.ElementAt(0).Status);
			Assert.AreEqual("NA", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);
		}

		[Test]
		public void InvalidVIN_AcuraTest()
		{
			// http://owners.acura.com/Recalls/GetRecallsByVin/11111111111111111/true
			string serviceResponse = @"{""LastUpdated"":""Dec 01, 2015"",""Warning"":true,""WarningCode"":""1000"",""WarningText"":""Invalid VIN"",""DivisionMismatch"":false,""VIN"":""11111111111111111"",""Make"":null,""ModelYear"":null,""DivisionCd"":null,""ModelGroupId"":null,""ModelGroupName"":null,""RecallsUrl"":null,""SaveVinUrl"":null,""IsLoggedIn"":false,""CampaignTypes"":null}";

			Exception e = null;
			try
			{
                MappingContext context = new MappingContext()
                {
                    MappingData = SeedDataHelper.GetHondaAcuraMappingData(),
                    ResponseText = serviceResponse
                };

                VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);
            }
			catch (Exception ex)
			{
				e = ex;
			}
			Assert.AreEqual(typeof (OEMVINNotFoundException), e.GetType());

		}

		[Test]
		public void InvalidVIN_AudiTest()
		{
			// http://web.audiusa.com/audirecall/vin/11111111111111111
			string serviceResponse = @"{""vin"":""11111111111111111"",""status"":false,""errorCode"":91}";

			Exception e = null;
			try
			{
                MappingContext context = new MappingContext()
                {
                    MappingData = SeedDataHelper.GetAudiMappingData(),
                    ResponseText = serviceResponse
                };

                VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);
            }
			catch (Exception ex)
			{
				e = ex;
			}
			Assert.AreEqual(typeof(OEMErrorException), e.GetType());
		}

		[Test]
		public void InvalidVIN_BuickTest()
		{
			// https://recalls.gm.com/recall/services/11111111111111111/recalls
			string serviceResponse =
				@"{""messages"":[""VEHICLE_INVALID_VIN""],""serverErrorMsgs"":[],""data"":null}";

			Exception e = null;
			try
			{
                MappingContext context = new MappingContext()
                {
                    MappingData = SeedDataHelper.GetGMMappingData(),
                    ResponseText = serviceResponse
                };

                VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);
            }
			catch (Exception ex)
			{
				e = ex;
			}
			Assert.AreEqual(typeof(OEMVINNotFoundException), e.GetType());
			
		}

		[Test]
		public void InvalidVIN_InfinitiTest()
		{
			// http://www.infinitiusa.com/dealercenter/api/recalls?vin=11111111111111111
			string serviceResponse =
				@"{""make"":"""",""modelYear"":0,""recalls"":[],""vehicleName"":"""",""vin"":""11111111111111111""}";

			Exception e = null;
			try
			{
                MappingContext context = new MappingContext()
                {
                    MappingData = SeedDataHelper.GetInfinitiMappingData(),
                    ResponseText = serviceResponse
                };

                VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);
            }
			catch (Exception ex)
			{
				e = ex;
			}
			Assert.AreEqual(typeof(OEMErrorException), e.GetType());
		}

		[Test]
		public void InvalidVIN_JaguarTest()
		{
			// http://www.jaguarusa.com/owners/vin-recall.html?view=vinRecallQuery&vin=11111111111111111
			string serviceResponse =
				@"{""errorMessage"":""Please check your details and try again."",""error"":400,""errorTitle"":""Sorry, that is not a valid VIN."",""vin"":""11111111111111111""}";

			Exception e = null;
			try
			{
                MappingContext context = new MappingContext()
                {
                    MappingData = SeedDataHelper.GetJaguarMappingData(),
                    ResponseText = serviceResponse
                };

                VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);
            }
			catch (Exception ex)
			{
				e = ex;
			}
			Assert.AreEqual(typeof(OEMVINNotFoundException), e.GetType());
		}

		[Test]
		public void InvalidVIN_LandRoverTest()
		{
			// http://www.landroverusa.com/ownership/product-recall-search.html?view=vinRecallQuery&vin=11111111111111111
			string serviceResponse =
				@"{""errorMessage"":""Please check your details and try again."",""error"":400,""errorTitle"":""Sorry, that is not a valid VIN."",""vin"":""11111111111111111""}";

			Exception e = null;
			try
			{
                MappingContext context = new MappingContext()
                {
                    MappingData = SeedDataHelper.GetLandRoverMappingData(),
                    ResponseText = serviceResponse
                };

                VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);
            }
			catch (Exception ex)
			{
				e = ex;
			}
			Assert.AreEqual(typeof(OEMVINNotFoundException), e.GetType());
		}

		[Test]
		public void InvalidVIN_KiaTest()
		{
			// http://www.kia.com/us/en/data/owners/recalls/search/11111111111111111
			string serviceResponse = @"{""statusCode"":500,""statusDesc"":""INVALID_VIN"",""result"":{}}";

			Exception e = null;
			try
			{
                MappingContext context = new MappingContext()
                {
                    MappingData = SeedDataHelper.GetKiaMappingData(),
                    ResponseText = serviceResponse
                };

                VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);
            }
			catch (Exception ex)
			{
				e = ex;
			}
			Assert.AreEqual(typeof(OEMVINNotFoundException), e.GetType());
		}

		[Test]
		public void InvalidVIN_FordTest()
		{
			// https://owner.ford.com/sharedServices/recalls/query.do?country=USA&language=EN&vin=11111111111111111
			string serviceResponse = @"{""lang_script_c"":"""",""return_status"":{""description"":""Input parameter invalid"",""code"":""01""},""fsa_items"":{""count"":0},""lang_iso_c"":""EN"",""lang_region_c"":"""",""stpr_state_c"":""  "",""country_iso3_c"":""USA"",""vin"":11111111111111111}";

			Exception e = null;
			try
			{
                MappingContext context = new MappingContext()
                {
                    MappingData = SeedDataHelper.GetFordLincolnMercuryData(),
                    ResponseText = serviceResponse
                };

                VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);
            }
			catch (Exception ex)
			{
				e = ex;
			}
			Assert.AreEqual(typeof(OEMVINNotFoundException), e.GetType());

		}

		[Test]
		public void InvalidVIN_MitsubishiTest()
		{
			// https://www.mitsubishicars.com/rs/warranty?vin=11111111111111111
			string serviceResponse = @"{""vehicleInfo"":null,""ownerWarrantyDetails"":null,""subsequentOwnerWarrantyDetails"":null,""openedRecalls"":null,""completedRecalls"":null,""promoMessageDetails"":null,""serviceMessageDetails"":null,""warExtMessageDetails"":null,""customer"":null,""claimDetails"":null,""nhtsaVehicle"":null,""nhtsaOpenRecalls"":null,""success"":false,""statusCode"":""500"",""statusMessage"":""VIN DOES NOT EXIST                                                           ""}";

			Exception e = null;
			try
			{
                MappingContext context = new MappingContext()
                {
                    MappingData = SeedDataHelper.GetMitsubishiData(),
                    ResponseText = serviceResponse
                };

                VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);
            }
			catch (Exception ex)
			{
				e = ex;
			}
			Assert.AreEqual(typeof(OEMVINNotFoundException), e.GetType());
		}

		[Test]
		public void InvalidVIN_NissanTest()
		{
			// http://www.nissanusa.com/dealercenter/api/recalls?vin=11111111111111111
			string serviceResponse = @"{""make"":"""",""modelYear"":0,""recalls"":[],""vehicleName"":"""",""vin"":""11111111111111111""}";

			Exception e = null;
			try
			{
                MappingContext context = new MappingContext()
                {
                    MappingData = SeedDataHelper.GetNissanData(),
                    ResponseText = serviceResponse
                };

                VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);
            }
			catch (Exception ex)
			{
				e = ex;
			}
			Assert.AreEqual(typeof(OEMErrorException), e.GetType());

		}

		//[Test]
		//public void InvalidVIN_SubaruTest()
		//{
		//	ExternalRecallServiceMappingData data = SeedDataHelper.GetSubaruMappingData();

		//	// http://www.subaru.com/services/vehicles/recalls/?vin=11111111111111111
		//	string serviceResponse =
		//		@"";
		//	int statusCode = 404;

		//	VINResponse mappedResponse = _responseMappingService.MapResponse(serviceResponse, data);

		//}

		[Test]
		public void InvalidVIN_VolkswagenTest()
		{
			// https://www.vw.com/s2f/vwrecall-nhtsa2/vin/11111111111111111
			string serviceResponse =
				@"{""vin"":""11111111111111111"",""status"":false,""errorCode"":91}";

			Exception e = null;
			try
			{
                MappingContext context = new MappingContext()
                {
                    MappingData = SeedDataHelper.GetVolkswagenMappingData(),
                    ResponseText = serviceResponse
                };

                VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);
            }
			catch (Exception ex)
			{
				e = ex;
			}
			Assert.AreEqual(typeof(OEMErrorException), e.GetType());
		}

		[Test]
		public void InvalidVIN_BMWMTest()
		{
			// http://www.bmwusa.com/Services/VinRecallService.svc/GetRecallCampaignsForVin/11111111111111111
			string serviceResponse =
				@"{""Status"":0,""StatusData"":{""ContractValidationResults"":[],""ExceptionDetail"":null},""ViewModel"":{""Error"":2,""Make"":null,""Model"":null,""RecallCampaigns"":[],""RefreshDate"":null,""Vin"":null,""Year"":null}}";

			Exception e = null;
			try
			{
                MappingContext context = new MappingContext()
                {
                    MappingData = SeedDataHelper.GetBMWMappingData(),
                    ResponseText = serviceResponse
                };

                VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);
            }
			catch (Exception ex)
			{
				e = ex;
			}
			Assert.AreEqual(typeof(OEMErrorException), e.GetType());
		}


		[Test]
		public void InvalidVIN_MiniTest()
		{
			// http://www.miniusa.com/api/safetyRecall.action?vin=11111111111111111
			string serviceResponse =
				@"{""vin"":""11111111111111111"",""status"":false,""error_code"":91}";

			Exception e = null;
			try
			{
                MappingContext context = new MappingContext()
                {
                    MappingData = SeedDataHelper.GetMiniData(),
                    ResponseText = serviceResponse
                };

                VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);
            }
			catch (Exception ex)
			{
				e = ex;
			}
			Assert.AreEqual(typeof(OEMErrorException), e.GetType());

		}

		/*template
			Assert.AreEqual("JN1BV7AP5EM686084", mappedResponse.VIN);
			Assert.AreEqual("", mappedResponse.Year);
			Assert.AreEqual("", mappedResponse.Make);
			Assert.AreEqual("", mappedResponse.Model);
			Assert.AreEqual(new DateTime(2015, 11, 9), mappedResponse.LastUpdated);

			Assert.AreEqual(1, mappedResponse.OpenRecalls.Count());

			Assert.AreEqual("", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
			Assert.AreEqual("", mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
			Assert.AreEqual("", mappedResponse.OpenRecalls.ElementAt(0).Description);
			Assert.AreEqual("", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
			Assert.AreEqual("", mappedResponse.OpenRecalls.ElementAt(0).Title);
			Assert.AreEqual(new DateTime(1999, 3, 30), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
			Assert.AreEqual(RecallStatusEnum.Unknown, mappedResponse.OpenRecalls.ElementAt(0).Status);
		 */
	}
}
