﻿using System;
using System.Linq;
using Homenet.RecallSplat.Core.Interfaces;
using Homenet.RecallSplat.Core.Model;
using Homenet.RecallSplat.Core.Model.Exceptions;
using Homenet.RecallSplat.Core.Services;
using Homenet.RecallSplat.DependencyInjection;
using Homenet.RecallSplat.Framework.Bootstrapper;
using Homenet.RecallSplat.Setup;
using NUnit.Framework;

namespace Homenet.RecallSplat.Tests
{
    public class HtmlResponseMapperTests
    {
        private IResponseMappingService _responseMappingService;

        [TestFixtureSetUp]
        public void TestFixtureSetup()
        {
            RecallSplatKernelConfigurator.ConfigureDefaults();

            _responseMappingService = RecallSplatKernel.GetByName<IResponseMappingService>("HtmlPath");
        }

        [Test]
        public void MoparTest()
        {
            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetMoparData(),
                ResponseText = Properties.Resources.MoparResponse1
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("1C6RD6FT0CS259510", mappedResponse.VIN);
            Assert.AreEqual(2012, mappedResponse.Year);
            Assert.AreEqual("DODGE", mappedResponse.Make);
            Assert.AreEqual("RAM ST 4X2 1500 QUAD CAB PICKUP", mappedResponse.Model);
            Assert.AreEqual(new DateTime(2016, 4, 29), mappedResponse.OEMFeedLastUpdated);

            Assert.AreEqual(1, mappedResponse.OpenRecalls.Count());

            Assert.AreEqual("15V-459", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
            Assert.AreEqual("THE STEERING WHEEL WIRE HARNESS MAY BECOME CHAFFED BY THE DRIVER AIRBAG MODULE RETAINER SPRING ENDS. A CHAFFED STEERING WHEEL WIRE HARNESS COULD CAUSE AN ELECTRICAL SHORT AND/OR AN INADVERTENT DRIVER AIRBAG DEPLOYMENT. INADVERTENT DRIVER AIRBAG DEPLOYMENT, DURING CERTAIN DRIVING CONDITIONS, MAY INCREASE THE RISK OF A CRASH AND/OR VEHICLE OCCUPANT INJURY.", mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
            Assert.AreEqual(null, mappedResponse.OpenRecalls.ElementAt(0).Description);
            Assert.AreEqual("THE STEERING WHEEL WIRE HARNESS WILL BE INSPECTED, AND IF NEEDED REPAIRED. THE STEERING WHEEL WIRE HARNESS WILL BE REROUTED AND SECURED TO PREVENT WIRE CHAFFING AND PROTECTIVE CAPS WILL BE INSTALLED ONTO THE AIRBAG RETAINER SPRING ENDS.", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
            Assert.AreEqual(new DateTime(2015, 7, 21), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
            Assert.AreEqual(RecallStatusEnum.RecallIncomplete, mappedResponse.OpenRecalls.ElementAt(0).Status);
            Assert.AreEqual("R36", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);
        }

        [Test]
        public void MoparNoneTest()
        {
            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetMoparData(),
                ResponseText = Properties.Resources.MoparResponse2
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("1B3HB48B87D542087", mappedResponse.VIN);
            Assert.AreEqual(2007, mappedResponse.Year);
            Assert.AreEqual("DODGE", mappedResponse.Make);
            Assert.AreEqual("CALIBER  SXT FOUR-DOOR HATCHBACK", mappedResponse.Model);
            Assert.AreEqual(null, mappedResponse.OEMFeedLastUpdated);

            Assert.AreEqual(0, mappedResponse.OpenRecalls.Count());
        }

        [Test]
        public void InvalidVIN_MoparTest()
        {
            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetMoparData(),
                ResponseText = Properties.Resources.MoparResponse3
            };

            Exception e = null;
            try
            {
                VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);
            }
            catch (Exception ex)
            {
                e = ex;
            }
            Assert.AreEqual(typeof(OEMVINNotFoundException), e.GetType());

        }

        //Mazda test
        [Test]
        public void MazdaTest()
        {
            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetMazdaData(),
                ResponseText = Properties.Resources.MazdaResponse1
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("1YVHP80C165M67229", mappedResponse.VIN);
            Assert.AreEqual(2006, mappedResponse.Year);
            Assert.AreEqual("Mazda", mappedResponse.Make);
            Assert.AreEqual("MZ6  I   A", mappedResponse.Model);
            Assert.AreEqual(new DateTime(2016, 5, 11), mappedResponse.OEMFeedLastUpdated);

            Assert.AreEqual(3, mappedResponse.OpenRecalls.Count());
            /*
            Assert.AreEqual(null, mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
            Assert.AreEqual(null, mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
            Assert.AreEqual("PCM Reprogramming for OBD-II system. The OBD  monitor cannot detect a Short Fuel Trim threshold control malfunction.", mappedResponse.OpenRecalls.ElementAt(0).Description);
            Assert.AreEqual("Dealers will reprogram the PCM to include the latest calibration, free of charge.", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
            Assert.AreEqual(new DateTime(2007, 9, 27), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
            Assert.AreEqual(RecallStatusEnum.Repaired, mappedResponse.OpenRecalls.ElementAt(0).Status);
            Assert.AreEqual("4907H", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);
            */
            Assert.AreEqual("15V-345", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
            Assert.AreEqual("In the event of a crash necessitating deployment of the driver's side frontal air bag, the inflator could rupture, resulting in metal fragments striking the driver or other occupants, resulting in serious injury or death.", mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
            Assert.AreEqual("In the subject vehicles, continued exposure to high levels of absolute humidity may cause the driver air bag inflator housing to rupture and deploy abnormally if the vehicle is involved in a crash necessitating deployment of the driver side frontal bag.", mappedResponse.OpenRecalls.ElementAt(0).Description);
            Assert.AreEqual("Dealers will replace the driver air bag inflator with a new one, free of charge. This is an interim repair.", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
            Assert.AreEqual(new DateTime(2014, 6, 19), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
            Assert.AreEqual(RecallStatusEnum.RecallIncomplete_RemedyNotYetAvailable, mappedResponse.OpenRecalls.ElementAt(0).Status);
            Assert.AreEqual("7914J", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);

            Assert.AreEqual("15V-382", mappedResponse.OpenRecalls.ElementAt(1).CampaignID);
            Assert.AreEqual("An inflator rupture could result in metal fragments striking vehicle occupants, resulting in serious injury or death.", mappedResponse.OpenRecalls.ElementAt(1).SafetyRisk);
            Assert.AreEqual("This recall replaces 7914J, which was an interim repair. In the subject vehicles, continued exposure to high levels of absolute humidity may cause the driver air bag inflator housing to rupture and deploy abnormally if the vehicle is involved in a crash necessitating deployment of the driver side frontal bag.", mappedResponse.OpenRecalls.ElementAt(1).Description);
            Assert.AreEqual("Dealers will replace the driver air bag inflator with a new one, free of charge.", mappedResponse.OpenRecalls.ElementAt(1).RemedyDescription);
            Assert.AreEqual(new DateTime(2015, 6, 17), mappedResponse.OpenRecalls.ElementAt(1).CampaignDate);
            Assert.AreEqual(RecallStatusEnum.RecallIncomplete, mappedResponse.OpenRecalls.ElementAt(1).Status);
            Assert.AreEqual("8215F", mappedResponse.OpenRecalls.ElementAt(1).OEMCampaignID);
        }
        
        [Test]
        public void MazdaNoneTest()
        {
            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetMazdaData(),
                ResponseText = Properties.Resources.MazdaResponse2
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("JM1NC2PF2F0242655", mappedResponse.VIN);
            Assert.AreEqual(null, mappedResponse.Year);
            Assert.AreEqual("Mazda", mappedResponse.Make);
            Assert.AreEqual(null, mappedResponse.Model);
            Assert.AreEqual(null, mappedResponse.OEMFeedLastUpdated);

            Assert.AreEqual(0, mappedResponse.OpenRecalls.Count());
        }
        
        [Test]
        public void InvalidVIN_MazdaTest()
        {
            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetMazdaData(),
                ResponseText = Properties.Resources.MazdaResponse3
            };

            Exception e = null;
            try
            {
                VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);
            }
            catch (Exception ex)
            {
                e = ex;
            }
            Assert.AreEqual(typeof(OEMVINNotFoundException), e.GetType());

        }

        [Test]
        public void PorscheTest()
        {
            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetPorscheData(),
                ResponseText = Properties.Resources.PorscheResponse1
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("WP0CA2A12FS800180", mappedResponse.VIN);
            Assert.AreEqual(null, mappedResponse.Year);
            Assert.AreEqual("Porsche", mappedResponse.Make);
            Assert.AreEqual("918 Spyder", mappedResponse.Model);
            Assert.AreEqual(new DateTime(2016, 5, 17), mappedResponse.OEMFeedLastUpdated);

            Assert.AreEqual(1, mappedResponse.OpenRecalls.Count());

            Assert.AreEqual("15V-335", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
            Assert.AreEqual("This may lead to an electric short which can result in vehicle malfunction or, in the worst case, a fire.", mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
            Assert.AreEqual("Porsche has determined that there is a possibility that the wiring harness for the left rear radiator fan, located in the engine compartment, could make contact with a heat insulation plate made of carbon fiber.  If the wiring harness makes contact, relative movements of the harness and the vehicle structure can damage wiring harness or the insulation on individual wires of the harness.", mappedResponse.OpenRecalls.ElementAt(0).Description);
            Assert.AreEqual("Porsche is conducting a safety recall on these vehicles.  The affected vehicles will be brought to the workshop and the routing of the wiring harness will be inspected.  The wiring harness will be secured to a coolant tube using an additional relocation clip.  Any related wiring found to be damaged will be repaired or replaced as necessary.", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
            Assert.AreEqual("Rework rear left radiator fan", mappedResponse.OpenRecalls.ElementAt(0).Title);
            Assert.AreEqual(new DateTime(2015, 5, 21), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
            Assert.AreEqual(RecallStatusEnum.Unknown, mappedResponse.OpenRecalls.ElementAt(0).Status);
            Assert.AreEqual("AF05", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);
        }

        [Test]
        public void PorscheNoneTest()
        {
            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetPorscheData(),
                ResponseText = Properties.Resources.PorscheResponse2
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("WP0CA2A82ES120131", mappedResponse.VIN);
            Assert.AreEqual(null, mappedResponse.Year);
            Assert.AreEqual("Porsche", mappedResponse.Make);
            Assert.AreEqual("Boxster", mappedResponse.Model);
            Assert.AreEqual(new DateTime(2016, 5, 17), mappedResponse.OEMFeedLastUpdated);

            Assert.AreEqual(0, mappedResponse.OpenRecalls.Count());
        }

        [Test]
        public void InvalidVIN_PorscheTest()
        {
            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetPorscheData(),
                ResponseText = Properties.Resources.PorscheResponse3
            };

            Exception e = null;
            try
            {
                VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);
            }
            catch (Exception ex)
            {
                e = ex;
            }
            Assert.AreEqual(typeof(OEMVINNotFoundException), e.GetType());

        }

        [Test]
        public void VolvoTest()
        {
            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetVolvoData(),
                ResponseText = Properties.Resources.VolvoResponse1
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual("YV1MC68217J014618", mappedResponse.VIN);
            Assert.AreEqual(null, mappedResponse.Year);
            Assert.AreEqual("Volvo", mappedResponse.Make);
            Assert.AreEqual("C70               P15 CAB", mappedResponse.Model);
            Assert.AreEqual(new DateTime(2016, 5, 17), mappedResponse.OEMFeedLastUpdated);

            Assert.AreEqual(1, mappedResponse.OpenRecalls.Count());

            Assert.AreEqual("11V508", mappedResponse.OpenRecalls.ElementAt(0).CampaignID);
            Assert.AreEqual("Volvo Cars of North America, LLC (Volvo) has decided that certain model year 2006 – 2012 C70 vehicles with a non-factory installed spare tire fail to conform to Federal Motor Vehicle Safety Standard No.110, “Tire selection and Rims.” The reason for Recall 247B: The Tire and Loading Information label located on the \"B\" Post in the driver's door opening; required by Federal Motor Vehicle Safety Standard (FMVSS) 110 Section 4.3; may not contain the correct spare tire size and inflation pressure information on vehicles with a non-factory installed spare tire. However, the information on the tire sidewall and in the owner's manual is correct. Improperly inflated tires can result in premature tire failure, increasing the risk of a crash.", mappedResponse.OpenRecalls.ElementAt(0).SafetyRisk);
            Assert.AreEqual("The corrective action is to inspect the Tire and Loading Information label to ensure it has the correct spare tire information, and replace the label if necessary.", mappedResponse.OpenRecalls.ElementAt(0).RemedyDescription);
            Assert.AreEqual("Recall 247B: Tire Label", mappedResponse.OpenRecalls.ElementAt(0).Title);
            Assert.AreEqual(new DateTime(2012,4, 20), mappedResponse.OpenRecalls.ElementAt(0).CampaignDate);
            Assert.AreEqual(RecallStatusEnum.RecallIncomplete, mappedResponse.OpenRecalls.ElementAt(0).Status);
            Assert.AreEqual("R79305", mappedResponse.OpenRecalls.ElementAt(0).OEMCampaignID);
        }

        [Test]
        public void VolvoNoneTest()
        {
            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetVolvoData(),
                ResponseText = Properties.Resources.VolvoResponse2
            };

            VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);

            Assert.AreEqual(null, mappedResponse.VIN);
            Assert.AreEqual(null, mappedResponse.Year);
            Assert.AreEqual("Volvo", mappedResponse.Make);
            Assert.AreEqual(null, mappedResponse.Model);
            Assert.AreEqual(new DateTime(2016, 5, 17), mappedResponse.OEMFeedLastUpdated);

            Assert.AreEqual(0, mappedResponse.OpenRecalls.Count());
        }

        [Test]
        public void InvalidVIN_VolvoTest()
        {
            MappingContext context = new MappingContext()
            {
                MappingData = SeedDataHelper.GetVolvoData(),
                ResponseText = Properties.Resources.VolvoResponse3
            };

            Exception e = null;
            try
            {
                VINResponse mappedResponse = RecallSplatService.MapResponse(_responseMappingService, context);
            }
            catch (Exception ex)
            {
                e = ex;
            }
            Assert.AreEqual(typeof(OEMVINNotFoundException), e.GetType());

        }
    }
}