﻿using Homenet.RecallSplat.Framework.Bootstrapper;

namespace Homenet.RecallSplat.DependencyInjection
{
	public class RecallSplatKernelConfigurator
	{
		public static void ConfigureDefaults()
		{
			RecallSplatKernel.Configure(new CoreBindingModule());
		}
	}
}