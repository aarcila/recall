﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using Couchbase.Configuration.Client;
using CoxAuto.ObjectStores;
using CoxAuto.ObjectStores.CouchBase;
using Homenet.MoreCommon;
using Homenet.RecallSplat.Core;
using Homenet.RecallSplat.Core.Caching;
using Homenet.RecallSplat.Core.Interfaces;
using Homenet.RecallSplat.Core.Mappings;
using Homenet.RecallSplat.Core.Model;
using Homenet.RecallSplat.Core.Repositories;
using Homenet.RecallSplat.Core.Services;
using Homenet.RecallSplat.Core.Services.Diagnostics;
using Homenet.RecallSplat.Core.Services.Diagnostics.HealthChecks;
using Homenet.RecallSplat.Framework.Bootstrapper;
using Newtonsoft.Json;
using Ninject.Modules;
using BucketConfiguration = Homenet.RecallSplat.Core.BucketConfiguration;

namespace Homenet.RecallSplat.DependencyInjection
{
	public class CoreBindingModule : NinjectModule, IRecallSplatBindingModule
	{
		public override void Load()
		{
			Bind<ExternalRecallServiceConfigRepository>().ToSelf();
			Bind<VINManufacturerConfigMappingRepository>().ToSelf();
			Bind<MappingDataRepository>().ToSelf();
			Bind<OEMResponseCachingService>().ToSelf();
			Bind<IRepository<VINManufacturerConfigMapping>>().To<CouchbaseRepository<VINManufacturerConfigMapping>>().WithConstructorArgument("viewName", RecallSplatSettings.ConfigMappingKeysViewName);
			Bind<IRepository<ExternalRecallServiceConfig>>().To<CouchbaseRepository<ExternalRecallServiceConfig>>().WithConstructorArgument("viewName", RecallSplatSettings.RecallServiceConifgKeysViewName);
			Bind<IRepository<ExternalRecallServiceMappingData>>()
				.To<CouchbaseRepository<ExternalRecallServiceMappingData>>().WithConstructorArgument("viewName", RecallSplatSettings.MappingDataKeysViewName);
			Bind<IRecallSplatService>().To<RecallSplatService>();
			Bind<IMetricGraphingService>().To<GraphzillaService>();

			Bind<IResponseMappingService>().To<JsonPathResponseMappingService>().Named("JsonPath");
            Bind<IResponseMappingService>().To<HtmlPathResponseMappingService>().Named("HtmlPath");

            //Bind<ICouchbaseClusterService>().To<CouchbaseClusterService>().InSingletonScope();
		    if (Settings.DefaultEnvironment().IsProduction)
		    {
                Bind<ITimedObjectStore>().To<CouchbaseObjectStore>().When(p => p.Target.Name == "objectStoreCache").WithConstructorArgument("objectStoreName", "Cache");
                Bind<IObjectStore>().To<CouchbaseObjectStore>().WithConstructorArgument("objectStoreName", "Data");
            }
		    else
		    {
                Bind<ITimedObjectStore>().To<CouchbaseObjectStore>().When(p => p.Target.Name == "objectStoreCache").WithConstructorArgument("objectStoreName", Homenet.MoreCommon.Settings.DefaultEnvironment().ActiveEnvironment + "_Cache");
                Bind<IObjectStore>().To<CouchbaseObjectStore>().WithConstructorArgument("objectStoreName", Homenet.MoreCommon.Settings.DefaultEnvironment().ActiveEnvironment + "_Data");
            }
			

			Bind<IHttpFetcher>().To<HttpFetcher>();
			Bind<ILoggingService>().To<LoggingService>();
			Bind<RecallSplatLogger>().ToMethod(p => RecallSplatLogger.GetConfiguredInstance());

			Bind<ICachingService<OEMResponse>>().To<ObjectStoreCachingService<OEMResponse>>();

            Bind<IHealthChecker>().To<CouchbaseHealthChecker>();
            Bind<IDiagnosticsService>().To<DiagnosticsService>();



            string[] couchbaseServers = Settings.DefaultEnvironment().ListMachinesForApplication(@"InventoryOnline2-CouchbaseServer", includeRedundantEnvironment: false);
            List<Uri> couchBaseUris = couchbaseServers.Select(server => new UriBuilder("http", server, 8091, "/Pools").Uri).ToList();

            ClientConfiguration clientConfig = new ClientConfiguration();
            clientConfig.Servers.AddRange(couchBaseUris);

            var homenetCouchbaseConfig = new CouchbaseClusterService();

            foreach (string bucketName in homenetCouchbaseConfig.BucketNames)
            {
                BucketConfiguration homenetBucketConfig = homenetCouchbaseConfig.GetConfiguration(bucketName);

                Couchbase.Configuration.Client.BucketConfiguration couchbaseBucketConfig = new Couchbase.Configuration.Client.BucketConfiguration();
                couchbaseBucketConfig.BucketName = homenetBucketConfig.Name;
                couchbaseBucketConfig.Username = homenetBucketConfig.Name;
                couchbaseBucketConfig.Password = homenetBucketConfig.Password;

                clientConfig.BucketConfigs.Add(couchbaseBucketConfig.BucketName, couchbaseBucketConfig);
            }


            CoxAuto.ObjectStores.CouchBase.ClientManager.Initialize(clientConfig);

            CoxAuto.ObjectStores.CouchBase.ClientManager.Instance.WithSerializer((object obj) =>
		    {
                string jsonData = JsonConvert.SerializeObject(obj, Formatting.None,
                    new JsonSerializerSettings
                    {
                        TypeNameHandling = TypeNameHandling.All
                    });
		        return jsonData;
		    }).WithDeserializer((string value, Type type) =>
		    {
                var data = JsonConvert.DeserializeObject(value,
                new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Auto
                });
		        return data;
		    });
        }
    }
}
