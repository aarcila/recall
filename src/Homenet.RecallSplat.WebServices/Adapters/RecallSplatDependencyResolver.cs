﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.Http.Dependencies;
using Homenet.RecallSplat.Framework.Bootstrapper;

namespace Homenet.RecallSplat.WebServices.Adapters
{
	public class RecallSplatDependencyResolver : IDependencyResolver
	{

		public object GetService(Type serviceType)
		{
			try
			{
				MethodInfo method = typeof(RecallSplatKernel).GetMethod("Get");
				MethodInfo generic = method.MakeGenericMethod(serviceType);

				return generic.Invoke(this, null);
			}
			catch (Exception)
			{
				return null;
			}

		}

		public IEnumerable<object> GetServices(Type serviceType)
		{
			try
			{
				MethodInfo method = typeof(RecallSplatKernel).GetMethod("GetAll");
				MethodInfo generic = method.MakeGenericMethod(serviceType);

				return generic.Invoke(this, null) as IEnumerable<object>;
			}
			catch (Exception)
			{
				return null;
			}
		}

		public IDependencyScope BeginScope()
		{
			return this;
		}

		public void Dispose()
		{
		}
	}

}