﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Homenet.RecallSplat.Core.Interfaces;
using Homenet.RecallSplat.Core.Model;

namespace Homenet.RecallSplat.WebServices.Controllers
{
    public class DiagnosticsController : ApiController
    {
        private readonly IDiagnosticsService _diagnosticsService;

        public DiagnosticsController(IDiagnosticsService diagnosticsService)
        {
            _diagnosticsService = diagnosticsService;
        }

        [Route("diagnostics/basic-connectivity-test")]
        public HttpResponseMessage Get()
        {
            List<HealthCheck> healthChecks = _diagnosticsService.GetHealthChecks();

            return Request.CreateResponse(healthChecks.Any(hc => hc.HealthCheckStatus == HealthCheckStatusKind.Failure) 
                ? HttpStatusCode.ServiceUnavailable
                : HttpStatusCode.OK
                , healthChecks.ToArray());
        }
    }
}
