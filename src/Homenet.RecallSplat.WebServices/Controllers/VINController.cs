﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Homenet.RecallSplat.Core.Interfaces;
using Homenet.RecallSplat.Core.Model;

namespace Homenet.RecallSplat.WebServices.Controllers
{

// ReSharper disable once InconsistentNaming
	public class VINController : ApiController
	{
		private readonly IRecallSplatService _recallSplatService;
		
		public VINController(IRecallSplatService recallSplatService)
		{
			_recallSplatService = recallSplatService;
		}

		//[Route("vin/{vin}/{cacheFlag}")]
		//public VINRecallResponse Get(string vin, string cacheFlag)
		//{
		//	return _recallSplatService.Lookup(vin, cacheFlag == "0");			
		//}

		[Route("vin/{vin}")]
		public VINRecallResponse Get(string vin, bool bypassCache = false)
		{
			return _recallSplatService.Lookup(vin, bypassCache);
		}


		 //For multiple VINs
		[Route("vins")]
		public VINRecallResponse Get([ModelBinder]List<string> vin, bool bypassCache = false)
		{
			return _recallSplatService.Lookup(vin.Distinct().Take(RecallSplatSettings.MaxVinsPerApiCall), bypassCache);
		}
	}
}
