﻿using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using Homenet.RecallSplat.DependencyInjection;
using Homenet.RecallSplat.WebServices.Adapters;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace Homenet.RecallSplat.WebServices
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

			RecallSplatKernelConfigurator.ConfigureDefaults();
			GlobalConfiguration.Configuration.DependencyResolver = new RecallSplatDependencyResolver();

            var formatters = GlobalConfiguration.Configuration.Formatters;
            formatters.JsonFormatter.SerializerSettings.Formatting = Formatting.Indented;
            formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            formatters.JsonFormatter.SerializerSettings.Converters.Add(new StringEnumConverter());
            formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
        }
    }
}
