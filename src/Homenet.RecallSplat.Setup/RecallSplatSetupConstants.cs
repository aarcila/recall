﻿namespace Homenet.RecallSplat.Setup
{
	public class RecallSplatSetupConstants
	{
		public static class AppPools
		{
			public const string WebServices = "RecallSplatWebServices";
			public const string AdminWebsite = "RecallSplatAdminWebsite";
		}
		public static class Websites
		{
			public const string WebServices = "RecallSplat API";
			public const string AdminWebsite = "RecallSplat Admin Website";
		} 
	}
}