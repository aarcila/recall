﻿namespace Homenet.RecallSplat.Setup
{
	internal enum Action
	{
		/// <summary>Do nothing.</summary>
		Default,

		CreateSeedData,

		CreateTestData,

		DeployAdminWebsite,

		EnableAdminWebsite,

		DeployWebServices,

		EnableWebServices,

		/// <summary>
		/// Insert the Couchbase design document for RecallSplat        
		/// </summary>
		PublishDesignDocument,

		GenerateUpdateManifest,

		CreateCouchbaseBuckets
	}
}