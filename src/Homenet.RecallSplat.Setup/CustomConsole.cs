﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Homenet.RecallSplat.Setup
{
	internal static class CustomConsole
	{
		private static readonly Dictionary<MessageLevel, ConsoleColor> _defaultMessageLevelColors = new Dictionary<MessageLevel, ConsoleColor>
		{
			{MessageLevel.Debug, ConsoleColor.Cyan},
			{MessageLevel.Information, ConsoleColor.White},
			{MessageLevel.Warning, ConsoleColor.Yellow},
			{MessageLevel.Error, ConsoleColor.DarkRed},
			{MessageLevel.Exception, ConsoleColor.Red},
		};
		private const MessageLevel DefaultMessageLevel = MessageLevel.Information;

		public static MessageLevel MinimumMessageLevelForOutput { get { return _minimumMessageLevelForOutput; } set { _minimumMessageLevelForOutput = value; } }
		private static MessageLevel _minimumMessageLevelForOutput = MessageLevel.Debug;

		private static bool ConsoleHasWindowHandle
		{
			get
			{
				if (_consoleHasWindowHandle == null)
				{
					_consoleHasWindowHandle = false;
					try
					{
						// ReSharper disable once UnusedVariable
						int windowHeight = Console.WindowHeight;
						_consoleHasWindowHandle = true;
					}
					catch (IOException) { _consoleHasWindowHandle = false; }
				}
				return _consoleHasWindowHandle.Value;
			}
		}
		private static bool? _consoleHasWindowHandle;

		#region Write

		public static void Write(string format, object arg0)
		{
			Write(DefaultMessageLevel, _defaultMessageLevelColors[DefaultMessageLevel], string.Format(format, arg0));
		}
		public static void Write(string format, object arg0, object arg1)
		{
			Write(DefaultMessageLevel, _defaultMessageLevelColors[DefaultMessageLevel], string.Format(format, arg0, arg1));
		}
		public static void Write(string format, params object[] args)
		{
			Write(DefaultMessageLevel, _defaultMessageLevelColors[DefaultMessageLevel], string.Format(format, args));
		}
		public static void Write(string value)
		{
			Write(DefaultMessageLevel, _defaultMessageLevelColors[DefaultMessageLevel], value);
		}
		public static void Write(MessageLevel messageLevel, string format, object arg0)
		{
			Write(messageLevel, _defaultMessageLevelColors[messageLevel], string.Format(format, arg0));
		}
		public static void Write(MessageLevel messageLevel, string format, object arg0, object arg1)
		{
			Write(messageLevel, _defaultMessageLevelColors[messageLevel], string.Format(format, arg0, arg1));
		}
		public static void Write(MessageLevel messageLevel, string format, params object[] args)
		{
			Write(messageLevel, _defaultMessageLevelColors[messageLevel], string.Format(format, args));
		}
		public static void Write(MessageLevel messageLevel, string value)
		{
			Write(messageLevel, _defaultMessageLevelColors[messageLevel], value);
		}
		public static void Write(ConsoleColor color, string format, object arg0)
		{
			Write(DefaultMessageLevel, color, string.Format(format, arg0));
		}
		public static void Write(ConsoleColor color, string format, object arg0, object arg1)
		{
			Write(DefaultMessageLevel, color, string.Format(format, arg0, arg1));
		}
		public static void Write(ConsoleColor color, string format, params object[] args)
		{
			Write(DefaultMessageLevel, color, string.Format(format, args));
		}
		public static void Write(ConsoleColor color, string value)
		{
			Write(DefaultMessageLevel, color, value);
		}
		public static void Write(MessageLevel messageLevel, ConsoleColor color, string format, object arg0)
		{
			Write(messageLevel, color, string.Format(format, arg0));
		}
		public static void Write(MessageLevel messageLevel, ConsoleColor color, string format, object arg0, object arg1)
		{
			Write(messageLevel, color, string.Format(format, arg0, arg1));
		}
		public static void Write(MessageLevel messageLevel, ConsoleColor color, string format, params object[] args)
		{
			Write(messageLevel, color, string.Format(format, args));
		}
		public static void Write(MessageLevel messageLevel, ConsoleColor color, string value)
		{
			if (messageLevel < MinimumMessageLevelForOutput)
			{
				return;
			}

			if (ConsoleHasWindowHandle)
			{
				lock (Console.Out)
				{
					ConsoleColor prevColor = Console.ForegroundColor;
					Console.ForegroundColor = color;
					Console.Write(value);
					Console.ForegroundColor = prevColor;
				}
			}
			else
			{
				Console.Write(value);
			}
		}

		#endregion

		#region WriteLine

		public static void WriteLine(string format, object arg0)
		{
			WriteLine(DefaultMessageLevel, _defaultMessageLevelColors[DefaultMessageLevel], string.Format(format, arg0));
		}
		public static void WriteLine(string format, object arg0, object arg1)
		{
			WriteLine(DefaultMessageLevel, _defaultMessageLevelColors[DefaultMessageLevel], string.Format(format, arg0, arg1));
		}
		public static void WriteLine(string format, params object[] args)
		{
			WriteLine(DefaultMessageLevel, _defaultMessageLevelColors[DefaultMessageLevel], string.Format(format, args));
		}
		public static void WriteLine(string value)
		{
			WriteLine(DefaultMessageLevel, _defaultMessageLevelColors[DefaultMessageLevel], value);
		}
		public static void WriteLine(MessageLevel messageLevel, string format, object arg0)
		{
			WriteLine(messageLevel, _defaultMessageLevelColors[messageLevel], string.Format(format, arg0));
		}
		public static void WriteLine(MessageLevel messageLevel, string format, object arg0, object arg1)
		{
			WriteLine(messageLevel, _defaultMessageLevelColors[messageLevel], string.Format(format, arg0, arg1));
		}
		public static void WriteLine(MessageLevel messageLevel, string format, params object[] args)
		{
			WriteLine(messageLevel, _defaultMessageLevelColors[messageLevel], string.Format(format, args));
		}
		public static void WriteLine(MessageLevel messageLevel, string value)
		{
			WriteLine(messageLevel, _defaultMessageLevelColors[messageLevel], value);
		}
		public static void WriteLine(ConsoleColor color, string format, object arg0)
		{
			WriteLine(DefaultMessageLevel, color, string.Format(format, arg0));
		}
		public static void WriteLine(ConsoleColor color, string format, object arg0, object arg1)
		{
			WriteLine(DefaultMessageLevel, color, string.Format(format, arg0, arg1));
		}
		public static void WriteLine(ConsoleColor color, string format, params object[] args)
		{
			WriteLine(DefaultMessageLevel, color, string.Format(format, args));
		}
		public static void WriteLine(ConsoleColor color, string value)
		{
			WriteLine(DefaultMessageLevel, color, value);
		}
		public static void WriteLine(MessageLevel messageLevel, ConsoleColor color, string format, object arg0)
		{
			WriteLine(messageLevel, color, string.Format(format, arg0));
		}
		public static void WriteLine(MessageLevel messageLevel, ConsoleColor color, string format, object arg0, object arg1)
		{
			WriteLine(messageLevel, color, string.Format(format, arg0, arg1));
		}
		public static void WriteLine(MessageLevel messageLevel, ConsoleColor color, string format, params object[] args)
		{
			WriteLine(messageLevel, color, string.Format(format, args));
		}
		public static void WriteLine(MessageLevel messageLevel, ConsoleColor color, string value)
		{
			if (messageLevel < MinimumMessageLevelForOutput)
			{
				return;
			}

			if (ConsoleHasWindowHandle)
			{
				lock (Console.Out)
				{
					ConsoleColor prevColor = Console.ForegroundColor;
					Console.ForegroundColor = color;
					Console.WriteLine(value);
					Console.ForegroundColor = prevColor;
				}
			}
			else
			{
				Console.WriteLine(value);
			}
		}

		public static void WriteLine(Exception ex)
		{
			string message = string.Empty;
			Exception currentException = ex;

			while (currentException != null)
			{
				if (message.Length > 0) message += "\r\n\r\n";
				message += string.Format("{0}: {1}\r\n", currentException.GetType().Name, currentException.Message);
				message += currentException.StackTrace;

				currentException = currentException.InnerException;
			}

			WriteLine(MessageLevel.Exception, message.Trim());
		}


		#endregion

	}
}