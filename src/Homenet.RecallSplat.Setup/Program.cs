﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Homenet.RecallSplat.DependencyInjection;
using Homenet.RecallSplat.Framework.Bootstrapper;
using Homenet.RecallSplat.Setup.Processors;

namespace Homenet.RecallSplat.Setup
{
	internal static class Program
	{
		private enum ReturnCodes
		{
			Success = 0,
			Help = -1,
			Exception = -2,
		}

		internal static CommandLine CommandLineArgs;

		static int Main(string[] args)
		{
			System.AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionTrapper;

			InitKernel();

			CommandLineArgs = new CommandLine();

			if (CommandLineArgs.DebugOnStart)
			{
				Debugger.Break();
			}

			try
			{
				SetupProcessorContext context = new SetupProcessorContext(CommandLineArgs);
				IEnumerable<ISetupProcessor> processors = ProcessorFactory.GetProcessors(CommandLineArgs);

				foreach (ISetupProcessor processor in processors)
				{
					processor.Process(context);
				}
			}
			catch (Exception ex)
			{
				CustomConsole.WriteLine(ex);
				if (CommandLineArgs.DebugOnStart || Debugger.IsAttached) Debugger.Break();
				return (int)ReturnCodes.Exception;
			}

			return (int)ReturnCodes.Success;
		}

		private static void InitKernel()
		{
			RecallSplatKernelConfigurator.ConfigureDefaults();
			RecallSplatKernel.Instance.Bind<CreateSeedDataProcessor>().ToSelf();
			RecallSplatKernel.Instance.Bind<CreateTestDataProcessor>().ToSelf();

			RecallSplatKernel.Instance.Bind<DeployAdminWebsiteProcessor>().ToSelf();
			RecallSplatKernel.Instance.Bind<DeployWebServicesProcessor>().ToSelf();
			RecallSplatKernel.Instance.Bind<EnableAdminWebsiteProcessor>().ToSelf();
			RecallSplatKernel.Instance.Bind<EnableWebServicesProcessor>().ToSelf();
			RecallSplatKernel.Instance.Bind<GenerateUpdateManifestProcessor>().ToSelf();
			RecallSplatKernel.Instance.Bind<CreateCouchbaseBucketsProcessor>().ToSelf();
			
		}

		// HACK: (Joe) I was forced to do this because of a bug in CoxAuto.ObjectStores.CouchBase
		// Remove once the library is fixed
		static void UnhandledExceptionTrapper(object sender, UnhandledExceptionEventArgs e)
		{
			if (e.ExceptionObject.GetType() == typeof (ObjectDisposedException))
			{
				// GULP
				Environment.Exit(0);
			}
			else
			{
				throw (Exception)e.ExceptionObject;
			}
			
		}


		private static void ShowHelp()
		{

			//----------------------------------------------------------
			//CustomConsole.WriteLine(MessageLevel.Information, SetupUtilities.ExtractResourceAsString("HelpText.txt"));
			//TODO: show help
			Console.WriteLine("TODO: show help");
		}
	}
}
