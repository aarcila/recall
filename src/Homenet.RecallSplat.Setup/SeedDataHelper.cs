﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using Homenet.RecallSplat.Core.Model;
using Homenet.RecallSplat.Core.Processing;

namespace Homenet.RecallSplat.Setup
{
	public class SeedDataHelper
	{
		public static ExternalRecallServiceMappingData GetHondaAcuraMappingData()
		{
			return new ExternalRecallServiceMappingData()
			{
				VINFieldMappingItems = new List<VINFieldMappingItem>()
				{
					new VINFieldMappingItem(VINProperty.VIN, "VIN"),
					new VINFieldMappingItem(VINProperty.Make, "Make"),
					new VINFieldMappingItem(VINProperty.Model, "ModelGroupName"),
					new VINFieldMappingItem(VINProperty.ModelYear, "ModelYear"),
					new VINFieldMappingItem(VINProperty.LastUpdated, "LastUpdated", new FieldParseData(FieldParseType.DateTime, "MMM dd, yyyy"))
				},

				RecallsArrayPath = "CampaignTypes[0].Campaigns",

				RecallFieldMappingItems = new List<RecallFieldMappingItem>()
				{
					new RecallFieldMappingItem(RecallProperty.ManufacturerRecallID, "Id"),
					new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "RecallNumber"),
					new RecallFieldMappingItem(RecallProperty.Consequence, "SafetyRiskDescription"),
					new RecallFieldMappingItem(RecallProperty.Summary, "Description"),
					new RecallFieldMappingItem(RecallProperty.Notes, "RecallDescription"),
					new RecallFieldMappingItem(RecallProperty.Remedy, "RemedyDescription"),
					new RecallFieldMappingItem(RecallProperty.RecallDate, "RecallDate", new FieldParseData(FieldParseType.DateTime, "MM/dd/yyyy")),
					new RecallFieldMappingItem(RecallProperty.Status, "RecallStatus",
						new FieldValueMappingData()
						{
							MappingItems = new List<ValueMappingItem>()
							{
								new ValueMappingItem("Recall INCOMPLETE", "RecallIncomplete"),
                                new ValueMappingItem("Recall INCOMPLETE. Remedy not yet available.", "RecallIncomplete_RemedyNotYetAvailable")
							}
						}),
				},

                NonRecallFieldMappingItems = new List<RecallFieldMappingItem>()
                {
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, ""),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "na"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NA"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "null"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NULL"),
                    new RecallFieldMappingItem(RecallProperty.Status, "Repaired"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, null),
                },
                

				DataVerificationRules = new List<ExternalRecallServiceDataVerificationRule>()
				{
					new ExternalRecallServiceDataVerificationRule(DataVerificationRuleType.EnsureZeroOrOneElements, "CampaignTypes")
				},

				ErrorFlagPath = "Warning",
				ErrorFlagValue = "true",
				ErrorTextPath = "WarningText",
				InvalidVinErrorTextValue = "Invalid VIN"
			};
		}

		public static ExternalRecallServiceMappingData GetAudiMappingData()
		{
			return new ExternalRecallServiceMappingData()
			{
				VINFieldMappingItems = new List<VINFieldMappingItem>()
				{
					new VINFieldMappingItem(VINProperty.VIN, "vin"),
					new VINFieldMappingItem(VINProperty.Make, "make"),
					new VINFieldMappingItem(VINProperty.Model, "model"),
					new VINFieldMappingItem(VINProperty.ModelYear, "year"),
					new VINFieldMappingItem(VINProperty.LastUpdated, "refreshDate", new FieldParseData(FieldParseType.DateTime, "MMM dd, yyyy"))
				},

				RecallsArrayPath = "recalls",

				RecallFieldMappingItems = new List<RecallFieldMappingItem>()
				{
					new RecallFieldMappingItem(RecallProperty.ManufacturerRecallID, "mfrRecallNumber"),
					new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "nhtsaRecallNumber"),
					new RecallFieldMappingItem(RecallProperty.Consequence, "safetyRiskDescription"),
					new RecallFieldMappingItem(RecallProperty.Summary, "vwgoaActionTitle"),
					new RecallFieldMappingItem(RecallProperty.Notes, "recallDescription"),
					new RecallFieldMappingItem(RecallProperty.Remedy, "remedyDescription"),
					new RecallFieldMappingItem(RecallProperty.RecallDate, "recallDate", new FieldParseData(FieldParseType.DateTime, "MMM dd, yyyy")),
                    new RecallFieldMappingItem(RecallProperty.Status, "mfrRecallStatus",
						new FieldValueMappingData()
						{
							MappingItems = new List<ValueMappingItem>()
							{
                                new ValueMappingItem("11", "RecallIncomplete"),
                                new ValueMappingItem("12", "RecallIncomplete_RemedyNotYetAvailable")
							}
						})
				},
                NonRecallFieldMappingItems = new List<RecallFieldMappingItem>()
                {
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, ""),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "na"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NA"),
                    new RecallFieldMappingItem(RecallProperty.Status, "Repaired"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "null"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NULL"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, null),
                },

				ErrorFlagPath = "status",
				ErrorFlagValue = "false",
			};
		}

		public static ExternalRecallServiceMappingData GetGMMappingData()
		{
			return new ExternalRecallServiceMappingData()
			{
				VINFieldMappingItems = new List<VINFieldMappingItem>()
				{
					new VINFieldMappingItem(VINProperty.VIN, "data.vin"),
					new VINFieldMappingItem(VINProperty.Make, "data.make"),
					new VINFieldMappingItem(VINProperty.Model, "data.model"),
					new VINFieldMappingItem(VINProperty.ModelYear, "data.year"),
					new VINFieldMappingItem(VINProperty.LastUpdated, "data.refresh_date", new FieldParseData(FieldParseType.DateTime, "MMM dd, yyyy")),
				},

				RecallsArrayPath = "data.recalls",

				RecallFieldMappingItems = new List<RecallFieldMappingItem>()
				{
					new RecallFieldMappingItem(RecallProperty.ManufacturerRecallID, "mfr_recall_number"),
					new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "nhtsa_recall_number"),
					new RecallFieldMappingItem(RecallProperty.Consequence, "safety_risk_description"),
					new RecallFieldMappingItem(RecallProperty.Summary, "recall_title"),
					new RecallFieldMappingItem(RecallProperty.Notes, "recall_description"),
					new RecallFieldMappingItem(RecallProperty.Remedy, "remedy_description"),
					new RecallFieldMappingItem(RecallProperty.RecallDate, "recall_date", new FieldParseData(FieldParseType.DateTime, "MMM dd, yyyy")),

					new RecallFieldMappingItem(RecallProperty.Status, "mfr_recall_status",
						new FieldValueMappingData()
						{
							MappingItems = new List<ValueMappingItem>()
							{
								new ValueMappingItem("11", "RecallIncomplete"),
								new ValueMappingItem("12", "RecallIncomplete_RemedyNotYetAvailable"),
							}
						})
				},
                NonRecallFieldMappingItems = new List<RecallFieldMappingItem>()
                {
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, ""),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "na"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NA"),
                    new RecallFieldMappingItem(RecallProperty.Status, "Repaired"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "null"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NULL"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, null),
                },

				ErrorTextArrayPath = "messages",
				InvalidVinErrorTextValue = "VEHICLE_INVALID_VIN"
			};
		}

		public static ExternalRecallServiceMappingData GetInfinitiMappingData()
		{
			return new ExternalRecallServiceMappingData()
			{
				VINFieldMappingItems = new List<VINFieldMappingItem>()
				{
					new VINFieldMappingItem(VINProperty.VIN, "vin"),
					new VINFieldMappingItem(VINProperty.Make, "make"),
					new VINFieldMappingItem(VINProperty.Model, "vehicleName"),
					new VINFieldMappingItem(VINProperty.ModelYear, "modelYear"),
				},

				RecallsArrayPath = "recalls",

				RecallFieldMappingItems = new List<RecallFieldMappingItem>()
				{
					new RecallFieldMappingItem(RecallProperty.ManufacturerRecallID, "nnaId"),
					new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "nhtsaId"),
					new RecallFieldMappingItem(RecallProperty.Consequence, "riskIfNotRepaired"),
					new RecallFieldMappingItem(RecallProperty.Summary, "secondaryDescription"),
					new RecallFieldMappingItem(RecallProperty.Notes, "primaryDescription"),
					new RecallFieldMappingItem(RecallProperty.Remedy, "remedyDescription"),
					new RecallFieldMappingItem(RecallProperty.RecallDate, "effectiveDate", new FieldParseData(FieldParseType.DateTime, null)),
				},
                NonRecallFieldMappingItems = new List<RecallFieldMappingItem>()
                {
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, ""),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "na"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NA"),
                    new RecallFieldMappingItem(RecallProperty.Status, "Repaired"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "null"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NULL"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, null),
                },

				ErrorFlagPath = "modelYear",
				ErrorFlagValue = "0"
			};
		}

		public static ExternalRecallServiceMappingData GetJaguarMappingData()
		{
			return new ExternalRecallServiceMappingData()
			{
				VINFieldMappingItems = new List<VINFieldMappingItem>()
				{
					new VINFieldMappingItem(VINProperty.VIN, "vin"),
					new VINFieldMappingItem(VINProperty.Make, FieldMappingItemType.Static, "Jaguar"),
					new VINFieldMappingItem(VINProperty.Model, "vehicle"),
					new VINFieldMappingItem(VINProperty.ModelYear, "model"),
				},

				RecallsArrayPath = "results",

				RecallFieldMappingItems = new List<RecallFieldMappingItem>()
				{
					new RecallFieldMappingItem(RecallProperty.ManufacturerRecallID, "manufacturerRecallNumber"),
					new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "number"),
					new RecallFieldMappingItem(RecallProperty.Consequence, "safetyDescription"),
					new RecallFieldMappingItem(RecallProperty.Summary, null),
					new RecallFieldMappingItem(RecallProperty.Notes, "recallDesc"),
					new RecallFieldMappingItem(RecallProperty.Remedy, "repairDesc"),
					new RecallFieldMappingItem(RecallProperty.RecallDate, "date", new FieldParseData(FieldParseType.DateTime, "MM/dd/yyyy")),
					new RecallFieldMappingItem(RecallProperty.Status, "status",
						new FieldValueMappingData()
						{
							MappingItems = new List<ValueMappingItem>()
							{
								new ValueMappingItem("Incomplete But Repair Available", "RecallIncomplete"),
								new ValueMappingItem("Recall outstanding", "RecallIncomplete"),
                                new ValueMappingItem("Repair Not Yet Available", "RecallIncomplete_RemedyNotYetAvailable")
							}
						})
				},

                NonRecallFieldMappingItems = new List<RecallFieldMappingItem>()
                {
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, ""),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "na"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NA"),
                    new RecallFieldMappingItem(RecallProperty.Status, "Repaired"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "null"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NULL"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, null),
                },

				ErrorTextPath = "errorTitle",
				InvalidVinErrorTextValue = "Sorry, that is not a valid VIN."
			};
		}

		public static ExternalRecallServiceMappingData GetLandRoverMappingData()
		{
			return new ExternalRecallServiceMappingData()
			{
				VINFieldMappingItems = new List<VINFieldMappingItem>()
				{
					new VINFieldMappingItem(VINProperty.VIN, "vin"),
					new VINFieldMappingItem(VINProperty.Make, FieldMappingItemType.Static, "Land Rover"),
					new VINFieldMappingItem(VINProperty.Model, "vehicle"),
					new VINFieldMappingItem(VINProperty.ModelYear, "model"),
				},

				RecallsArrayPath = "results",

				RecallFieldMappingItems = new List<RecallFieldMappingItem>()
				{
					new RecallFieldMappingItem(RecallProperty.ManufacturerRecallID, "manufacturerRecallNumber"),
					new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "number"),
					new RecallFieldMappingItem(RecallProperty.Consequence, "safetyDescription"),
					new RecallFieldMappingItem(RecallProperty.Summary, null),
					new RecallFieldMappingItem(RecallProperty.Notes, "recallDesc"),
					new RecallFieldMappingItem(RecallProperty.Remedy, "repairDesc"),
					new RecallFieldMappingItem(RecallProperty.RecallDate, "date", new FieldParseData(FieldParseType.DateTime, "MM/dd/yyyy")),
					new RecallFieldMappingItem(RecallProperty.Status, "status",
						new FieldValueMappingData()
						{
							MappingItems = new List<ValueMappingItem>()
							{
								new ValueMappingItem("Incomplete But Repair Available", "RecallIncomplete"),
								new ValueMappingItem("Recall outstanding", "RecallIncomplete"),
                                new ValueMappingItem("Recall outstanding. Remedy not yet available...", "RecallIncomplete_RemedyNotYetAvailable")
							}
						})
				},

                NonRecallFieldMappingItems = new List<RecallFieldMappingItem>()
                {
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, ""),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "na"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NA"),
                    new RecallFieldMappingItem(RecallProperty.Status, "Repaired"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "null"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NULL"),
                },

				ErrorTextPath = "errorTitle",
				InvalidVinErrorTextValue = "Sorry, that is not a valid VIN."
			};
		}

		public static ExternalRecallServiceMappingData GetKiaMappingData()
		{
			return new ExternalRecallServiceMappingData()
			{
				VINFieldMappingItems = new List<VINFieldMappingItem>()
				{
					new VINFieldMappingItem(VINProperty.VIN, "result.recallsResult.vin"),
					new VINFieldMappingItem(VINProperty.Make, "result.recallsResult.make"),
					new VINFieldMappingItem(VINProperty.Model, "result.recallsResult.model"),
					new VINFieldMappingItem(VINProperty.ModelYear, "result.recallsResult.year"),
					new VINFieldMappingItem(VINProperty.LastUpdated, "result.recallsResult.refresh_date", new FieldParseData(FieldParseType.EpochConvert, null)),
				},

				RecallsArrayPath = "result.recallsResult.recalls",

				RecallFieldMappingItems = new List<RecallFieldMappingItem>()
				{
					new RecallFieldMappingItem(RecallProperty.ManufacturerRecallID, "mfr_recall_number"),
					new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "nhtsa_recall_number"),
					new RecallFieldMappingItem(RecallProperty.Consequence, "safety_risk_description"),
					new RecallFieldMappingItem(RecallProperty.Summary, null),
					new RecallFieldMappingItem(RecallProperty.Notes, "recall_description"),
					new RecallFieldMappingItem(RecallProperty.Remedy, "remedy_description"),
					new RecallFieldMappingItem(RecallProperty.RecallDate, "recall_date", new FieldParseData(FieldParseType.EpochConvert, null)),
					new RecallFieldMappingItem(RecallProperty.Status, "recall_status",
						new FieldValueMappingData()
						{
							MappingItems = new List<ValueMappingItem>()
							{
								new ValueMappingItem("RECALL INCOMPLETE", "RecallIncomplete"),
							}
						})
				},

                NonRecallFieldMappingItems = new List<RecallFieldMappingItem>()
                {
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, ""),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "na"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NA"),
                    new RecallFieldMappingItem(RecallProperty.Status, "Repaired"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "null"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NULL"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, null),
                },

				ErrorTextPath = "statusDesc",
				InvalidVinErrorTextValue = "INVALID_VIN"
			};
		}

		public static ExternalRecallServiceMappingData GetFordLincolnMercuryData()
		{
			return new ExternalRecallServiceMappingData()
			{
				VINFieldMappingItems = new List<VINFieldMappingItem>()
				{
					new VINFieldMappingItem(VINProperty.VIN, "vin"),
					new VINFieldMappingItem(VINProperty.Make, "nhtsa_header_details.make"),
					new VINFieldMappingItem(VINProperty.Model, "nhtsa_header_details.model"),
					new VINFieldMappingItem(VINProperty.ModelYear, "nhtsa_header_details.year"),
					new VINFieldMappingItem(VINProperty.LastUpdated, "nhtsa_header_details.refresh_date", new FieldParseData(FieldParseType.DateTime, "MMM dd, yyyy")),
				},

				RecallsArrayPath = "recalls.nhtsa_recall_item",

				RecallFieldMappingItems = new List<RecallFieldMappingItem>()
				{
					new RecallFieldMappingItem(RecallProperty.ManufacturerRecallID, "mfr_recall_number"),
					new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "nhtsa_recall_number"),
					new RecallFieldMappingItem(RecallProperty.Consequence, "safety_risk_description"),
					new RecallFieldMappingItem(RecallProperty.Summary, "description_eng"),
					new RecallFieldMappingItem(RecallProperty.Notes, "recall_description"),
					new RecallFieldMappingItem(RecallProperty.Remedy, "remedy_description"),
					new RecallFieldMappingItem(RecallProperty.RecallDate, "recall_date", new FieldParseData(FieldParseType.DateTime, "MMM dd, yyyy")),
					new RecallFieldMappingItem(RecallProperty.Status, "mfr_recall_status",
						new FieldValueMappingData()
						{
							MappingItems = new List<ValueMappingItem>()
							{
                                new ValueMappingItem("12 - RECALL INCOMPLETE. REMEDY NOT YET AVAILABLE.", "RecallIncomplete_RemedyNotYetAvailable"),
								new ValueMappingItem("11 - RECALL INCOMPLETE", "RecallIncomplete")
							}
						})
				},

                NonRecallFieldMappingItems = new List<RecallFieldMappingItem>()
                {
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, ""),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "na"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NA"),
                    new RecallFieldMappingItem(RecallProperty.Status, "Repaired"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "null"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NULL"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, null),
                },

				ErrorTextPath = "return_status.description",
				InvalidVinErrorTextValue = "Input parameter invalid"
			};
		}

		public static ExternalRecallServiceMappingData GetMitsubishiData()
		{
			return new ExternalRecallServiceMappingData()
			{
				VINFieldMappingItems = new List<VINFieldMappingItem>()
				{
					new VINFieldMappingItem(VINProperty.VIN, "vehicleInfo.vin"),
					new VINFieldMappingItem(VINProperty.Make, FieldMappingItemType.Static, "Mitsubishi"),
					new VINFieldMappingItem(VINProperty.Model, "vehicleInfo.model"),
					new VINFieldMappingItem(VINProperty.ModelYear, "vehicleInfo.modelYear"),
					new VINFieldMappingItem(VINProperty.LastUpdated, "nhtsaVehicle.recallRefreshDate", new FieldParseData(FieldParseType.DateTime, "MMM dd, yyyy"))
				},

				RecallsArrayPath = "nhtsaOpenRecalls",

				RecallFieldMappingItems = new List<RecallFieldMappingItem>()
				{
					new RecallFieldMappingItem(RecallProperty.ManufacturerRecallID, "mfrRecallNumber"),
					new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "nhtsaRecallNumber"),
					new RecallFieldMappingItem(RecallProperty.Consequence, "safetyRiskDescription"),
					new RecallFieldMappingItem(RecallProperty.Notes, "recallDescription"),
					new RecallFieldMappingItem(RecallProperty.Remedy, "remedyDescription"),
					new RecallFieldMappingItem(RecallProperty.RecallDate, "recallDate", new FieldParseData(FieldParseType.DateTime, "MMM dd, yyyy")),
					new RecallFieldMappingItem(RecallProperty.Status, "mfrRecallStatus",
						new FieldValueMappingData()
						{
							MappingItems = new List<ValueMappingItem>()
							{
								new ValueMappingItem("INCOMPLETE BUT REPAIR AVAILABLE", "RecallIncomplete"),
							}
						})
				},

                NonRecallFieldMappingItems = new List<RecallFieldMappingItem>()
                {
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, ""),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "na"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NA"),
                    new RecallFieldMappingItem(RecallProperty.Status, "Repaired"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "null"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NULL"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, null),
                },

				ErrorTextPath = "statusMessage",
				InvalidVinErrorTextValue = "VIN DOES NOT EXIST"
			};
		}

		public static ExternalRecallServiceMappingData GetNissanData()
		{
			return new ExternalRecallServiceMappingData()
			{
				VINFieldMappingItems = new List<VINFieldMappingItem>()
				{
					new VINFieldMappingItem(VINProperty.VIN, "vin"),
					new VINFieldMappingItem(VINProperty.Make, "make"),
					new VINFieldMappingItem(VINProperty.Model, "vehicleName"),
					new VINFieldMappingItem(VINProperty.ModelYear, "modelYear")
				},

				RecallsArrayPath = "recalls",

				RecallFieldMappingItems = new List<RecallFieldMappingItem>()
				{
					new RecallFieldMappingItem(RecallProperty.ManufacturerRecallID, "nnaId"),
					new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "nhtsaId"),
					new RecallFieldMappingItem(RecallProperty.Consequence, "riskIfNotRepaired"),
					new RecallFieldMappingItem(RecallProperty.Notes, "primaryDescription"),
					new RecallFieldMappingItem(RecallProperty.Remedy, "remedyDescription"),
					new RecallFieldMappingItem(RecallProperty.Summary, "secondaryDescription"),
					new RecallFieldMappingItem(RecallProperty.RecallDate, "effectiveDate", new FieldParseData(FieldParseType.DateTime, null))
				},

                NonRecallFieldMappingItems = new List<RecallFieldMappingItem>()
                {
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, ""),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "na"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NA"),
                    new RecallFieldMappingItem(RecallProperty.Status, "Repaired"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "null"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NULL"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, null),
                },

				ErrorFlagPath = "modelYear",
				ErrorFlagValue = "0"
			};
		}

		public static ExternalRecallServiceMappingData GetSubaruMappingData()
		{
			return new ExternalRecallServiceMappingData()
			{
				VINFieldMappingItems = new List<VINFieldMappingItem>()
				{
					new VINFieldMappingItem(VINProperty.VIN, "vin"),
					new VINFieldMappingItem(VINProperty.Make, FieldMappingItemType.Static, "Subaru"),
					new VINFieldMappingItem(VINProperty.Model, "modelName"),
					new VINFieldMappingItem(VINProperty.ModelYear, "modelYear"),
					new VINFieldMappingItem(VINProperty.LastUpdated, "lastModified.fullDate", new FieldParseData(FieldParseType.DateTime, null)),
				},

				RecallsArrayPath = "recalls",

				RecallFieldMappingItems = new List<RecallFieldMappingItem>()
				{
					new RecallFieldMappingItem(RecallProperty.ManufacturerRecallID, "code"),
					new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "nhtsaRecallNumber"),
					new RecallFieldMappingItem(RecallProperty.Consequence, "safetyRiskDescription"),
					new RecallFieldMappingItem(RecallProperty.Notes, "description"),
					new RecallFieldMappingItem(RecallProperty.Remedy, "remedyDescription"),
					new RecallFieldMappingItem(RecallProperty.Summary, "shortDescription"),
					new RecallFieldMappingItem(RecallProperty.RecallDate, "date.fullDate", new FieldParseData(FieldParseType.DateTime, null)),
					new RecallFieldMappingItem(RecallProperty.Status, "status",
						new FieldValueMappingData()
						{
							MappingItems = new List<ValueMappingItem>()
							{
								new ValueMappingItem("Open", "RecallIncomplete"),
                                new ValueMappingItem("Completed", "Repaired"),
                                new ValueMappingItem("Open-Remedy not yet available", "RecallIncomplete_RemedyNotYetAvailable"),
							}
						})
				},
                NonRecallFieldMappingItems = new List<RecallFieldMappingItem>()
                {
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, ""),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "na"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NA"),
                    new RecallFieldMappingItem(RecallProperty.Status, "Repaired"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "null"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NULL"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, null),
                },
			};
		}

		public static ExternalRecallServiceMappingData GetVolkswagenMappingData()
		{
			return new ExternalRecallServiceMappingData()
			{
				VINFieldMappingItems = new List<VINFieldMappingItem>()
				{
					new VINFieldMappingItem(VINProperty.VIN, "vin"),
					new VINFieldMappingItem(VINProperty.Make, "make"),
					new VINFieldMappingItem(VINProperty.Model, "model"),
					new VINFieldMappingItem(VINProperty.ModelYear, "year"),
					new VINFieldMappingItem(VINProperty.LastUpdated, "refreshDate", new FieldParseData(FieldParseType.DateTime, "MMM dd, yyyy")),
				},

				RecallsArrayPath = "recalls",

				RecallFieldMappingItems = new List<RecallFieldMappingItem>()
				{
					new RecallFieldMappingItem(RecallProperty.ManufacturerRecallID, "mfrRecallNumber"),
					new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "nhtsaRecallNumber"),
					new RecallFieldMappingItem(RecallProperty.Consequence, "safetyRiskDescription"),
					new RecallFieldMappingItem(RecallProperty.Notes, "recallDescription"),
					new RecallFieldMappingItem(RecallProperty.Remedy, "remedyDescription"),
					new RecallFieldMappingItem(RecallProperty.Summary, "vwgoaActionTitle"),
					new RecallFieldMappingItem(RecallProperty.RecallDate, "recallDate", new FieldParseData(FieldParseType.DateTime, "MMM dd, yyyy")),
					new RecallFieldMappingItem(RecallProperty.Status, "mfrRecallStatus",
						new FieldValueMappingData()
						{
							MappingItems = new List<ValueMappingItem>()
							{
								new ValueMappingItem("11", "RecallIncomplete"),
								new ValueMappingItem("12", "RecallIncomplete_RemedyNotYetAvailable"),
							}
						})
				},

                NonRecallFieldMappingItems = new List<RecallFieldMappingItem>()
                {
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, ""),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "na"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NA"),
                    new RecallFieldMappingItem(RecallProperty.Status, "Repaired"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "null"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NULL"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, null),
                },

				ErrorFlagValue = "false",
				ErrorFlagPath = "status"
			};
		}

		public static ExternalRecallServiceMappingData GetBMWMappingData()
		{
			return new ExternalRecallServiceMappingData()
			{
				VINFieldMappingItems = new List<VINFieldMappingItem>()
				{
					new VINFieldMappingItem(VINProperty.VIN, "ViewModel.Vin"),
					new VINFieldMappingItem(VINProperty.Make, "ViewModel.Make"),
					new VINFieldMappingItem(VINProperty.Model, "ViewModel.Model"),
					new VINFieldMappingItem(VINProperty.ModelYear, "ViewModel.Year"),
					new VINFieldMappingItem(VINProperty.LastUpdated, "ViewModel.RefreshDate", new FieldParseData(FieldParseType.DateTime, null)),
				},

				RecallsArrayPath = "ViewModel.RecallCampaigns",

				RecallFieldMappingItems = new List<RecallFieldMappingItem>()
				{
					new RecallFieldMappingItem(RecallProperty.ManufacturerRecallID, "ManufacturerRecallNumber"),
					new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "RecallNumber"),
					new RecallFieldMappingItem(RecallProperty.Consequence, "SafetyRiskDescription"),
					new RecallFieldMappingItem(RecallProperty.Notes, "Description"),
					new RecallFieldMappingItem(RecallProperty.Remedy, "RemedyDescription"),
					new RecallFieldMappingItem(RecallProperty.Summary, "Title"),
					new RecallFieldMappingItem(RecallProperty.RecallDate, "RecallDate", new FieldParseData(FieldParseType.DateTime, null)),
                    new RecallFieldMappingItem(RecallProperty.Status, "ManufacturerRecallStatus",
						new FieldValueMappingData()
						{
							MappingItems = new List<ValueMappingItem>()
							{
                                //guess
								new ValueMappingItem("11", "RecallIncomplete"),
								new ValueMappingItem("12", "RecallIncomplete_RemedyNotYetAvailable"),
							}
						})
				},

                NonRecallFieldMappingItems = new List<RecallFieldMappingItem>()
                {
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, ""),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "na"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NA"),
                    new RecallFieldMappingItem(RecallProperty.Status, "Repaired"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "null"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NULL"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, null),
                },

				ErrorFlagPath = "ViewModel.Error",
				ErrorFlagValue = "0",
				ErrorFlagValueOperation = ErrorFlagValueOperation.NotEqual
			};
		}

		public static ExternalRecallServiceMappingData GetMiniData()
		{
			return new ExternalRecallServiceMappingData()
			{
				VINFieldMappingItems = new List<VINFieldMappingItem>()
				{
					new VINFieldMappingItem(VINProperty.VIN, "vin"),
					new VINFieldMappingItem(VINProperty.Make, "make"),
					new VINFieldMappingItem(VINProperty.Model, "model"),
					new VINFieldMappingItem(VINProperty.ModelYear, "year"),
					new VINFieldMappingItem(VINProperty.LastUpdated, "refresh_date", new FieldParseData(FieldParseType.DateTime, null)),
				},

				RecallsArrayPath = "recalls",

				RecallFieldMappingItems = new List<RecallFieldMappingItem>()
				{
					new RecallFieldMappingItem(RecallProperty.ManufacturerRecallID, "mfr_recall_number"),
					new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "nhtsa_recall_number"),
					new RecallFieldMappingItem(RecallProperty.Consequence, "safety_risk_description"),
					new RecallFieldMappingItem(RecallProperty.Notes, "recall_description"),
					new RecallFieldMappingItem(RecallProperty.Remedy, "remedy_description"),
					new RecallFieldMappingItem(RecallProperty.Summary, "recall_title"),
					new RecallFieldMappingItem(RecallProperty.RecallDate, "recall_date", new FieldParseData(FieldParseType.DateTime, null)),
                    new RecallFieldMappingItem(RecallProperty.Status, "mfr_recall_status",
						new FieldValueMappingData()
						{
							MappingItems = new List<ValueMappingItem>()
							{
								new ValueMappingItem("11", "RecallIncomplete"),
                                new ValueMappingItem("12", "RecallIncomplete_RemedyNotYetAvailable"),
							}
						})
				},

                NonRecallFieldMappingItems = new List<RecallFieldMappingItem>()
                {
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, ""),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "na"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NA"),
                    new RecallFieldMappingItem(RecallProperty.Status, "Repaired"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "null"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NULL"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, null),
                },

				ErrorFlagValue = "false",
				ErrorFlagPath = "status"
			};
		}

        public static ExternalRecallServiceMappingData GetMoparData()
        {
            return new ExternalRecallServiceMappingData()
            {
                VINFieldMappingItems = new List<VINFieldMappingItem>()
				{
					new VINFieldMappingItem(VINProperty.VIN, "//input[@id= 'g_88e3cfe4_4b16_4f68_a2e6_ba80f8e020bc_RecallsQuery_ctl00_RR_VinNo_hidden']", new HtmlPathData() { ElementSource = HtmlPathElementSource.ValueAttribute}),
					new VINFieldMappingItem(VINProperty.Make, "//input[@id= 'g_88e3cfe4_4b16_4f68_a2e6_ba80f8e020bc_RecallsQuery_ctl00_RR_Vehicle_hidden']", new HtmlPathData() { RegEx = @"^([^\s]+)", ElementSource = HtmlPathElementSource.ValueAttribute}),
                    new VINFieldMappingItem(VINProperty.Model, "//input[@id= 'g_88e3cfe4_4b16_4f68_a2e6_ba80f8e020bc_RecallsQuery_ctl00_RR_Vehicle_hidden']", new HtmlPathData() { RegEx = @"\s(.*)", ElementSource = HtmlPathElementSource.ValueAttribute}),
					new VINFieldMappingItem(VINProperty.ModelYear, "//input[@id='g_88e3cfe4_4b16_4f68_a2e6_ba80f8e020bc_RecallsQuery_ctl00_RR_Year_hidden']", new HtmlPathData() { ElementSource = HtmlPathElementSource.ValueAttribute }),
					new VINFieldMappingItem(VINProperty.LastUpdated, "//div[@id='divLastResults']", new FieldParseData(FieldParseType.DateTime, "MMMM d, yyyy"), new HtmlPathData() { RegEx = @"[^:]*$", ElementSource = HtmlPathElementSource.InnerText }),
				},


                MappingServiceAdditionalData = new HtmlPathResponseData() { HeaderRows  = 1 },
                RecallsArrayPath = "//table[@id='g_88e3cfe4_4b16_4f68_a2e6_ba80f8e020bc_RecallsQuery_ctl00_gvSafetyRecalls']//tr",

                RecallFieldMappingItems = new List<RecallFieldMappingItem>()
				{
					new RecallFieldMappingItem(RecallProperty.ManufacturerRecallID, "td[3]", new HtmlPathData() {ElementSource = HtmlPathElementSource.InnerText}),
					new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "td[4]", new HtmlPathData() {ElementSource = HtmlPathElementSource.InnerText}),
                    new RecallFieldMappingItem(RecallProperty.Consequence, "td[6]", new HtmlPathData() {ElementSource = HtmlPathElementSource.InnerText}),
					new RecallFieldMappingItem(RecallProperty.Remedy, "td[7]", new HtmlPathData() {ElementSource = HtmlPathElementSource.InnerText}),
                    new RecallFieldMappingItem(RecallProperty.RecallDate, "td[5]", new FieldParseData(FieldParseType.DateTime, null), new HtmlPathData() {ElementSource = HtmlPathElementSource.InnerText}),

                    new RecallFieldMappingItem(RecallProperty.Status, "td[8]",
                    new FieldValueMappingData()
                        {
                            MappingItems = new List<ValueMappingItem>()
                            {
                                new ValueMappingItem("REPAIRED", "Repaired"),
                                new ValueMappingItem("INSPECTED", "Repaired"),
                                new ValueMappingItem("INCOMPLETE BUT REPAIR PARTS ARE AVAILABLE", "RecallIncomplete"),
                                new ValueMappingItem("INCOMPLETE BUT REPAIR PARTS ARE NOT AVAILABLE", "RecallIncomplete_RemedyNotYetAvailable"),
                            }
                        },
                    new HtmlPathData() {ElementSource = HtmlPathElementSource.InnerText}),
                },

                NonRecallFieldMappingItems = new List<RecallFieldMappingItem>()
                {
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, ""),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "na"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NA"),
                    new RecallFieldMappingItem(RecallProperty.Status, "Repaired"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "null"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NULL"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, null),
                },

                ErrorFlagPath = "//div[@id='g_88e3cfe4_4b16_4f68_a2e6_ba80f8e020bc_RecallsQuery_ctl00_divError']",
                ErrorFlagValueOperation = ErrorFlagValueOperation.Exists,
                ErrorTextPath = "//span[@id='g_88e3cfe4_4b16_4f68_a2e6_ba80f8e020bc_RecallsQuery_ctl00_lblError']",
                InvalidVinErrorTextValue = "is not a valid VIN",
                NoRecallsErrorTextValue = "No Outstanding Recalls or Customer Satisfaction Notifications Exist."
            };


        }

        public static ExternalRecallServiceMappingData GetMazdaData()
        {
            return new ExternalRecallServiceMappingData()
            {
                VINFieldMappingItems = new List<VINFieldMappingItem>()
				{
					new VINFieldMappingItem(VINProperty.VIN, "//input[@id= 'recallTextBox']", new HtmlPathData() { ElementSource = HtmlPathElementSource.ValueAttribute}),
					new VINFieldMappingItem(VINProperty.Make, FieldMappingItemType.Static, "Mazda"),
                    new VINFieldMappingItem(VINProperty.Model, "//div[@id='recallResult']//div[@class='leftSide']", new HtmlPathData() { RegEx = @"\s(.*)", ElementSource = HtmlPathElementSource.InnerText}),
					new VINFieldMappingItem(VINProperty.ModelYear, "//div[@id='recallResult']//div[@class='leftSide']", new HtmlPathData() { RegEx = @"[^:]+\d+\d", ElementSource = HtmlPathElementSource.InnerText }),
					new VINFieldMappingItem(VINProperty.LastUpdated, "//div[@id='recallResult']//div[@class ='rightSide']//p[3]", new FieldParseData(FieldParseType.DateTime, "MMMM d, yyyy"), new HtmlPathData() { RegEx = @"[^:]*$", ElementSource = HtmlPathElementSource.InnerText }),
				},
                PreProcessors = new List<IProcessor>()
                {
                    new FindReplaceProcessor("<div class=\"recall_row recall_info\">", "<div class=\"table\"><div class=\"recall_row recall_info\">"),
                    new FindReplaceProcessor("<hr>","</div>"),
                    new FindReplaceProcessor("Last Updated: ",""),
                    new FindReplaceProcessor("<br/><b>Safety Risk: </b>","</p></div><div class='risk'><p>"),
                    new FindReplaceProcessor("<b>NHTSA Recall #: </b>","</div><div class='nhtsa'><b>NHTSA Recall #: </b>")
                },

                MappingServiceAdditionalData = new HtmlPathResponseData() { HeaderRows = 0 },
                RecallsArrayPath = "//div[@class = 'table']",

                RecallFieldMappingItems = new List<RecallFieldMappingItem>()
				{
					new RecallFieldMappingItem(RecallProperty.ManufacturerRecallID, "//div[@class='recall_row recall_info']", new HtmlPathData() {RegEx = @"[^:]*$", ElementSource = HtmlPathElementSource.InnerText}),
					new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "//div[@class='nhtsa']", new HtmlPathData() {RegEx = @"[^:]*$", ElementSource = HtmlPathElementSource.InnerText}),
					new RecallFieldMappingItem(RecallProperty.Remedy, "//div[@class='recall_row table_container']//div[@id='recallSecondCol']", new HtmlPathData() {ElementSource = HtmlPathElementSource.InnerText}),
                    new RecallFieldMappingItem(RecallProperty.Notes, "//div[@class='recall_row table_container']//div[@id='recallFirstCol']", new HtmlPathData() {ElementSource = HtmlPathElementSource.InnerText}),
                    new RecallFieldMappingItem(RecallProperty.Consequence, "//div[@class='risk']//p", new HtmlPathData() {ElementSource = HtmlPathElementSource.InnerText}),
                    new RecallFieldMappingItem(RecallProperty.RecallDate, "//div[@class='recall_row table_container']//div[@id='recallFourthCol']", new FieldParseData(FieldParseType.DateTime, null), new HtmlPathData() {ElementSource = HtmlPathElementSource.InnerText}),

                    new RecallFieldMappingItem(RecallProperty.Status, "//div[@class='recall_row table_container']//div[@id='recallThirdCol']",
                    new FieldValueMappingData()
                        {
                            MappingItems = new List<ValueMappingItem>()
                            {
                                new ValueMappingItem("Repair Completed", "Repaired"),
                                new ValueMappingItem("Recall Incomplete. Remedy Not Yet Available", "RecallIncomplete_RemedyNotYetAvailable"),
                                new ValueMappingItem("Incomplete", "RecallIncomplete"),
                            }
                        },
                    new HtmlPathData() {ElementSource = HtmlPathElementSource.InnerText}),
                },

                NonRecallFieldMappingItems = new List<RecallFieldMappingItem>()
                {
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, ""),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "na"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NA"),
                    new RecallFieldMappingItem(RecallProperty.Status, "Repaired"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "null"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NULL"),
                },

                ErrorFlagPath = "(//div[@class='recall_row recall_error'])[3]",
                ErrorFlagValueOperation = ErrorFlagValueOperation.Exists,
                ErrorTextPath = "(//div[@class='recall_row recall_error'])[3]",
                InvalidVinErrorTextValue = "The VIN you entered is invalid. Please check and retype your VIN."
            };


        }
       
        public static ExternalRecallServiceMappingData GetPorscheData()
        {
            return new ExternalRecallServiceMappingData()
            {
                VINFieldMappingItems = new List<VINFieldMappingItem>()
				{
					new VINFieldMappingItem(VINProperty.VIN, "//div//span[@class='subTitle']", new HtmlPathData() { RegEx = @"([^;])*(?=\))", ElementSource = HtmlPathElementSource.InnerText}),
					new VINFieldMappingItem(VINProperty.Make, FieldMappingItemType.Static, "Porsche"),
                    new VINFieldMappingItem(VINProperty.Model, "//div//span[@class='subTitle']", new HtmlPathData() { RegEx = @"^[^&]*", ElementSource = HtmlPathElementSource.InnerText}),
					new VINFieldMappingItem(VINProperty.LastUpdated, "//div//span[@style='padding-left:4px;']", new FieldParseData(FieldParseType.DateTime, "MMM d,yyyy"), new HtmlPathData() { RegEx = @"[^;]*$", ElementSource = HtmlPathElementSource.InnerText }),
				},

                MappingServiceAdditionalData = new HtmlPathResponseData() { HeaderRows = 0 },
                RecallsArrayPath = "//div[@class = 'ps-ui-accordion-recalls']",

                RecallFieldMappingItems = new List<RecallFieldMappingItem>()
				{
                    new RecallFieldMappingItem(RecallProperty.Summary, "//h3[@style='padding-right:32px;']", new HtmlPathData() {RegEx = @"[A-Z a-z]*(?=&nbsp;)", ElementSource = HtmlPathElementSource.InnerText}),
					new RecallFieldMappingItem(RecallProperty.ManufacturerRecallID, "//span[@style='font-family:Arial;font-size:14px;font-weight:normal;']", new HtmlPathData() {RegEx = @"[^(&nbsp;\|&nbsp;)]+", ElementSource = HtmlPathElementSource.InnerText}),
					new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "//span[@style='font-family:Arial;font-size:14px;font-weight:normal;']", new HtmlPathData() {RegEx = @"[^;]*$", ElementSource = HtmlPathElementSource.InnerText}),
					new RecallFieldMappingItem(RecallProperty.Remedy, "//div//div[3]", new HtmlPathData() {ElementSource = HtmlPathElementSource.InnerText}),
                    new RecallFieldMappingItem(RecallProperty.Notes, "//div//div[1]", new HtmlPathData() {ElementSource = HtmlPathElementSource.InnerText}),
                    new RecallFieldMappingItem(RecallProperty.Consequence, "//div//div[2]", new HtmlPathData() {ElementSource = HtmlPathElementSource.InnerText}),
                    new RecallFieldMappingItem(RecallProperty.RecallDate, "//span[@style='font-family:Arial;font-size:14px;font-weight:normal;']", new FieldParseData(FieldParseType.DateTime, "MMM d,yyyy"), new HtmlPathData() {RegEx = @"[^(?&nbsp;.*?)]{11}", ElementSource = HtmlPathElementSource.InnerText}),

                },

                NonRecallFieldMappingItems = new List<RecallFieldMappingItem>()
                {
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, ""),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "na"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NA"),
                    new RecallFieldMappingItem(RecallProperty.Status, "Repaired"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "null"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NULL"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, null),
                },

                ErrorFlagPath = "//div[@class='alert alert-warning']",
                ErrorFlagValueOperation = ErrorFlagValueOperation.Exists,
                ErrorTextPath = "//div[@class='alert alert-warning']",
                InvalidVinErrorTextValue = "The vehicle identification number you entered is WP0CA2A82ES120132. We could not find campaign information about this vehicle because the number you entered contains an error or is not in our system."
            };

        }
        public static ExternalRecallServiceMappingData GetVolvoData()
        {
            return new ExternalRecallServiceMappingData()
            {
                VINFieldMappingItems = new List<VINFieldMappingItem>()
				{
					new VINFieldMappingItem(VINProperty.VIN, "//li[1]//div[@class='right']", new HtmlPathData() { ElementSource = HtmlPathElementSource.InnerText}),
					new VINFieldMappingItem(VINProperty.Make, FieldMappingItemType.Static, "Volvo"),
                    new VINFieldMappingItem(VINProperty.Model, "//ul[@class='recall-item-info']//li[3]//div[@class='right']", new HtmlPathData() { ElementSource = HtmlPathElementSource.InnerText}),
					new VINFieldMappingItem(VINProperty.LastUpdated, "//p[@class='small']", new FieldParseData(FieldParseType.DateTime, "MMM d, yyyy"), new HtmlPathData() { RegEx = @"[^:]*$", ElementSource = HtmlPathElementSource.InnerText }),
				},

                MappingServiceAdditionalData = new HtmlPathResponseData() { HeaderRows = 0 },
                RecallsArrayPath = "//ul[@class='recall-item-info']",

                RecallFieldMappingItems = new List<RecallFieldMappingItem>()
				{
                    new RecallFieldMappingItem(RecallProperty.Summary, "//li[4]//div[@class='right']", new HtmlPathData() { ElementSource = HtmlPathElementSource.InnerText}),
					new RecallFieldMappingItem(RecallProperty.ManufacturerRecallID, "//li[5]//div[@class='right']", new HtmlPathData() {ElementSource = HtmlPathElementSource.InnerText}),
					new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "//li[6]//div[@class='right']", new HtmlPathData() {ElementSource = HtmlPathElementSource.InnerText}),
					new RecallFieldMappingItem(RecallProperty.Remedy, "//li[8]//div[@class='right']", new HtmlPathData() {ElementSource = HtmlPathElementSource.InnerText}),
                    new RecallFieldMappingItem(RecallProperty.Consequence, "//li[7]//div[@class='right']", new HtmlPathData() {ElementSource = HtmlPathElementSource.InnerText}),
                    new RecallFieldMappingItem(RecallProperty.RecallDate, "//li[2]//div[@class='right']", new FieldParseData(FieldParseType.DateTime, "MMM d, yyyy"), new HtmlPathData() { ElementSource = HtmlPathElementSource.InnerText}),
                    new RecallFieldMappingItem(RecallProperty.Status, "//li[9]//div[2]",
                    new FieldValueMappingData()
                        {
                            MappingItems = new List<ValueMappingItem>()
                            {
                                //TODO: add more status mappings
                                new ValueMappingItem("Recall INCOMPLETE.", "RecallIncomplete"),
                            }
                        },
                    new HtmlPathData() {ElementSource = HtmlPathElementSource.InnerText}),

                },

                NonRecallFieldMappingItems = new List<RecallFieldMappingItem>()
                {
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, ""),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "na"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NA"),
                    new RecallFieldMappingItem(RecallProperty.Status, "Repaired"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "null"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, "NULL"),
                    new RecallFieldMappingItem(RecallProperty.NHTSACampaignNumber, null),
                },

                ErrorFlagPath = "//div[@class='recall-list-wrapper']//h2",
                ErrorFlagValueOperation = ErrorFlagValueOperation.Equal,
                ErrorTextPath = "//div[@class='recall-list-wrapper']//h2",
                InvalidVinErrorTextValue = "No results",
                NoRecallsErrorTextValue = "No Recalls"
            };


        }
	}
}