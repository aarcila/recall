﻿using Homenet.Deployment;

namespace Homenet.RecallSplat.Setup.Processors
{
	internal class GenerateUpdateManifestProcessor : ISetupProcessor
	{
		public void Process(SetupProcessorContext context)
		{
			SetupUtilities.ApplicationLogSource = ApplicationLogSource.RecallSplatSetup;

			SetupUtilities.WriteToApplicationLog("Starting generate update manifest...", ApplicationLogSource.RecallSplatSetup);

			SetupUtilities.GenerateUpdateManifests(context.Parameters.ActiveEnvironment, "RecallSplat-WebServices", "Setup", @"deployment", @"artifacts", true);

			SetupUtilities.WriteToApplicationLog("Completed generate update manifest.", ApplicationLogSource.RecallSplatSetup);
		}
	}
}