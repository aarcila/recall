﻿using Homenet.Deployment;

namespace Homenet.RecallSplat.Setup.Processors
{
	internal class EnableWebServicesProcessor : ISetupProcessor
	{
		public void Process(SetupProcessorContext context)
		{
			RecallSplatSetupUtilities.WriteToLog("Starting enabling of web service...");

			SetupUtilities.StartAppPool(RecallSplatSetupConstants.AppPools.WebServices);

			RecallSplatSetupUtilities.WriteToLog("Completed enabling of web service.");
		}
	}
}