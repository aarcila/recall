﻿using System;
using Couchbase;
using Couchbase.Core;
using Couchbase.Management;
using CoxAuto.ObjectStores.CouchBase;
using Homenet.MoreCommon;
using Homenet.RecallSplat.Core.Model;

namespace Homenet.RecallSplat.Setup.Processors
{
    /// <summary>
    /// Creates the Couchbase design document for RecallSplat 
    /// </summary>
    internal class PublishCouchbaseDesignDocument : ISetupProcessor
    {
        #region Fields

        /// <summary>
        /// Used to get access to the CouchBase cluster
        /// </summary>
        //private readonly ICouchbaseClusterService _couchbaseClusterService;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PublishCouchbaseDesignDocument"/> class.
        /// </summary>
        /// <param name="couchbaseClusterService">Used to get access to the CouchBase cluster</param>
        //public PublishCouchbaseDesignDocument(ICouchbaseClusterService couchbaseClusterService)
        //{
        //    _couchbaseClusterService = couchbaseClusterService;
        //}

        #endregion Constructors

        public void Process(SetupProcessorContext context)
        {
            //Cluster cluster = CouchbaseClusterService.GetCluster(Settings.DefaultEnvironment());

            //// If there are no machines running setup with CouchBase in this environment, then this processor is useless, so move on
            //if (cluster == null)
            //{
            //    CustomConsole.WriteLine(
            //        "CouchBase cluster not configured for this environment.  Skipping CouchBase bucket creation.",
            //        ConsoleColor.Yellow);
            //    return;
            //}

            //string bucketName = Settings.DefaultEnvironment().ActiveEnvironment + "_Data";
            //try
            //{
            //    _couchbaseClusterService.GetBucket(bucketName);

            //}
            //catch (Exception)
            //{
            //    CustomConsole.WriteLine(
            //                       "CouchBase cluster does not have a bucket named '{0}'.  Skipping creation of the design document for this bucket", 
            //                       bucketName,
            //                       ConsoleColor.Yellow);
            //    return;
            //}


            //CustomConsole.WriteLine("Publishing design document {0} for {1}", RecallSplatSettings.DesignDocumentName, bucketName, ConsoleColor.DarkGray);
            //IBucketManager bucketManager = GetBucketManager(bucketName);
            //IResult result = bucketManager.InsertDesignDocument(RecallSplatSettings.DesignDocumentName, RecallSplatSettings.DesignDocumentViews);

            //if (!result.Success)
            //{
            //    throw new Exception(string.Format("Failed to create DesignDocument {0} for {1}", RecallSplatSettings.DesignDocumentName, bucketName));
            //}

        }

        /// <summary>
        /// Gets the bucket manager used for inserting the design documents
        /// </summary>
        /// <param name="bucketName">The bucket to manage</param>
        /// <returns>A bucket manager for the bucket</returns>
        //private IBucketManager GetBucketManager(string bucketName)
        //{
        //    string adminUserName = Settings.DefaultEnvironment().CouchbaseServer.Username;
        //    string adminPassword = Settings.DefaultEnvironment().CouchbaseServer.Password;
        //    IBucket bucket = _couchbaseClusterService.GetBucket(bucketName);
        //    IBucketManager bucketManager = bucket.CreateManager(adminUserName, adminPassword);
        //    return bucketManager;
        //}
    }
}