﻿using System;
using System.Text.RegularExpressions;
using Homenet.MoreCommon;
using Homenet.Deployment;

namespace Homenet.RecallSplat.Setup.Processors
{
	internal class DeployWebServicesProcessor : ISetupProcessor
	{
		public static readonly string ProjectRootPath = Settings.DefaultEnvironment().RecallSplat.ApplicationsPath;

		public void Process(SetupProcessorContext context)
		{
			RecallSplatSetupUtilities.WriteToLog("Starting deploy webservices...");

			// Ensure IIS is enabled and started
			SetupUtilities.EnableIIS();

			string hostHeader = Regex.Match(Settings.GetEnvironment(context.Parameters.ActiveEnvironment).RecallSplat.WebServiceBaseURL, "//(.*?)/").Groups[1].Value;

            var creds = Settings.GetEnvironment(context.Parameters.ActiveEnvironment).InventoryOnline2.WebsiteCredentials.Split('|');

            if (Settings.DefaultEnvironment().ActiveEnvironment.Contains("production") ||
                Settings.DefaultEnvironment().ActiveEnvironment.Contains("staging") ||
                Settings.DefaultEnvironment().ActiveEnvironment.Contains("sdc"))
            {
                hostHeader = null;
            }

            // Setup IIS
            string applicationPath = string.Format(@"{0}WebServices", ProjectRootPath);
			SetupUtilities.DisableDefaultWebSite();
			SetupUtilities.ConfigureAppPool(RecallSplatSetupConstants.AppPools.WebServices, false, creds[0], creds[1], "v4.0");
			SetupUtilities.ConfigureAppPoolIdleTimeout(RecallSplatSetupConstants.AppPools.WebServices, new TimeSpan(1, 0, 0, 0, 0));
			SetupUtilities.StopAppPool(RecallSplatSetupConstants.AppPools.WebServices);

			// Copy files
			SetupUtilities.DeployFolder(string.Format(@"{0}Update\WebServices", ProjectRootPath), string.Format(@"{0}WebServices", ProjectRootPath), SetupUtilities.NGenCommand.DoNotNGen, true);
			SetupUtilities.ConfigureWebSite(RecallSplatSetupConstants.Websites.WebServices, hostHeader, applicationPath, RecallSplatSetupConstants.AppPools.WebServices);

            SetupUtilities.StartAppPool(RecallSplatSetupConstants.AppPools.WebServices);

			RecallSplatSetupUtilities.WriteToLog("Completed deploy web services.");
		}
	}
}