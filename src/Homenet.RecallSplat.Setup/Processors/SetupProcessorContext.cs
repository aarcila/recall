﻿namespace Homenet.RecallSplat.Setup.Processors
{
	internal class SetupProcessorContext
	{
		public CommandLine Parameters { get; private set; }

		public SetupProcessorContext(CommandLine parameters)
		{
			Parameters = parameters;
		}
	}
}