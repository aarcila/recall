﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlClient;
using Homenet.MoreCommon;
using Homenet.RecallSplat.Core.Model;
using Homenet.RecallSplat.Core.Repositories;

namespace Homenet.RecallSplat.Setup.Processors
{
	internal class CreateSeedDataProcessor : ISetupProcessor
	{
		private readonly ExternalRecallServiceConfigRepository _externalRecallServiceConfigRepository;
		private readonly MappingDataRepository _mappingDataRepository;
		private readonly VINManufacturerConfigMappingRepository _vinManufacturerConfigMappingRepository;

		private readonly HashSet<string> _mappingDataKeys = new HashSet<string>();
		private readonly HashSet<string> _configKeys = new HashSet<string>();
		private readonly HashSet<string> _manufacturerKeysNotFound = new HashSet<string>();

		public CreateSeedDataProcessor(ExternalRecallServiceConfigRepository externalRecallServiceConfigRepository, MappingDataRepository mappingDataRepository, VINManufacturerConfigMappingRepository vinManufacturerConfigMappingRepository)
		{
			_externalRecallServiceConfigRepository = externalRecallServiceConfigRepository;
			_mappingDataRepository = mappingDataRepository;
			_vinManufacturerConfigMappingRepository = vinManufacturerConfigMappingRepository;
		}

		public void Process(SetupProcessorContext context)
		{
			SetupMappingData();
			SetupExternalRecallServiceConfigs();
			SetupVINManufacturerConfigMappings();
		}


		private void SetupMappingData()
		{
			StoreMappingData("HondaAcura", SeedDataHelper.GetHondaAcuraMappingData());

			StoreMappingData("Audi", SeedDataHelper.GetAudiMappingData());

			StoreMappingData("GM", SeedDataHelper.GetGMMappingData());

			StoreMappingData("Infiniti", SeedDataHelper.GetInfinitiMappingData());

			StoreMappingData("Jaguar", SeedDataHelper.GetJaguarMappingData());

			StoreMappingData("LandRover", SeedDataHelper.GetLandRoverMappingData());

			StoreMappingData("Kia", SeedDataHelper.GetKiaMappingData());

			StoreMappingData("FordLincolnMercury", SeedDataHelper.GetFordLincolnMercuryData());

			StoreMappingData("Mitsubishi", SeedDataHelper.GetMitsubishiData());

			StoreMappingData("Nissan", SeedDataHelper.GetNissanData());

			StoreMappingData("Subaru", SeedDataHelper.GetSubaruMappingData());

			StoreMappingData("Volkswagen", SeedDataHelper.GetVolkswagenMappingData());

			StoreMappingData("BMW", SeedDataHelper.GetBMWMappingData());

			StoreMappingData("Mini", SeedDataHelper.GetMiniData());

            StoreMappingData("Mopar", SeedDataHelper.GetMoparData());

            StoreMappingData("Mazda", SeedDataHelper.GetMazdaData());

            StoreMappingData("Porsche", SeedDataHelper.GetPorscheData());

            StoreMappingData("Volvo", SeedDataHelper.GetVolvoData());
		}

		private void SetupExternalRecallServiceConfigs()
		{
			StoreExternalRecallServiceConfig("HondaOEM", new ExternalRecallServiceConfig
			{
				MappingDataKey = "HondaAcura",
				MappingMethod = "JsonPath",
				OpenRecallVINURL = "http://owners.honda.com/Recalls/GetRecallsByVin/{vin}/true",
				LearnMoreURL = "http://owners.honda.com/service-maintenance/recalls"
			});

			StoreExternalRecallServiceConfig("AcuraOEM", new ExternalRecallServiceConfig
			{
				MappingDataKey = "HondaAcura",
				MappingMethod = "JsonPath",
				OpenRecallVINURL = "http://owners.acura.com/Recalls/GetRecallsByVin/{vin}/true",
				LearnMoreURL = "http://owners.acura.com/service-maintenance/recalls"
			});

			StoreExternalRecallServiceConfig("AudiOEM", new ExternalRecallServiceConfig
			{
				MappingDataKey = "Audi",
				MappingMethod = "JsonPath",
				OpenRecallVINURL = "http://web.audiusa.com/audirecall/vin/{vin}",
				LearnMoreURL = "http://web.audiusa.com/recall/"
			});

			// Buick, Cadillac, Chevrolet, Hummer, GMC, Oldsmobile, Pontiac, Saturn, SAAB
			StoreExternalRecallServiceConfig("GMOEM", new ExternalRecallServiceConfig
			{
				MappingDataKey = "GM",
				MappingMethod = "JsonPath",
				OpenRecallVINURL = "https://my.gm.com/na-gm/services/{vin}/recalls",
				LearnMoreURL = "https://recalls.gm.com/"
			});

			StoreExternalRecallServiceConfig("InfinitiOEM", new ExternalRecallServiceConfig
			{
				MappingDataKey = "Infiniti",
				MappingMethod = "JsonPath",
				OpenRecallVINURL = "http://www.infinitiusa.com/dealercenter/api/recalls?vin={vin}",
				LearnMoreURL = "http://www.infinitiusa.com/recalls-vin"
			});

			StoreExternalRecallServiceConfig("JaguarOEM", new ExternalRecallServiceConfig
			{
				MappingDataKey = "Jaguar",
				MappingMethod = "JsonPath",
				OpenRecallVINURL = "http://www.jaguarusa.com/owners/vin-recall.html?view=vinRecallQuery&vin={vin}",
				LearnMoreURL = "http://www.jaguarusa.com/owners/vin-recall.html"
			});

			StoreExternalRecallServiceConfig("LandRoverOEM", new ExternalRecallServiceConfig
			{
				MappingDataKey = "LandRover",
				MappingMethod = "JsonPath",
				OpenRecallVINURL = "http://www.landroverusa.com/ownership/warranty.html?view=vinRecallQuery&vin={vin}",
				LearnMoreURL = "http://www.landroverusa.com/ownership/product-recall-search.html"
			});

			StoreExternalRecallServiceConfig("KiaOEM", new ExternalRecallServiceConfig
			{
				MappingDataKey = "Kia",
				MappingMethod = "JsonPath",
				OpenRecallVINURL = "http://www.kia.com/us/en/data/owners/recalls/search/{vin}",
				LearnMoreURL = "http://www.kia.com/us/en/content/owners/safety-recall"
			});

			// Ford, Lincoln, Mercury
			StoreExternalRecallServiceConfig("FordOEM", new ExternalRecallServiceConfig
			{
				MappingDataKey = "FordLincolnMercury",
				MappingMethod = "JsonPath",
				OpenRecallVINURL = "https://owner.ford.com/sharedServices/recalls/query.do?country=USA&language=EN&vin={vin}",
				LearnMoreURL = "https://owner.ford.com/tools/account/maintenance/recalls/results.html"
			});

			StoreExternalRecallServiceConfig("MitsubishiOEM", new ExternalRecallServiceConfig
			{
				MappingDataKey = "Mitsubishi",
				MappingMethod = "JsonPath",
				OpenRecallVINURL = "https://www.mitsubishicars.com/rs/warranty?vin={vin}",
				LearnMoreURL = "https://www.mitsubishicars.com/owners/service"
			});

			StoreExternalRecallServiceConfig("NissanOEM", new ExternalRecallServiceConfig
			{
				MappingDataKey = "Nissan",
				MappingMethod = "JsonPath",
				OpenRecallVINURL = "http://www.nissanusa.com/dealercenter/api/recalls?vin={vin}",
				LearnMoreURL = "http://www.nissanusa.com/recalls-vin"
			});

			StoreExternalRecallServiceConfig("SubaruOEM", new ExternalRecallServiceConfig
			{
				MappingDataKey = "Subaru",
				MappingMethod = "JsonPath",
				OpenRecallVINURL = "http://www.subaru.com/services/vehicles/recalls/?vin={vin}",
				LearnMoreURL = "http://www.subaru.com/vehicle-recalls.html"
			});

			StoreExternalRecallServiceConfig("VolkswagenOEM", new ExternalRecallServiceConfig
			{
				MappingDataKey = "Volkswagen",
				MappingMethod = "JsonPath",
				OpenRecallVINURL = "https://www.vw.com/s2f/vwrecall-nhtsa2/vin/{vin}",
				LearnMoreURL = "https://www.vw.com/owners-recalls/"
			});

			StoreExternalRecallServiceConfig("BMWOEM", new ExternalRecallServiceConfig
			{
				MappingDataKey = "BMW",
				MappingMethod = "JsonPath",
				OpenRecallVINURL = "http://www.bmwusa.com/Services/VinRecallService.svc/GetRecallCampaignsForVin/{vin}",
				LearnMoreURL = "http://www.bmwusa.com/standard/content/owner/safetyrecalls.aspx?mobileoverride=true"
			});

			StoreExternalRecallServiceConfig("MiniOEM", new ExternalRecallServiceConfig
			{
				MappingDataKey = "Mini",
				MappingMethod = "JsonPath",
				OpenRecallVINURL = "http://www.miniusa.com/api/safetyRecall.action?vin={vin}",
				LearnMoreURL = "http://www.miniusa.com/content/miniusa/en/owners/tools-support/recall.html"
			});

            //Mopar
            StoreExternalRecallServiceConfig("DodgeOEM", new ExternalRecallServiceConfig
            {
                MappingDataKey = "Mopar",
                MappingMethod = "HtmlPath",
                OpenRecallVINURL = "https://www.moparownerconnect.com/oc/us/en-us/sub/Pages/RecallsResults.aspx?Vin={vin}",
                LearnMoreURL = "https://chrysler.custhelp.com/app/chrysler/b2c_detail/a_id/18737/session/L2F2LzEvdGltZS8xNDA5NzY4OTY2L3NpZC9CMm4xdnUxbQ%3D%3D"
            });

            StoreExternalRecallServiceConfig("ChryslerOEM", new ExternalRecallServiceConfig
            {
                MappingDataKey = "Mopar",
                MappingMethod = "HtmlPath",
                OpenRecallVINURL = "https://www.moparownerconnect.com/oc/us/en-us/sub/Pages/RecallsResults.aspx?Vin={vin}",
                LearnMoreURL = "https://chrysler.custhelp.com/app/chrysler/b2c_detail/a_id/18737/session/L2F2LzEvdGltZS8xNDA5NzY4OTY2L3NpZC9CMm4xdnUxbQ%3D%3D"
            });

            StoreExternalRecallServiceConfig("FiatOEM", new ExternalRecallServiceConfig
            {
                MappingDataKey = "Mopar",
                MappingMethod = "HtmlPath",
                OpenRecallVINURL = "https://www.moparownerconnect.com/oc/us/en-us/sub/Pages/RecallsResults.aspx?Vin={vin}",
                LearnMoreURL = "https://chrysler.custhelp.com/app/chrysler/b2c_detail/a_id/18737/session/L2F2LzEvdGltZS8xNDA5NzY4OTY2L3NpZC9CMm4xdnUxbQ%3D%3D"
            });
            StoreExternalRecallServiceConfig("JeepOEM", new ExternalRecallServiceConfig
            {
                MappingDataKey = "Mopar",
                MappingMethod = "HtmlPath",
                OpenRecallVINURL = "https://www.moparownerconnect.com/oc/us/en-us/sub/Pages/RecallsResults.aspx?Vin={vin}",
                LearnMoreURL = "https://chrysler.custhelp.com/app/chrysler/b2c_detail/a_id/18737/session/L2F2LzEvdGltZS8xNDA5NzY4OTY2L3NpZC9CMm4xdnUxbQ%3D%3D"
            });
            StoreExternalRecallServiceConfig("RamOEM", new ExternalRecallServiceConfig
            {
                MappingDataKey = "Mopar",
                MappingMethod = "HtmlPath",
                OpenRecallVINURL = "https://www.moparownerconnect.com/oc/us/en-us/sub/Pages/RecallsResults.aspx?Vin={vin}",
                LearnMoreURL = "https://chrysler.custhelp.com/app/chrysler/b2c_detail/a_id/18737/session/L2F2LzEvdGltZS8xNDA5NzY4OTY2L3NpZC9CMm4xdnUxbQ%3D%3D"
            });
            StoreExternalRecallServiceConfig("MazdaOEM", new ExternalRecallServiceConfig
		    {
		        MappingDataKey = "Mazda",
		        MappingMethod = "HtmlPath",
		        OpenRecallVINURL = "http://www.mazdausa.com/MusaWeb/displayRecallOwners.action",
                LearnMoreURL = "http://www.mazdausa.com/MusaWeb/contactMazda.action",

		        RequestType = ExternalRecallServiceRequestType.POST,
		        PostParameters = new Dictionary<string, string>() {{"vin", "{vin}"}}
		    });
            StoreExternalRecallServiceConfig("PorscheOEM", new ExternalRecallServiceConfig
            {
                MappingDataKey = "Porsche",
                MappingMethod = "HtmlPath",
                OpenRecallVINURL = "https://recall.porsche.com/prod/pag/vinrecalllookup.nsf/GetVINResult?OpenAgent&fin={vin}&language=us&_=1463494808874",
                LearnMoreURL = "https://recall.porsche.com/prod/pag/vinrecalllookup.nsf/VIN?ReadForm&c=gb",
            });
            StoreExternalRecallServiceConfig("VolvoOEM", new ExternalRecallServiceConfig
            {
                MappingDataKey = "Volvo",
                MappingMethod = "HtmlPath",
                OpenRecallVINURL = "http://www.volvocars.com/us/own/explore/recall-information?vin={vin}#recall-search",
                LearnMoreURL = "http://volvo.custhelp.com/app/answers/list/c/215",
            });
        }

		private void StoreExternalRecallServiceConfig(string key, ExternalRecallServiceConfig config)
		{

			if (!_mappingDataKeys.Contains(config.MappingDataKey))
			{
				throw new Exception(string.Format("Unknown mapping: {0}", config.MappingDataKey));
			}

			_externalRecallServiceConfigRepository.Store(key, config);

			_configKeys.Add(key);
		}

		private void StoreMappingData(string key, ExternalRecallServiceMappingData data)
		{
			_mappingDataRepository.Store(key, data);

			_mappingDataKeys.Add(key);
		}

		private void SetupVINManufacturerConfigMappings()
		{
			string sql = @";WITH
				MANUFACTURERVINPATTERNS AS (SELECT DISTINCT SUBSTRING(VINPattern, 1, 3) AS Pattern, VINDivisionName AS DivisionName FROM VINMatch_VINPattern)
				SELECT
					mfr.ManufacturerID,
					mfr.ManufacturerName AS Manufacturer,
					make.DivisionID,
					make.DivisionName AS Make,
					vinPattern.Pattern
				FROM
					Manufacturers mfr
					INNER JOIN Divisions make
						ON mfr.ManufacturerID = make.ManufacturerID
					INNER JOIN MANUFACTURERVINPATTERNS vinPattern
						ON make.DivisionName = vinPattern.DivisionName
				ORDER BY
					mfr.ManufacturerName,
					make.DivisionName,
					vinPattern.Pattern";

			using (SqlConnection con = new SqlConnection(Settings.DefaultEnvironment().Databases.ChromeData.SQLConnectionString))
			{
				con.Open();

				using (SqlCommand cmd = new SqlCommand(sql, con))
				{
					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							string manufacturer = (string)rdr["Manufacturer"];
							string make = (string) rdr["Make"];
							string pattern = (string) rdr["Pattern"];

							VINManufacturerConfigMapping obj = new VINManufacturerConfigMapping();
							//obj.ManufacturerKey = pattern;
							obj.ManufacturerName = manufacturer;
							obj.Make = make;
							obj.ConfigKey = GetConfigKey(manufacturer, make);

							if (obj.ConfigKey != null)
							{
								StoreVINManufacturerConfigMapping(pattern, obj);
							}
						}
					}
				}
			}
		}

		private void StoreVINManufacturerConfigMapping(string key, VINManufacturerConfigMapping obj)
		{

			if (!_configKeys.Contains(obj.ConfigKey))
			{
				throw new Exception(string.Format("Unknown config key: {0}", obj.ConfigKey));
			}

			_vinManufacturerConfigMappingRepository.Store(key, obj);
		}

		private string GetConfigKey(string manufacturer, string make)
		{
			string checkName = string.Format("{0}OEM", make);
			checkName = checkName.Replace(" ", "");

			if (string.Equals(manufacturer, "General Motors", StringComparison.OrdinalIgnoreCase) ||
				string.Equals(make, "SAAB", StringComparison.OrdinalIgnoreCase))
			{
				checkName = "GMOEM";
			}

			if (string.Equals(manufacturer, "Ford", StringComparison.OrdinalIgnoreCase))
			{
				checkName = "FordOEM";
			}

			foreach (string configKey in _configKeys)
			{
				if (string.Equals(configKey, checkName, StringComparison.OrdinalIgnoreCase))
				{
					return configKey;
				}
			}
			if (!_manufacturerKeysNotFound.Contains(make))
			{
				Console.WriteLine("No mapping found for {0}", make);
				_manufacturerKeysNotFound.Add(make);
			}
			return null;
		}
	}
}