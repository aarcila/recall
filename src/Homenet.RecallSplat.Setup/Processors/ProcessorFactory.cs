﻿using System.Collections.Generic;
using Homenet.RecallSplat.Framework.Bootstrapper;

namespace Homenet.RecallSplat.Setup.Processors
{
	internal static class ProcessorFactory
	{
		public static IEnumerable<ISetupProcessor> GetProcessors(CommandLine parameters)
		{
			if (parameters.HasAction(Action.CreateSeedData)) yield return RecallSplatKernel.Get<CreateSeedDataProcessor>();
			if (parameters.HasAction(Action.CreateTestData)) yield return RecallSplatKernel.Get<CreateTestDataProcessor>();

			if (parameters.HasAction(Action.DeployAdminWebsite)) yield return RecallSplatKernel.Get<DeployAdminWebsiteProcessor>();
			if (parameters.HasAction(Action.DeployWebServices)) yield return RecallSplatKernel.Get<DeployWebServicesProcessor>();
			if (parameters.HasAction(Action.EnableAdminWebsite)) yield return RecallSplatKernel.Get<EnableAdminWebsiteProcessor>();
			if (parameters.HasAction(Action.EnableWebServices)) yield return RecallSplatKernel.Get<EnableWebServicesProcessor>();
			if (parameters.HasAction(Action.GenerateUpdateManifest)) yield return RecallSplatKernel.Get<GenerateUpdateManifestProcessor>();

			if (parameters.HasAction(Action.PublishDesignDocument)) yield return RecallSplatKernel.Get<PublishCouchbaseDesignDocument>();

			if (parameters.HasAction(Action.CreateCouchbaseBuckets)) yield return RecallSplatKernel.Get<CreateCouchbaseBucketsProcessor>();
		}
	}
}