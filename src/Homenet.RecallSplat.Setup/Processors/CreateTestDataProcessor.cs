﻿using System;

namespace Homenet.RecallSplat.Setup.Processors
{
	internal class CreateTestDataProcessor : ISetupProcessor
	{
		public void Process(SetupProcessorContext context)
		{
			Console.WriteLine("Create Test Data Here");
		}
	}
}