﻿using System;
using System.Text.RegularExpressions;
using Homenet.Deployment;
using Homenet.MoreCommon;

namespace Homenet.RecallSplat.Setup.Processors
{
	internal class DeployAdminWebsiteProcessor : ISetupProcessor
	{
		public static readonly string ProjectRootPath = Settings.DefaultEnvironment().RecallSplat.ApplicationsPath;

		public void Process(SetupProcessorContext context)
		{
			RecallSplatSetupUtilities.WriteToLog("Starting deploy admin web site...");

			// Ensure IIS is enabled and started
			SetupUtilities.EnableIIS();

			string hostHeader = Regex.Match(Settings.GetEnvironment(context.Parameters.ActiveEnvironment).RecallSplat.WebsiteBaseURL, "//(.*?)/").Groups[1].Value;

            var creds = Settings.GetEnvironment(context.Parameters.ActiveEnvironment).InventoryOnline2.WebsiteCredentials.Split('|');

            // Setup IIS
            string applicationPath = string.Format(@"{0}AdminWebsite", ProjectRootPath);
			SetupUtilities.DisableDefaultWebSite();
			SetupUtilities.ConfigureAppPool(RecallSplatSetupConstants.AppPools.AdminWebsite, false, creds[0], creds[1], "v4.0");
			SetupUtilities.ConfigureAppPoolIdleTimeout(RecallSplatSetupConstants.AppPools.AdminWebsite, new TimeSpan(1, 0, 0, 0, 0));
			SetupUtilities.StopAppPool(RecallSplatSetupConstants.AppPools.AdminWebsite);

			// Copy files
			SetupUtilities.DeployFolder(string.Format(@"{0}Update\AdminWebSite", ProjectRootPath), string.Format(@"{0}AdminWebSite", ProjectRootPath), SetupUtilities.NGenCommand.DoNotNGen, true);
			SetupUtilities.ConfigureWebSite(RecallSplatSetupConstants.Websites.AdminWebsite, hostHeader, applicationPath, RecallSplatSetupConstants.AppPools.AdminWebsite);

			SetupUtilities.StartAppPool(RecallSplatSetupConstants.AppPools.AdminWebsite);

			RecallSplatSetupUtilities.WriteToLog("Completed deploy admin web site.");
		}
	}
}