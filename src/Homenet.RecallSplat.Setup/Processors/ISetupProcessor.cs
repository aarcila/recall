﻿namespace Homenet.RecallSplat.Setup.Processors
{
	internal interface ISetupProcessor
	{
		void Process(SetupProcessorContext context);
	}
}