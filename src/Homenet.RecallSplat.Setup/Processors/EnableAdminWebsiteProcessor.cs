﻿using Homenet.Deployment;

namespace Homenet.RecallSplat.Setup.Processors
{
	internal class EnableAdminWebsiteProcessor : ISetupProcessor
	{
		public void Process(SetupProcessorContext context)
		{
			RecallSplatSetupUtilities.WriteToLog("Starting enabling of admin website...");

			SetupUtilities.StartAppPool(RecallSplatSetupConstants.AppPools.AdminWebsite);

			RecallSplatSetupUtilities.WriteToLog("Completed enabling of admin website.");
		}
	}
}