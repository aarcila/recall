﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Reflection;
using System.ServiceProcess;
using Homenet.Deployment;

namespace Homenet.RecallSplat.Setup
{
	public static class RecallSplatSetupUtilities
	{
		private const int DefaultServiceControlTimeout = 120000;

		static RecallSplatSetupUtilities()
		{
			// TODO: change this to RecallSplat
			SetupUtilities.ApplicationLogSource = ApplicationLogSource.RecallSplatSetup;
		}

		public static void WriteToLog(string text, params object[] args)
		{
			string formatted = String.Format(text, args);
			WriteToLog(formatted);
		}

		public static void WriteToLog(string text)
		{
			SetupUtilities.WriteToApplicationLog(text, ApplicationLogSource.RecallSplatSetup);
			CustomConsole.WriteLine(text);
		}

		public static void CopyFiles(string sourceFolder, string destFolder)
		{
			if (!Directory.Exists(sourceFolder))
			{
				WriteToLog("{0} does not exist.", sourceFolder, destFolder);
				return;
			}

			WriteToLog("Copying files from {0} to {1}", sourceFolder, destFolder);

			Directory.CreateDirectory(destFolder);
			DirectoryInfo sourceDirectory = new DirectoryInfo(sourceFolder);
			foreach (FileInfo fi in sourceDirectory.GetFiles())
			{
				fi.CopyTo(Path.Combine(destFolder, fi.Name), true);
			}
		}

		public static void StartService(string serviceName)
		{
			StartService(serviceName, DefaultServiceControlTimeout);
		}

		public static void StartService(string serviceName, int timeoutMilliseconds)
		{
			WriteToLog("Starting service {0}", serviceName);

			ServiceController service = new ServiceController(serviceName);
			try
			{
				TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

				service.Start();
				service.WaitForStatus(ServiceControllerStatus.Running, timeout);
			}
			catch (Exception ex)
			{
				string message = String.Format("Unable to start windows service: {0}. Reason: {1}", serviceName, ex.Message);
				WriteToLog(message);
				throw new InvalidOperationException(message);
			}
		}

		public static bool TryStopService(string serviceName)
		{
			bool success = false;

			try
			{
				StopService(serviceName);
				success = true;
			}
			catch
			{
				success = false;
			}

			return success;
		}

		public static void StopService(string serviceName)
		{
			StopService(serviceName, DefaultServiceControlTimeout);
		}

		public static void StopService(string serviceName, int timeoutMilliseconds)
		{
			WriteToLog("Stopping service {0}", serviceName);

			ServiceController service = new ServiceController(serviceName);
			try
			{
				TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

				service.Stop();
				service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
			}
			catch (Exception ex)
			{
				string message = String.Format("Unable to stop windows service: {0}. Reason: {1}", serviceName, ex.Message);
				WriteToLog(message);
				throw new InvalidOperationException(message);
			}
		}

		public static string GetAssemblyDirectory()
		{
			string currentAssemblyDirectoryName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			return currentAssemblyDirectoryName;
		}

		public static void KillProcess(string processName)
		{
			WriteToLog("Attempting to kill process: {0}", processName);

			Process[] processes = Process.GetProcessesByName(processName);
			if (processes.Length == 0)
			{
				WriteToLog("No instances of process found to kill");
			}
			else
			{

				foreach (var process in processes)
				{
					WriteToLog("Killing process id {0}", process.Id);

					process.Kill();
				}
			}

			System.Threading.Thread.Sleep(1000);
		}

		public static void ExecuteProgram(string exeName)
		{
			if (!File.Exists(exeName))
			{
				string message = string.Format("File not found: {0}", exeName);
				WriteToLog(message);
				throw new InvalidOperationException(message);
			}

			WriteToLog("Executing {0}", exeName);
			System.Diagnostics.Process.Start(exeName);
		}

		public static void CreateNetworkShare(string shareName, string folderPath)
		{
			// Calling Win32_Share class to create a shared folder
			ManagementClass managementClass = new ManagementClass("Win32_Share");

			// Get the parameter for the Create Method for the folder
			ManagementBaseObject inParams = managementClass.GetMethodParameters("Create");
			ManagementBaseObject outParams;

			// Assigning the values to the parameters
			inParams["Description"] = "Network share created by Setup";
			inParams["Name"] = shareName;
			inParams["Path"] = folderPath;
			inParams["Type"] = 0x0;

			// Finally Invoke the Create Method to do the process
			outParams = managementClass.InvokeMethod("Create", inParams, null);
			// Validation done here to check sharing is done or not
			uint returnCode = (uint)(outParams.Properties["ReturnValue"].Value);
			if (returnCode != 0)
			{
				WriteToLog("Create share return code: " + returnCode);
			}
		}

		public static void GrantNTFSPermissions(string folderPath, string domain, string group)
		{
			// Create a new DirectoryInfo object.
			DirectoryInfo directoryInfo = new DirectoryInfo(folderPath);

			// Get a DirectorySecurity object that represents the 
			// current security settings.
			System.Security.AccessControl.DirectorySecurity directorySecurity = directoryInfo.GetAccessControl();

			// Add the FileSystemAccessRule to the security settings. 
			directorySecurity.AddAccessRule(
				new System.Security.AccessControl.FileSystemAccessRule(
					new System.Security.Principal.NTAccount(domain, group),
					System.Security.AccessControl.FileSystemRights.Modify | System.Security.AccessControl.FileSystemRights.ReadAndExecute | System.Security.AccessControl.FileSystemRights.ListDirectory | System.Security.AccessControl.FileSystemRights.Read | System.Security.AccessControl.FileSystemRights.Write,
					System.Security.AccessControl.InheritanceFlags.ContainerInherit | System.Security.AccessControl.InheritanceFlags.ObjectInherit, System.Security.AccessControl.PropagationFlags.None,
					System.Security.AccessControl.AccessControlType.Allow)
				);

			// Set the new access settings.
			directoryInfo.SetAccessControl(directorySecurity);

		}

		public static bool DoesServiceExist(string serviceName)
		{
			ServiceController[] services = ServiceController.GetServices();
			ServiceController service = services.FirstOrDefault(s => s.ServiceName == serviceName);
			return service != null;
		}
	}
}