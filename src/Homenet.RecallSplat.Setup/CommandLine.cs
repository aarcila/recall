﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoxAuto.CommandLine;
using CoxAuto.Extensions;
using Homenet.MoreCommon;

namespace Homenet.RecallSplat.Setup
{
	internal class CommandLine : CommandLineOptions<CommandLine>
	{
		private readonly List<Action> _actions = new List<Action>();

		public bool DebugOnStart { get; set; }

		public List<Action> Actions
		{
			get
			{
				return _actions;
			}
		}

		public string ActiveEnvironment
		{
			get
			{
				if (string.IsNullOrEmpty(_activeEnvironment))
				{
					_activeEnvironment = Settings.DefaultEnvironment().ActiveEnvironment;
				}
				return _activeEnvironment;
			}
			private set
			{
				_activeEnvironment = value;
				Environment.SetEnvironmentVariable("HOMENET_SETTINGS_ACTIVEENVIRONMENT", _activeEnvironment);
			}
		}

		public bool BuildServerOverrideSupplied { get; private set; }

		private string _activeEnvironment;

		public CommandLine()
			: base(_options)
		{
		}

		public bool HasAction(Action action)
		{
			if (action == Action.Default) return false;
			return Actions.Contains(action);
		}

		private static readonly Option[] _options =
		{
			new Option
			{
				Short = "-d",
				Long = "--debugOnStart",
				ArgsAreOptional = true,
				Description = "Attach Debugger on start.",
				Handler =
					(args, result) =>
						result.DebugOnStart = !(args.FirstOrDefault() ?? string.Empty).Equals("false", StringComparison.OrdinalIgnoreCase),
			},

			new Option
			{
				Short = "-e",
				Long = "--environment",
				ArgsAreOptional = false,
				ArgCount = 1,
				Description = "The targeted environment name.",
				Handler =
					(args, result) =>{result.ActiveEnvironment = args.First();},
			},

			new Option {
				Short = "-a", Long = "--actions", ArgsAreOptional = false, ArgCount = 1, Description = "Comma-separated list of actions.",
				Handler = (args, result) =>
				{
					IEnumerable<Action> actions = (args.FirstOrDefault() ?? string.Empty)
						.Split(new []{','}, StringSplitOptions.RemoveEmptyEntries)
						.Select(a => a.ParseEnum<Action>())
						.ToArray();

					foreach (Action action in actions)
					{
						result.Actions.Add(action);
					}
				},
			},
			new Option {
				Short = "", Long = "--build", ArgCount = 0, Description = "", LegacyHyphenMode = true,
				Handler = (args, result) => { result.BuildServerOverrideSupplied = true; }
			},
		};


	}
}