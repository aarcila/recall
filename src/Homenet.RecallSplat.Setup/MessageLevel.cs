﻿namespace Homenet.RecallSplat.Setup
{
	internal enum MessageLevel
	{
		Debug = 0,
		Information = 1,
		Warning = 2,
		Error = 3,
		Exception = 4
	}
}