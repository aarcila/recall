﻿using System;
using System.Collections.Generic;
using System.Net;

namespace Homenet.RecallSplat.Core.Utilities
{
	internal static class CookieParser
	{
		/// <summary>
		/// This pisses me off and makes me cry that this isn't a thing that exists natively.
		/// </summary>
		/// <param name="headerValue"></param>
		/// <param name="host"></param>
		/// <returns></returns>
		public static CookieCollection ParseHeader(string headerValue, string host)
		{
			CookieCollection cc = new CookieCollection();
			if (headerValue == string.Empty)
			{
				return cc;
			}

			List<string> al = ConvertCookieHeaderToList(headerValue);
			cc = ConvertCookieArraysToCookieCollection(al, host);
			return cc;
		}


		private static List<string> ConvertCookieHeaderToList(string headerValue)
		{
			headerValue = headerValue.Replace("\r", "").Replace("\n", "");
			string[] temp = headerValue.Split(',');
			List<string> cookies = new List<string>();
			int i = 0;
			int n = temp.Length;
			while (i < n)
			{
				if (temp[i].IndexOf("expires=", StringComparison.OrdinalIgnoreCase) > 0)
				{
					cookies.Add(temp[i] + "," + temp[i + 1]);
					i++;
				}
				else
				{
					cookies.Add(temp[i]);
				}
				i++;
			}
			return cookies;
		}


		private static CookieCollection ConvertCookieArraysToCookieCollection(IReadOnlyList<string> al, string host)
		{
			CookieCollection cc = new CookieCollection();

			int alcount = al.Count;
			string strEachCook;
			string[] strEachCookParts;
			for (int i = 0; i < alcount; i++)
			{
				strEachCook = al[i];
				strEachCookParts = strEachCook.Split(';');
				int intEachCookPartsCount = strEachCookParts.Length;
				string strCNameAndCValue = string.Empty;
				string strPNameAndPValue = string.Empty;
				string strDNameAndDValue = string.Empty;
				string[] NameValuePairTemp;

				Cookie cookTemp = new Cookie();

				for (int j = 0; j < intEachCookPartsCount; j++)
				{
					if (j == 0)
					{
						strCNameAndCValue = strEachCookParts[j];
						if (strCNameAndCValue != string.Empty)
						{
							int firstEqual = strCNameAndCValue.IndexOf("=");
							string firstName = strCNameAndCValue.Substring(0, firstEqual);
							string allValue = strCNameAndCValue.Substring(firstEqual + 1, strCNameAndCValue.Length - (firstEqual + 1));
							cookTemp.Name = firstName;
							cookTemp.Value = allValue;
						}
						continue;
					}
					if (strEachCookParts[j].IndexOf("path", StringComparison.OrdinalIgnoreCase) >= 0)
					{
						strPNameAndPValue = strEachCookParts[j];
						if (strPNameAndPValue != string.Empty)
						{
							NameValuePairTemp = strPNameAndPValue.Split('=');
							if (NameValuePairTemp[1] != string.Empty)
							{
								cookTemp.Path = NameValuePairTemp[1];
							}
							else
							{
								cookTemp.Path = "/";
							}
						}
						continue;
					}

					if (strEachCookParts[j].IndexOf("domain", StringComparison.OrdinalIgnoreCase) >= 0)
					{
						strPNameAndPValue = strEachCookParts[j];
						if (strPNameAndPValue != string.Empty)
						{
							NameValuePairTemp = strPNameAndPValue.Split('=');

							if (NameValuePairTemp[1] != string.Empty)
							{
								cookTemp.Domain = NameValuePairTemp[1];
							}
							else
							{
								cookTemp.Domain = host;
							}
						}
						continue;
					}
				}

				if (cookTemp.Path == string.Empty)
				{
					cookTemp.Path = "/";
				}
				if (cookTemp.Domain == string.Empty)
				{
					cookTemp.Domain = host;
				}
				cc.Add(cookTemp);
			}
			return cc;
		}
	}
}