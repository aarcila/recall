﻿namespace Homenet.RecallSplat.Core.Model
{
	public enum RecallProperty
	{
		NHTSACampaignNumber,
		Summary,
		Consequence,
		Remedy,
		Notes,
		RecallDate,
		Status,
		ManufacturerRecallID
	}
}