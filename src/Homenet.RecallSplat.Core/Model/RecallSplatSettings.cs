﻿using Newtonsoft.Json.Linq;

namespace Homenet.RecallSplat.Core.Model
{
	public static class RecallSplatSettings
	{
		public const string ApplicationName = "RecallSplat";
// ReSharper disable once InconsistentNaming
		public const string VINManufacturerConfigMappingKeyPrefix = ApplicationName + @"\ConfigMapping\";
		public const string ExternalRecallServiceConfigKeyPrefix = ApplicationName + @"\RecallServiceConfig\";
		public const string MappingDataKeyPrefix = ApplicationName + @"\MappingData\";
		public const string DesignDocumentName = ApplicationName;
		public const string ConfigMappingKeysViewName = "ConfigMappingKeys";
		public const string MappingDataKeysViewName = "MappingDataKeys";
		public const string RecallServiceConifgKeysViewName = "RecallServiceConfigKeys";
		public const double MetricSampleRate = 100d;
		public const int MaxVinsPerApiCall = 20;

		public static string DesignDocumentViews
		{
			get { return JObject.Parse(@"{
                  'views': {
                    'ConfigMappingKeys': {
                      'map': 'function (doc, meta) {\n  var keyPrefix = \'RecallSplat\\\\ConfigMapping\\\\\';  \n  if (meta.id.indexOf(keyPrefix) == 0){\n    var key = meta.id.substr(keyPrefix.length);\n    emit(meta.id, key);\n  }\n}'
                    },
                    'MappingDataKeys': {
                      'map': 'function (doc, meta) {\n  var keyPrefix = \'RecallSplat\\\\MappingData\\\\\';  \n  if(meta.id.indexOf(keyPrefix) == 0){\n    var key = meta.id.substr(keyPrefix.length); \n    emit(meta.id, key);\n  }\n}'
                    },
                    'RecallServiceConfigKeys': {
                      'map': 'function (doc, meta) {\n  var  keyPrefix = \'RecallSplat\\\\RecallServiceConfig\\\\\';\n  if(meta.id.indexOf(keyPrefix) == 0){\n    var key = meta.id.substr(keyPrefix.length); \n    emit(meta.id, key);\n  }\n}'
                    }
                  }
                }").ToString(); }
		}

// ReSharper disable once InconsistentNaming
		public static class OEMServiceCallMetricKeyParts
		{
			public const string AppName = "recall_splat";
			public const string Root = "oem_service_calls";
			public const string Make = "make";
			public const string AllMakes = "all_makes";
			public const string Failures = "failed";
			public const string Successes = "succeeded";
			public const string AllCalls = "attempted";
			public const string Cached = "cached";
			public const string Timing = "call_duration_in_milliseconds";
		}
	}
	public enum ConfigurationType
	{
		VinManufacturerConfigMapping = 1,
		ExternalRecallServiceConfig,
		ExternalRecallServiceMappingData
	}

}