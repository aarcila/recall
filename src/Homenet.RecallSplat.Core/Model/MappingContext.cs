﻿using System.Collections.Generic;

namespace Homenet.RecallSplat.Core.Model
{
	public class MappingContext
	{
		public MappingContext()
		{
			Variables = new Dictionary<string, string>();
		}
		public Dictionary<string, string> Variables { get; set; }

		public ExternalRecallServiceMappingData MappingData { get; set; }

        public string ResponseText { get; set; }
	}
}