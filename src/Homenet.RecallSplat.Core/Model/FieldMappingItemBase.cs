﻿using System.Collections.Generic;

namespace Homenet.RecallSplat.Core.Model
{
	public class FieldMappingItemBase
	{
		public FieldMappingItemBase(string source)
		{
			Source = source;
			Type = FieldMappingItemType.Mapped;
		}

        public FieldMappingItemBase(string source, object mappingServiceAdditionalData)
        {
            Source = source;
            Type = FieldMappingItemType.Mapped;
            MappingServiceAdditionalData = mappingServiceAdditionalData;
        }

        public FieldMappingItemBase(string source, FieldMappingItemType type)
		{
			Source = source;
			Type = type;
		}

		public FieldMappingItemBase(string source, FieldParseData parseData)
		{
			Source = source;
			Type = FieldMappingItemType.Mapped;
			ParseData = parseData;
		}

        public FieldMappingItemBase(string source, FieldParseData parseData, object mappingServiceAdditionalData)
        {
            Source = source;
            Type = FieldMappingItemType.Mapped;
            ParseData = parseData;
            MappingServiceAdditionalData = mappingServiceAdditionalData;
        }

        public FieldMappingItemBase(string source, FieldValueMappingData valueMappingData)
		{
			Source = source;
			Type = FieldMappingItemType.Mapped;
			ValueMappingData = valueMappingData;
		}

        public FieldMappingItemBase(string source, FieldValueMappingData valueMappingData, object mappingServiceAdditionalData)
        {
            Source = source;
            Type = FieldMappingItemType.Mapped;
            ValueMappingData = valueMappingData;
            MappingServiceAdditionalData = mappingServiceAdditionalData;
        }

        public string Source { get; set; }

		public FieldParseData ParseData { get; set; }

		public FieldValueMappingData ValueMappingData { get; set; }

		public FieldMappingItemType Type { get; set; }

        public object MappingServiceAdditionalData { get; set; }
	}
}