﻿namespace Homenet.RecallSplat.Core.Model
{
	public enum FieldParseType
	{
		None,
		DateTime,
		EpochConvert
	}
}