﻿using System;
using System.Diagnostics;
using System.Net;

namespace Homenet.RecallSplat.Core.Model
{
	[DebuggerDisplay("{IPAddress}:{Port}")]
	public class ProxyAddress : IEquatable<ProxyAddress>
	{

		public IPAddress IPAddress { get; set; }
		public int Port { get; set; }


		public ProxyAddress() { }

		public ProxyAddress(string s)
		{
			if (string.IsNullOrWhiteSpace(s)) throw new ArgumentNullException("s", "ProxyAddress value was not supplied.");

			string ipPart = s;
			int indexOfCommaOrColon = s.LastIndexOfAny(new[] { ',', ':' });
			if (indexOfCommaOrColon >= 0)
			{
				string portPart = s.Substring(indexOfCommaOrColon + 1).Trim();
				ipPart = s.Substring(0, indexOfCommaOrColon);

				int port;
				if (!int.TryParse(portPart, out port)) throw new FormatException("Port was specified, but was not an integer.");
				Port = port;
			}

			IPAddress = IPAddress.Parse(ipPart);
		}

		public static ProxyAddress Parse(string s)
		{
			return new ProxyAddress(s);
		}

		public bool Equals(ProxyAddress other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return Equals(IPAddress, other.IPAddress) && Port == other.Port;
		}

		public override bool Equals(object obj)
		{
			return Equals((ProxyAddress)obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return ((IPAddress != null ? IPAddress.GetHashCode() : 0) * 397) ^ Port;
			}
		}

		public override string ToString()
		{
			return string.Format("{0}:{1}", IPAddress, Port);
		}



		public static implicit operator ProxyAddress(string s)
		{
			if (string.IsNullOrWhiteSpace(s)) return null;
			return Parse(s);
		}
		public static implicit operator string(ProxyAddress pa)
		{
			if (ReferenceEquals(pa, null)) return null;
			return pa.ToString();
		}
	}
}