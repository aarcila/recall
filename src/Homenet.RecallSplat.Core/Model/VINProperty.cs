﻿namespace Homenet.RecallSplat.Core.Model
{
	public enum VINProperty
	{
		VIN,
		Manufacturer,
		ModelYear,
		Make,
		Model,
		LastUpdated
	}
}