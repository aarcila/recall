﻿using System;
using System.Net;

namespace Homenet.RecallSplat.Core.Model
{
	public class OEMResponse
	{
		public DateTime DateRetrieved { get; set; }
		public HttpStatusCode StatusCode { get; set; }
		public string ResponseText { get; set; }
		public string Url { get; set; }

		public Guid RequestID { get; set; }
	}
}