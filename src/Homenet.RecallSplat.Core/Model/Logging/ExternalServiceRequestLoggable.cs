﻿using System;
using Homenet.RecallSplat.Core.Services;

namespace Homenet.RecallSplat.Core.Model.Logging
{
	public class ExternalServiceRequestLoggable : RecallSplatLoggable
	{
		public string ConfigKey { get; set; }

		public string Url { get; set; }

		public int ResponseStatusCode { get; set; }

		public TimeSpan Duration { get; set; }

		public bool HadError { get; set; }

		public Exception Exception { get; set; }

		

		public Guid RequestID { get; set; }

		public ExternalServiceRequestLoggable()
			: base(LogLevel.Info)
		{
		}
	}
}