﻿using CoxAuto.Logging;
using Homenet.RecallSplat.Core.Services;

namespace Homenet.RecallSplat.Core.Model.Logging
{

	/// <summary>
	/// The base Loggable class for the RecallSplat project.
	/// All loggable shapes should inherit from this.
	/// </summary>
	public class RecallSplatLoggable : Loggable
	{
		public LogLevel Level { get; set; }

		public RecallSplatLoggable(LogLevel logLevel)
		{
			Level = logLevel;
		}
	}
}