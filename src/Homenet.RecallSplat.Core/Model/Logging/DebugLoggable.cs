﻿using Homenet.RecallSplat.Core.Services;

namespace Homenet.RecallSplat.Core.Model.Logging
{
    public class DebugLoggable : RecallSplatLoggable
    {
        public string Message { get; set; }

        public DebugLoggable() : base(LogLevel.Debug)
        {
        }
    }
}