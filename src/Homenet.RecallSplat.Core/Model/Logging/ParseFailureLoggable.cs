﻿using Homenet.RecallSplat.Core.Services;

namespace Homenet.RecallSplat.Core.Model.Logging
{
	public class ParseFailureLoggable : RecallSplatLoggable
	{
		public FieldParseType FieldParseType { get; set; }

		public string FormatString { get; set; }
		public string PropertyName { get; set; }
		public string SourceData { get; set; }

		public string Url { get; set; }

		public ParseFailureLoggable()
			: base(LogLevel.Warning)
		{
		}
	}
}