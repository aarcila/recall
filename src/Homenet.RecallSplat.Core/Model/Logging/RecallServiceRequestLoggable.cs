﻿using System;
using Homenet.RecallSplat.Core.Services;

namespace Homenet.RecallSplat.Core.Model.Logging
{
	public class RecallServiceRequestLoggable : RecallSplatLoggable
	{
		private const int _version = 3;

		public string VIN { get; set; }

		public bool HadError { get; set; }

		public Exception Exception { get; set; }

		public string Manufacturer { get; set; }

		public string Make { get; set; }

		public string ConfigKey { get; set; }

		public string MappingDataKey { get; set; }

		public string MappingMethod { get; set; }


		public TimeSpan? TotalDuration { get; set; }


		public Guid? RequestID { get; set; }
		public DateTime? RequestDate { get; set; }
		public int? ResponseStatusCode { get; set; }
		public TimeSpan? RequestDuration { get; set; }
		public bool FromCache { get; set; }
		public DateTime? OEMFeedLastUpdated { get; set; }
		public int? RecallCount { get; set; }
		public int? SoftRecallCount { get; set; }

		public int LoggableVersion { get { return _version; } }


		public RecallServiceRequestLoggable() : base(LogLevel.Info)
		{
		}
	}
}