﻿using Homenet.RecallSplat.Core.Services;

namespace Homenet.RecallSplat.Core.Model.Logging
{
	public class ValueMappingNotFoundLoggable : RecallSplatLoggable
	{
		public string Url { get; set; }
		public string PropertyName { get; set; }
		public string SourceData { get; set; }

		public ValueMappingNotFoundLoggable()
			: base(LogLevel.Warning)
		{
		}
	}
}