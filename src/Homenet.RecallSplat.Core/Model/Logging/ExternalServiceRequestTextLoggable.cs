﻿using System;
using Homenet.RecallSplat.Core.Services;

namespace Homenet.RecallSplat.Core.Model.Logging
{
	public class ExternalServiceRequestTextLoggable : RecallSplatLoggable
	{
		public ExternalServiceRequestTextLoggable()
			: base(LogLevel.Debug)
		{
		}

		public Guid RequestID { get; set; }
		public string ResponseText { get; set; }
	}
}