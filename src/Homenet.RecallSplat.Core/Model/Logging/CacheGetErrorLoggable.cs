﻿using System;
using Homenet.RecallSplat.Core.Services;

namespace Homenet.RecallSplat.Core.Model.Logging
{
    public class CacheGetErrorLoggable : RecallSplatLoggable
    {
        public string VIN { get; set; }
        public Exception Exception { get; set; }

        public CacheGetErrorLoggable() : base(LogLevel.Info)
        {
        }
    }
}