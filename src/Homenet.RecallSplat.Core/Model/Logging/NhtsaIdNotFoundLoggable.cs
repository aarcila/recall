﻿using CoxAuto.Logging;
using Homenet.RecallSplat.Core.Services;

namespace Homenet.RecallSplat.Core.Model.Logging
{
    public class NhtsaIdNotFoundLoggable : RecallSplatLoggable
    {
        public string Url { get; set; }

        public NhtsaIdNotFoundLoggable()
            : base(LogLevel.Info)
        {
        }
    }
}
