﻿using System;
using Homenet.RecallSplat.Core.Services;

namespace Homenet.RecallSplat.Core.Model.Logging
{
	public class MappingLoggable : RecallSplatLoggable
	{
		public string ConfigKey { get; set; }

		public bool FromCache { get; set; }

		public DateTime MappingDateTime { get; set; }

		public DateTime? FeedLastUpdated { get; set; }

		public Guid? RequestID { get; set; }

		public bool HadError { get; set; }

		public Exception Exception { get; set; }

		public MappingLoggable()
			: base(LogLevel.Info)
		{
		}
	}
}