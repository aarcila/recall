﻿using System.Collections.Generic;

namespace Homenet.RecallSplat.Core.Model
{
	public class VINFieldMappingItem : FieldMappingItemBase
	{
		public VINFieldMappingItem() : base(null)
		{
		}

		public VINFieldMappingItem(VINProperty vinProperty, string source) : base (source)
		{
			VinProperty = vinProperty;
		}

        public VINFieldMappingItem(VINProperty vinProperty, string source, object mappingServiceAdditionalData) : base(source, mappingServiceAdditionalData)
        {
            VinProperty = vinProperty;
        }

        public VINFieldMappingItem(VINProperty vinProperty, FieldMappingItemType type, string source)
			: base(source, type)
		{
			VinProperty = vinProperty;
		}

		public VINFieldMappingItem(VINProperty vinProperty, string source, FieldParseData parseData, object mappingServiceAdditionalData)
			: base(source, parseData, mappingServiceAdditionalData)
		{
			VinProperty = vinProperty;
		}

        public VINFieldMappingItem(VINProperty vinProperty, string source, FieldParseData parseData)
    : base(source, parseData)
        {
            VinProperty = vinProperty;
        }

        public VINFieldMappingItem(VINProperty vinProperty, string source, FieldValueMappingData valueMappingData)
			: base(source, valueMappingData)
		{
			VinProperty = vinProperty;
		}

		public VINProperty VinProperty { get; set; }
	}
}