﻿namespace Homenet.RecallSplat.Core.Model
{
    public class HealthCheck
    {
        public HealthCheckStatusKind HealthCheckStatus { get; set; }

        public string Message { get; set; }

        public HealthCheckCategoryKind HealthCheckCategory { get; set; }
    }

    public enum HealthCheckCategoryKind
    {
        None = 0,
        Couchbase,
    }

    public enum HealthCheckStatusKind
    {
        None = 0,
        Ok,
        Failure,
        PartialFailure,
    }
}
