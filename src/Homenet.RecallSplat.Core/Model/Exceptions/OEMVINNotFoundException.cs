﻿using System;
using System.Runtime.Serialization;

namespace Homenet.RecallSplat.Core.Model.Exceptions
{
	[Serializable]
	public class OEMVINNotFoundException : Exception
	{
		//
		// For guidelines regarding the creation of new exception types, see
		//    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpgenref/html/cpconerrorraisinghandlingguidelines.asp
		// and
		//    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dncscol/html/csharp07192001.asp
		//

		public OEMVINNotFoundException()
		{
		}

		public OEMVINNotFoundException(string message) : base(message)
		{
		}

		public OEMVINNotFoundException(string message, Exception inner) : base(message, inner)
		{
		}

		protected OEMVINNotFoundException(
			SerializationInfo info,
			StreamingContext context) : base(info, context)
		{
		}
	}
}