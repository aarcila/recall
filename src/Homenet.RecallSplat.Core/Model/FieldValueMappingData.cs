﻿using System.Collections.Generic;

namespace Homenet.RecallSplat.Core.Model
{
	public class FieldValueMappingData
	{
		public FieldValueMappingData()
		{
			MappingItems = new List<ValueMappingItem>();
		}

		public List<ValueMappingItem> MappingItems { get; set; } 
	}

	public class ValueMappingItem
	{
		public ValueMappingItem(string key, string val)
		{
			Key = key;
			Value = val;
		}

		public string Key { get; set; }
		public string Value { get; set; }
	}
}