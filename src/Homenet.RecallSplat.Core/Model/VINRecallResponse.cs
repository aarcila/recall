﻿using System;
using System.Linq;
using System.Web.Configuration;
using System.Configuration;

namespace Homenet.RecallSplat.Core.Model
{
	public class VINRecallResponse
	{
		public string ErrorMessage { get; set; }
		public RecallRequestStatusEnum Status { get; set; }

		public VINResponse[] VINs { get; set; }
	}

	public class VINResponse
	{
		public VINResponse()
		{
			Status = RecallVINStatusEnum.Unknown;
		}

		public string VIN { get; set; }
		public int? Year { get; set; }
		public string Make { get; set; }
		public string Model { get; set; }
		public RecallVINStatusEnum Status { get; set; }
		public DateTime? FeedLastUpdated { get; set; }
		public DateTime? OEMFeedLastUpdated { get; set; }
		public string LearnMoreOEMURL { get; set; }
		public string RecallDataSourceURL { get; set; }

		public int OpenRecallCount {
			get
			{
				int ret = 0;
				if (OpenRecalls != null)
				{
					ret = OpenRecalls.Length;
				}
				return ret;
			}
		}

		public int SoftRecallCount
		{
			get
			{
				int ret = 0;
				if (OpenRecalls != null)
				{
					ret = OpenRecalls.Count(p => string.IsNullOrWhiteSpace(p.CampaignID));
				}
				return ret;
			}
		}

		public OpenRecallResponse[] OpenRecalls { get; set; }

		public string ErrorMessage { get; set; }
		public bool HadError { get; set; }
	}

	public class OpenRecallResponse
	{
		public OpenRecallResponse()
		{
			Status = RecallStatusEnum.Unknown;
		}

		public string CampaignID { get; set; }
		public string OEMCampaignID { get; set; }
        public string NHTSACampaignURL { get; set; }
		public DateTime? CampaignDate { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public string SafetyRisk { get; set; }
		public string RemedyDescription { get; set; }
		public RecallStatusEnum Status { get; set; }
	}

	public enum RecallRequestStatusEnum
	{
		Unknown = 0,
		AllSuccessful = 1,
		SomeErrors = 2,
		AllErrors = 3,
		SystemError = 4
	}

	public enum RecallVINStatusEnum
	{
		Unknown = 0,
		OK = 1,
		OEMNotFound = 2,
		OEMCouldNotFindVIN = 3,
		OEMError = 4,
		SystemError = 5
	}

	public enum RecallStatusEnum
	{
		Unknown = 0,
		RecallIncomplete = 11,
		RecallIncomplete_RemedyNotYetAvailable = 12,
        Repaired = 99
	}
}