﻿namespace Homenet.RecallSplat.Core.Model
{
    public class HtmlPathResponseData
    {
        public int HeaderRows { get; set; }
    }

    public class HtmlPathData
    {
        public HtmlPathData()
        {
            RemoveNewLines = true;
            RemoveTabs = true;
        }

        public HtmlPathElementSource ElementSource { get; set; }

        public string RegEx { get; set; }

        public bool RemoveNewLines { get; set; }

        public bool RemoveTabs { get; set; }
    }

    public enum HtmlPathElementSource
    {
        ValueAttribute,
        InnerText
    }
}