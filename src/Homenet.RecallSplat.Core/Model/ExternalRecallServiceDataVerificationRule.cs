﻿namespace Homenet.RecallSplat.Core.Model
{
	public class ExternalRecallServiceDataVerificationRule
	{
		public ExternalRecallServiceDataVerificationRule()
		{
		}

		public ExternalRecallServiceDataVerificationRule(DataVerificationRuleType type, string data)
		{
			Type = type;
			Data = data;
		}

		public DataVerificationRuleType Type { get; set; }
		public string Data { get; set; }
	}
}