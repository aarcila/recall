﻿namespace Homenet.RecallSplat.Core.Model
{
	public class RecallFieldMappingItem : FieldMappingItemBase
	{
		public RecallFieldMappingItem() : base(null)
		{
		}

		public RecallFieldMappingItem(RecallProperty recallProperty, string source) : base(source)
		{
			RecallProperty = recallProperty;
		}

        public RecallFieldMappingItem(RecallProperty recallProperty, string source, object mappingServiceAdditionalData) : base(source)
        {
            RecallProperty = recallProperty;
            MappingServiceAdditionalData = mappingServiceAdditionalData;
        }

        public RecallFieldMappingItem(RecallProperty recallProperty, string source, FieldParseData parseData)
			: base(source, parseData)
		{
			RecallProperty = recallProperty;
		}

        public RecallFieldMappingItem(RecallProperty recallProperty, string source, FieldParseData parseData, object mappingServiceAdditionalData)
    : base(source, parseData, mappingServiceAdditionalData)
        {
            RecallProperty = recallProperty;
        }

        public RecallFieldMappingItem(RecallProperty recallProperty, string source, FieldValueMappingData valueMappingData)
			: base(source, valueMappingData)
		{
			RecallProperty = recallProperty;
		}

        public RecallFieldMappingItem(RecallProperty recallProperty, string source, FieldValueMappingData valueMappingData, object mappingServiceAdditionalData)
    : base(source, valueMappingData, mappingServiceAdditionalData)
        {
            RecallProperty = recallProperty;
        }

        public RecallProperty RecallProperty { get; set; }
	}
}