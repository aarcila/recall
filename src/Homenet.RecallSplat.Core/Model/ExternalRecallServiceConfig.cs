﻿using System.Collections.Generic;
using System.Collections.Specialized;

namespace Homenet.RecallSplat.Core.Model
{
	public class ExternalRecallServiceConfig
	{
	    public ExternalRecallServiceConfig()
	    {
	        RequestType = ExternalRecallServiceRequestType.GET;
	        PostParameters = new Dictionary<string, string>();

	    }

		//public string ConfigKey { get; set; }

		public string OpenRecallVINURL { get; set; }

		public string MappingMethod { get; set; }

		public string MappingDataKey { get; set; }

		public string LearnMoreURL { get; set; }


        public ExternalRecallServiceRequestType RequestType { get; set; }
        public Dictionary<string,string> PostParameters { get; set; }
    }

    public enum ExternalRecallServiceRequestType
    {
        GET,
        POST
    }
}