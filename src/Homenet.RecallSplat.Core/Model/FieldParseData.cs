﻿namespace Homenet.RecallSplat.Core.Model
{
	public class FieldParseData
	{
		public FieldParseData()
		{
		}

		public FieldParseData(FieldParseType parseType, string formatString)
		{
			ParseType = parseType;
			FormatString = formatString;
		}

		public FieldParseType ParseType { get; set; }

		public string FormatString { get; set; }
	}
}