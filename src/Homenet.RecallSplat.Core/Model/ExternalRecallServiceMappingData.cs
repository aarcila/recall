﻿using System.Collections.Generic;
using Homenet.RecallSplat.Core.Processing;

namespace Homenet.RecallSplat.Core.Model
{
	public class ExternalRecallServiceMappingData
	{
		public ExternalRecallServiceMappingData()
		{
			DataVerificationRules = new List<ExternalRecallServiceDataVerificationRule>();
			VINFieldMappingItems = new List<VINFieldMappingItem>();
			RecallFieldMappingItems = new List<RecallFieldMappingItem>();
            NonRecallFieldMappingItems = new List<RecallFieldMappingItem>();

			ErrorFlagValueOperation = ErrorFlagValueOperation.Equal;

		    PreProcessors = new List<IProcessor>();
		}

		public string RecallsArrayPath { get; set; }

		public List<VINFieldMappingItem> VINFieldMappingItems { get; set; }

		public List<RecallFieldMappingItem> RecallFieldMappingItems { get; set; }

		public List<ExternalRecallServiceDataVerificationRule> DataVerificationRules { get; set; }

        public List<RecallFieldMappingItem> NonRecallFieldMappingItems { get; set; }

		public string ErrorFlagPath { get; set; }
		public string ErrorFlagValue { get; set; }

		public ErrorFlagValueOperation ErrorFlagValueOperation { get; set; }

		public string ErrorTextPath { get; set; }
		public string InvalidVinErrorTextValue { get; set; }

		public string ErrorTextArrayPath { get; set; }

        public object MappingServiceAdditionalData { get; set; }

        public string NoRecallsErrorTextValue { get; set; }

        public List<IProcessor> PreProcessors { get; set; }
    }

	public enum ErrorFlagValueOperation
	{
		Equal,
		NotEqual,
        Exists
	}

}