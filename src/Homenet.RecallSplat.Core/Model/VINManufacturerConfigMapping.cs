﻿namespace Homenet.RecallSplat.Core.Model
{
	public class VINManufacturerConfigMapping
	{
		//public string ManufacturerKey { get; set; }

		public string ManufacturerName { get; set; }

		public string Make { get; set; }

		public string ConfigKey { get; set; }
	}
}