﻿using System.Collections.Generic;
using Homenet.RecallSplat.Core.Interfaces;
using Homenet.RecallSplat.Core.Model;

namespace Homenet.RecallSplat.Core.Repositories
{
	public class ExternalRecallServiceConfigRepository
	{
		private readonly IRepository<ExternalRecallServiceConfig> _repository;

		public ExternalRecallServiceConfigRepository(IRepository<ExternalRecallServiceConfig> repository)
		{
			_repository = repository;
		}

		protected string KeyPrefix
		{
			get { return RecallSplatSettings.ExternalRecallServiceConfigKeyPrefix; }
		}

		public void Store(string key, ExternalRecallServiceConfig value)
		{
			if (key.StartsWith(KeyPrefix))
			{
				_repository.Store(key, value);
			}
			else
			{
				_repository.Store(KeyPrefix + key, value);
			}
		}

		public ExternalRecallServiceConfig Get(string key)
		{
			return key.StartsWith(KeyPrefix) ? _repository.Get(key) : _repository.Get(KeyPrefix + key);
		}

		public IEnumerable<string> GetKeys()
		{
			return _repository.GetKeys(KeyPrefix);
		}

		public IEnumerable<ExternalRecallServiceConfig> GetValues()
		{
			return _repository.GetValues(KeyPrefix);
		}

		public void Remove(string key)
		{
			if (key.StartsWith(KeyPrefix))
			{
				_repository.Remove(key);
			}
			else
			{
				_repository.Remove(KeyPrefix + key);
			}
		}
	}
}
