﻿using System;
using System.Collections.Generic;
using System.Linq;
using Couchbase.Core;
using Couchbase.Views;
using CoxAuto.ObjectStores;
using CoxAuto.ObjectStores.CouchBase;
using Homenet.RecallSplat.Core.Interfaces;
using Homenet.RecallSplat.Core.Model;

namespace Homenet.RecallSplat.Core.Repositories
{
	public class CouchbaseRepository<TValue> : IRepository<TValue> where TValue : class
	{
		#region Fields

		//private readonly ICouchbaseClusterService _couchbaseClusterService;
		private readonly IObjectStore _objectStore;
		private readonly string _viewName;

		#endregion

		#region Constructors

		public CouchbaseRepository(IObjectStore objectStore,
			string viewName)
		{
			//_couchbaseClusterService = couchbaseClusterService;
			_objectStore = objectStore;
			_viewName = viewName;
		}

		public string ViewName
		{
			get { return _viewName; }
		}

		#endregion

		#region IRepository methods

		public virtual void Store(string key, TValue config)
		{
			_objectStore.Store(key, config );
		}

		public virtual TValue Get(string key)
		{
			var osr = _objectStore.Get<TValue>(key);
			if (osr.Status == ObjectStoreResultStatus.KeyNotFound)
			{
				return null;
			}

			return osr.Value;
		}

		public IEnumerable<string> GetKeys(string keyPrefix)
		{
            throw new NotImplementedException();
			//if (string.IsNullOrWhiteSpace(ViewName))
			//{
			//	// TODO: Log this error
			//	throw new ApplicationException("ViewName cannot be Null or Empty");
			//}

			//IViewQuery viewQuery = Bucket.CreateQuery(RecallSplatSettings.DesignDocumentName, ViewName)
			//	.Stale(StaleState.False);
			//IViewResult<string> viewResult = Bucket.Query<string>(viewQuery);

			//if (!viewResult.Success)
			//{
			//	//TODO: Log this error
			//	throw new ApplicationException(viewResult.Message, viewResult.Exception);
			//}

			//return viewResult.Values.Select(key => keyPrefix + key);
		}

		public IEnumerable<TValue> GetValues(string keyPrefix)
		{
			foreach (string key in GetKeys(keyPrefix))
			{
				TValue value = default(TValue);
				var osr = _objectStore.Get<TValue>(key);
				if (osr.Status != ObjectStoreResultStatus.KeyNotFound)
				{
					value = osr.Value;
				}
				if (value == null) continue;
				yield return value;
			}
		}


		public virtual void Remove(string key)
		{
			_objectStore.Remove(key);
		}

		#endregion

		#region properties

		//private IBucket Bucket
		//{
		//	get
		//	{
		//		string bucketName = _couchbaseClusterService.BucketNames.FirstOrDefault(s => s.EndsWith("Data"));
		//		if (bucketName == null)
		//		{
		//			throw new Exception("Could not find bucket that ends with Data.");
		//		}

		//		return _couchbaseClusterService.GetBucket(bucketName);
		//	}
		//}

		#endregion

	}
}