﻿using System.Collections.Generic;
using Homenet.RecallSplat.Core.Interfaces;
using Homenet.RecallSplat.Core.Model;

namespace Homenet.RecallSplat.Core.Repositories
{
	public class MappingDataRepository
	{
		private readonly IRepository<ExternalRecallServiceMappingData> _repository;

		public MappingDataRepository(IRepository<ExternalRecallServiceMappingData> repository)
		{
			_repository = repository;
		}

		protected string KeyPrefix
		{
			get { return RecallSplatSettings.MappingDataKeyPrefix; }
		}

		public void Store(string key, ExternalRecallServiceMappingData value)
		{
			if (key.StartsWith(KeyPrefix))
			{
				_repository.Store(key, value);
			}
			else
			{
				_repository.Store(KeyPrefix + key, value);
			}
		}

		public ExternalRecallServiceMappingData Get(string key)
		{
			return key.StartsWith(KeyPrefix) ? _repository.Get(key) : _repository.Get(KeyPrefix + key);
		}

		public IEnumerable<string> GetKeys()
		{
			return _repository.GetKeys(KeyPrefix);
		}

		public IEnumerable<ExternalRecallServiceMappingData> GetValues()
		{
			return _repository.GetValues(KeyPrefix);
		}

		public void Remove(string key)
		{
			if (key.StartsWith(KeyPrefix))
			{
				_repository.Remove(key);
			}
			else
			{
				_repository.Remove(KeyPrefix + key);
			}
		}

	}
}