﻿using System.Collections.Generic;
using Homenet.RecallSplat.Core.Interfaces;
using Homenet.RecallSplat.Core.Model;

namespace Homenet.RecallSplat.Core.Repositories
{
// ReSharper disable once InconsistentNaming
	public class VINManufacturerConfigMappingRepository
	{
		private readonly IRepository<VINManufacturerConfigMapping> _repository;

		public VINManufacturerConfigMappingRepository(IRepository<VINManufacturerConfigMapping> repository)
		{
			_repository = repository;
		}

		protected string KeyPrefix
		{
			get { return RecallSplatSettings.VINManufacturerConfigMappingKeyPrefix; }
		}


		public void Store(string key, VINManufacturerConfigMapping value)
		{
			if (key.StartsWith(KeyPrefix))
			{
				_repository.Store(key, value);
			}
			else
			{
				_repository.Store(KeyPrefix + key, value);
			}
		}

		public VINManufacturerConfigMapping Get(string key)
		{
			return key.StartsWith(KeyPrefix) ? _repository.Get(key) : _repository.Get(KeyPrefix + key);
		}

		public IEnumerable<string> GetKeys()
		{
			return _repository.GetKeys(KeyPrefix);
		}

		public IEnumerable<VINManufacturerConfigMapping> GetValues()
		{
			return _repository.GetValues(KeyPrefix);
		}

		public void Remove(string key)
		{
			if (key.StartsWith(KeyPrefix))
			{
				_repository.Remove(key);
			}
			else
			{
				_repository.Remove(KeyPrefix + key);
			}
		}
	}
}