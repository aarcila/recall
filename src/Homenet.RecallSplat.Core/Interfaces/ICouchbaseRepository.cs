﻿using System.Collections.Generic;

namespace Homenet.RecallSplat.Core.Interfaces
{
    public interface ICouchbaseRepository<TValue> where TValue: class
    {
        void Store(string key, TValue value);

        TValue Get(string key);

        IEnumerable<string> GetKeys();

        IEnumerable<TValue> GetAll();

        void Remove(string key);
    }
}
