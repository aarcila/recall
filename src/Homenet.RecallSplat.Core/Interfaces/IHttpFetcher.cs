﻿using System.Collections.Specialized;
using System.IO;
using System.Net;
using Homenet.RecallSplat.Core.Model;

namespace Homenet.RecallSplat.Core.Interfaces
{
	public interface IHttpFetcher
	{
		HttpWebResponse Get(string url, NameValueCollection headers = null, ProxyAddress proxy = null);
		void Get(string url, out string text, out HttpStatusCode statusCode, NameValueCollection headers = null, ProxyAddress proxy = null);
		void Get(string url, out byte[] bytes, NameValueCollection headers = null, ProxyAddress proxy = null);
		void Get(string url, out Stream stream, NameValueCollection headers = null, ProxyAddress proxy = null);
		HttpWebResponse Post(string url, NameValueCollection form, NameValueCollection headers = null, ProxyAddress proxy = null);
		void Post(string url, NameValueCollection form, out string text, out HttpStatusCode statusCode, NameValueCollection headers = null, ProxyAddress proxy = null);
		void Post(string url, NameValueCollection form, out byte[] bytes, NameValueCollection headers = null, ProxyAddress proxy = null);
		void Post(string url, NameValueCollection form, out Stream stream, NameValueCollection headers = null, ProxyAddress proxy = null);
	}
}