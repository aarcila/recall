﻿using System.Collections.Generic;
using Homenet.RecallSplat.Core.Model;

namespace Homenet.RecallSplat.Core.Interfaces
{
    public interface IDiagnosticsService
    {
        List<HealthCheck> GetHealthChecks();
    }
}
