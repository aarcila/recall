﻿namespace Homenet.RecallSplat.Core.Interfaces
{
    public interface IMetricGraphingService
    {
        void SendTimerMetric<T>(string metricKey, T timerValue, double? sampleRate = null) where T : struct;
        void SendGaugeMetric<T>(string metricKey, T gaugheValue, double? sampleRate = null) where T : struct;
        void SendCounterMetric(string metricKey, int increment = 1, double? sampleRate = null);
    }
}