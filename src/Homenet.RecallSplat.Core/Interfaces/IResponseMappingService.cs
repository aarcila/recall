﻿using Homenet.RecallSplat.Core.Model;

namespace Homenet.RecallSplat.Core.Interfaces
{
	public interface IResponseMappingService
	{
		VINResponse MapResponse(MappingContext mappingContext);
	}
}