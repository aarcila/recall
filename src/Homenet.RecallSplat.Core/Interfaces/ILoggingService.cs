﻿using Homenet.RecallSplat.Core.Model.Logging;

namespace Homenet.RecallSplat.Core.Interfaces
{
	public interface ILoggingService
	{
		void Log(RecallSplatLoggable loggable);
	}
}