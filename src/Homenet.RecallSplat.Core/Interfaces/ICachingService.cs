﻿namespace Homenet.RecallSplat.Core.Interfaces
{
	public interface ICachingService<TValue> where TValue : class
	{
		void Store(string key, TValue value);

		TValue Get(string key);

	    void Remove(string key);
	}
}