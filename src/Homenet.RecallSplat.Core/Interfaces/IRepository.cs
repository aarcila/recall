﻿using System.Collections.Generic;

namespace Homenet.RecallSplat.Core.Interfaces
{
	public interface IRepository<TValue> where TValue : class
	{
		void Store(string key, TValue value);

		TValue Get(string key);

		IEnumerable<string> GetKeys(string keyPrefix);

		IEnumerable<TValue> GetValues(string keyPrefix);

		void Remove(string key);
	}
}
