﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Couchbase.Annotations;
using Homenet.RecallSplat.Core.Model;

namespace Homenet.RecallSplat.Core.Interfaces
{
    public interface IHealthChecker
    {
        IEnumerable<HealthCheck> CheckHealth();
    }
}
