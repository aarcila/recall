﻿using System.Collections;
using System.Collections.Generic;
using Homenet.RecallSplat.Core.Model;

namespace Homenet.RecallSplat.Core.Interfaces
{
	public interface IRecallSplatService
	{
		//VINResponse LookupByVIN(string vin);

		VINRecallResponse Lookup(string vin);
		VINRecallResponse Lookup(IEnumerable<string> vin);


		VINRecallResponse Lookup(string vin, bool bypassCache);
		VINRecallResponse Lookup(IEnumerable<string> vin, bool bypassCache);
	}
}