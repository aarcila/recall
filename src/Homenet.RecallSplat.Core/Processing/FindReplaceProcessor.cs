﻿using System.Collections.Generic;
using Homenet.RecallSplat.Core.Model;

namespace Homenet.RecallSplat.Core.Processing
{
    public class FindReplaceProcessor : IProcessor
    {
        public string Find { get; set; }
        public string ReplaceWith { get; set; }

        public FindReplaceProcessor(string find, string replaceWith)
        {
            Find = find;
            ReplaceWith = replaceWith;
        }

        public void Execute(MappingContext mappingContext)
        {
            mappingContext.ResponseText = mappingContext.ResponseText.Replace(Find, ReplaceWith);
        }
    }
}