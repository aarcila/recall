﻿using System.Collections.Generic;
using Couchbase.Annotations;
using Homenet.RecallSplat.Core.Model;

namespace Homenet.RecallSplat.Core.Processing
{
    public interface IProcessor
    {
        void Execute(MappingContext mappingContext);
    }
}