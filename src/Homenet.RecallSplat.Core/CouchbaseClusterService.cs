﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Couchbase;
using Couchbase.Configuration.Client;
using Couchbase.Core;
using Couchbase.Management;
using CoxAuto.ObjectStores.Converters;
using Homenet.MoreCommon;
using Newtonsoft.Json.Linq;

namespace Homenet.RecallSplat.Core
{
    public class CouchbaseClusterService : IDisposable
    {
        #region Fields

        /// <summary>
        /// The internal list of connected buckets
        /// </summary>
        private readonly ConcurrentDictionary<string, IBucket> _buckets = new ConcurrentDictionary<string, IBucket>();

        /// <summary>
        /// A list of the configured buckets
        /// </summary>
        private List<BucketConfiguration> _bucketConfigurations;

        /// <summary>
        /// Internal connection to the CouchBase Cluster
        /// </summary>
        private Cluster _cluster;

        /// <summary>
        /// The HNF setting value used to detect changes
        /// </summary>
        private string _currentBucketSettings;

        /// <summary>
        /// The HNF setting value used to detect changes
        /// </summary>
        private string _currentObjectStoreSettings;

        /// <summary>
        /// A list of the de-serialized object store configurations
        /// </summary>
        private List<ObjectstoreConfiguration> _objectstoreConfigurations;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CouchbaseClusterService"/> class.
        /// </summary>
        public CouchbaseClusterService()
        {
            Refresh();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="CouchbaseClusterService"/> class. 
        /// </summary>
        ~CouchbaseClusterService()
        {
            Dispose();
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Gets a list of buckets
        /// </summary>
        public List<string> BucketNames
        {
            get
            {
                return _bucketConfigurations.Select(bucketConfiguration => bucketConfiguration.Name).ToList();
            }
        }

        /// <summary>
        /// Gets the available object store names
        /// </summary>
        public List<string> ObjectStoreNames
        {
            get
            {
                return IsEnabled ? _objectstoreConfigurations.Select(objectStoreConfiguration => objectStoreConfiguration.Name).ToList() : new List<string>();
            }
        }

        public bool IsEnabled { get; private set; }

        /// <summary>
        /// Gets the connection to the CouchBase Cluster
        /// </summary>
        private Cluster Cluster
        {
            get { return _cluster ?? (_cluster = GetCluster(Settings.DefaultEnvironment())); }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Gets a shared connection to the CouchBase cluster
        /// </summary>
        /// <param name="environment">The HNF environment</param>
        /// <returns>A CouchBase Cluster object</returns>
        public static Cluster GetCluster(Settings environment)
        {
            List<Uri> couchBaseUris = GetCouchBaseServerUris(environment);
            if (couchBaseUris.Any())
            {
                ClientConfiguration clientConfiguration = new ClientConfiguration
                {
                    Servers = couchBaseUris,
                    EnableTcpKeepAlives = false,
                    // TcpKeepAliveTime = 1000 * 60 * 60, // set to 60mins
                    // TcpKeepAliveInterval = 5000 // KEEP ALIVE will be sent every 5 seconds after 1h
                };
                return new Cluster(clientConfiguration);
            }

            return null;
        }

        /// <summary>
        /// Gets a list of CouchBase servers in the cluster
        /// </summary>
        /// <param name="environment">The HNF environment</param>
        /// <returns>A list of CouchBase servers in the cluster</returns>
        public static List<Uri> GetCouchBaseServerUris(Settings environment)
        {
            string[] couchbaseServers = environment.ListMachinesForApplication(@"InventoryOnline2-CouchbaseServer", includeRedundantEnvironment: false);
            List<Uri> couchBaseUris = couchbaseServers.Select(server => new UriBuilder("http", server, 8091).Uri).ToList();
            return couchBaseUris;
        }

        /// <summary>
        /// Gets the CouchBase bucket 
        /// </summary>
        /// <param name="bucketName">The name of the bucket</param>
        /// <exception cref="ObjectStoreException">If the system cannot connect to a bucket or if CouchBase is not set for this environment.</exception>
        /// <returns>The CouchBase Bucket with a name</returns>
        public IBucket GetBucket(string bucketName)
        {
            IBucket bucket;

            if (!_buckets.TryGetValue(bucketName, out bucket))
            {
                BucketConfiguration bucketConfigurationConfiguration = GetConfiguration(bucketName);

                try
                {
                    bucket = bucketConfigurationConfiguration.Password == null
                        ? Cluster.OpenBucket(bucketConfigurationConfiguration.Name)
                        : Cluster.OpenBucket(bucketConfigurationConfiguration.Name, bucketConfigurationConfiguration.Password);
                }
                catch (Exception aggregateException)
                {
                    throw new Exception(string.Format("Could not connect to bucket {0}", bucketName), aggregateException);
                }

                _buckets.TryAdd(bucketName, bucket);
            }

            return bucket;
        }

        /// <summary>
        /// Gets a CouchBase configuration 
        /// </summary>
        /// <param name="bucketName">The name of the bucket to find</param>
        /// <returns>The CouchBase configuration for that name</returns>
        public BucketConfiguration GetConfiguration(string bucketName)
        {
            return _bucketConfigurations.FirstOrDefault(bucketConfig => string.Compare(bucketConfig.Name, bucketName, StringComparison.OrdinalIgnoreCase) == 0);
        }

        /// <summary>
        /// The get object store configuration.
        /// </summary>
        /// <param name="configurationName">The configuration name
        /// </param>
        /// <returns>The <see cref="ObjectstoreConfiguration"/></returns>
        public ObjectstoreConfiguration GetObjectstoreConfiguration(string configurationName)
        {
            return _objectstoreConfigurations.FirstOrDefault(objectstoreConfig => string.Compare(objectstoreConfig.Name, configurationName, StringComparison.OrdinalIgnoreCase) == 0);
        }

        /// <summary>
        /// Refreshes the configuration 
        /// </summary>
        public void Refresh()
        {
            List<Uri> uris = GetCouchBaseServerUris(Settings.DefaultEnvironment());
            string bucketJson = Settings.DefaultEnvironment().CouchbaseServer.BucketJSON;
            string objectStoreJson = Settings.DefaultEnvironment().CouchbaseServer.ObjectStoreJSON;

            BucketConfigurations bucketConfigurations = Serializers.JSONSerializedConverter.Deserialize<BucketConfigurations>(bucketJson);
            ObjectStoreConfigurations objectStoreConfigurations = Serializers.JSONSerializedConverter.Deserialize<ObjectStoreConfigurations>(objectStoreJson);

            if (_cluster == null || _currentBucketSettings != bucketJson || _currentObjectStoreSettings != objectStoreJson || !uris.SequenceEqual(_cluster.Configuration.Servers))
            {
                IsEnabled = uris.Any();
                DisconnectFromCouchbase();
                _bucketConfigurations = bucketConfigurations.Configurations.ToList();
                _objectstoreConfigurations = objectStoreConfigurations.Configurations.ToList();
                _currentBucketSettings = bucketJson;
                _currentObjectStoreSettings = objectStoreJson;
            }
        }

        /// <summary>
        /// Makes it go bye-bye!
        /// </summary>
        public void Dispose()
        {
            DisconnectFromCouchbase();
        }

        /// <summary>
        /// Disconnects this service from CouchBase
        /// </summary>
        private void DisconnectFromCouchbase()
        {
            while (!_buckets.IsEmpty)
            {
                IBucket bucket;
                if (_buckets.TryRemove(_buckets.Keys.First(), out bucket))
                {
                    try
                    {
                        bucket.Dispose();
                    }
                    catch (Exception)
                    {
                    }
                }
            }

            if (_cluster != null)
            {
                _cluster.Dispose();
                _cluster = null;
            }
        }

        #endregion Methods

    }

    public class BucketConfiguration
    {
        /// <summary>
        ///  Gets or sets the password for the bucket.  This can be null (no password)
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the RAM required for this bucket
        /// </summary>
        public uint SizeInMb { get; set; }

        /// <summary>
        /// Gets or sets the name of the object store
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the number of replicas <see cref="http://docs.couchbase.com/admin/admin/Concepts/concept-replication.html"/>
        /// </summary>
        public int ReplicaNumber { get; set; }

        /// <summary>
        /// Gets or sets the design documents (views) for the buckets
        /// </summary>
        public Dictionary<string, JObject> DesignDocuments { get; set; }

        /// <summary>
        /// Gets the number of replicas <see cref="http://docs.couchbase.com/admin/admin/Concepts/concept-replication.html"/>
        /// </summary>
        public ReplicaNumber Replicas
        {
            get
            {
                switch (ReplicaNumber)
                {
                    case 0:
                        return Couchbase.Management.ReplicaNumber.Zero;
                    case 1:
                        return Couchbase.Management.ReplicaNumber.One;
                    case 2:
                        return Couchbase.Management.ReplicaNumber.Two;
                    case 3:
                        return Couchbase.Management.ReplicaNumber.Three;
                    default:
                        return Couchbase.Management.ReplicaNumber.Two;
                }
            }
        }
    }

    public class BucketConfigurations
    {
        /// <summary>
        /// Gets or sets the array of configurations.
        /// </summary>
        public BucketConfiguration[] Configurations { get; set; }
    }

    public class ObjectStoreConfigurations
    {
        /// <summary>
        /// Gets or sets the array of configurations.
        /// </summary>
        public ObjectstoreConfiguration[] Configurations { get; set; }
    }

    public class ObjectstoreConfiguration
    {
        /// <summary>
        /// Gets or sets the name of the object store
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the bucket name underlying the object store
        /// </summary>
        public string BucketName { get; set; }

        /// <summary>
        /// Gets or sets the default time to live for the buckets in hours.  If the value is null then there is no expiration for values in the bucket.
        /// </summary>
        public double? HoursToLive { get; set; }
    }
}