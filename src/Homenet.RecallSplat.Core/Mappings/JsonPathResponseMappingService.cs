﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Homenet.RecallSplat.Core.Interfaces;
using Homenet.RecallSplat.Core.Model;
using Homenet.RecallSplat.Core.Model.Exceptions;
using Homenet.RecallSplat.Core.Model.Logging;
using Newtonsoft.Json.Linq;
using CoxAuto.Extensions;
// ReSharper disable CanBeReplacedWithTryCastAndCheckForNull
// ReSharper disable MergeSequentialChecks

namespace Homenet.RecallSplat.Core.Mappings
{
	public class JsonPathResponseMappingService : ResponseMappingServiceBase
    {
		public JsonPathResponseMappingService(ILoggingService loggingService):base(loggingService)
		{
		}

		public override VINResponse MapResponse(MappingContext mappingContext)
		{
			return MapResponseInternal(mappingContext);
		}

		internal VINResponse MapResponseInternal(MappingContext mappingContext)
		{
			JObject o = JObject.Parse(mappingContext.ResponseText);
			VINResponse recallResponse = new VINResponse();
			List<OpenRecallResponse> recallList = new List<OpenRecallResponse>();

			ExternalRecallServiceMappingData mappingData = mappingContext.MappingData;

			CheckForErrors(o, mappingData);

			VerifyData(o, mappingData);

			foreach (VINFieldMappingItem vinFieldMappingItem in mappingData.VINFieldMappingItems)
			{
				if (vinFieldMappingItem.Type == FieldMappingItemType.Static)
				{
					SetVINProperty(recallResponse, vinFieldMappingItem.VinProperty, vinFieldMappingItem.Source);
				}
				else if (vinFieldMappingItem.Type == FieldMappingItemType.Mapped)
				{
					string sourceValueString = GetTokenValue(o, vinFieldMappingItem.Source);

					sourceValueString = ProcessValueMappings(sourceValueString, vinFieldMappingItem, mappingContext);

					object sourceValue = ParseValue(sourceValueString, vinFieldMappingItem, mappingContext);

					SetVINProperty(recallResponse, vinFieldMappingItem.VinProperty, sourceValue);
				}
			}


			if (!string.IsNullOrWhiteSpace(mappingData.RecallsArrayPath))
			{
				JToken recalls = o.SelectToken(mappingData.RecallsArrayPath);

				List<JToken> recallTokens = new List<JToken>();

				if (recalls is JArray)
				{
					recallTokens.AddRange(recalls);
				}
				else
				{
					if (recalls != null && recalls.Type != JTokenType.Null)
					{
						recallTokens.Add(recalls);
					}
				}

				foreach (JToken recall in recallTokens)
				{
					OpenRecallResponse thisRecall = new OpenRecallResponse();
				    bool addRecall = true;

					foreach (RecallFieldMappingItem recallFieldMappingItem in mappingData.RecallFieldMappingItems)
					{
                        
						if (recallFieldMappingItem.Type == FieldMappingItemType.Static)
						{
							SetRecallProperty(thisRecall, recallFieldMappingItem.RecallProperty, recallFieldMappingItem.Source);
						}
						else if (recallFieldMappingItem.Type == FieldMappingItemType.Mapped)
						{
							string sourceValueString = GetTokenValue(recall, recallFieldMappingItem.Source);

							sourceValueString = ProcessValueMappings(sourceValueString, recallFieldMappingItem, mappingContext);

                            foreach (RecallFieldMappingItem nonRecallFieldMappingItem in mappingData.NonRecallFieldMappingItems)
                            {
                                if (nonRecallFieldMappingItem.RecallProperty == recallFieldMappingItem.RecallProperty &&
                                    nonRecallFieldMappingItem.Source == sourceValueString)
                                    addRecall = false;
                            }

							object sourceValue = ParseValue(sourceValueString, recallFieldMappingItem, mappingContext);

							SetRecallProperty(thisRecall, recallFieldMappingItem.RecallProperty, sourceValue);
						}

					}
                    if(addRecall)
                        recallList.Add(thisRecall);
				}
			}

			recallResponse.OpenRecalls = recallList.ToArray();

			if (string.IsNullOrWhiteSpace(recallResponse.Make))
			{
				if (mappingContext.Variables.ContainsKey("Manufacturer"))
				{
					recallResponse.Make = mappingContext.Variables["Manufacturer"];
				}
			}

			return recallResponse;
		}

		private void CheckForErrors(JObject o, ExternalRecallServiceMappingData mappingData)
		{
			bool hadError = false;

			if (!string.IsNullOrWhiteSpace(mappingData.ErrorFlagPath) && !string.IsNullOrWhiteSpace(mappingData.ErrorFlagValue))
			{
				string errorFlag = GetTokenValue(o, mappingData.ErrorFlagPath);

				if (mappingData.ErrorFlagValueOperation == ErrorFlagValueOperation.Equal)
				{

					if (string.Equals(errorFlag, mappingData.ErrorFlagValue, StringComparison.InvariantCultureIgnoreCase))
					{
						hadError = true;
					}
				}
				else if (mappingData.ErrorFlagValueOperation == ErrorFlagValueOperation.NotEqual)
				{
					if (!string.Equals(errorFlag, mappingData.ErrorFlagValue, StringComparison.InvariantCultureIgnoreCase))
					{
						hadError = true;
					}
				}
				else
				{
					throw new NotImplementedException();
				}
			}

			string errorText = null;

			if (!string.IsNullOrWhiteSpace(mappingData.ErrorTextPath))
			{
				errorText = GetTokenValue(o, mappingData.ErrorTextPath);
			}

			List<string> allMessages = new List<string>();
			if (!string.IsNullOrWhiteSpace(mappingData.ErrorTextArrayPath))
			{
				JToken token = o.SelectToken(mappingData.ErrorTextArrayPath);

				if (token.Type == JTokenType.Array)
				{
					foreach (var val in token.Values())
					{
						string thisVal = val.Value<string>();
						if (!string.IsNullOrWhiteSpace(thisVal))
						{
							allMessages.Add(thisVal);
						}
					}
				}

				if (allMessages.Count > 0)
				{
					errorText = allMessages.FirstOrDefault();
				}
			}

			if (!string.IsNullOrWhiteSpace(errorText))
			{
				if (string.Equals(errorText, mappingData.InvalidVinErrorTextValue, StringComparison.InvariantCultureIgnoreCase))
				{
					throw new OEMVINNotFoundException();
				}
			}

			if (hadError)
			{
				string message = null;
				if (!string.IsNullOrWhiteSpace(errorText))
				{
					message = errorText;
				}

				throw new OEMErrorException(message);
			}
		}

		

		

		





		private void VerifyData(JObject o, ExternalRecallServiceMappingData mappingData)
		{
			if (mappingData.DataVerificationRules == null) return;

			foreach (ExternalRecallServiceDataVerificationRule dataVerificationRule in mappingData.DataVerificationRules)
			{
				switch (dataVerificationRule.Type)
				{
					case DataVerificationRuleType.EnsureZeroOrOneElements:
						JToken token = o.SelectToken(dataVerificationRule.Data);
						if (((JArray) token).Count > 1)
						{
							throw new DataVerificationRuleFailedException(string.Format("Sequence contains more than 1 element: {0}", dataVerificationRule.Data));
						}
						break;
					default:
						throw new NotImplementedException("Specified DataVerificationRule is not implemented");
				}
			}
		}

		private string GetTokenValue(JToken o, string p)
		{
			if (string.IsNullOrWhiteSpace(p)) return null;

			string returnValue = null;

			JToken token = o.SelectToken(p);

			if (token != null)
			{
				string v = token.Value<string>();

				if (v != null)
				{
					v = v.Trim();

					if (!string.IsNullOrWhiteSpace(v))
					{
						returnValue = v;

						// some of the responses use the nbsp; char... convert to a regular space
						if (returnValue.Contains(((char) 160).ToString()))
						{
							returnValue = returnValue.Replace((char) 160, (char) 32);
						}
					}
				}
			}

			return returnValue;
		}


	}
}