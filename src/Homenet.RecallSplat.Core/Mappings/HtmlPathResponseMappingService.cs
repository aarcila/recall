﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using Homenet.RecallSplat.Core.Interfaces;
using Homenet.RecallSplat.Core.Model;
using Homenet.RecallSplat.Core.Model.Exceptions;
using HtmlAgilityPack;

namespace Homenet.RecallSplat.Core.Mappings
{
    public class HtmlPathResponseMappingService : ResponseMappingServiceBase
    {
        public HtmlPathResponseMappingService(ILoggingService loggingService) : base(loggingService)
        {
        }

        public override VINResponse MapResponse(MappingContext mappingContext)
        {
            return MapResponseInternal(mappingContext);
        }

        internal VINResponse MapResponseInternal(MappingContext mappingContext)
        {
            HtmlDocument htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(mappingContext.ResponseText);

            VINResponse recallResponse = new VINResponse();
            List<OpenRecallResponse> recallList = new List<OpenRecallResponse>();

            ExternalRecallServiceMappingData mappingData = mappingContext.MappingData;

            HtmlPathResponseData htmlResponseData = (HtmlPathResponseData) mappingData.MappingServiceAdditionalData;

            CheckForErrors(htmlDocument.DocumentNode, mappingData, htmlResponseData);


            foreach (VINFieldMappingItem vinFieldMappingItem in mappingData.VINFieldMappingItems)
			{
			    HtmlPathData pathData = null;
			    if (vinFieldMappingItem.MappingServiceAdditionalData != null)
			    {
			        pathData = vinFieldMappingItem.MappingServiceAdditionalData as HtmlPathData;
			    }

			    if (vinFieldMappingItem.Type == FieldMappingItemType.Mapped && pathData == null) continue;

			    if (vinFieldMappingItem.Type == FieldMappingItemType.Static)
				{
					SetVINProperty(recallResponse, vinFieldMappingItem.VinProperty, vinFieldMappingItem.Source);
				}
				else if (vinFieldMappingItem.Type == FieldMappingItemType.Mapped)
				{
                    string sourceValueString = GetTokenValue(htmlDocument.DocumentNode, vinFieldMappingItem.Source, pathData);

				    //sourceValueString = ProcessValueMappings(sourceValueString, vinFieldMappingItem, mappingContext);

				    object sourceValue = ParseValue(sourceValueString, vinFieldMappingItem, mappingContext);


                    SetVINProperty(recallResponse, vinFieldMappingItem.VinProperty, sourceValue);
				}
			}

            if (!string.IsNullOrWhiteSpace(mappingData.RecallsArrayPath))
            {
                var nodes = htmlDocument.DocumentNode.SelectNodes(mappingData.RecallsArrayPath);

                if (nodes != null)
                {

                    for (int i = htmlResponseData.HeaderRows; i < nodes.Count; i++)
                    {
                        var thisNode = nodes[i];

                        HtmlDocument htmlSnippet = new HtmlDocument();
                        htmlSnippet.LoadHtml(thisNode.InnerHtml);

                        OpenRecallResponse thisRecall = new OpenRecallResponse();
                        bool addRecall = true;
                        //recallList.Add(thisRecall);

                        foreach (RecallFieldMappingItem recallFieldMappingItem in mappingData.RecallFieldMappingItems)
                        {
                            HtmlPathData pathData = null;
                            if (recallFieldMappingItem.MappingServiceAdditionalData != null)
                            {
                                pathData = recallFieldMappingItem.MappingServiceAdditionalData as HtmlPathData;
                            }

                            if (recallFieldMappingItem.Type == FieldMappingItemType.Mapped && pathData == null) continue;

                            if (recallFieldMappingItem.Type == FieldMappingItemType.Static)
                            {
                                SetRecallProperty(thisRecall, recallFieldMappingItem.RecallProperty,
                                    recallFieldMappingItem.Source);
                            }
                            else if (recallFieldMappingItem.Type == FieldMappingItemType.Mapped)
                            {
                                string sourceValueString = GetTokenValue(htmlSnippet.DocumentNode, recallFieldMappingItem.Source,
                                    pathData);

                                sourceValueString = ProcessValueMappings(sourceValueString, recallFieldMappingItem,
                                    mappingContext);

                                foreach (RecallFieldMappingItem nonRecallFieldMappingItem in mappingData.NonRecallFieldMappingItems)
                                {
                                    if (nonRecallFieldMappingItem.RecallProperty == recallFieldMappingItem.RecallProperty &&
                                        nonRecallFieldMappingItem.Source == sourceValueString)
                                        addRecall = false;
                                }

                                object sourceValue = ParseValue(sourceValueString, recallFieldMappingItem,
                                    mappingContext);

                                SetRecallProperty(thisRecall, recallFieldMappingItem.RecallProperty, sourceValue);
                            }

                        }
                        if(addRecall)
                            recallList.Add(thisRecall);
                    }
                }
            }

            recallResponse.OpenRecalls = recallList.ToArray();

            if (string.IsNullOrWhiteSpace(recallResponse.Make))
            {
                if (mappingContext.Variables.ContainsKey("Manufacturer"))
                {
                    recallResponse.Make = mappingContext.Variables["Manufacturer"];
                }
            }

            return recallResponse;
        }

        private string GetTokenValue(HtmlNode htmlNode, string source, HtmlPathData pathData)
        {
            HtmlNode node = htmlNode.SelectSingleNode(source);

            if (node == null) return null;

            string returnValue = null;

            if (pathData.ElementSource == HtmlPathElementSource.ValueAttribute)
            {
                returnValue = node.Attributes["value"].Value;
            }
            else if (pathData.ElementSource == HtmlPathElementSource.InnerText)
            {
                returnValue = node.InnerText;
            }

            if (returnValue != null && pathData.RemoveNewLines)
            {
                returnValue = returnValue.Replace("\r\n", " ");
                returnValue = returnValue.Replace("\r", " ");
                returnValue = returnValue.Replace("\n", " ");
            }

            if (returnValue != null && pathData.RemoveTabs)
            {
                returnValue = returnValue.Replace("\t", " ");
            }

            if (!String.IsNullOrWhiteSpace(pathData.RegEx) && !String.IsNullOrWhiteSpace(returnValue))
            {
                Regex pattern = new Regex(pathData.RegEx);
                Match match = pattern.Match(returnValue);
                returnValue = match.Value.Trim();
            }

            if (returnValue != null)
            {
                returnValue = HttpUtility.HtmlDecode(returnValue);
                returnValue = returnValue.Trim();
            }
            if (string.IsNullOrWhiteSpace(returnValue))
                returnValue = null;
            return returnValue;
        }

        private void CheckForErrors(HtmlNode htmlNode, ExternalRecallServiceMappingData mappingData, HtmlPathResponseData pathData)
        {
            bool hadError = false;

            if (!string.IsNullOrWhiteSpace(mappingData.ErrorFlagPath)) // && !string.IsNullOrWhiteSpace(mappingData.ErrorFlagValue))
            {
                

                if (mappingData.ErrorFlagValueOperation == ErrorFlagValueOperation.Exists)
                {
                    var node = htmlNode.SelectSingleNode(mappingData.ErrorFlagPath);
                    hadError = (node != null);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(mappingData.ErrorFlagValue))
                    {
                        string errorFlag = GetTokenValue(htmlNode, mappingData.ErrorFlagPath,
                            new HtmlPathData() {ElementSource = HtmlPathElementSource.InnerText});

                        if (mappingData.ErrorFlagValueOperation == ErrorFlagValueOperation.Equal)
                        {

                            if (string.Equals(errorFlag, mappingData.ErrorFlagValue,
                                StringComparison.InvariantCultureIgnoreCase))
                            {
                                hadError = true;
                            }
                        }
                        else if (mappingData.ErrorFlagValueOperation == ErrorFlagValueOperation.NotEqual)
                        {
                            if (
                                !string.Equals(errorFlag, mappingData.ErrorFlagValue,
                                    StringComparison.InvariantCultureIgnoreCase))
                            {
                                hadError = true;
                            }
                        }
                    }
                }
            }

            string errorText = null;

            if (!string.IsNullOrWhiteSpace(mappingData.ErrorTextPath))
            {
                errorText = GetTokenValue(htmlNode, mappingData.ErrorTextPath,
                    new HtmlPathData() {ElementSource = HtmlPathElementSource.InnerText});
            }

            List<string> allMessages = new List<string>();
            if (!string.IsNullOrWhiteSpace(mappingData.ErrorTextArrayPath))
            {
                //JToken token = o.SelectToken(mappingData.ErrorTextArrayPath);

                //if (token.Type == JTokenType.Array)
                //{
                //    foreach (var val in token.Values())
                //    {
                //        string thisVal = val.Value<string>();
                //        if (!string.IsNullOrWhiteSpace(thisVal))
                //        {
                //            allMessages.Add(thisVal);
                //        }
                //    }
                //}

                //if (allMessages.Count > 0)
                //{
                //    errorText = allMessages.FirstOrDefault();
                //}
            }

            if (!string.IsNullOrWhiteSpace(errorText))
            {
                if (errorText.ToLower().Contains(mappingData.InvalidVinErrorTextValue.ToLower()))
                {
                    throw new OEMVINNotFoundException();
                }
                
                if (mappingData.NoRecallsErrorTextValue != null && errorText.ToLower().Contains(mappingData.NoRecallsErrorTextValue.ToLower()))
                {
                    hadError = false;
                    errorText = null;
                }
            }

            if (hadError)
            {
                string message = null;
                if (!string.IsNullOrWhiteSpace(errorText))
                {
                    message = errorText;
                }

                throw new OEMErrorException(message);
            }
        }
    }
}