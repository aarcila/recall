﻿using System;
using System.Globalization;
using Homenet.RecallSplat.Core.Interfaces;
using Homenet.RecallSplat.Core.Model;
using Homenet.RecallSplat.Core.Model.Logging;

namespace Homenet.RecallSplat.Core.Mappings
{
    public abstract class ResponseMappingServiceBase: IResponseMappingService
    {
        protected readonly ILoggingService _loggingService;

        protected ResponseMappingServiceBase(ILoggingService loggingService)
        {
            _loggingService = loggingService;
        }

        public abstract VINResponse MapResponse(MappingContext mappingContext);

        protected void SetRecallProperty(OpenRecallResponse recall, RecallProperty recallProperty, object sourceValue)
        {
            if (sourceValue == null) return;

            switch (recallProperty)
            {
                case RecallProperty.ManufacturerRecallID:
                    recall.OEMCampaignID = (string)sourceValue;
                    break;
                case RecallProperty.NHTSACampaignNumber:
                    recall.CampaignID = (string)sourceValue;
                    break;
                case RecallProperty.Summary:
                    recall.Title = (string)sourceValue;
                    break;
                case RecallProperty.Consequence:
                    recall.SafetyRisk = (string)sourceValue;
                    break;
                case RecallProperty.Remedy:
                    recall.RemedyDescription = (string)sourceValue;
                    break;
                case RecallProperty.Notes:
                    recall.Description = (string)sourceValue;
                    break;
                case RecallProperty.RecallDate:
                    recall.CampaignDate = (DateTime)sourceValue;
                    break;
                case RecallProperty.Status:
                    RecallStatusEnum status;
                    if (Enum.TryParse((string)sourceValue, out status))
                    {
                        recall.Status = status;
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException("recallProperty", recallProperty, null);
            }
        }

        protected void SetVINProperty(VINResponse recallResponse, VINProperty vinProperty, object sourceValue)
        {
            if (sourceValue == null) return;

            switch (vinProperty)
            {
                case VINProperty.VIN:
                    recallResponse.VIN = (string)sourceValue;
                    break;
                case VINProperty.Manufacturer:
                    recallResponse.Make = (string)sourceValue;
                    break;
                case VINProperty.ModelYear:
                    recallResponse.Year = null;
                    int parsedYear;
                    if (int.TryParse((string)sourceValue, out parsedYear))
                    {
                        recallResponse.Year = parsedYear;
                    }
                    break;
                case VINProperty.Make:
                    recallResponse.Make = (string)sourceValue;
                    break;
                case VINProperty.Model:
                    recallResponse.Model = (string)sourceValue;
                    break;
                case VINProperty.LastUpdated:
                    recallResponse.OEMFeedLastUpdated = (DateTime)sourceValue;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("vinProperty", vinProperty, null);
            }
        }

        protected object ParseValue(string sourceValueString, FieldMappingItemBase fieldMappingItem, MappingContext mappingContext)
        {
            object returnValue = sourceValueString;

            if (!string.IsNullOrWhiteSpace(sourceValueString))
            {
                if (fieldMappingItem.ParseData != null)
                {
                    FieldParseData parseData = fieldMappingItem.ParseData;

                    switch (parseData.ParseType)
                    {
                        case FieldParseType.None:
                            break;
                        case FieldParseType.EpochConvert:
                            long preConvert;
                            if (long.TryParse(sourceValueString, out preConvert))
                            {
                                returnValue = FromUnixTime(preConvert);
                            }
                            else
                            {
                                _loggingService.Log(new ParseFailureLoggable()
                                {
                                    FieldParseType = parseData.ParseType,
                                    SourceData = sourceValueString,
                                    PropertyName = GetPropertyNameForLogging(fieldMappingItem),
                                    FormatString = parseData.FormatString
                                });
                            }
                            break;
                        case FieldParseType.DateTime:
                            DateTime parseDateTime;

                            if (!string.IsNullOrWhiteSpace(parseData.FormatString))
                            {
                                if (DateTime.TryParseExact(sourceValueString, parseData.FormatString, null, DateTimeStyles.None,
                                    out parseDateTime))
                                {
                                    returnValue = parseDateTime;
                                }
                                else
                                {
                                    _loggingService.Log(new ParseFailureLoggable()
                                    {
                                        FieldParseType = parseData.ParseType,
                                        SourceData = sourceValueString,
                                        PropertyName = GetPropertyNameForLogging(fieldMappingItem),
                                        FormatString = parseData.FormatString,
                                        Url = GetUrlForLogging(mappingContext)
                                    });
                                }
                            }
                            else
                            {
                                if (DateTime.TryParse(sourceValueString, out parseDateTime))
                                {
                                    returnValue = parseDateTime;
                                }
                                else
                                {
                                    _loggingService.Log(new ParseFailureLoggable()
                                    {
                                        FieldParseType = parseData.ParseType,
                                        SourceData = sourceValueString,
                                        PropertyName = GetPropertyNameForLogging(fieldMappingItem),
                                        FormatString = parseData.FormatString
                                    });
                                }
                            }
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
            }

            // Strip the time off datetime for consistency
            if (returnValue != null && returnValue is DateTime)
            {
                DateTime returnDateTime = (DateTime)returnValue;
                returnValue = returnDateTime.Date;
            }

            return returnValue;
        }

        protected DateTime FromUnixTime(long unixTime)
        {
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddMilliseconds(unixTime);
        }

        protected static string GetPropertyNameForLogging(FieldMappingItemBase fieldMappingItem)
        {
            string propName = "";
            if (fieldMappingItem is VINFieldMappingItem)
            {
                propName = ((VINFieldMappingItem)fieldMappingItem).VinProperty.ToString();
            }
            else if (fieldMappingItem is RecallFieldMappingItem)
            {
                propName = ((RecallFieldMappingItem)fieldMappingItem).RecallProperty.ToString();
            }
            return propName;
        }

        protected static string GetUrlForLogging(MappingContext mappingContext)
        {
            string url = "";
            if (mappingContext.Variables.ContainsKey(("URL")))
            {
                url = mappingContext.Variables["URL"];
            }
            return url;
        }

        protected string ProcessValueMappings(string sourceValueString, FieldMappingItemBase fieldMappingItem, MappingContext mappingContext)
        {
            string returnValue = sourceValueString;

            if (!string.IsNullOrWhiteSpace(sourceValueString))
            {
                if (fieldMappingItem.ValueMappingData != null)
                {
                    returnValue = null;
                    bool mappingFound = false;

                    FieldValueMappingData valueMappingData = fieldMappingItem.ValueMappingData;
                    foreach (ValueMappingItem mappingItem in valueMappingData.MappingItems)
                    {
                        if (string.Equals(mappingItem.Key, sourceValueString, StringComparison.OrdinalIgnoreCase))
                        {
                            mappingFound = true;
                            returnValue = mappingItem.Value;
                            break;
                        }

                        //List<StringComparisonHelper.StringComparisonCharStatus> diff = StringComparisonHelper.DetermineDifference(mappingItem.Key, sourceValueString);
                    }

                    if (!mappingFound)
                    {
                        _loggingService.Log(new ValueMappingNotFoundLoggable()
                        {
                            PropertyName = GetPropertyNameForLogging(fieldMappingItem),
                            SourceData = sourceValueString,
                            Url = GetUrlForLogging(mappingContext)
                        });
                    }
                }

            }

            if (GetPropertyNameForLogging(fieldMappingItem) == "NHTSACampaignNumber" && (returnValue == "" || returnValue == "Awaiting#" || returnValue == null || returnValue == "NA" || returnValue == "AWAITING#" || returnValue == "na"))
            {
                _loggingService.Log(new NhtsaIdNotFoundLoggable()
                {
                    Url = GetUrlForLogging(mappingContext)
                });
            }

            return returnValue;
        }
    }
}