﻿using System;
using CoxAuto.ObjectStores;
using Homenet.RecallSplat.Core.Interfaces;

namespace Homenet.RecallSplat.Core.Caching
{
	public class ObjectStoreCachingService<TValue> : ICachingService<TValue> where TValue : class
	{
		private readonly ITimedObjectStore _objectStore;

	    private readonly TimeSpan TTL = new TimeSpan(1, 0, 0, 0);

		public ObjectStoreCachingService(ITimedObjectStore objectStoreCache)
		{
			_objectStore = objectStoreCache;
		}

		public void Store(string key, TValue value)
		{
			_objectStore.Store(key, value, TTL);
		}

		public TValue Get(string key)
		{

			var osr = _objectStore.Get<TValue>(key);
			if (osr.Status == ObjectStoreResultStatus.KeyNotFound)
			{
				return null;
			}

			return osr.Value;

		}

	    public void Remove(string key)
	    {
	        _objectStore.Remove(key);
	    }
	}
}