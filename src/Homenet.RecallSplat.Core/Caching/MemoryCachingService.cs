﻿using System.Collections.Concurrent;
using Homenet.RecallSplat.Core.Interfaces;

namespace Homenet.RecallSplat.Core.Caching
{
	public class MemoryCachingService<TValue> : ICachingService<TValue> where TValue : class
	{
		private static readonly ConcurrentDictionary<string, TValue> _dict = new ConcurrentDictionary<string, TValue>();

		public void Store(string key, TValue value)
		{
			_dict[key] = value;
		}

		public TValue Get(string key)
		{
			if (!_dict.ContainsKey(key))
			{
				return default(TValue);
			}

			return _dict[key];
		}

	    public void Remove(string key)
	    {
	        if (_dict.ContainsKey(key))
	        {
	            TValue t;
	            _dict.TryRemove(key, out t);
	        }
	    }
	}
}