﻿using Homenet.RecallSplat.Core.Interfaces;
using Homenet.RecallSplat.Core.Model;

namespace Homenet.RecallSplat.Core.Caching
{
	public class OEMResponseCachingService
	{
		private readonly ICachingService<OEMResponse> _cachingService;

		public OEMResponseCachingService(ICachingService<OEMResponse> cachingService)
		{
			_cachingService = cachingService;
		}

		public void Store(string vin, OEMResponse value)
		{
			_cachingService.Store(GetKey(vin), value);
		}

		public OEMResponse Get(string vin)
		{
			return _cachingService.Get(GetKey(vin));
		}

		private string GetKey(string vin)
		{
			return string.Format("RecallSplat\\OEMResponseCachingService\\{0}", vin);
		}

	    public void Remove(string vin)
	    {
	        _cachingService.Remove(GetKey(vin));
	    }
	}
}