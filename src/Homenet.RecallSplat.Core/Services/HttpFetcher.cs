﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Web;
using CoxAuto.Extensions;
using Homenet.RecallSplat.Core.Interfaces;
using Homenet.RecallSplat.Core.Model;
using Homenet.RecallSplat.Core.Utilities;

namespace Homenet.RecallSplat.Core.Services
{
	public class HttpFetcher : IHttpFetcher
	{
		public HttpWebResponse Get(string url, NameValueCollection headers = null, ProxyAddress proxy = null)
		{
			return GetResponseInternal("GET", url, null, headers, proxy);
		}

		public void Get(string url, out string text, out HttpStatusCode statusCode, NameValueCollection headers = null, ProxyAddress proxy = null)
		{
			FetchResult fr = FetchInternal("GET", url, null, headers, proxy);
			statusCode = fr.StatusCode;

			using (Stream stream = fr.Stream)
			{
				using (StreamReader reader = new StreamReader(stream))
				{
					text = reader.ReadToEnd();
				}
			}
		}

		public void Get(string url, out byte[] bytes, NameValueCollection headers = null, ProxyAddress proxy = null)
		{
			using (Stream stream = FetchInternal("GET", url, null, headers, proxy).Stream)
			{
				bytes = stream.ToByteArray();
			}
		}

		public void Get(string url, out Stream stream, NameValueCollection headers = null, ProxyAddress proxy = null)
		{
			stream = FetchInternal("GET", url, null, headers, proxy).Stream;
		}

		public HttpWebResponse Post(string url, NameValueCollection form, NameValueCollection headers = null, ProxyAddress proxy = null)
		{
			return GetResponseInternal("POST", url, form, headers, proxy);
		}

		public void Post(string url, NameValueCollection form, out string text, out HttpStatusCode statusCode, NameValueCollection headers = null, ProxyAddress proxy = null)
		{
            FetchResult fr = FetchInternal("POST", url, form, headers, proxy);
            statusCode = fr.StatusCode;

            using (Stream stream = fr.Stream)
			{
				using (StreamReader reader = new StreamReader(stream))
				{
					text = reader.ReadToEnd();
				}
			}
		}

		public void Post(string url, NameValueCollection form, out byte[] bytes, NameValueCollection headers = null, ProxyAddress proxy = null)
		{
			using (Stream stream = FetchInternal("POST", url, form, headers, proxy).Stream)
			{
				bytes = stream.ToByteArray();
			}
		}

		public void Post(string url, NameValueCollection form, out Stream stream, NameValueCollection headers = null, ProxyAddress proxy = null)
		{
			stream = FetchInternal("POST", url, form, headers, proxy).Stream;
		}

		protected virtual HttpWebResponse GetResponseInternal(
			string method,
			string url,
			NameValueCollection form,
			NameValueCollection headers,
			ProxyAddress proxy
			)
		{
			Uri uri = new Uri(url);
			HttpWebRequest request = WebRequest.CreateHttp(uri);
            request.CookieContainer = new CookieContainer();
            if (!string.IsNullOrEmpty(method))
			{
				request.Method = method;
				if (method == "POST") request.ContentType = "application/x-www-form-urlencoded";
			}
			if (proxy != null) request.Proxy = new WebProxy(proxy.IPAddress.ToString(), proxy.Port);
			if (headers != null)
			{
				foreach (string key in headers)
				{
					switch (key.ToLowerInvariant())
					{
						// TODO: Reserved properties
						case "accept":
							if (!string.IsNullOrEmpty(headers[key])) request.Accept = headers[key];
							break;
						case "connection":
							if (!string.IsNullOrEmpty(headers[key]))
							{
								if (string.Equals(headers[key], "Keep-Alive", StringComparison.OrdinalIgnoreCase)) request.KeepAlive = true;
								else if (string.Equals(headers[key], "Close", StringComparison.OrdinalIgnoreCase)) request.KeepAlive = false;
								else request.Connection = headers[key];
							}
							break;
						case "cookie":
							if (!string.IsNullOrEmpty(headers[key]))
							{
								if (request.CookieContainer == null) request.CookieContainer = new CookieContainer();
								request.CookieContainer.Add(CookieParser.ParseHeader(headers[key], uri.Host));
							}
							break;
						case "referer":
							if (!string.IsNullOrEmpty(headers[key])) request.Referer = headers[key];
							break;
						case "user-agent":
							if (!string.IsNullOrEmpty(headers[key])) request.UserAgent = headers[key];
							break;
						default:
							request.Headers.Add(key, headers[key]);
							break;
					}
				}
			}
			if (form != null)
			{
				string body = string.Empty;
				foreach (string key in form)
				{
					if (body.Length > 0) body += "&";
					body += HttpUtility.UrlEncode(key);
					body += "=";
					body += HttpUtility.UrlEncode(form[key]);
				}

				using (Stream requestStream = request.GetRequestStream())
				{
					using (StreamWriter requestWriter = new StreamWriter(requestStream))
					{
						requestWriter.Write(body);
					}
				}
			}

			return (HttpWebResponse)request.GetResponse();
		}

		protected virtual FetchResult FetchInternal(
			string method,
			string url,
			NameValueCollection form,
			NameValueCollection headers,
			ProxyAddress proxy
			)
		{

			FetchResult fr = new FetchResult();


			MemoryStream outputStream = new MemoryStream();

			using (HttpWebResponse response = GetResponseInternal(method, url, form, headers, proxy))
			{
				fr.StatusCode = response.StatusCode;
				using (Stream responseStream = response.GetResponseStream())
				{
					responseStream.CopyTo(outputStream);
					outputStream.Position = 0L;
				}
			}
			fr.Stream = outputStream;


			return fr;
		}

		protected class FetchResult
		{
			public Stream Stream { get; set; }
			public HttpStatusCode StatusCode { get; set; }
		}
	}

	
}