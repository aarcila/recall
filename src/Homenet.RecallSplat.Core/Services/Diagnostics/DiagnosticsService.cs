﻿using System.Collections.Generic;
using Homenet.RecallSplat.Core.Interfaces;
using Homenet.RecallSplat.Core.Model;

namespace Homenet.RecallSplat.Core.Services.Diagnostics
{
    public class DiagnosticsService : IDiagnosticsService
    {
        private readonly IEnumerable<IHealthChecker> _healthCheckers;
        public DiagnosticsService(IEnumerable<IHealthChecker> healthCheckers)
        {
            _healthCheckers = healthCheckers;
        }

        public List<HealthCheck> GetHealthChecks()
        {
            List<HealthCheck> healthChecks = new List<HealthCheck>();

            foreach (IHealthChecker healthChecker in _healthCheckers)
            {
                healthChecks.AddRange(healthChecker.CheckHealth());
            }

            return healthChecks;
        }
    }
}
