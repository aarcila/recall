﻿using System;
using System.Collections.Generic;
using System.Net;
using Couchbase;
using CoxAuto.ObjectStores.CouchBase;
using Homenet.MoreCommon;
using Homenet.RecallSplat.Core.Interfaces;
using Homenet.RecallSplat.Core.Model;

namespace Homenet.RecallSplat.Core.Services.Diagnostics.HealthChecks
{
    public class CouchbaseHealthChecker: IHealthChecker
    {
        public IEnumerable<HealthCheck> CheckHealth()
        {
            List<HealthCheck> healthChecks = new List<HealthCheck>();

            Cluster cluster = CouchbaseClusterService.GetCluster(Settings.DefaultEnvironment());
            if (cluster == null || cluster.Configuration == null)
            {
                healthChecks.Add(new HealthCheck { HealthCheckStatus = HealthCheckStatusKind.Failure, HealthCheckCategory = HealthCheckCategoryKind.Couchbase, Message = "No Couchbase cluster is configured for this environment." });
            }
            else
            {
                foreach (Uri serverUri in cluster.Configuration.Servers)
                {
                    WebClient wc = new WebClient
                    {
                        Credentials = new NetworkCredential(Settings.DefaultEnvironment().CouchbaseServer.Username, Settings.DefaultEnvironment().CouchbaseServer.Password)
                    };

                    try
                    {
                        wc.DownloadString(serverUri);

                        healthChecks.Add(new HealthCheck { HealthCheckStatus = HealthCheckStatusKind.Ok, HealthCheckCategory = HealthCheckCategoryKind.Couchbase, Message = string.Format("Can connect to Couchbase server on [{0}]", serverUri.Host) });
                    }
                    catch (WebException)
                    {
                        healthChecks.Add(new HealthCheck { HealthCheckStatus = HealthCheckStatusKind.PartialFailure, HealthCheckCategory = HealthCheckCategoryKind.Couchbase, Message = string.Format("Could not connect to Couchbase server on [{0}]", serverUri.Host) });
                    }
                }
            }

            if (healthChecks.TrueForAll(hc => hc.HealthCheckStatus == HealthCheckStatusKind.PartialFailure))
            {
                healthChecks.Add(new HealthCheck { HealthCheckStatus = HealthCheckStatusKind.Failure, HealthCheckCategory = HealthCheckCategoryKind.Couchbase, Message = "Cannot reach any Couchbase server defined in the cluster." });
            }

            return healthChecks;
        }
    }
}
