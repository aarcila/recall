﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Net;
using Homenet.RecallSplat.Core.Caching;
using Homenet.RecallSplat.Core.Interfaces;
using Homenet.RecallSplat.Core.Model;
using Homenet.RecallSplat.Core.Model.Exceptions;
using Homenet.RecallSplat.Core.Model.Logging;
using Homenet.RecallSplat.Core.Repositories;
using Homenet.RecallSplat.Framework.Bootstrapper;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Homenet.RecallSplat.Core.Services
{
    public class CustomParameters
    {
        public string Vin { get; set; }
        public bool BypassCache { get; set; }
        public VINResponse VinResponse { get; set; }
        public Thread Thread { get; set; }
    }
    public class RecallSplatService : IRecallSplatService
	{
		private readonly VINManufacturerConfigMappingRepository _vinManufacturerConfigMappingRepository;
		private readonly ExternalRecallServiceConfigRepository _externalRecallServiceConfigRepository;
		private readonly MappingDataRepository _mappingDataRepository;
		private readonly IHttpFetcher _httpFetcher;
		private readonly ILoggingService _loggingService;
		private readonly OEMResponseCachingService _oemResponseCachingService;
		private readonly IMetricGraphingService _metricGraphingService;
		private const string RequestUserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36";
		private const string RequestAcceptType = "application/json";

        private const string NHTSACampaignURL =
            "http://www-odi.nhtsa.dot.gov/owners/SearchResults?refurl=email&amp;searchType=ID&amp;targetCategory=R&amp;searchCriteria.nhtsa_ids={0}";
	    
		public RecallSplatService(VINManufacturerConfigMappingRepository vinManufacturerConfigMappingRepository,
			ExternalRecallServiceConfigRepository externalRecallServiceConfigRepository,
			MappingDataRepository mappingDataRepository, IHttpFetcher httpFetcher, ILoggingService loggingService,
			OEMResponseCachingService oemResponseCachingService, IMetricGraphingService metricGraphingService)
		{
			_vinManufacturerConfigMappingRepository = vinManufacturerConfigMappingRepository;
			_externalRecallServiceConfigRepository = externalRecallServiceConfigRepository;
			_mappingDataRepository = mappingDataRepository;
			_httpFetcher = httpFetcher;
			_loggingService = loggingService;
			_oemResponseCachingService = oemResponseCachingService;
			_metricGraphingService = metricGraphingService;
		}

		public VINRecallResponse Lookup(string vin, bool bypassCache)
		{
			List<string> vins = new List<string>();
			vins.Add(vin);
			return Lookup(vins, bypassCache);
		}
        //Called by threads when multiple vins are looked up
        public void ThreadLookup(CustomParameters customParameters)
        {
            string vin = customParameters.Vin;
            bool bypass = customParameters.BypassCache;
            customParameters.VinResponse = LookupByVIN(vin, bypass);
        }

        public VINRecallResponse Lookup(IEnumerable<string> vins, bool bypassCache)
		{
            //DateTime StartTime = DateTime.Now;

			List<VINResponse> vinResponses = new List<VINResponse>();
			VINRecallResponse serviceResponse = new VINRecallResponse();

			try
			{
				int numSuccess = 0;
				int numFailed = 0;
				int total = 0;

			    if (vins.Count() <= 1)
			    {
			        VINResponse vr = LookupByVIN(vins.ElementAt(0), bypassCache);
			        vinResponses.Add(vr);

			        if (vr.HadError)
			        {
			            numFailed++;
			        }
			        else
			        {
			            numSuccess++;
			        }
			        total++;
			    }
			    else
			    {
			        List<ParameterizedThreadStart> pThreadList = new List<ParameterizedThreadStart>();
			        //List<Thread> threadList = new List<Thread>();
			        List<CustomParameters> resList = new List<CustomParameters>();

			        int numVins = vins.Count();

			        //delegate creation
			        for (int i = 0; i < numVins; i++)
			        {
			            ParameterizedThreadStart ts = delegate(object o) { ThreadLookup((CustomParameters) o); };
			            pThreadList.Add(ts);
			        }

			        //delegate execution
			        for (int i = 0; i < numVins; i++)
			        {
			            CustomParameters cp = null;
			            Thread t = new Thread(pThreadList[i]);
			            t.Start(cp = new CustomParameters() {Vin = vins.ElementAt(i), BypassCache = bypassCache, Thread = t});

			            //threadList.Add(t);
			            resList.Add(cp);
			        }
			        //Join Threads
			        for (int i = 0; i < numVins; i++)
			        {
			            resList[i].Thread.Join();
			        }
			        //Track successes and failures
			        for (int i = 0; i < numVins; i++)
			        {
			            VINResponse vr = resList[i].VinResponse;
			            vinResponses.Add(vr);

			            if (vr.HadError)
			            {
			                numFailed++;
			            }
			            else
			            {
			                numSuccess++;
			            }
			            total++;
			        }
			    }


			    serviceResponse.VINs = vinResponses.ToArray();

				if (total > 0)
				{
					if (numFailed == numSuccess)
					{
						serviceResponse.Status = RecallRequestStatusEnum.AllErrors;
					}
					else if (numFailed == 0)
					{
						serviceResponse.Status = RecallRequestStatusEnum.AllSuccessful;
					}
					else if (numSuccess == 0)
					{
						serviceResponse.Status = RecallRequestStatusEnum.AllErrors;
					}
					else if (numFailed > 0 && numSuccess > 0)
					{
						serviceResponse.Status = RecallRequestStatusEnum.SomeErrors;
					}
				}
			}
			catch (Exception ex)
			{
				serviceResponse.Status= RecallRequestStatusEnum.SystemError;
				serviceResponse.ErrorMessage = ex.Message;
			}

            //TimeSpan elapsedtime = DateTime.Now - StartTime;
            //Debug.WriteLine("Time in milliseconds: " + elapsedtime.TotalMilliseconds);

			return serviceResponse;
		}

		public VINRecallResponse Lookup(string vin)
		{
			return Lookup(vin, false);
		}

		public VINRecallResponse Lookup(IEnumerable<string> vin)
		{
			return Lookup(vin, false);
		}

		private VINResponse LookupByVIN(string vin, bool bypassCache)
		{
			VINResponse vinResponse = new VINResponse()
			{
				VIN = vin
			};

			RecallServiceRequestLoggable serviceRequestLoggable = new RecallServiceRequestLoggable {VIN = vin};
			Stopwatch stopwatch = Stopwatch.StartNew();


			try
			{
				if (string.IsNullOrWhiteSpace(vin))
				{
					throw new ArgumentNullException("vin", "Please provide a VIN number");
				}

				if (vin.Length < 4)
				{
					throw new ArgumentOutOfRangeException("vin", "Invalid VIN number specified");
				}

				//vin = vin.Trim().ToUpper();
				string prefix = vin.Substring(0, 3);

				VINManufacturerConfigMapping manufacturerConfigMapping =
					_vinManufacturerConfigMappingRepository.Get(prefix);

				if (manufacturerConfigMapping == null)
				{
					throw new ManufacturerDataNotAvailableException();
				}

				ExternalRecallServiceConfig externalRecallServiceConfig =
					_externalRecallServiceConfigRepository.Get(manufacturerConfigMapping.ConfigKey);

				ExternalRecallServiceMappingData mappingData =
					_mappingDataRepository.Get(externalRecallServiceConfig.MappingDataKey);

				IResponseMappingService responseMapper =
					RecallSplatKernel.GetByName<IResponseMappingService>(externalRecallServiceConfig.MappingMethod);

				serviceRequestLoggable.Manufacturer = manufacturerConfigMapping.ManufacturerName;
				serviceRequestLoggable.Make = manufacturerConfigMapping.Make;
				serviceRequestLoggable.ConfigKey = manufacturerConfigMapping.ConfigKey;
				serviceRequestLoggable.MappingDataKey = externalRecallServiceConfig.MappingDataKey;
				serviceRequestLoggable.MappingMethod = externalRecallServiceConfig.MappingMethod;

				vinResponse = GetAndMapOEMResponse(vin, externalRecallServiceConfig, manufacturerConfigMapping,
					responseMapper, mappingData, bypassCache, serviceRequestLoggable);
				vinResponse.LearnMoreOEMURL = externalRecallServiceConfig.LearnMoreURL;


				//serviceRequestLoggable


				vinResponse.Status = RecallVINStatusEnum.OK;
			}
			catch (ManufacturerDataNotAvailableException mdna)
			{
				vinResponse.ErrorMessage = "OEM not found";
				vinResponse.HadError = true;
				vinResponse.Status = RecallVINStatusEnum.OEMNotFound;

				serviceRequestLoggable.HadError = true;
				serviceRequestLoggable.Exception = mdna;
			}
			catch (OEMVINNotFoundException vnf)
			{
				vinResponse.ErrorMessage = "VIN not found";
				vinResponse.HadError = true;
				vinResponse.Status = RecallVINStatusEnum.OEMCouldNotFindVIN;

				serviceRequestLoggable.HadError = true;
				serviceRequestLoggable.Exception = vnf;
			}
			catch (OEMErrorException oemee)
			{
				vinResponse.ErrorMessage = "OEM Error";

				if (!string.IsNullOrWhiteSpace(oemee.Message))
				{
					vinResponse.ErrorMessage += string.Format(" ({0})", oemee.Message);
				}

				vinResponse.HadError = true;
				vinResponse.Status = RecallVINStatusEnum.OEMError;

				serviceRequestLoggable.HadError = true;
				serviceRequestLoggable.Exception = oemee;
			}
			catch (Exception ex)
			{
				vinResponse.ErrorMessage = ex.Message;
				vinResponse.HadError = true;
				vinResponse.Status = RecallVINStatusEnum.SystemError;

				serviceRequestLoggable.HadError = true;
				serviceRequestLoggable.Exception = ex;
			}
			finally
			{
				stopwatch.Stop();
				serviceRequestLoggable.TotalDuration = stopwatch.Elapsed;

				_loggingService.Log(serviceRequestLoggable);
			}

			return vinResponse;
		}

		private OEMResponse GetOEMResponse(string vin,
			ExternalRecallServiceConfig externalRecallServiceConfig,
			VINManufacturerConfigMapping manufacturerConfigMapping,
			bool bypassCache, out bool fromCache , out TimeSpan? duration)
		{
			fromCache = false;
			duration = null;

			if (!bypassCache)
			{
				OEMResponse cachedResponse = null;

			    try
			    {
			        cachedResponse = _oemResponseCachingService.Get(vin);
			    }
			    catch (Exception ex)
			    {
                    _loggingService.Log(new CacheGetErrorLoggable() { Exception = ex, VIN = vin});

			        try
			        {
                        _oemResponseCachingService.Remove(vin);
			        }
			        catch (Exception eex)
			        {
			            _loggingService.Log(new DebugLoggable() {Message = "Failed to remove item from cache: " + vin});
			        }
                }
			    if (cachedResponse != null && cachedResponse.DateRetrieved.AddDays(1).CompareTo(DateTime.Today) <= 0)
			    {
                    try
                    {
                        _oemResponseCachingService.Remove(vin);
                    }
                    catch (Exception eex)
                    {
                        _loggingService.Log(new DebugLoggable() { Message = "Failed to remove item from cache: " + vin });
                    }
			        cachedResponse = null;
			    }
			    if (cachedResponse != null)
				{
					SendOEMServiceCallCounterMetric(RecallSplatSettings.OEMServiceCallMetricKeyParts.AllMakes,
						RecallSplatSettings.OEMServiceCallMetricKeyParts.Cached, 1, RecallSplatSettings.MetricSampleRate, false);

					SendOEMServiceCallCounterMetric(RecallSplatSettings.OEMServiceCallMetricKeyParts.AllMakes,
						RecallSplatSettings.OEMServiceCallMetricKeyParts.AllCalls, 1, RecallSplatSettings.MetricSampleRate, false);

					fromCache = true;

					return cachedResponse;
				}
			}

			string url = externalRecallServiceConfig.OpenRecallVINURL.Replace("{vin}", vin);

			ExternalServiceRequestLoggable externalServiceRequestLoggable = new ExternalServiceRequestLoggable
			{
				ConfigKey = manufacturerConfigMapping.ConfigKey,
				Url = url,
				RequestID = Guid.NewGuid()
			};

			OEMResponse oemResponse = null;

			try
			{
				Stopwatch stopwatch = Stopwatch.StartNew();
				HttpStatusCode statusCode;
				string serviceResponse;

				System.Collections.Specialized.NameValueCollection headers = new System.Collections.Specialized.NameValueCollection();
				headers.Add("user-agent", RequestUserAgent);
				headers.Add("accept", RequestAcceptType);

			    if (externalRecallServiceConfig.RequestType == ExternalRecallServiceRequestType.GET)
			    {
			        _httpFetcher.Get(url, out serviceResponse, out statusCode, headers);
			    }
                else if (externalRecallServiceConfig.RequestType == ExternalRecallServiceRequestType.POST)
                {
                    NameValueCollection nvc = new NameValueCollection();
                    foreach (var postParameter in externalRecallServiceConfig.PostParameters)
                    {
                        nvc.Add(postParameter.Key, postParameter.Value.Replace("{vin}", vin));
                    }

                    _httpFetcher.Post(url, nvc, out serviceResponse,
                        out statusCode, headers);
                }
                else
                {
                    throw new NotImplementedException();
                }

			    stopwatch.Stop();
				externalServiceRequestLoggable.Duration = stopwatch.Elapsed;
				duration = externalServiceRequestLoggable.Duration;
				externalServiceRequestLoggable.ResponseStatusCode = (int)statusCode;
				SendOEMServiceCallTimerMetric(manufacturerConfigMapping.Make, stopwatch.ElapsedMilliseconds);

				// log the service response text as a debug loggable
				ExternalServiceRequestTextLoggable tl = new ExternalServiceRequestTextLoggable()
				{
					RequestID = externalServiceRequestLoggable.RequestID,
					ResponseText = serviceResponse
				};
				_loggingService.Log(tl);

				oemResponse = new OEMResponse()
				{
					DateRetrieved = DateTime.UtcNow,
					ResponseText = serviceResponse,
					StatusCode = statusCode,
					Url = url,
					RequestID = externalServiceRequestLoggable.RequestID
				};

				if (statusCode == HttpStatusCode.OK && !bypassCache)
				{
					_oemResponseCachingService.Store(vin, oemResponse);
				}

				return oemResponse;
			}
			catch (WebException we)
			{
				externalServiceRequestLoggable.Exception = we;
				externalServiceRequestLoggable.HadError = true;

				if (we.Status == WebExceptionStatus.ProtocolError)
				{
					var statusCode = ((HttpWebResponse)we.Response).StatusCode;
					externalServiceRequestLoggable.ResponseStatusCode = (int)statusCode;
					throw new OEMErrorException(statusCode.ToString());
				}
				else
				{
					throw;
				}
			}
			catch (Exception ex)
			{
				externalServiceRequestLoggable.Exception = ex;
				externalServiceRequestLoggable.HadError = true;
				throw;
			}
			finally
			{
				_loggingService.Log(externalServiceRequestLoggable);
				SendOEMServiceCallCounterMetric(manufacturerConfigMapping.Make,
					externalServiceRequestLoggable.HadError
						? RecallSplatSettings.OEMServiceCallMetricKeyParts.Failures
						: RecallSplatSettings.OEMServiceCallMetricKeyParts.Successes);

				SendOEMServiceCallCounterMetric(manufacturerConfigMapping.Make,
					RecallSplatSettings.OEMServiceCallMetricKeyParts.AllCalls);
			}

			return oemResponse;
		}

		private VINResponse GetAndMapOEMResponse(string vin,
			ExternalRecallServiceConfig externalRecallServiceConfig,
			VINManufacturerConfigMapping manufacturerConfigMapping,
			IResponseMappingService responseMapper, ExternalRecallServiceMappingData mappingData, bool bypassCache, RecallServiceRequestLoggable serviceRequestLoggable)
		{
			VINResponse thisResponse = null;
			MappingLoggable mappingLoggable = new MappingLoggable()
			{
				ConfigKey = manufacturerConfigMapping.ConfigKey,
				MappingDateTime = DateTime.UtcNow
			};

			try
			{
				bool fromCache;
				TimeSpan? duration;
				OEMResponse oemResponse = GetOEMResponse(vin, externalRecallServiceConfig, manufacturerConfigMapping, bypassCache, out fromCache, out duration);


				// record mapping loggable
				mappingLoggable.FromCache = fromCache;
				mappingLoggable.RequestID = oemResponse.RequestID;

				// add data to service request loggable
				serviceRequestLoggable.FromCache = fromCache;
				serviceRequestLoggable.RequestDate = oemResponse.DateRetrieved;
				serviceRequestLoggable.RequestID = oemResponse.RequestID;
				serviceRequestLoggable.ResponseStatusCode = (int)oemResponse.StatusCode;
				serviceRequestLoggable.RequestDuration = duration;


				MappingContext mappingContext = new MappingContext();

			    mappingContext.ResponseText = oemResponse.ResponseText;
			    mappingContext.MappingData = mappingData;

                mappingContext.Variables.Add("Manufacturer", manufacturerConfigMapping.ManufacturerName);
				mappingContext.Variables.Add("Make", manufacturerConfigMapping.Make);
				mappingContext.Variables.Add("VIN", vin);
				mappingContext.Variables.Add("URL", oemResponse.Url);

			    thisResponse = MapResponse(responseMapper, mappingContext);
			    thisResponse.FeedLastUpdated = oemResponse.DateRetrieved;
				thisResponse.RecallDataSourceURL = oemResponse.Url;

                //Set NHTSACampaignURL
			    foreach (OpenRecallResponse recall in thisResponse.OpenRecalls)
			    {
			        if (recall.CampaignID == null)
			            recall.NHTSACampaignURL = null;
                    else
                        recall.NHTSACampaignURL = string.Format( NHTSACampaignURL, recall.CampaignID);
			    }

				// add data to mapping loggable
				mappingLoggable.FeedLastUpdated = thisResponse.OEMFeedLastUpdated;

				// add data to service request loggable
				serviceRequestLoggable.OEMFeedLastUpdated = thisResponse.OEMFeedLastUpdated;
				serviceRequestLoggable.RecallCount = thisResponse.OpenRecallCount;
				serviceRequestLoggable.SoftRecallCount = thisResponse.SoftRecallCount;
			}
			catch (Exception ex)
			{
				mappingLoggable.Exception = ex;
				mappingLoggable.HadError = true;
				throw;
			}
			finally
			{
				_loggingService.Log(mappingLoggable);
			}
			return thisResponse;
		}

	    public static VINResponse MapResponse(IResponseMappingService responseMapper, MappingContext mappingContext)
	    {
	        foreach (var preProcessor in mappingContext.MappingData.PreProcessors)
	        {
	            preProcessor.Execute(mappingContext);
	        }

	        var thisResponse = responseMapper.MapResponse(mappingContext);
	        return thisResponse;
	    }

// ReSharper disable once InconsistentNaming
		private void SendOEMServiceCallCounterMetric(string make, string keySuffix, int increment = 1,
			double? sampleRate = RecallSplatSettings.MetricSampleRate, bool includeAllMakes = true)
		{
			if (string.IsNullOrWhiteSpace(keySuffix) || string.IsNullOrWhiteSpace(make)) return;

			string key;
			if (includeAllMakes)
			{
				key = string.Format("{0}.{1}.{2}",
					RecallSplatSettings.OEMServiceCallMetricKeyParts.Root,
					RecallSplatSettings.OEMServiceCallMetricKeyParts.AllMakes,
					keySuffix);

				_metricGraphingService.SendCounterMetric(key, increment, sampleRate);
			}

			key = string.Format("{0}.{1}.{2}",
				RecallSplatSettings.OEMServiceCallMetricKeyParts.Root,
				make,
				keySuffix);

			_metricGraphingService.SendCounterMetric(key, increment, sampleRate);
		}

// ReSharper disable once InconsistentNaming
		private void SendOEMServiceCallTimerMetric(string make, long timerValue,
			double? sampleRate = RecallSplatSettings.MetricSampleRate,
			bool includeAllMakes = true)
		{
			if (string.IsNullOrWhiteSpace(make)) return;

			string key;
			if (includeAllMakes)
			{
				key = string.Format("{0}.{1}.{2}",
					RecallSplatSettings.OEMServiceCallMetricKeyParts.Root,
					RecallSplatSettings.OEMServiceCallMetricKeyParts.AllMakes,
					RecallSplatSettings.OEMServiceCallMetricKeyParts.Timing);

				_metricGraphingService.SendTimerMetric(key, timerValue, sampleRate);
			}

			key = string.Format("{0}.{1}.{2}",
				RecallSplatSettings.OEMServiceCallMetricKeyParts.Root,
				make,
				RecallSplatSettings.OEMServiceCallMetricKeyParts.Timing);

			_metricGraphingService.SendTimerMetric(key, timerValue, sampleRate);
		}
	}
}