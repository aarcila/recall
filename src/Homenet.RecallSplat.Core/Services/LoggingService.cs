﻿using Homenet.RecallSplat.Core.Interfaces;
using Homenet.RecallSplat.Core.Model.Logging;

namespace Homenet.RecallSplat.Core.Services
{
	public class LoggingService : ILoggingService
	{
		private readonly RecallSplatLogger _recallSplatLogger;

		public LoggingService(RecallSplatLogger recallSplatLogger)
		{
			_recallSplatLogger = recallSplatLogger;
		}

		/// <summary>
		/// Log a message to disk in JSON format.
		/// </summary>
		/// <param name="loggable">The shape to log.</param>
		/// TODO: This method should go away in favor of more exporessive and explicit methods.
		public void Log(RecallSplatLoggable loggable)
		{
			_recallSplatLogger.Log(loggable);
		}
	}
}