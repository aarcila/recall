﻿using System;
using Graphzilla.Clients;
using Graphzilla.Clients.Library;
using Homenet.RecallSplat.Core.Interfaces;
using Homenet.RecallSplat.Core.Model;

namespace Homenet.RecallSplat.Core.Services
{
    public class GraphzillaService : IMetricGraphingService
    {
        private readonly AppBucket _bucket;

        public GraphzillaService()
        {
            _bucket = GraphzillaFactory.Using(RecallSplatSettings.OEMServiceCallMetricKeyParts.AppName);
        }

        public void SendTimerMetric<T>(string metricKey, T timerValue, double? sampleRate = null) where T : struct
        {
            try
            {
                if (sampleRate == null)
                {
                    _bucket.WithStatsD(metricKey).Timing(timerValue);
                }
                else
                {
                    _bucket.WithStatsD(metricKey).Timing(timerValue, sampleRate.Value);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                //TODO: Log the exception                
            }
        }

        public void SendCounterMetric(string metricKey, int increment = 1, double? sampleRate = null)
        {
            try
            {
                if (sampleRate == null)
                {
                    _bucket.WithStatsD(metricKey).IncrementBy(increment);
                }
                else
                {
                    _bucket.WithStatsD(metricKey).IncrementBy(increment, sampleRate.Value);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                //TODO: Log the exception
            }
        }

        public void SendGaugeMetric<T>(string metricKey, T value, double? sampleRate = null) where T : struct
        {
            try
            {
                if (sampleRate == null)
                {
                    _bucket.WithStatsD(metricKey).Gauge(value);
                }
                else
                {
                    _bucket.WithStatsD(metricKey).Gauge(value, sampleRate.Value);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                //TODO: Log the exception
            }
        }
    }
}