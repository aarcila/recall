﻿using System;
using System.Threading;
using CoxAuto.Logging;
using CoxAuto.Logging.Decorations;
using CoxAuto.Logging.Storers;
using Homenet.MoreCommon;
using Homenet.RecallSplat.Core.Model.Logging;

namespace Homenet.RecallSplat.Core.Services
{
	public class RecallSplatLogger : Logger<RecallSplatLoggable>
	{
		private RecallSplatLogger(IStorer logMessageStorer) : base(logMessageStorer) { }

		private static readonly Lazy<LogLevel> _minimumLogLevel = new Lazy<LogLevel>(() =>
		{
			LogLevel thisLevel = LogLevel.Debug;

			LogLevel parsedLevel;
			if (Enum.TryParse(Settings.DefaultEnvironment().RecallSplat.LogMinimumLevel, out parsedLevel))
			{
				thisLevel = parsedLevel;
			}
			return thisLevel;
		}, LazyThreadSafetyMode.ExecutionAndPublication);

		public static LogLevel MinimumLogLevel
		{
			get { return _minimumLogLevel.Value; }
		}

		public static string LogDirectoryPath
		{
			get { 
				return Settings.DefaultEnvironment().RecallSplat.LogDirectoryPath; 
			}
		}

		public static RecallSplatLogger GetConfiguredInstance()
		{
			string filename = "RecallSplat";
			RecallSplatLogger logger = new RecallSplatLogger(new AsyncFileStorer(LogDirectoryPath, filename));

			logger.AddFilter(log => log.Level >= MinimumLogLevel);
			logger.AddPlugin(loggable => new TimestampDecoration());
			logger.AddPlugin(loggable => new MachineNameDecoration());
			logger.AddPlugin(loggable => new ActiveEnvironmentDecoration());
			logger.AddPlugin(loggable => new CategoryDecoration(loggable));
			return logger;
		}
	}

	public enum LogLevel
	{
		Debug = 0,
		Info = 1,
		Warning = 2,
		Error = 3
	}

	public class ActiveEnvironmentDecoration : Decoration
	{
		public readonly string ActiveEnvironment = Settings.DefaultEnvironment().ActiveEnvironment;
	}

	public class MachineNameDecoration : Decoration
	{
		public readonly string MachineName = Environment.MachineName;
	}
}