﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using CoxAuto.Extensions;
using Homenet.MoreCommon;
using RestSharp;
namespace Homenet.RecallSplat.CallingHarness
{
	class Program
	{
		private const int DefaultExecutionWindowInMilliseconds = 1000*60*60*12; // 12 Hrs
		private const int BatchSize = 200;

		private static readonly string DefaultEndPoint =
			Settings.GetEnvironment("production-sdc").RecallSplat.WebServiceBaseURL;

		private static int _executionWindowInMilliseconds;
		private static string _endPoint;
		private static string _processName;
		private static string _processFolder;
		private static int _threadCount;
		private static TextWriter _logger;
// ReSharper disable once UnusedParameter.Local
		static void Main(string[] args)
		{
			CommandLine commandLine = new CommandLine();
			if (commandLine.DebugOnStart && !Debugger.IsAttached) Debugger.Break();

			string processFullPath = Assembly.GetExecutingAssembly().Location;
			_processName = Path.GetFileNameWithoutExtension(processFullPath);
			_processFolder = Path.GetDirectoryName(processFullPath);
			
			string logFilepath = string.Format(@"{0}\{1}_log_{2}.txt", _processFolder, _processName, Process.GetCurrentProcess().Id);
			_logger = TextWriter.Synchronized(File.CreateText(logFilepath));

			if (Process.GetProcessesByName(_processName).Count() > 1)
			{
				_logger.WriteLine("[{1}] {0} is already running", _processName, GetTimeStamp());
				DisposeLogger();
				return;
			}

			string inputFile = commandLine.InputFile;
			_endPoint = string.IsNullOrEmpty(commandLine.EndPoint)
				? DefaultEndPoint
				: commandLine.EndPoint;

			
			if (!File.Exists(inputFile))
			{
				_logger.WriteLine("[{1}] Cannot find input file '{0}'", inputFile, GetTimeStamp());
				DisposeLogger();
				return;
			}

			_executionWindowInMilliseconds = string.IsNullOrEmpty(commandLine.ExecutionWindowInMilliseconds)
				? DefaultExecutionWindowInMilliseconds
				: Math.Abs(commandLine.ExecutionWindowInMilliseconds.ToInteger(DefaultExecutionWindowInMilliseconds)); // negative integers should not be used, but handle them anyway

			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start();
			_logger.WriteLine("[{0}] Reading input file '{1}'", GetTimeStamp(), inputFile);
			string[] vins = File.ReadAllLines(inputFile);
			_logger.WriteLine("[{0}] Done reading input file (VIN count = {1})", GetTimeStamp(), vins.Count());
			int batchCount = vins.Count() / BatchSize;

			if (commandLine.ExecutionWindowInMilliseconds == "0")
			{
				DoSynchronousProcessing(vins);
			}
			else
			{
				DoConcurrentProcessing(vins, batchCount, _executionWindowInMilliseconds/BatchSize);
								
				// Putting this back in since command line option for execution window is
				// not guranteed to be long enough to allow all the other threads to complete before
				// reaching this point
				while (_threadCount > 0)
				{
					Thread.Sleep(500);
				}
			}

			stopwatch.Stop();
			_logger.WriteLine("[{0}] Done", GetTimeStamp());
			_logger.WriteLine("[{1}] Took {0} seconds", stopwatch.Elapsed.TotalSeconds, GetTimeStamp());
			DisposeLogger();
		}

		/// <summary>
		/// Process vins in synchronous fashion
		/// </summary>
		/// <param name="vins">The collection of vins to process</param>
		private static void DoSynchronousProcessing(IEnumerable<string> vins)
		{
			foreach (string vin in vins)
			{
				MakeApiCall(vin);
			}
		}

		/// <summary>
		/// Make concurrent API calls to all OEMs (one call per OEM). 
		/// This maximizes the wait time (for a given execution window) between successive calls to each OEM's API.
		/// This assumes that the vins are sorted by OEM (All the data for a single OEM = 1 batch)
		/// and that the number of vins per OEM (i.e. batch size) is the same for all OEMS.
		// This assumption is based on the method used to generate the inputFile.
		/// </summary>
		/// <param name="vins">The collection of vins to process, ordered by OEM</param>
		/// <param name="batchCount">Number of batches (i.e. OEMs) in the collection of vins</param>
		/// <param name="millisecondsToWait">Number of milliseconds to wait between seccessive calls to the same OEM API</param>
		private static void DoConcurrentProcessing(IList<string> vins, int batchCount, int millisecondsToWait)
		{
			for (int batchItemIndex = 0; batchItemIndex < BatchSize; batchItemIndex++)
			{
				// Select the vin in position "batchItemIndex" from each batch and concurrently process the selected vins
				for (int batchIndex = 0; batchIndex < batchCount; batchIndex++)
				{
					ThreadPool.QueueUserWorkItem(MakeApiCall, vins[batchIndex*BatchSize + batchItemIndex]);

					// Putting this back in (see comment near end of Main)
					Interlocked.Increment(ref _threadCount);
				}

				// Optionally, wait a while before processing next selection of vins
				if (millisecondsToWait > 0) Thread.Sleep(millisecondsToWait);
			}
		}

		private static void MakeApiCall(object arg)
		{
			string vin = arg as string;

			try
			{
				RestClient client = new RestClient(_endPoint);
				RestRequest request = new RestRequest("{id}?bypassCache=true", Method.GET);
				request.AddUrlSegment("id", vin);
				_logger.WriteLine("[{0}] Making API call for VIN {1}", GetTimeStamp(), vin);
				IRestResponse response = client.Execute(request);
				_logger.WriteLine("[{0}] API response for VIN {1}: {2}", GetTimeStamp(), vin, response.Content);
			}
			catch (Exception e)
			{
				_logger.WriteLine("[{0}] API Exception for VIN {1}: {2} {3}", GetTimeStamp(), vin, e.Message,
					e.InnerException == null ? null : e.InnerException.Message);
			}
			finally
			{
				// Putting this back in (see comment near end of Main)
				Interlocked.Decrement(ref _threadCount);
			}
		}

		private static string GetTimeStamp()
		{
			return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
		}

		private static void DisposeLogger()
		{
			_logger.Close();
			_logger.Dispose();
		}
	}
}
