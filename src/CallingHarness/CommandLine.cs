﻿using System;
using System.Linq;
using CoxAuto.CommandLine;
using CoxAuto.Extensions;

namespace Homenet.RecallSplat.CallingHarness
{
	internal class CommandLine : CommandLineOptions<CommandLine>
	{
		private static readonly Option[] Options =
		{
			new Option
			{
				Short = "-d",
				Long = "--debugOnStart",
				ArgsAreOptional = true,
				Description = "Attach Debugger on start.",
				Handler =
					(args, result) =>
						result.DebugOnStart = !(args.FirstOrDefault() ?? string.Empty).Equals("false", StringComparison.OrdinalIgnoreCase),
			},
			new Option
			{
				Short = "-i",
				Long = "--input",
				ArgsAreOptional = false,
				ArgCount = 1,
				Description = "Input file.",
				Handler = (args, result) => result.InputFile = args.FirstOrDefault(),
			},
			new Option
			{
				Short = "-e",
				Long = "--endPoint",
				ArgsAreOptional = false,
				ArgCount = 1,
				Description = "End point address.",
				Handler = (args, result) => result.EndPoint = args.FirstOrDefault(),
			},
			new Option
			{				
				Short = "-w",
				Long = "--window",
				ArgsAreOptional = false,
				ArgCount = 1,
				Description = "Execution window in milliseconds (0 for none)",
				Handler = (args, result) => result.ExecutionWindowInMilliseconds = args.FirstOrDefault(),
			},
		};

		public bool DebugOnStart { get; set; }
		public string InputFile { get; set; }
		public string EndPoint { get; set; }
		public string ExecutionWindowInMilliseconds { get; set; }

		public CommandLine() : base(Options)
		{
		}
	}
}